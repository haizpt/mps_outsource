--------------------------------------------------------
--  File created - Wednesday-June-28-2017   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table SERVICES
--------------------------------------------------------

  CREATE TABLE "MPS"."SERVICES" 
   (	"SERVICE_CODE" VARCHAR2(5 BYTE), 
	"PROVIDER_NAME" VARCHAR2(50 BYTE), 
	"SERVICE_NAME" VARCHAR2(50 BYTE), 
	"TIME_CREATE" TIMESTAMP (6), 
	"DESCRIPTIONS" VARCHAR2(1000 BYTE), 
	"STATUS" NUMBER, 
	"PUBLIC_CP" VARCHAR2(4000 BYTE), 
	"PUBLIC_VT_CP" VARCHAR2(4000 BYTE), 
	"PRIVATE_CP" VARCHAR2(4000 BYTE), 
	"PRIVATE_VT_CP" VARCHAR2(4000 BYTE), 
	"GROUP_CHARGE_TYPE" VARCHAR2(20 BYTE), 
	"SHORT_CODE" VARCHAR2(20 BYTE), 
	"URL_RETURN" VARCHAR2(500 BYTE), 
	"DEFAULT_SMS" VARCHAR2(500 BYTE), 
	"DEFAULT_SMS_PRICE" NUMBER DEFAULT 0, 
	"SMSGW_URL" VARCHAR2(500 BYTE), 
	"SMSGW_USER" VARCHAR2(100 BYTE), 
	"SMSGW_PASSWORD" VARCHAR2(100 BYTE), 
	"LOGO_URL" VARCHAR2(100 BYTE), 
	"IP_SERVER_UPLOAD" VARCHAR2(100 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "MPS" ;
REM INSERTING into MPS.SERVICES
SET DEFINE OFF;
Insert into MPS.SERVICES (SERVICE_CODE,PROVIDER_NAME,SERVICE_NAME,TIME_CREATE,DESCRIPTIONS,STATUS,PUBLIC_CP,PUBLIC_VT_CP,PRIVATE_CP,PRIVATE_VT_CP,GROUP_CHARGE_TYPE,SHORT_CODE,URL_RETURN,DEFAULT_SMS,DEFAULT_SMS_PRICE,SMSGW_URL,SMSGW_USER,SMSGW_PASSWORD,LOGO_URL,IP_SERVER_UPLOAD) values ('00001','CPDemo','ServiceDemo',to_timestamp('22-JUL-15 03.33.10.000000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'test MPS VTC',1,null,null,null,null,null,'2304',null,'DK DEMO2',0,'http://10.79.123.14:9201/smsgw?wsdl','mps1764','mps1764','/logo/ServiceDemo_CPDemo.jpg','192.168.0.106');
--------------------------------------------------------
--  DDL for Index SS_SNE_IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "MPS"."SS_SNE_IDX" ON "MPS"."SERVICES" ("SERVICE_NAME") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "MPS" ;
--------------------------------------------------------
--  Constraints for Table SERVICES
--------------------------------------------------------

  ALTER TABLE "MPS"."SERVICES" ADD CONSTRAINT "SS_PK" PRIMARY KEY ("SERVICE_NAME")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "MPS"  ENABLE;
  ALTER TABLE "MPS"."SERVICES" MODIFY ("PROVIDER_NAME" NOT NULL ENABLE);
  ALTER TABLE "MPS"."SERVICES" MODIFY ("SERVICE_NAME" NOT NULL ENABLE);
  ALTER TABLE "MPS"."SERVICES" MODIFY ("STATUS" NOT NULL ENABLE);
