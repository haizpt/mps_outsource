--------------------------------------------------------
--  File created - Wednesday-June-28-2017   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure PR_SERVICES_UPLOADLOGO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MPS"."PR_SERVICES_UPLOADLOGO" 
(
    P_SERVICE_NAME   IN  VARCHAR2,
    P_PATH IN VARCHAR2,
    P_IP_SERVER IN VARCHAR2
)
AS
   ERRMSG VARCHAR2(200);
BEGIN
      UPDATE SERVICES SET
            LOGO_URL=P_PATH,
            IP_SERVER_UPLOAD=P_IP_SERVER
      WHERE
            SERVICE_NAME = P_SERVICE_NAME;
        COMMIT;
        EXCEPTION
      WHEN OTHERS THEN -- HANDLES ALL OTHER ERRORS

      ERRMSG := SUBSTR(SQLERRM, 1, 200); 
      VWRITELOG(ERRMSG,'EXCEPTION:PR_SERVICES_UPLOADLOGO','PR_SERVICES_UPLOADLOGO');
      RAISE;
END;

/
