/**
 *
 * handsome boy
 */

package com.viettel.process;

import com.viettel.database.datatype.SmsData;
import com.viettel.utilities.CallWS;
import com.viettel.utilities.MyLog;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Dec 11, 2015
 * @mail haind25@viettel.com.vn
 */
public class SendSmsProcess {
    private final static int THREAD_SEND_SMS = 3;
    
    private final List<SmsData> queue = new LinkedList<SmsData>();
    private final Object lock = new Object();
    
    private static SendSmsProcess _instance = null;
    
    private SendSmsProcess() {
    }
    
    public static SendSmsProcess getInstance() {
        if (_instance == null) {
            _instance = new SendSmsProcess();
            for(int i = 0; i < THREAD_SEND_SMS; i++) {
                SmsSender sender = new SmsSender(i, _instance);
                Thread thread = new Thread(sender);
                thread.setName("SendSmsProcess-" + i);
                thread.start();
            }
        }
        return _instance;
    }
   
    public void enqueue(SmsData sms) {
        synchronized (lock) {
            queue.add(sms);
            MyLog.Infor("Smsqueue size: " + queue.size());
        }
    }

    public SmsData dequeue() {
        synchronized (lock) {
            if (queue.isEmpty()) {
                return null;
            }

            return queue.remove(0);
        }
    }

}
    
class SmsSender implements Runnable {
        
        private final String threadName;
        private final SendSmsProcess queue;
        
        public SmsSender(int i, SendSmsProcess queue) {
            this.threadName = SmsSender.class.getName() + "-" + i;
            this.queue = queue;
        }
        
        public String getThreadName() {
            return this.threadName;
        }
        
        @Override
        public void run() {
            while (true) {
                try {
                    SmsData smsData = queue.dequeue();
                    if (smsData != null) {
                        process(smsData);
                    } else {
                            Thread.sleep(3000);// sleep 3s
                    }
                } catch (Exception ex) {
                    MyLog.Error(ex);
                }
            }
        }
        
        private void process(SmsData sms) {
            int result = CallWS.sendMT(sms.getUrl(), sms.getUser(), sms.getPass(), 
                    sms.getMsisdn(),sms.getContent(), sms.getShortCode(), 
                    sms.getAlias(), sms.getSmsType(), 1);
            MyLog.Infor(getThreadName() + ": send result " + result + ", " + sms);
        }
}


    
    

