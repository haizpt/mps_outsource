/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.service;

import com.viettel.database.datatype.Command;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 */
public interface BussinessService {

    BusinessCode register(String subServiceName, String isdn, String tranID, Map<String, String> params, Command command, String stepsRegister);

    BusinessCode checkRegister(String subServiceName, String isdn, Command command);

    BusinessCode checkRegister(String subServiceName, String isdn);

    BusinessCode checkRegisterByServiceName(String serviceName, String isdn);
//    BusinessCode unregister(String subServiceName, String isdn, String tranID, Map<String,String> params, Command command, String stepsUnRegister);

    BusinessCode unregisterAllSub(String isdn, String tranID, Map<String, String> params, String serviceName, String channel, String stepsUnregisterAllSub);

    //add 30 09 2016 hungtv45
    /**
     * @deprecated 
     */
    BusinessCode unregisterSub(String isdn, String tranID, Map<String, String> params, String serviceName, String subServiceName, String channel, String stepsUnregisterSub);
    
    /*haind25 07/02/2017 */
    BusinessCode unregisterSub(String isdn, String tranID, Map<String,String> params, String subServiceName, String channel, String stepsUnregisterSub);   
    
    BusinessCode forward(String subServiceName, String isdn, String tranID, Map<String, String> params, Command command, String stepsForward);

    BusinessCode getPassword(String subServiceName, String isdn, String tranID, Map<String, String> params, Command command, String stepsGetPassword);

     /*haind25 07/02/2017 */
    public BusinessCode checkRegisterBySubServiceName(String subServiceName, String msidn);
    
    public BusinessCode renewBySubServiceName(String msisdn, String subSerivceName, String tranID, Map<String,String> params,String channel, String log);
    
    public BusinessCode renewByServiceName(String msisdn, String serviceName, String tranID, Map<String,String> params, String channel, String log);
    
    public BusinessCode cancelSilentBySubservice(String msisdn, String subSerivceName,
            String tranID, Map<String,String> params, String channel, String log);
    
    public BusinessCode cancelSilent(String msisdn, 
            String tranID, Map<String,String> params, String channel, String log, Date registerBeforeTime) ;
    
//
//    enum BusinessCode {
//
//        SUCCESS("SUCCESS"),
//        FAIL("FAIL"), CONFLICT("CONFLICT"), ALREADY("ALREADY"),
//        NOT_REGISTER("NOT_REGISTER"), NOT_ENOUGH_MONEY("NOT_ENOUGH_MONEY"), SUCCESS_REQUIRE_MPS_ACCOUNT("SUCCESS_REQUIRE_MPS_ACCOUNT");
//
//        private final String value;
//        private String description;
//        private String response;
//        
//        private Object data;
//        
//        private BusinessCode(String value) {
//            this.value = value;
//        }
//
//        public String getValue() {
//            return this.value;
//        }
//
//        @Override
//        public String toString() {
//            return value;
//        }
//
//        public String getDescription() {
//            return description;
//        }
//
//        public void setDescription(String description) {
//            this.description = description;
//        }
//
//        public String getResponse() {
//            return response;
//        }
//
//        public void setResponse(String response) {
//            this.response = response;
//        }
//        
//        public void setData(Object data) {
//            this.data = data;
//        }
//        
//        public Object getData() {
//            return data;
//        }
//        
//    }
}
