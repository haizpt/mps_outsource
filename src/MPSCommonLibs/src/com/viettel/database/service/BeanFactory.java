/**
 *
 * handsome boy
 */

package com.viettel.database.service;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Dec 4, 2015
 * @mail haind25@viettel.com.vn
 */
public interface BeanFactory<T> {
    T getBean();
}
