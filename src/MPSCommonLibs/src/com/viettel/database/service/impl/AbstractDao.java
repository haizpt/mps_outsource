/**
 *
 * handsome boy
 */

package com.viettel.database.service.impl;

import com.viettel.utilities.MyLog;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Dec 9, 2015
 * @mail haind25@viettel.com.vn
 */
public class AbstractDao {
    protected DataSource dataSource;
    private boolean connectionCloseable = true;
    
    public AbstractDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    protected Connection getConnection(){
        if (dataSource == null) {
            return null;
        }
        
        try {
            Connection connection = dataSource.getConnection();
            return connection;
        } catch (SQLException ex) {
            MyLog.Error("Can not get Connection - " + ex.getMessage());
        }
        
        return null;
    }
    
    protected void closeConnection(Connection conn){
        if (isConnectionCloseable() && conn != null && dataSource != null ) {
            try {
                conn.close();
            } catch (Exception ex) {
                MyLog.Error(ex);
            }
        }
    }
  
    protected long getTimeStamp(ResultSet r, String column, long defaultValue) {
        long result = defaultValue;
        try {
            if (r.getTimestamp(column) != null) {
                result = r.getTimestamp(column).getTime();
            }
        } catch (Exception ex) {
            MyLog.Debug(" get Timestamp for " + column + " failed, use default: " + defaultValue);
            result = defaultValue;
        }
        
        return result;
    }

    public boolean isConnectionCloseable() {
        return connectionCloseable;
    }

    public void setConnectionCloseable(boolean connectionCloseable) {
        this.connectionCloseable = connectionCloseable;
    }
    
}
