/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.database.service.impl;

import com.viettel.cache.HCache;
import com.viettel.database.datatype.Command;
import com.viettel.database.datatype.PromotionCommand;
import com.viettel.database.oracle.OracleConnection;
import com.viettel.utilities.GlobalConfig;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author haind25
 */
public class PromotionManager {
    public static final int FIVE_MINUTE = 5 * 60000; //5m
    
    private static final HCache<String,List<PromotionCommand>> cache_promotioncommonds = 
            new HCache<String, List<PromotionCommand>>("PromotionManager",100, HCache.NO_MAX_CAPACITY, FIVE_MINUTE);
    
    private final OracleConnection oracleConnection ;
    
    private static PromotionManager _instance = null;
    
    private final static Logger log = Logger.getLogger(PromotionManager.class);

    
    private PromotionManager(OracleConnection oracleConnection) {
        this.oracleConnection = oracleConnection;
    }

    public static PromotionManager config(OracleConnection dataSource) {
        if (_instance == null) {
            _instance = new PromotionManager(dataSource);
        }
        
        return _instance;
    }
    
    public static PromotionManager getInstance() {
        return _instance;
    }

    
    /**
     * lay nhung goi khuyen mai hien tai cua goi cuoc, khong can quan tam co 
     * dung kenh hay khong, dung loai hay khong
     * Danh sach khuyen mai cua goi cuoc se bi xoa trong 
     * @author haind25
     * @param subServiceName
     * @return all PromotionCommand is current valid for this sub-service
     */
    private List<PromotionCommand> getPromotionCommands(String subServiceName) {
        String key = subServiceName;
        List<PromotionCommand> commands = cache_promotioncommonds.getOnly(key);
        if (commands != null) {
            return commands;
        }
        
        commands = oracleConnection.getPromotionCommands(subServiceName);
        
        if (commands == null) {
            //khoi tao de tranh select db qua nhieu, he thong tu lay sau 5m
            commands = new ArrayList<PromotionCommand>();
        }
        
        cache_promotioncommonds.put(key, commands);
//        
        return commands;
    }
    
    
    /**
     * lay thong tin
     * @param type. @see PromotionCommand PROMOTION_TYPE
     * @param channel 
     * @see Command
     * @return 
     */
    public PromotionCommand getPromotionCommand(String subServiceName, String type, String channel) {
        if (!GlobalConfig.ENABLE_PROMOTION()) {
            log.info("no promotion, GlobalConfig.ENABLE_PROMOTION() disable");
            return null;
        }
        
        if (type == null || subServiceName == null || channel == null) {
            log.info("no promotion command");
            return null;
        }
        
        PromotionCommand command = null;
        List<PromotionCommand> commands = getPromotionCommands(subServiceName);
        if (commands == null || commands.isEmpty()) {
            log.info("no promotion command for sub: " + subServiceName);
            return null;
        }
        
        for (PromotionCommand promotionCommand : commands) {
            if (!type.equals(promotionCommand.getType())) {
                continue;
            }
            
            if (!Command.COMMAND_ALL.equals(promotionCommand.getPromotionChannel())
                    && !channel.equals(promotionCommand.getPromotionChannel())) {
                continue;
            }
            
            command = promotionCommand;
            break;
        }

        log.info("promotion command: " + command);
        return command;
    }
    
}
