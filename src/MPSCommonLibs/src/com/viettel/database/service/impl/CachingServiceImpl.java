/**
 *
 * co gi cu hoi haind25
 */

package com.viettel.database.service.impl;

import com.viettel.cache.HCache;
import com.viettel.database.datatype.CPService;
import com.viettel.database.datatype.CPWebservice;
import com.viettel.database.datatype.CpSubscirber;
import com.viettel.database.datatype.IvrCommand;
import com.viettel.database.datatype.MOCommand;
import com.viettel.database.datatype.PriceCharge;
import com.viettel.database.datatype.SubService;
import com.viettel.database.datatype.Subscriber;
import com.viettel.database.datatype.UssdCommand;
import com.viettel.database.oracle.DBConst;
import com.viettel.database.oracle.OracleConnection;
import com.viettel.database.service.CpSubscriberDao;
import com.viettel.database.service.CachingeService;
import com.viettel.utilities.GlobalConfig;
import com.viettel.utilities.MyLog;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Nov 24, 2015
 * @mail haind25@viettel.com.vn
 */
public class CachingServiceImpl implements CachingeService{
    private OracleConnection oracleConnection ;
    private final static int TIMEOUT_DEFAULT = 7 * 24 * 60;
    
    public CachingServiceImpl(){
    }
    
    //HCache(String name, int initialCapacity, int maxCapacity, int expireMinutes, Class<V> clazz)
    private static final HCache<String,UssdCommand> cache_ussdcommonds = 
            new HCache<String, UssdCommand>("UssdServiceImpl_getUssdCommand",100, HCache.NO_MAX_CAPACITY, HCache.NO_TIMEOUT);
    
    @Override
    public UssdCommand getUssdCommand(String subServiceName, int type) {
        String key = subServiceName + "_T_" + type;
//        oracleConnection()
        UssdCommand command = cache_ussdcommonds.get(key);
        if (command != null) {
            return command;
        }
        
        command = oracleConnection.getUssdCommand(subServiceName, type);
        
        if (command != null) {
            cache_ussdcommonds.put(key, command);
        }
        
        return command;
    }

    private static final HCache<String,SubService> cache_subservices = 
            new HCache<String, SubService>("UssdServiceImpl_getSubService",100, HCache.NO_MAX_CAPACITY, HCache.NO_TIMEOUT);
    
    @Override
    public SubService getSubService(String subServiceName) {
        SubService subService = cache_subservices.get(subServiceName);
        if (subService != null) {
            return subService;
        }
        subService = oracleConnection.getSubService(subServiceName);
        
        if (subService != null) {
            cache_subservices.put(subServiceName, subService);
        }
        
        return subService;
    }

    private static final HCache<String,CPService> cache_CPServices = 
            new HCache<String, CPService>("UssdServiceImpl_getCpService",100,HCache.NO_MAX_CAPACITY, HCache.NO_TIMEOUT);
    
    @Override
    public CPService getCpService(String serviceName) {
        CPService cPService = cache_CPServices.get(serviceName);
        if (cPService != null) {
            return cPService;
        }
        
        cPService = oracleConnection.getCpService(serviceName);
        
        if (cPService != null) {
            cache_CPServices.put(serviceName, cPService);
        }
        
        return cPService;
    }

    @Override
    public PriceCharge getPriceCharge(int cmdPrice, int moPrice, int status) {         
        return new PriceCharge(cmdPrice, moPrice, status);
    }

    public void setOracleConnection(OracleConnection oracleConnection) {
        this.oracleConnection = oracleConnection;
    }

    private static final HCache<String,CPService> cache_getCpServiceBySubSerivceName = 
            new HCache<String, CPService>("UssdServiceImpl_getCpServiceBySubSerivceName",100, HCache.NO_MAX_CAPACITY, HCache.NO_TIMEOUT);
    
    @Override
    public CPService getCpServiceBySubSerivceName(String subServiceName) {
        CPService cPService = cache_getCpServiceBySubSerivceName.get(subServiceName);
        if (cPService != null) {
            return cPService;
        }
        
        cPService = oracleConnection.getCpServiceBySubSerivceName(subServiceName);
        if (cPService != null) {
            cache_getCpServiceBySubSerivceName.put(subServiceName, cPService);
        }
        
        return cPService;
    }

    private static final HCache<String,MOCommand> cache_getMOCommand = 
            new HCache<String, MOCommand>("UssdServiceImpl_getMOCommand",100, HCache.NO_MAX_CAPACITY, HCache.NO_TIMEOUT);
    
    @Override
    public MOCommand getMOCommand(String subServiceName, int type) {
        String key = subServiceName + "_H_" + type;
        MOCommand command = cache_getMOCommand.get(key);
        if (command != null) {
            return command;
        }
        command = oracleConnection.getMOCommand(subServiceName, type);
        
        if (command != null) {
            cache_getMOCommand.put(key, command);
        }
        
        return command;
    }
    
    private  static final  HCache<String,IvrCommand> cache_getIvrCommand = 
            new HCache<String, IvrCommand>("UssdServiceImpl_getIvrCommand",100, HCache.NO_MAX_CAPACITY, HCache.NO_TIMEOUT);
    
    @Override
    public IvrCommand getIvrCommand(String subServiceName, int type) {
        String key = subServiceName + "_H_" + type;
        IvrCommand command = cache_getIvrCommand.get(key);
        if (command != null) {
            return command;
        }
        
        command = oracleConnection.getIvrCommand(subServiceName, type);
        
        if (command != null) {
            cache_getIvrCommand.put(key, command);
        }
        
        return command;
    }

    @Override
    public Subscriber getSubscriber(String msisdn, String subServiceName) {
        CpSubscriberDao cpSubscriberDao = ServiceFactory.getBean(CpSubscriberDao.class);
        CpSubscirber cpSubscirber = cpSubscriberDao.find(subServiceName, msisdn);
        if (cpSubscirber == null) {
            MyLog.Debug("no sub for: " + msisdn + ", subserviceName: " + subServiceName);
            return null;
        }
        
        SubService subService = getSubService(subServiceName);
        if (subService == null) {
            MyLog.Error("has CpSubscirber but get SubService by subserviceName is null, subservice: " + subServiceName);
            return null;
        }
        
        CPService cPService = getCpServiceBySubSerivceName(subServiceName);
        if (cPService == null) {
            MyLog.Error("has CpSubscirber but get CPService by subserviceName is null, subservice: " + subServiceName);
            return null;
        }
                                       
        Subscriber subscriber = new Subscriber(msisdn, cPService.getProvider(), cPService.getServiceName(),  subServiceName, 
                subService.getSubServiceCode(), cpSubscirber.getDescription(), cpSubscirber.getStatus(), cpSubscirber.getChargeStatus(),
                subService.getSubType(), subService.getDayDebitAllowed(), cpSubscirber.getIsPromotion(), cpSubscirber.getRegisterTime(), 
                cpSubscirber.getCancelTime(), cpSubscirber.getLastMonfeeChargeTime(), cpSubscirber.getNextMonfeeChargeTime(), cpSubscirber.getMonfeeSuccessCount(), 
                cpSubscirber.getLastSendNotify());
        return subscriber;
        
    }

    
    private static final HCache<Integer,CPWebservice> cache_getCPWebServices = 
            new HCache<Integer, CPWebservice>("UssdServiceImpl_getCPWebService",200, HCache.NO_MAX_CAPACITY, HCache.NO_TIMEOUT);
    
    @Override
    public CPWebservice getCPWebService(int webserviceId) {
        CPWebservice data = cache_getCPWebServices.get(webserviceId);
        if (data != null) {
            return data;
        }
        
        data = oracleConnection.getCPWebService(webserviceId);
        if (data != null) {
            if (data.getTimeout() > GlobalConfig.KPI_CP_TIMEOUT_MAX) {
                data.setTimeout(GlobalConfig.KPI_CP_TIMEOUT_MAX);
            }
            cache_getCPWebServices.put(webserviceId, data);
        }
        
        return data;
    }

    private static final HCache<String,Map<String, SubService>> cache_findSubServices = 
            new HCache<String, Map<String, SubService>>("cache_findSubServices",100, HCache.NO_MAX_CAPACITY, HCache.NO_TIMEOUT);
    
    
    /**
     * 
     */
    @Override
    public Map<String, SubService> findSubServices(String serviceName) {
         Map<String, SubService> data = cache_findSubServices.get(serviceName);
        if (data != null) {
            return data;
        }
        
        data = oracleConnection.findSubServices(serviceName);
        
        if (data != null) {
            cache_findSubServices.put(serviceName, data);
        }
        
        return data;
    }
    
    @Override
    public Subscriber checkConflict(String msisdn, String serviceName, String currentSubService) {
        Map<String,SubService> subServiceSameServices = findSubServices(serviceName);
        if (subServiceSameServices == null || subServiceSameServices.isEmpty()) {
            return null;
        }
        
        List<String> subServiceNames = new LinkedList<String>();
        for (Map.Entry<String, SubService> entry : subServiceSameServices.entrySet()) {
            SubService subService = entry.getValue();
            subServiceNames.add(subService.getSubServiceName());
        }
        
        CpSubscriberDao cpSubscriberDao = ServiceFactory.getBean(CpSubscriberDao.class);
        List<CpSubscirber> cpSubscirberS = cpSubscriberDao.find(subServiceNames, msisdn);
        if (cpSubscirberS == null || cpSubscirberS.isEmpty()) {
            return null;
        }
        
        SubService otherSubService = null;
        
        CpSubscirber conflictSubservice = null;
        
        for (CpSubscirber cpSubscirber : cpSubscirberS) {
            if (cpSubscirber.getSubServiceName().equals(currentSubService)){
                //subscriber same subService
                continue;
            }
            
            if (cpSubscirber.getStatus() != DBConst.STATUS_ACTIVE && cpSubscirber.getStatus() != DBConst.STATUS_PENDING) {
                //subscriber not active and not pending
                continue;
            }
            
            if (!subServiceSameServices.containsKey(cpSubscirber.getSubServiceName())){
                //subscriber in other service
                continue;
            }
            
            otherSubService = subServiceSameServices.get(cpSubscirber.getSubServiceName());
            
            if(otherSubService!=null){
                if (otherSubService.getStatus() != DBConst.STATUS_ACTIVE) {
                    //other subservice not active
                    continue;
                }
            
            
                if (!otherSubService.isCheckConflict()) {
                    //same service, but not check conflict
                    continue;
                }
            }
            conflictSubservice = cpSubscirber;
            break;
        }
        
        if (conflictSubservice == null) {
            // not conflict
            return null;
        }
        
        SubService subService = getSubService(currentSubService);
        if (subService == null) {
            MyLog.Error("has CpSubscirber but get SubService by subserviceName is null, subservice: " + currentSubService);
            return null;
        }
        
        CPService cPService = getCpService(serviceName);
        if (cPService == null) {
            MyLog.Error("has CpSubscirber but get CPService by subserviceName is null, subservice: " + serviceName);
            return null;
        }
                                       
        Subscriber subscriber = new Subscriber(msisdn, cPService.getProvider(), cPService.getServiceName(),  currentSubService, 
                subService.getSubServiceCode(), conflictSubservice.getDescription(), conflictSubservice.getStatus(), conflictSubservice.getChargeStatus(),
                subService.getSubType(), subService.getDayDebitAllowed(), conflictSubservice.getIsPromotion(), conflictSubservice.getRegisterTime(), 
                conflictSubservice.getCancelTime(), conflictSubservice.getLastMonfeeChargeTime(), conflictSubservice.getNextMonfeeChargeTime(), conflictSubservice.getMonfeeSuccessCount(), 
                conflictSubservice.getLastSendNotify());
        
        
        return subscriber;
    }
    
    @Override
    public List<Subscriber> findSubscriberByServiceName(String msisdn, String serviceName) {
        Map<String,SubService> subServiceSameServices = findSubServices(serviceName);
        if (subServiceSameServices == null || subServiceSameServices.isEmpty()) {
            return null;
        }
        
        List<String> subServiceNames = new LinkedList<String>();
        for (Map.Entry<String, SubService> entry : subServiceSameServices.entrySet()) {
            SubService subService = entry.getValue();
            subServiceNames.add(subService.getSubServiceName());
        }
        
        CPService cPService = getCpService(serviceName);
        if (cPService == null) {
            MyLog.Error("has CpSubscirber but get CPService by subserviceName is null, subservice: " + serviceName);
            return null;
        }
        
        CpSubscriberDao cpSubscriberDao = ServiceFactory.getBean(CpSubscriberDao.class);
        List<CpSubscirber> cpSubscirberS = cpSubscriberDao.find(subServiceNames, msisdn);
        if (cpSubscirberS == null || cpSubscirberS.isEmpty()) {
            return null;
        }
        
        List<Subscriber> subscribers = new LinkedList<Subscriber>();
        
        SubService subService = null;
        
        
        for (CpSubscirber cpSubscirber : cpSubscirberS) {
            subService = getSubService(cpSubscirber.getSubServiceName());
            if (subService == null) {
                MyLog.Error("has CpSubscirber but get SubService by subserviceName is null, subservice: ");
                continue;
            }
            
             Subscriber subscriber = new Subscriber(msisdn, cPService.getProvider(), cPService.getServiceName(),  cpSubscirber.getSubServiceName(), 
                subService.getSubServiceCode(), cpSubscirber.getDescription(), cpSubscirber.getStatus(), cpSubscirber.getChargeStatus(),
                subService.getSubType(), subService.getDayDebitAllowed(), cpSubscirber.getIsPromotion(), cpSubscirber.getRegisterTime(), 
                cpSubscirber.getCancelTime(), cpSubscirber.getLastMonfeeChargeTime(), cpSubscirber.getNextMonfeeChargeTime(), cpSubscirber.getMonfeeSuccessCount(), 
                cpSubscirber.getLastSendNotify());
            subscribers.add(subscriber);
        }
        
        return subscribers;
    }

  

     private static final HCache<String,CPWebservice> cache_getCPWebServices_by_name = 
            new HCache<String, CPWebservice>("CachingImpl_getCPWebService_NAME",200,
                    HCache.NO_MAX_CAPACITY, HCache.NO_TIMEOUT);
    
    @Override
    public CPWebservice getCPWebService(String webserviceName) {
        CPWebservice data = cache_getCPWebServices_by_name.get(webserviceName);
        if (data != null) {
            MyLog.Error("getCPWebService from Cache: " + data);
            return data;
        }
        
        data = oracleConnection.getCPWebService(webserviceName);
        if (data != null) {
            if (data.getTimeout() > GlobalConfig.KPI_CP_TIMEOUT_MAX) {
                data.setTimeout(GlobalConfig.KPI_CP_TIMEOUT_MAX);
            }
            cache_getCPWebServices_by_name.put(webserviceName, data);
            MyLog.Error("getCPWebService from Database: " + data);
        }
         
        return data;
    }

    @Override
    public List<Subscriber> findSubscriberByMsisdn(String msisdn) {
        CpSubscriberDao cpSubscriberDao = ServiceFactory.getBean(CpSubscriberDao.class);
        List<CpSubscirber> cpSubscirberS = cpSubscriberDao.find(msisdn);
        if (cpSubscirberS == null || cpSubscirberS.isEmpty()) {
            MyLog.Infor("NOT subscriber of any service: " + msisdn);
            return null;
        }
        
        List<Subscriber> subscribers = new LinkedList<Subscriber>();
        
        SubService subService = null;
        
        for (CpSubscirber cpSubscirber : cpSubscirberS) {
            subService = getSubService(cpSubscirber.getSubServiceName());
            if (subService == null) {
                MyLog.Error("has CpSubscirber but get SubService by subserviceName is null, subservice: ");
                continue;
            }
            
            Subscriber subscriber = new Subscriber(msisdn, "UNDEFINE", subService.getServiceName(),  cpSubscirber.getSubServiceName(), 
                subService.getSubServiceCode(), cpSubscirber.getDescription(), cpSubscirber.getStatus(), cpSubscirber.getChargeStatus(),
                subService.getSubType(), subService.getDayDebitAllowed(), cpSubscirber.getIsPromotion(), cpSubscirber.getRegisterTime(), 
                cpSubscirber.getCancelTime(), cpSubscirber.getLastMonfeeChargeTime(), cpSubscirber.getNextMonfeeChargeTime(), cpSubscirber.getMonfeeSuccessCount(), 
                cpSubscirber.getLastSendNotify());
            subscribers.add(subscriber);
        }
        
        return subscribers;
    }
}
