/**
 *
 * handsome boy
 */

package com.viettel.database.service.impl;

import com.viettel.database.datatype.CpSubscirber;
import com.viettel.database.service.CpSubscriberDao;
import com.viettel.utilities.MyLog;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Dec 9, 2015
 * @mail haind25@viettel.com.vn
 */
public class CpSubscriberDaoImpl extends AbstractDao implements CpSubscriberDao {
    
    public CpSubscriberDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public CpSubscirber find(String subServiceName, String isdn) {
        MyLog.Debug("get get getSubScription for '" + isdn + "' - '" + subServiceName + "'");
        CpSubscirber result = null;
        PreparedStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "SELECT * FROM services_subscriptions WHERE msisdn = ? AND SUB_SERVICE_NAME=?";
            conn = getConnection();
            if (conn == null) {
                MyLog.Error("error can not get connection");
                throw new NullPointerException("can not get connection");
            }
            s = conn.prepareStatement(strSQL);
            // Set timeout
//            s.setQueryTimeout(30); 
            s.setString(1, isdn);
            s.setString(2, subServiceName);
            
            ResultSet r = s.executeQuery();
            if (r.next()) {
                int status = r.getInt("STATUS");
                long registerTime = getTimeStamp(r, "REGISTER_TIME", 0);
                long cancelTime = getTimeStamp(r, "CANCEL_TIME", 0);
                String description = r.getString("DESCRIPTIONS");
                long lastMonFeeCharge = getTimeStamp(r, "LAST_MONFEE_CHARGE_TIME", 0);
                long nextMonFeeCharge = getTimeStamp(r, "NEXT_MONFEE_CHARGE_TIME", 0);
                int chargeStatus = r.getInt("CHARGE_STATUS");
                int isPromotion = r.getInt("IS_PROMOTION");
                int monfeeCount = r.getInt("MONFEE_SUCCESS_COUNT");
                Timestamp lastNotify = r.getTimestamp("LAST_SEND_NOTIFY");
                
                result = new CpSubscirber(isdn,subServiceName, description, 
                                                status, chargeStatus,isPromotion, registerTime, cancelTime, 
                                                lastMonFeeCharge, nextMonFeeCharge, monfeeCount, lastNotify);
            }
            r.close();
        } catch (Exception ex) {
            MyLog.Error("ERROR " + ex.getMessage());
            MyLog.Error(ex);
            result = null;
        } finally {
            if (s != null) {
                try { 
                    s.close(); 
                } 
                catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }
        
        MyLog.Infor("Finish getSubScription for '" + isdn + "' - '" + subServiceName + "' -> " + result + "(" 
                        + (System.currentTimeMillis() - start) + " ms)");
        
        return result;
    }
    
    @Override
    public List<CpSubscirber> find(List<String> subServiceNames, String isdn) {
        if (isdn == null) {
            MyLog.Error("get get getSubScription for '");
            return null;
        }
        MyLog.Debug("get get getSubScription for '" + isdn);
        List<CpSubscirber> results = new LinkedList<CpSubscirber>();
        Statement s = null;
        Connection conn = null;
        long start = System.currentTimeMillis();
        StringBuilder subServiceCodition = null;
        if (subServiceNames != null && !subServiceNames.isEmpty()) {
            subServiceCodition = new StringBuilder();
            subServiceCodition.append( " AND SUB_SERVICE_NAME in ('");
            int i = 0;
            for (String subServiceName : subServiceNames) {
                
                subServiceName = subServiceName.replaceAll("'", "");
                if (i >0 ) {
                    subServiceCodition.append(",'");
                }
                subServiceCodition.append(subServiceName + "'");
                
                i++;
            }
            subServiceCodition.append( ")");
        }

        try {
            isdn = isdn.replaceAll("'", "").trim();
            String strSQL = "SELECT * FROM services_subscriptions WHERE msisdn =  '"+ isdn + "' ";
            if (subServiceCodition != null) {
                strSQL = strSQL + subServiceCodition.toString();
            }
             conn = getConnection();
            s = conn.createStatement();
            // Set timeout
            s.setQueryTimeout(30); 
//            s.setString(1, isdn);
            
            ResultSet r = s.executeQuery(strSQL);
            while (r.next()) {
                int status = r.getInt("STATUS");
                long registerTime = getTimeStamp(r, "REGISTER_TIME", 0);
                long cancelTime = getTimeStamp(r, "CANCEL_TIME", 0);
                String subServiceName = r.getString("SUB_SERVICE_NAME");
                String description = r.getString("DESCRIPTIONS");
                long lastMonFeeCharge = getTimeStamp(r, "LAST_MONFEE_CHARGE_TIME", 0);
                long nextMonFeeCharge = getTimeStamp(r, "NEXT_MONFEE_CHARGE_TIME", 0);
                int chargeStatus = r.getInt("CHARGE_STATUS");
                int isPromotion = r.getInt("IS_PROMOTION");
                int monfeeCount = r.getInt("MONFEE_SUCCESS_COUNT");
                Timestamp lastNotify = r.getTimestamp("LAST_SEND_NOTIFY");
                
                CpSubscirber subscirber  = new CpSubscirber(isdn,subServiceName, description, 
                                                status, chargeStatus,isPromotion, registerTime, cancelTime, 
                                                lastMonFeeCharge, nextMonFeeCharge, monfeeCount, lastNotify);
                results.add(subscirber);
            }
            r.close();
        } catch (SQLException ex) {
            MyLog.Error("ERROR " + ex.getMessage());
            MyLog.Error(ex);
        } finally {
            if (s != null) {
                try { 
                    s.close(); 
                } 
                catch (Exception ee) {}
            }
                closeConnection(conn);
            
        }
        
        MyLog.Infor("Finish getSubScriptions for '" + isdn  + "(" 
                        + (System.currentTimeMillis() - start) + " ms)");
        
        return results;
    }
    
    @Override
    public List<CpSubscirber> find(String isdn) {
        MyLog.Debug("get get getSubScription for '" + isdn + "' - '");
        List<CpSubscirber> results = new LinkedList<CpSubscirber>();
        CpSubscirber result = null;
        PreparedStatement s = null;
        Connection conn = null;
        long start = System.currentTimeMillis();

        try {
            String strSQL = "SELECT * FROM services_subscriptions WHERE msisdn = ?";
            conn = getConnection();
            s = conn.prepareStatement(strSQL);
            s.setString(1, isdn);
            
            ResultSet r = s.executeQuery();
            while (r.next()) {
                int status = r.getInt("STATUS");
                long registerTime = getTimeStamp(r, "REGISTER_TIME", 0);
                long cancelTime = getTimeStamp(r, "CANCEL_TIME", 0);
                String description = r.getString("DESCRIPTIONS");
                String subServiceName = r.getString("SUB_SERVICE_NAME");
                long lastMonFeeCharge = getTimeStamp(r, "LAST_MONFEE_CHARGE_TIME", 0);
                long nextMonFeeCharge = getTimeStamp(r, "NEXT_MONFEE_CHARGE_TIME", 0);
                int chargeStatus = r.getInt("CHARGE_STATUS");
                int isPromotion = r.getInt("IS_PROMOTION");
                int monfeeCount = r.getInt("MONFEE_SUCCESS_COUNT");
                Timestamp lastNotify = r.getTimestamp("LAST_SEND_NOTIFY");
                
                result = new CpSubscirber(isdn,subServiceName, description, 
                                                status, chargeStatus,isPromotion, registerTime, cancelTime, 
                                                lastMonFeeCharge, nextMonFeeCharge, monfeeCount, lastNotify);
                results.add(result);
            }
            r.close();
        } catch (Exception ex) {
            MyLog.Error("ERROR " + ex.getMessage());
            MyLog.Error(ex);
            result = null;
        } finally {
            if (s != null) {
                try { 
                    s.close(); 
                } 
                catch (Exception ee) {}
            }
            closeConnection(conn);
        }
        
        MyLog.Infor("Finish getSubScription for '" + isdn + "(" 
                        + (System.currentTimeMillis() - start) + " ms)");
        
        return results;
    }
    
}
