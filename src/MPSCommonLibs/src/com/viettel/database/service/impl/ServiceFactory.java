/**
 *
 * handsome boy
 */

package com.viettel.database.service.impl;

import com.viettel.database.service.BeanFactory;
import java.util.Map;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Nov 24, 2015
 * @mail haind25@viettel.com.vn
 */
public class ServiceFactory {
    
    private static final Object lock = new Object();
    
    private static Map<Class<?>, BeanFactory<?>> clazzs = null;
    
    public static void setBeanMapper(Map<Class<?>,BeanFactory<?>> p_clazzs) {
        synchronized(lock) {
            clazzs = p_clazzs;
        }
    }
    
    
    public static <T> T getBean(Class<T> clazz) {
        synchronized(lock) {
            BeanFactory<?> service = clazzs.get(clazz);
            if (service != null) {
                Object bean = service.getBean();
                if (bean != null) {
                    return (T) service.getBean();
                }
            }
            return null;
        }
    }

}
