package com.viettel.database.service.impl;

//import com.viettel.cdrdumper.CDR;
import com.viettel.cdrdumper.CDR;
import com.viettel.cdrdumper.CDRDumper;
import com.viettel.database.datatype.CPService;
import com.viettel.database.datatype.CPWebservice;
import com.viettel.database.datatype.Command;
import com.viettel.database.datatype.KpiCpSoap;
import com.viettel.database.datatype.MPSUser;
import com.viettel.database.datatype.PriceCharge;
import com.viettel.database.datatype.PromotionCommand;
import com.viettel.database.datatype.SubLogs;
import com.viettel.database.datatype.SubService;
import com.viettel.database.datatype.Subscriber;
import com.viettel.database.oracle.DBConst;
import com.viettel.database.oracle.KpiCpSoapDao;
import com.viettel.database.oracle.OracleConnection;
import com.viettel.database.oracle.SubLogsInserDao;
import com.viettel.database.oracle.SubscriberInserDao;
import com.viettel.database.oracle.SubscriberUpdateDao;
import com.viettel.database.service.BusinessCode;
import com.viettel.database.service.BussinessService;
import com.viettel.database.service.CachingeService;
import com.viettel.utilities.CallWS;
import com.viettel.utilities.ContentResult;
import com.viettel.utilities.DBConfiguration;
import com.viettel.utilities.EncryptManager;
import com.viettel.utilities.GlobalConfig;
import com.viettel.utilities.MyLog;
import com.viettel.utilities.PublicLibs;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Nov 24, 2015
 * @mail haind25@viettel.com.vn
 */
public class BussinessServiceImpl implements BussinessService{

    private OracleConnection mDBConnection;
    private CDRDumper mCDRDumper;
    // Replacement for SMS to Subscriber
    // ***********************************************************************
    public static final String CONFLICT_SUBSERVICE = "#CONFLIC_SUB";
    public static final String CONFLICT_SUB_START = "#CONFLICT_SUB_START";
    public static final String CONFLICT_SUB_END = "#CONFLICT_SUB_END";
    public static final String CP_RESULT = "#RESULT";
    public static final String SUB_NAME = "#SUB_NAME";
    public static final String SERVICE_NAME = "#SERVICE_NAME";
    public static final String SUB_START_DATE = "#SUB_START_DATE";
    public static final String SUB_END_DATE = "#SUB_END_DATE";
    public static final String CMD_PRICE = "#AMOUNT";
    public static final String MO_PRICE = "#MO_AMOUNT";
    public static final String SMS_ITEM = "#SMS_ITEM";
    public static final String TRANS_ID = "#TRANS_ID";
    public static final String SUB_STATUS = "#SUB_STATUS";
    
    
    @Deprecated
    private final int RETRY_UPDATE_SUB = 3;
    @Deprecated
    private final int TYPE_CANCEL = 2;
    
    private static final String ENABLEPROMO = "1";
    
//    private final static HtmlLogger htmlLogger = new HtmlLogger("cp_slow_");
    
    @Override
    public BusinessCode register(String subServiceName, String isdn, String tranID, Map<String, String> params,Command command, String stepsRegister) {
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        CPService cpService = ussdService.getCpServiceBySubSerivceName(command.getSubServiceName());
        if (cpService == null) {
            MyLog.Error("register error cpService null");
            return BusinessCode.FAIL;
        }
       PriceCharge priceCharge = new PriceCharge(command.getCmdPrice(), command.getMoPrice(), command.getStatus());
       return processRegister(isdn, cpService, command, priceCharge, tranID, params, stepsRegister);
    }

//    @Override
//    public BusinessCode unregister(String subServiceName, String isdn, String tranID, Map<String, String> params, Command command, String stepsUnRegister) {
//        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
//        CPService cpService = ussdService.getCpServiceBySubSerivceName(command.getSubServiceName());
//         if (cpService == null) {
//            MyLog.Error("unregister error cpService null");
//            return BusinessCode.FAIL;
//        }
//        PriceCharge priceCharge = new PriceCharge(command.getCmdPrice(), command.getMoPrice(), command.getStatus());
//        return processUnRegister(isdn, cpService, command, priceCharge, tranID, params, stepsUnRegister);
//        
//    }
    
    /**
     *
     * @param isdn
     * @param tranID
     * @param params
     * @param serviceName
     * @param subServiceName
     * @param channel
     * @param stepsUnregisterSub
     * @return
     */
        
    @Override
    public BusinessCode unregisterSub(String isdn, String tranID, Map<String, String> params, 
            String serviceName, String subServiceName, String channel, String stepsUnregisterSub) {
        MyLog.Infor(tranID + " - unregister serviceName: " + serviceName 
                + " , sub-service: " + subServiceName);
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        CPService cpService = ussdService.getCpService(serviceName);
        if (cpService == null) {
            MyLog.Error("unregister error cpService null");
            return BusinessCode.FAIL;
        }
        
        Map<String, SubService> subServices = ussdService.findSubServices(serviceName);
        if (subServices == null) {
            MyLog.Error("could not find subservice, service: " + serviceName);
            return BusinessCode.FAIL;
        }
        
        BusinessCode result = BusinessCode.FAIL;
        int i = 0;
         
        Command command = null;
        PriceCharge priceCharge = null;
        // get subservice which isdn was register, include active, in_active and pending
        List<Subscriber> subscribers = ussdService.findSubscriberByServiceName(isdn, serviceName);        
        
        boolean isRegister = false;
        if (subscribers != null && !subscribers.isEmpty()) {
            for (Subscriber subscriber : subscribers) {
                if(subscriber.getSubServiceName().equals(subServiceName)){
                    if (subscriber.getStatus() != DBConst.STATUS_ACTIVE && 
                           subscriber.getStatus() != DBConst.STATUS_PENDING) {
                       continue;
                   }

                   if (Command.COMMAND_IVR.equals(channel)) {
                       command = ussdService.getIvrCommand(subscriber.getSubServiceName(), Command.TYPE_UNREGISTER);
                   } else if (Command.COMMAND_USSD.equals(channel)){
                       command = ussdService.getUssdCommand(subscriber.getSubServiceName(), Command.TYPE_UNREGISTER);
                   }

                   if (command == null) {
                       MyLog.Error("could not find command unregister: " + subscriber.getSubServiceName());
                       continue;
                   }

                   isRegister = true;

                   priceCharge = new PriceCharge(command.getCmdPrice(), command.getMoPrice(), command.getStatus());
                   BusinessCode resultSubService = processUnRegister(isdn, cpService, command, priceCharge, tranID, params, stepsUnregisterSub);
                   if (i == 0) {
                       //default get first process
                       result = resultSubService;
                       result.setDescription(command.getSubServiceName());
                   }
                   i++;

                   if (BusinessCode.SUCCESS.getValue().equals(resultSubService.getValue())) {
                       result = resultSubService;
                       result.setDescription(command.getSubServiceName());
                   }
                
                }                
            }
        } 
        
        // if isdn is one sub of Subservice, return;
        if (isRegister) {
            return result;
        }
        
         /**
         * neu chua tung dang ky goi nao cua dich vu, lay goi cuoc dau tien,
         * loai conflict de huy hoac ko co goi conlict thi lay goi dau tien
         */
        
        
        SubService fistSubService = null;
        command = null;
        i = 0;
        
        for (Map.Entry<String, SubService> entry : subServices.entrySet()) {
            SubService subService = entry.getValue();
            
            if (i == 0) {
                fistSubService = subService;
                i++;
            }
            
            /**
             * thuong loai checkConflict la goi daily, weekly, month thi message
             * se day du hon, uu tien goi nay hon other. gap goi nay thi break luon.
             */
            if (subService.isCheckConflict()) {
                fistSubService = subService;
                break;
            }
        }
        
        if (fistSubService == null) {
            return result;
        }
        
        if (Command.COMMAND_IVR.equals(channel)) {
            command = ussdService.getIvrCommand(fistSubService.getSubServiceName(), Command.TYPE_UNREGISTER);
        } else if (Command.COMMAND_USSD.equals(channel)) {
            command = ussdService.getUssdCommand(fistSubService.getSubServiceName(), Command.TYPE_UNREGISTER);
        }
        
        if (command == null) {
            result.setDescription(fistSubService.getSubServiceName());
            return result;
        }
        
        priceCharge = new PriceCharge(command.getCmdPrice(), command.getMoPrice(), command.getStatus());
        result = processUnRegister(isdn, cpService, command, priceCharge, tranID, params, stepsUnregisterSub);
        result.setDescription(fistSubService.getSubServiceName());
         
        return result;
    }
    

    /**
     * @param isdn
     * @param tranID
     * @param params
     * @param serviceName
     * @param channel : IVR, USSD, SMS @see Command.class
     * @param stepsUnregisterAllSub
     * @return 
     */
    
    @Override
    public BusinessCode unregisterAllSub(String isdn, String tranID, Map<String, String> params, String serviceName, String channel, String stepsUnregisterAllSub) {
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        CPService cpService = ussdService.getCpService(serviceName);
        if (cpService == null) {
            MyLog.Error("unregister error cpService null");
            return BusinessCode.FAIL;
        }
        
        Map<String, SubService> subServices = ussdService.findSubServices(serviceName);
        if (subServices == null) {
            MyLog.Error("could not find subservice, service: " + serviceName);
            return BusinessCode.FAIL;
        }
        
        BusinessCode result = BusinessCode.FAIL;
        int i = 0;
         
        Command command = null;
        PriceCharge priceCharge = null;
        // get subservice which isdn was register, include active, in_active and pending
        List<Subscriber> subscribers = ussdService.findSubscriberByServiceName(isdn, serviceName);
        
        boolean isRegister = false;
        if (subscribers != null && !subscribers.isEmpty()) {
            for (Subscriber subscriber : subscribers) {
                if (subscriber.getStatus() != DBConst.STATUS_ACTIVE && 
                        subscriber.getStatus() != DBConst.STATUS_PENDING) {
                    continue;
                }
                
                if (Command.COMMAND_IVR.equals(channel)) {
                    command = ussdService.getIvrCommand(subscriber.getSubServiceName(), Command.TYPE_UNREGISTER);
                } else if (Command.COMMAND_USSD.equals(channel)){
                    command = ussdService.getUssdCommand(subscriber.getSubServiceName(), Command.TYPE_UNREGISTER);
                }
                
                if (command == null) {
                    MyLog.Error("could not find command unregister: " + subscriber.getSubServiceName());
                    continue;
                }
                
                isRegister = true;
                 
                priceCharge = new PriceCharge(command.getCmdPrice(), command.getMoPrice(), command.getStatus());
                BusinessCode resultSubService = processUnRegister(isdn, cpService, command, priceCharge, tranID, params, stepsUnregisterAllSub);
                if (i == 0) {
                    //default get first process
                    result = resultSubService;
                    result.setDescription(command.getSubServiceName());
                }
                i++;
                
                if (BusinessCode.SUCCESS.getValue().equals(resultSubService.getValue())) {
                    result = resultSubService;
                    result.setDescription(command.getSubServiceName());
                }
            }
        } 
        
        // if isdn is one sub of Subservice, return;
        if (isRegister) {
            return result;
        }
        
         /**
         * neu chua tung dang ky goi nao cua dich vu, lay goi cuoc dau tien,
         * loai conflict de huy hoac ko co goi conlict thi lay goi dau tien
         */
        
        
        SubService fistSubService = null;
        command = null;
        i = 0;
        
        for (Map.Entry<String, SubService> entry : subServices.entrySet()) {
            SubService subService = entry.getValue();
            
            if (i == 0) {
                fistSubService = subService;
                i++;
            }
            
            /**
             * thuong loai checkConflict la goi daily, weekly, month thi message
             * se day du hon, uu tien goi nay hon other. gap goi nay thi break luon.
             */
            if (subService.isCheckConflict()) {
                fistSubService = subService;
                break;
            }
        }
        
        if (fistSubService == null) {
            return result;
        }
        
        if (Command.COMMAND_IVR.equals(channel)) {
            command = ussdService.getIvrCommand(fistSubService.getSubServiceName(), Command.TYPE_UNREGISTER);
        } else if (Command.COMMAND_USSD.equals(channel)) {
            command = ussdService.getUssdCommand(fistSubService.getSubServiceName(), Command.TYPE_UNREGISTER);
        }
        
        if (command == null) {
            result.setDescription(fistSubService.getSubServiceName());
            return result;
        }
        
        priceCharge = new PriceCharge(command.getCmdPrice(), command.getMoPrice(), command.getStatus());
        result = processUnRegister(isdn, cpService, command, priceCharge, tranID, params, stepsUnregisterAllSub);
        result.setDescription(fistSubService.getSubServiceName());
         
        return result;
    }
    
    @Override
    public BusinessCode forward(String subServiceName, String isdn, String tranID, Map<String, String> params,Command command, String stepsForward) {
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        CPService cpService = ussdService.getCpServiceBySubSerivceName(command.getSubServiceName());
         if (cpService == null) {
            MyLog.Error("processForward error cpService null");
            return BusinessCode.FAIL;
        }
        PriceCharge priceCharge = new PriceCharge(command.getCmdPrice(), command.getMoPrice(), command.getStatus());
        return processForward(isdn, cpService, command, priceCharge, tranID, params, stepsForward);
    }

    @Override
    public BusinessCode getPassword(String subServiceName, String isdn,String tranID, Map<String, String> params,Command command, String stepsGetPassword) {
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        CPService cpService = ussdService.getCpServiceBySubSerivceName(command.getSubServiceName());
         if (cpService == null) {
            MyLog.Error("getPassword error cpService null");
            return BusinessCode.FAIL;
        }
        PriceCharge priceCharge = new PriceCharge(command.getCmdPrice(), command.getMoPrice(), command.getStatus());
        return processGetMPSPassword(isdn, cpService, command, priceCharge, tranID, params, stepsGetPassword);
    }
    
    /**
     * @author haind25
     * @since 24/02/2017
     */
    private PromotionCommand getPromotion(String msisdn, String providerName, String subServiceName, 
            String tranID, Map<String,String> params, String type, String channel, int action) {
        PromotionCommand promotionCommand = null;
        promotionCommand = PromotionManager.getInstance()
                    .getPromotionCommand(subServiceName, type, channel);
        
        if (promotionCommand == null) {
            MyLog.Debug("no promotion: " + tranID);
            return null;
        }
        
        if (promotionCommand.getCheckRequired() <= 0) {
            MyLog.Error("promotion error, no need check with cp promotion, default ok: " + tranID);
            return promotionCommand;
        }
        
        int promotionCpWebcheckId = promotionCommand.getWebserviceId();
        
        if (promotionCpWebcheckId <= 0) {
            MyLog.Error("promotion error, wrong config promotionCpWebcheckId: " + tranID);
            return null;
        }
          
        //kiem tra voi cp xem co thuoc tap danh sach khuyen mai ko
        if (promotionCpWebcheckId > 0) {
            Map<String, String> promotionParams = new HashMap<String, String>();
            promotionParams.put(CallWS.WS_PROMOTION, promotionCommand.getCode()); // promotion
            promotionParams.put(CallWS.WS_CHANEL, channel);
            if (params != null) {
                for (Map.Entry<String, String> entry : promotionParams.entrySet()) {
                    String string = entry.getKey();
                    String string1 = entry.getValue();
                    promotionParams.put(string, string1);
                }
            }
            
            ContentResult content = sendRequestToCP(promotionCpWebcheckId, msisdn, 
                   action, tranID, providerName,
                    subServiceName, promotionCommand.getChargeCommand(), CallWS.WS_MODE_PROMOTION,promotionParams);
              
            if (content == null || ContentResult.RESULT_SUCCESS != content.getResult()) {
                MyLog.Infor("no promotion: " + tranID + ", type: " + type + ", cp response: " + msisdn);
                return null;
            } 
            
            MyLog.Infor("send ws to CP check promotion: " + tranID +  ", cp response:" +content);
        }
       
        
        return promotionCommand;
    }
    
    /**
     * Process Register command
     *
     * @param msisdn
     * @param shortCode
     * @param cmd
     */
    private BusinessCode processRegister(String msisdn, CPService service, Command cmd, PriceCharge price, String tranID, Map<String,String> params, String stepsRegister) {
        MyLog.Infor("transaction: " + tranID);
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        Subscriber sub = ussdService.getSubscriber(msisdn, cmd.getSubServiceName()); 
        boolean isSubNew = sub == null;
        boolean isPromotion = false;
        PromotionCommand promotion = null;
        
        ///////////////////////////// promotion  check ///////////////
        if (isSubNew) {
            promotion = getPromotion(msisdn, service.getProvider(), cmd.getSubServiceName(), 
                 tranID, params,  PromotionCommand.PROMOTION_TYPE_FIRST_REGISTER, cmd.getCommand(),  CallWS.SERVICE_REGISTER);
        }
        
        ///////////////////////////// promotion  end check ///////////////
        
        if (promotion != null) {
            isPromotion = true;
            MyLog.Infor("check promotion: " + tranID + ", for: " + msisdn + "=>" + promotion);
        } else {
            MyLog.Infor("no promotion: " + tranID + ", for: " + msisdn);
        }
        
        BusinessCode regResultUssd = BusinessCode.FAIL;
        Properties p = new Properties();
        int registerPrice = 0;
        int regResult = DBConst.RESULT_FAILED;
        SimpleDateFormat format = new SimpleDateFormat(GlobalConfig.datetimeFormat);
        SubService subService = ussdService.getSubService(cmd.getSubServiceName());
        
        p.put(SUB_NAME, subService.getSoftName());
        
        if (sub != null && sub.getStatus() != DBConst.STATUS_INACTIVE) {
            MyLog.Infor("User '" + msisdn + "' already register '" + cmd.getSubServiceName() + "'");
             if (params != null) {
                params.put(SUB_START_DATE, format.format(new Date(sub.getRegisterTime())));
                params.put(SUB_END_DATE, format.format(new Date(sub.getNextMonfeeChargeTime())));
                params.put(CONFLICT_SUBSERVICE, cmd.getSubServiceName());
                params.put(SUB_STATUS, String.valueOf(sub.getStatus()));
             }
            return BusinessCode.ALREADY;
        }
        
        MyLog.Infor("transaction: " + tranID + " , new sub");
        boolean isConflict = false;
        Subscriber conflict = null;
        boolean cancelResult = false;
        if (subService.isCheckConflict()) {
            // Check Conflict here
            conflict = ussdService.checkConflict(msisdn, subService.getServiceName(), subService.getSubServiceName());

            if (conflict != null) {
                regResultUssd = BusinessCode.ALREADY;
                MyLog.Infor(" User " + msisdn + " registered conflicted SUB_SERVICES (" + conflict.getSubServiceName() + ") "
                        + " Status " + conflict.getStatus());
                //neu cau hinh khong cho tu dong cancel khi bi conflict packet
                if (!subService.isAutoCancel()) {
                    SubService conflictSubService = ussdService.getSubService(conflict.getSubServiceName());
                    p.put(CONFLICT_SUBSERVICE, conflictSubService.getSoftName());
                    p.put(CONFLICT_SUB_START, format.format(new Date(conflict.getRegisterTime())));
                    p.put(CONFLICT_SUB_END, format.format(new Date(conflict.getNextMonfeeChargeTime())));
                    isConflict = true;

                } else {
                    // Auto cancel subServices
                    cancelResult = cancelSubscriber(conflict.clone(), cmd.getCommand() +"|AUTO CANCEL CONFLICT", tranID, cmd.getCommand(), stepsRegister);
                    if (!cancelResult) {
                        MyLog.Debug(" Can not cancel " + conflict + ", Abort Register");
                        isConflict = true;
                    }
                    //hungtv45: khong co else, ...???
                }
            }
        }
        // khong bi conflict
        if (!isConflict) {
            ContentResult checkResult = checkRegisterWS(cmd, msisdn, CallWS.SERVICE_REGISTER, tranID, service.getProvider(),params);
            if (checkResult.getResult() == ContentResult.RESULT_SUCCESS) {
                int chargeResult = CallWS.CHARGE_SUCCESS;
                // MO Fee
                if (price != null && price.getMOPrice() > 0) {
                    // Add Charge transaction
                    chargeResult = CallWS.chargeUser(GlobalConfig.paymentUrl, msisdn, CallWS.CMD_MO, price.getMOPrice(),
                            service.getProvider(), service.getServiceName(),
                            cmd.getSubServiceName(), tranID, cmd.getCommand());
                    // Add to replace in SMS
                    p.put(MO_PRICE, "" + price.getMOPrice());
                } else {
                    // Add to replace in SMS
                    p.put(MO_PRICE, "0");
                }

                registerPrice = price.getCMDPrice();
                if (chargeResult == CallWS.CHARGE_SUCCESS) {
                    String command = cmd.getChargeCommand();
                    if (!needToChargeRegisterFee(sub)) {
                        MyLog.Infor(" User " + msisdn + " is not charge fee");
                        registerPrice = 0;
                    }
                    else{
                        //HUNGTV45, PYC17950 check khuyen mai lan dau cho VTC
//                            String isPromo = mDBConnection.getPromoSubSerive(subService.getSubServiceName()); //haind25 bo doan code nay

//                            if (ENABLEPROMO.equals(isPromo)){
//                        
//                        if (ENABLEPROMO.equals(String.valueOf(subService.getPromotion()))){
//                            if(sub==null){
//                                registerPrice = 0;
//                            }
//                        }
                        /* haind25 promotion for
                         */
                        if (isPromotion) {
                            registerPrice = promotion.getCmdPrice();
                            command = promotion.getChargeCommand();
                            price.setCMDPrice(registerPrice);
                        }
                        
                    }

                    MyLog.Debug("Gonna charge User " + msisdn + " register fee: " + registerPrice);
                    // Charge Register price
                    chargeResult = CallWS.chargeUser(GlobalConfig.paymentUrl, msisdn, command, registerPrice,
                            service.getProvider(), service.getServiceName(),
                            cmd.getSubServiceName(), tranID , cmd.getCommand());
                }
                
                // Add to replace in SMS, command price
                DecimalFormat df = new DecimalFormat("###,###");
                p.put(CMD_PRICE, "" + df.format(registerPrice));
                if (chargeResult == CallWS.CHARGE_SUCCESS) {
                    boolean needUpdateCP = true;
                    boolean needUpdateDB = true;
                    String descCPResult = "";
                    

                    if (sub == null) {
                        isSubNew = true;
                        // User not register
                        sub = new Subscriber();
                        sub.setMsisdn(msisdn);
                        sub.setSubServiceName(cmd.getSubServiceName());
                        sub.setStatus(DBConst.STATUS_ACTIVE);
                        sub.setRegisterTime(System.currentTimeMillis());
                        sub.setLastMonfeeChargeTime(System.currentTimeMillis());

                        long addDate = 1;
                        
                        if (promotion != null) {
                            addDate = promotion.getFreeDay();
                        } else {
                            addDate = mDBConnection.getAdditonalRegisterDate(msisdn, subService.getSubType(),
                                subService.getSubServiceName(), subService.getServiceName(),
                                cmd.getChargeCommand(), "0", null, cmd.getType()) ;
                        }

                        sub.setNextMonfeeChargeTime(PublicLibs.getFirstMomentOfToday() + addDate * DBConst.A_DAY_MS);
                        sub.setDescription("");
                        sub.setChargeStatus(Subscriber.CHARGE_OK);
//                        int isPromotion = DBConst.PROMOTION_OUT;
//
//                        long current = System.currentTimeMillis();
//                        if (subService.getPromoBegin() < current && subService.getPromoEnd() > current) {
//                            isPromotion = DBConst.PROMOTION_IN;
//                        }
                        sub.setIsPromotion(isPromotion? Subscriber.PROMOTION_Y : Subscriber.PROMOTION_N);
                        sub.setDescription(cmd.getCommand() + "|REGISTER");
                    } else {
                        if (sub.getStatus() == DBConst.STATUS_ACTIVE) {
                            // User is already a subscriber, nothing to do here
                            needUpdateCP = false;
                            needUpdateDB = false;

                            MyLog.Infor("User '" + msisdn + "' already register '" + cmd.getSubServiceName() + "'");
//                                msgCode = DBConfiguration.REPLY_MSG_ALREADY_REGISTER;
                            p.put(SUB_START_DATE, format.format(new Date(sub.getRegisterTime())));
                            p.put(SUB_END_DATE, format.format(new Date(sub.getNextMonfeeChargeTime())));
                        } else {
                            // re-active
                            if (sub.getNextMonfeeChargeTime() < System.currentTimeMillis()) {
                                // User reactive out of charging period, need to reactivate, set next charge
                                sub.setStatus(DBConst.STATUS_ACTIVE);
                                sub.setRegisterTime(System.currentTimeMillis());
                                sub.setLastMonfeeChargeTime(System.currentTimeMillis());
                                long addDate = 1;
                        
                                if (promotion != null) {
                                    addDate = promotion.getFreeDay();
                                } else {
                                    addDate = mDBConnection.getAdditonalRegisterDate(msisdn, subService.getSubType(),
                                        subService.getSubServiceName(), subService.getServiceName(),
                                        cmd.getChargeCommand(), "0", null, cmd.getType()) ;
                                }
                                
                                sub.setNextMonfeeChargeTime(PublicLibs.getFirstMomentOfToday() + addDate * DBConst.A_DAY_MS);
                                sub.setChargeStatus(Subscriber.CHARGE_OK);
//                                int isPromotion = DBConst.PROMOTION_OUT;

//                                long current = System.currentTimeMillis();
//                                if (subService.getPromoBegin() < current && subService.getPromoEnd() > current) {
//                                    isPromotion = DBConst.PROMOTION_IN;
//                                }
//                                sub.setIsPromotion(isPromotion);
                            }
                            sub.setIsPromotion(isPromotion? Subscriber.PROMOTION_Y : Subscriber.PROMOTION_N);
                            sub.setStatus(DBConst.STATUS_ACTIVE);
                            sub.setChargeStatus(Subscriber.CHARGE_OK);
                            sub.setDescription(cmd.getCommand() + "|REGISTER");
                        }
                    }


                    if (needUpdateCP) {
                        // Need to call Register Webservice from CP
                        ContentResult cpRegisterResult; // = null;
                        if (cmd.getWebserviceId() > 0) {
                            CPWebservice ws = ussdService.getCPWebService(cmd.getWebserviceId());
                            if (ws != null) {
                                Properties pars = new Properties();
                                pars.put(CallWS.WS_USERNAME, ws.getUserName());
                                pars.put(CallWS.WS_PASSWORD, ws.getPassword());
                                pars.put(CallWS.WS_SERVICE_NAME, cmd.getSubServiceName());
                                pars.put(CallWS.WS_MSISDN, msisdn);
                                pars.put(CallWS.WS_CHARGE_TIME, PublicLibs.getCurrentDateTime());
                                pars.put(CallWS.WS_MODE, CallWS.WS_MODE_REAL);
                                pars.put(CallWS.WS_PARAMS, CallWS.SERVICE_REGISTER + "");
                                pars.put(CallWS.WS_AMOUNT, registerPrice + "");
//                                    pars.put(CallWS.WS_COMMAND, cmd.getRawSMS()); // Raw SMS
                                pars.put(CallWS.WS_TRANS_ID, tranID); // Transaction ID
                                pars.put(CallWS.WS_PROMOTION, promotion == null ? "" : promotion.getCode()); // promotion
                                pars.put(CallWS.WS_CHANEL, cmd.getCommand());
                                if (!params.isEmpty()){
                                   for (Map.Entry<String, String> par : params.entrySet()) {
                                           String key = par.getKey();
                                           String value = par.getValue();
                                           pars.put(key, value);
                                       }
                                }
                                long startTime = System.currentTimeMillis();
                                cpRegisterResult = CallWS.cpRegister(ws, pars, GlobalConfig.retryWebservice);

                                long durationTime = System.currentTimeMillis() - startTime;
                                boolean cpResponseOk = (cpRegisterResult != null) && cpRegisterResult.getResult() == ContentResult.RESULT_SUCCESS;
                                boolean kpiOk = durationTime < GlobalConfig.KPI_CP_TIMEOUT;

                                if (!kpiOk || !cpResponseOk) {
                                    MyLog.Infor(tranID + "log KPI, time: " + kpiOk + ", cpResponse: " + cpResponseOk);
                                    String errorCode = getKpiErrorCode(kpiOk, cpResponseOk, cpRegisterResult);
                                    String response = cpRegisterResult == null ? "CP Timeout" : cpRegisterResult.toString();
                                    KpiCpSoap kpiCpSoap = new KpiCpSoap(service.getProvider(), cmd.getSubServiceName(), cmd.getCommand(), tranID,
                                            msisdn,cmd.getChargeCommand(), durationTime, ws.getUrl(),
                                            parseValue(ws.getRawXML(), pars) , response, cpRegisterResult.getDesc(), startTime, errorCode, CallWS.WS_MODE_REAL);
                                    KpiCpSoapDao.getInstance().enqueue(kpiCpSoap);
                                }

                                MyLog.Infor("Call WS: " + tranID + " -> " + cpRegisterResult);
                                MyLog.Debug("Call WS " + ws + " -> " + cpRegisterResult);

                                if (cpRegisterResult != null && cpRegisterResult.getResult() == CallWS.RESULT_SUCCESS) {
                                    // Call CP Success
                                    descCPResult = cpRegisterResult.getDesc();
                                } else {
                                    // Update to DB with status = INACTIVE so User will not be charge when register again 
                                    sub.setStatus(DBConst.STATUS_INACTIVE);
                                    sub.setCancelTime(System.currentTimeMillis());
                                    sub.setDescription(cmd.getCommand() + "|Auto Cancel|CP Failed or Timeout");
//                                        msgCode = DBConfiguration.REPLY_MSG_FAILED;
                                }
                            }
                        }
                    }

                    if (needUpdateDB) {
                        //dua vao queue xu ly
                        if (isSubNew) {
                            SubscriberInserDao.getInstance().enqueue(sub);
                        } else {
                            SubscriberUpdateDao.getInstance().enqueue(sub);
                        }

                        if (sub.getStatus() == DBConst.STATUS_INACTIVE) {
                            // Failed when call to CP
                            regResult = DBConst.REGISTER_FAILED;
                            regResultUssd = BusinessCode.FAIL;
                        } else {
//                                msgCode = DBConfiguration.REPLY_MSG_SUCCESS;
                            regResult = DBConst.REGISTER_SUCCESS;
                            regResultUssd = BusinessCode.SUCCESS;
                            regResultUssd.setResponse(null);
                            if (isPromotion && promotion != null) {
                                MyLog.Infor(tranID + ", Promotion message: " + msisdn + ", message: " + promotion.getSuccessMessage() + "");
                                regResultUssd.setResponse(promotion.getSuccessMessage());
                            }
                        }
                    } else {
                        regResult = DBConst.REGISTER_FAILED;
                        regResultUssd = BusinessCode.FAIL;
                    }

                    // Add to parse WS
                    p.put(CP_RESULT, descCPResult);
                    p.put(SUB_START_DATE, format.format(new Date(sub.getRegisterTime())));
                    p.put(SUB_END_DATE, format.format(new Date(sub.getNextMonfeeChargeTime())));
                } else {
                    // Not enough money
//                        msgCode = DBConfiguration.REPLY_MSG_NOT_ENOUGH_MONEY;
                    regResult = DBConst.REGISTER_FAILED;
                    regResultUssd = BusinessCode.NOT_ENOUGH_MONEY;
                }
                // Restore service if cannot register
                if (regResult == DBConst.RESULT_FAILED) {
                    if (cancelResult) {
                        if (conflict != null) {
                            MyLog.Debug("Restore subscriber: " + conflict);
                            restoreSubscriber(conflict, "Restore Register failed", tranID,service.getProvider(), cmd.getCommand(),stepsRegister );
                        }
                    }
                }
            } else {
                MyLog.Infor("User '" + msisdn + "' already register '" + cmd.getSubServiceName() + "'");
                p.put(SUB_START_DATE, format.format(new Date(sub.getRegisterTime())));
                p.put(SUB_END_DATE, format.format(new Date(sub.getNextMonfeeChargeTime())));
            }

                // Insert Subscription log, dua vao queue

                //hungtv54: need edit
                SubLogs subLogs = new SubLogs(msisdn, cmd.getSubServiceName(),
                        tranID, "REGISTER " + price + (isPromotion ? "|PROMOTION" : ""),
                    regResult, System.currentTimeMillis(), stepsRegister, cmd.getCommand());
                SubLogsInserDao.getInstance().enqueue(subLogs);

            if (regResult == DBConst.RESULT_SUCCESS && subService.isRequireMPSAccount()) {
                // register MPS account
                regResultUssd = BusinessCode.SUCCESS_REQUIRE_MPS_ACCOUNT;
            }
        }
        
        
        //CDR
        if (getCDRDump() != null) {
            CDR cdr = new CDR();
            cdr.addField(msisdn); // MSISDN
            cdr.addField(""); // Ma thue bao tren BCCS
            cdr.addField(""); // Loai thue bao Tra truoc/tra sau
            cdr.addField(service.getServiceName() + "_" + subService.getSubServiceName()); // Ma dich vu VAS MaHeThong_MaDichVu -> Chua ro
            cdr.addField("1"); // Ma Tac dong -> 1 dang ky
            cdr.addField("3"); // Ma hinh thuc tac dong 
            // 1: SMS, 2: WEB, 3: USSD, 4: WAP, 
            // 5: IVR, 6: He thong xu ly, 7: Websevices, 
            // 8: Nguoi quan tri khai thác, 9: Các kenh tac d6666ong ben ngoai
            cdr.addField(PublicLibs.getCurrentDateTime()); // Thoi gian tac dong
            cdr.addField(msisdn); // 8 So dien thoai tac dong
            cdr.addField(PublicLibs.getCurrentDateTime()); // Thoi gian bat dau hieu luc
            cdr.addField(""); // Thoi gian ket thuc hieu luc
            cdr.addField("" + (regResult == DBConst.RESULT_SUCCESS ? registerPrice : 0)); // Cuoc
            cdr.addField("" + (regResult == DBConst.RESULT_SUCCESS ? 0 : -1)); // Trang thai tac dong
            cdr.addField("0"); // Ma loi giao dich
            cdr.addField(""); // Nguyen nhan loi
//            cdr.addField(cmd.getRawSMS()); // Noi dung tin nhan
//            cdr.addField(cmd.getShortCode()); // Dau so

//            cdr.addField(mtContent.replace("\n", "")); // Tin nhan tra ve
            cdr.addField(PublicLibs.getCurrentDateTime()); // Thoi gian tra ve
            cdr.addField(""); // Party code
            cdr.addField(""); // Thong tin du tru 1 -> Luu so luong MT cua tin nhan
            cdr.addField(""); // Thong tin du tru 2 -> Luu Username da ban tin
            cdr.addField(""); // Thong tin du tru 3
            cdr.addField(""); // Thong tin du tru 4
            cdr.addField(""); // Thong tin du tru 5
            cdr.addField(""); // Thong tin du tru 6
            cdr.addField(""); // Thong tin du tru 7
            cdr.addField(""); // Thong tin du tru 8
            cdr.addField(""); // Thong tin du tru 9
            cdr.addField(""); // Thong tin du tru 10
            mCDRDumper.addCDR(cdr);
        }
        
        if (params != null) {
            for (Map.Entry<Object, Object> entry : p.entrySet()) {
                Object key = entry.getKey();
                Object value = entry.getValue();
                if (key != null && value != null) {
                    params.put(String.valueOf(key), String.valueOf(value));
                }
            }
        }
        
        return regResultUssd;
    }
    
    //check register
    /**
     * Process Register command
     *
     * @param msisdn
     * @param shortCode
     * @param cmd
     */
    private BusinessCode processCheckRegister(String msisdn, Command cmd) {
        BusinessCode regResultUssd = BusinessCode.NOT_REGISTER;
        Properties p = new Properties();
//        Subscriber sub = mDBConnection.getSubscriber(msisdn, cmd.getSubServiceName());
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        Subscriber sub = ussdService.getSubscriber(msisdn, cmd.getSubServiceName());
        if (sub == null || sub.getStatus() == DBConst.STATUS_INACTIVE) {
            // if sub null or inactive
            regResultUssd = BusinessCode.NOT_REGISTER;
        }
        else{
            regResultUssd = BusinessCode.ALREADY;
        }
        
        return regResultUssd;
    }
       
    //hungtv45
    /**
     * Process Unregister command
     *
     * @param msisdn
     * @param shortCode
     * @param cmd
     */
    private BusinessCode processUnRegister(String msisdn, CPService service, Command cmd, 
            PriceCharge price, String tranID, Map<String,String> params, String stepUnRegister) {
        Properties p = new Properties();
        CachingeService cachingService = ServiceFactory.getBean(CachingeService.class);
        SubService subService = cachingService.getSubService(cmd.getSubServiceName());
        p.put(SUB_NAME, subService.getSoftName());        
//        String msgCode;
        int regResult = DBConst.RESULT_FAILED;
        BusinessCode regResultUssd = BusinessCode.FAIL;
        
//        SimpleDateFormat format = new SimpleDateFormat(GlobalConfig.datetimeFormat);
         
        Subscriber sub = cachingService.getSubscriber(msisdn, cmd.getSubServiceName()); 
        int cancelPrice = 0;
        boolean isSub = sub != null && sub.getStatus() != DBConst.STATUS_INACTIVE;
        boolean isCheckCpBeforeCharge = true;
        if (isSub) {
            ContentResult checkResult = checkRegisterWS(cmd, msisdn, CallWS.SERVICE_UNREGISTER, tranID, service.getProvider(),params);
            isCheckCpBeforeCharge = (checkResult != null) && ContentResult.RESULT_SUCCESS == checkResult.getResult();
        }
        
        if (isSub && isCheckCpBeforeCharge) {
            int chargeResult = CallWS.CHARGE_SUCCESS;
            
            // MO Fee, charge price MO
            if (price != null && price.getMOPrice() > 0) {
                
                chargeResult = CallWS.chargeUser(GlobalConfig.paymentUrl, msisdn, CallWS.CMD_MO, price.getMOPrice(),
                        service.getProvider(), service.getServiceName(),
                        cmd.getSubServiceName(), tranID, cmd.getCommand());
                p.put(MO_PRICE, "" + price.getMOPrice());
                MyLog.Infor("Gonna charge User for MO Cancel:" + tranID +", isdn: " + msisdn + ",  mpi response: " + chargeResult);
            } else {
                p.put(MO_PRICE, "0");
            }

            if (chargeResult == CallWS.CHARGE_SUCCESS) {
                cancelPrice = price == null ? 0 : price.getCMDPrice();
                // Charge Register price
                DecimalFormat df = new DecimalFormat("###,###");
                chargeResult = CallWS.chargeUser(GlobalConfig.paymentUrl, msisdn, cmd.getChargeCommand(), cancelPrice,
                    service.getProvider(), service.getServiceName(), cmd.getSubServiceName(),
                    tranID, cmd.getCommand());
                MyLog.Infor("Gonna charge User for Cancel:" + tranID +", isdn: " + msisdn + ",  mpi response: " + chargeResult);
                p.put(CMD_PRICE, "" + df.format(cancelPrice));
            }
            
            if (chargeResult == CallWS.CHARGE_SUCCESS) {
                boolean needUpdate = true;
                String descCPResult = "";
                if (cmd.getWebserviceId()> 0) {
                    // <editor-fold defaultstate="collapsed" desc="cp webservice">
                    CPWebservice ws = cachingService.getCPWebService(cmd.getWebserviceId());
                    if (ws != null) {
                        Properties pars = new Properties();
                        pars.put(CallWS.WS_USERNAME, ws.getUserName());
                        pars.put(CallWS.WS_PASSWORD, ws.getPassword());
                        pars.put(CallWS.WS_SERVICE_NAME, cmd.getSubServiceName());
                        pars.put(CallWS.WS_MSISDN, msisdn);
                        pars.put(CallWS.WS_CHARGE_TIME, PublicLibs.getCurrentDateTime());
                        pars.put(CallWS.WS_MODE, CallWS.WS_MODE_REAL);
                        pars.put(CallWS.WS_PARAMS, CallWS.SERVICE_UNREGISTER + "");
                        pars.put(CallWS.WS_AMOUNT, cancelPrice + "");
                        pars.put(CallWS.WS_TRANS_ID, tranID);
                        pars.put(CallWS.WS_CHANEL, cmd.getCommand());
                        if (!params.isEmpty()) {
                            for (Map.Entry<String, String> par : params.entrySet()) {
                                String key = par.getKey();
                                String value = par.getValue();
                                pars.put(key, value);

                            }
                        }
                        long startTime = System.currentTimeMillis();
                        // </editor-fold>
                        ContentResult cpRegisterResult = CallWS.cpRegister(ws, pars, GlobalConfig.retryWebservice);
                        
                        long durationTime = System.currentTimeMillis() - startTime;
                        boolean cpResponseOk = (cpRegisterResult != null) && cpRegisterResult.getResult() == ContentResult.RESULT_SUCCESS;
                        boolean kpiOk = durationTime < GlobalConfig.KPI_CP_TIMEOUT;
                        
                        String response = cpRegisterResult == null ? "CP Timeout" : cpRegisterResult.toString();
                        if (!kpiOk || !cpResponseOk) {
                            String errorCode = getKpiErrorCode(kpiOk, cpResponseOk, cpRegisterResult);
                            
                            KpiCpSoap kpiCpSoap = new KpiCpSoap(service.getProvider(), cmd.getSubServiceName(), cmd.getCommand(), tranID,
                                    msisdn,cmd.getChargeCommand(), durationTime, ws.getUrl(),
                                    parseValue(ws.getRawXML(), pars) , response, cpRegisterResult.getDesc(), startTime, errorCode, CallWS.WS_MODE_REAL);
                            KpiCpSoapDao.getInstance().enqueue(kpiCpSoap);
                        }
                                    
                        MyLog.Infor( "Call CP WS: " + tranID + " , cp response: " + cpRegisterResult);
                        if (cpRegisterResult != null 
                                && cpRegisterResult.getResult() == CallWS.RESULT_SUCCESS
                                && cpRegisterResult.getDesc() != null
                                && !cpRegisterResult.getDesc().trim().isEmpty()) {
                            descCPResult = cpRegisterResult.getDesc();
                        }
                    }
                }

                // Add to parse WS
                p.put(CP_RESULT, descCPResult);

                if (needUpdate) {
                    // update information to DB
                    sub.setStatus(DBConst.STATUS_INACTIVE);
                    sub.setCancelTime(System.currentTimeMillis());
                    sub.setDescription(cmd.getCommand() + "|CANCEL");
//                    mDBConnection.updateSubscriber(sub, RETRY_UPDATE_SUB); dua vao queue xu ly
                    SubscriberUpdateDao.getInstance().enqueue(sub);
                    MyLog.Infor( "enqueued cancel sub: " + tranID + " , isdn: " + sub.getMsisdn()
                            + " , sub-service: " + sub.getSubServiceName() );
//                    msgCode = DBConfiguration.REPLY_MSG_SUCCESS;
                    regResult = DBConst.RESULT_SUCCESS;
                    regResultUssd = BusinessCode.SUCCESS;
                } else {
                    // failed in some where
//                    msgCode = DBConfiguration.REPLY_MSG_FAILED;
                    regResult = DBConst.RESULT_FAILED;
                    regResultUssd = BusinessCode.FAIL;
                }
            } else {
//                msgCode = DBConfiguration.REPLY_MSG_NOT_ENOUGH_MONEY;
                regResult = DBConst.RESULT_FAILED;
                regResultUssd = BusinessCode.NOT_ENOUGH_MONEY;
            }

        } else {
            // User does not register service
//            msgCode = DBConfiguration.REPLY_MSG_NOT_REGISTER;
            regResult = DBConst.RESULT_FAILED;
            if (!isSub) {
                 regResultUssd = BusinessCode.NOT_REGISTER;
            } else if (!isCheckCpBeforeCharge) {
                regResultUssd = BusinessCode.FAIL;
            } else {
                regResultUssd = BusinessCode.FAIL;
            }
            
            if (subService.isCheckConflict()) {
                // Check Conflict here
//                Subscriber conflict = mDBConnection.checkConflict(msisdn, subService.getServiceName(), subService.getSubServiceName());
                Subscriber conflict = cachingService.checkConflict(msisdn, subService.getServiceName(), subService.getSubServiceName());
                if (conflict != null) {
                    MyLog.Debug( " User " + msisdn + " registered conflicted SUB_SERVICES (" + conflict.getSubServiceName() + ") "
                            + " Status " + conflict.getStatus());
                    regResultUssd = BusinessCode.CONFLICT;
                }
            }
        }
        // Insert Subscription log, dua va queue hungtv45
        SubLogs subLogs = new SubLogs(msisdn, cmd.getSubServiceName(), tranID, "UNREGISTER " + price,
                regResult, System.currentTimeMillis(),stepUnRegister, cmd.getCommand());
        
        SubLogsInserDao.getInstance().enqueue(subLogs);
        
        // <editor-fold defaultstate="collapsed" desc="Dump CDR">
        //CDR
        if (getCDRDump() != null) {
            CDR cdr = new CDR();
            cdr.addField(msisdn); // MSISDN
            cdr.addField(""); // Ma thue bao tren BCCS
            cdr.addField(""); // Loai thue bao Tra truoc/tra sau
            cdr.addField(service.getServiceName() + "_" + cmd.getSubServiceName()); // Ma dich vu VAS MaHeThong_MaDichVu -> Chua ro
            cdr.addField("2"); // Ma Tac dong -> chua ro
            //1: Dang ky       2: Hủy            3: Gia hạn            4: Kiểm tra
            //5: Hướng dẫn     6: Thêm số        7: Xóa số             8: Hủy gia hạn
            //9: Tải bài hát   10: Xem Video    11: Confirm (Xác nhận) 12: khôi phục

            cdr.addField("3"); // Ma hinh thuc tac dong 
            // 1: SMS, 2: WEB, 3: USSD, 4: WAP, 
            // 5: IVR, 6: He thong xu ly, 7: Websevices, 
            // 8: Nguoi quan tri khai thác, 9: Các kenh tac dong ben ngoai
            cdr.addField(PublicLibs.getCurrentDateTime()); // Thoi gian tac dong
            cdr.addField(msisdn); // 8 So dien thoai tac dong
            cdr.addField(PublicLibs.getCurrentDateTime()); // Thoi gian bat dau hieu luc
            cdr.addField(""); // Thoi gian ket thuc hieu luc
            cdr.addField("" + (regResult == DBConst.RESULT_SUCCESS ? cancelPrice : 0)); // Cuoc
            cdr.addField("" + (regResult == DBConst.RESULT_SUCCESS ? 0 : -1)); // Trang thai tac dong
            cdr.addField("0"); // Ma loi giao dich
            cdr.addField(""); // Nguyen nhan loi
//            cdr.addField(cmd.getRawSMS()); // Noi dung tin nhan
//            cdr.addField(cmd.getShortCode()); // Dau so
//            cdr.addField(mtContent.replace("\n", "")); // Tin nhan tra ve
            cdr.addField(PublicLibs.getCurrentDateTime()); // Thoi gian tra ve
            cdr.addField(""); // Party code
            cdr.addField(""); // Thong tin du tru 1 -> Luu so luong MT cua tin nhan
            cdr.addField(""); // Thong tin du tru 2 -> Luu Username da ban tin
            cdr.addField(""); // Thong tin du tru 3
            cdr.addField(""); // Thong tin du tru 4
            cdr.addField(""); // Thong tin du tru 5
            cdr.addField(""); // Thong tin du tru 6
            cdr.addField(""); // Thong tin du tru 7
            cdr.addField(""); // Thong tin du tru 8
            cdr.addField(""); // Thong tin du tru 9
            cdr.addField(""); // Thong tin du tru 10

            getCDRDump().addCDR(cdr);
        }
        // </editor-fold>
            
        if (params != null) {
            for (Map.Entry<Object, Object> entry : p.entrySet()) {
                Object key = entry.getKey();
                Object value = entry.getValue();
                if (key != null && value != null) {
                    params.put(String.valueOf(key), String.valueOf(value));
                }
            }
        }
            
       return regResultUssd;
    }
        
    /**
     * Process others command
     *
     * @param msisdn
     * @param shortCode
     * @param cmd
     */
    private BusinessCode processForward(String msisdn, CPService service, Command cmd, PriceCharge price, String tranID, Map<String,String> paramsAdd, String stepsForward) {
        Properties p = new Properties();
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
//        SubService subService = mDBConnection.getSubService(cmd.getSubServiceName());
        SubService subService = ussdService.getSubService(cmd.getSubServiceName());
        p.put(SUB_NAME, subService.getSoftName());
        String msgCode = DBConfiguration.REPLY_MSG_SUCCESS;
        SimpleDateFormat format = new SimpleDateFormat(GlobalConfig.datetimeFormat);
        int contentResult = DBConst.RESULT_SUCCESS;
        BusinessCode contentResultUssd = BusinessCode.FAIL;
        //SubService subService = mConfig.getSubServices(cmd.getSubServiceName());
        ContentResult checkResult = checkOthersWS(cmd, msisdn, price.getCMDPrice(), service, subService, tranID, paramsAdd);
        int chargePrice = 0;
        int chargeResult = CallWS.CHARGE_SUCCESS;
        boolean isCustomerLostMoneyByCpSecnario = false;
        // MO Fee
        if (price.getMOPrice() > 0) {
            // Add Charge transaction
            chargeResult = CallWS.chargeUser(GlobalConfig.paymentUrl, msisdn, CallWS.CMD_MO, price.getMOPrice(),
                    service.getProvider(), service.getServiceName(), cmd.getSubServiceName(),
                    tranID, cmd.getCommand());
            // Add to replace in SMS
            p.put(MO_PRICE, "" + price.getMOPrice());
        }
        
        if (chargeResult == CallWS.CHARGE_SUCCESS) {
            if ((cmd.getRequiredRegisted() == 1) && (!isRegisteredSubService(msisdn, cmd.getSubServiceName()))) {
                msgCode = DBConfiguration.REPLY_MSG_NOT_REGISTER;
                contentResult = DBConst.RESULT_FAILED;
                contentResultUssd = BusinessCode.FAIL;
            } else {
                if (checkResult.getResult() == ContentResult.RESULT_SUCCESS) {
//                    if (chargeResult == CallWS.CHARGE_SUCCESS && price.getCMDPrice() > 0) {
                    if (price.getCMDPrice() > 0) {
                        // Charge
                        chargeResult = CallWS.chargeUser(GlobalConfig.paymentUrl, msisdn, cmd.getChargeCommand(), price.getCMDPrice(),
                                service.getProvider(), service.getServiceName(), cmd.getSubServiceName(), tranID, cmd.getCommand());
                        // Add to replace in SMS
                        //p.put(CMD_PRICE, "" + price.getCMDPrice());
                        chargePrice = price.getCMDPrice();
                        DecimalFormat df = new DecimalFormat("###,###");
                        p.put(CMD_PRICE, "" + df.format(price.getCMDPrice()));
                    } else {
                        // Add to replace in SMS
                        p.put(CMD_PRICE, "0");
                    }

                    if (chargeResult == CallWS.CHARGE_SUCCESS) {
                        isCustomerLostMoneyByCpSecnario = price.getCMDPrice() > 0;
                        if (cmd.getWebserviceId()>= 0) {
                            // Call Webservice from CP
//                            CPWebservice ws = mDBConnection.getCPWebService(cmd.getWebserviceId());
                            CPWebservice ws = ussdService.getCPWebService(cmd.getWebserviceId());
                            if (ws != null) {
                                Properties pars = new Properties();
                                pars.put(CallWS.WS_USERNAME, ws.getUserName());
                                pars.put(CallWS.WS_PASSWORD, ws.getPassword());
                                pars.put(CallWS.WS_SERVICE_NAME, cmd.getSubServiceName());
                                pars.put(CallWS.WS_MSISDN, msisdn);
                                pars.put(CallWS.WS_CHARGE_TIME, PublicLibs.getCurrentDateTime());
                                pars.put(CallWS.WS_DATETIME, PublicLibs.getCurrentDateTime());
                                pars.put(CallWS.WS_MODE, CallWS.WS_MODE_REAL);
                                pars.put(CallWS.WS_AMOUNT, price.getCMDPrice() + "");
                                String params = CallWS.parseValue(ws.getParams(), pars);
                                pars.put(CallWS.WS_PARAMS, params);
                                pars.put(CallWS.WS_TRANS_ID, tranID);
                                pars.put(CallWS.WS_CHANEL, cmd.getCommand());
                                String sign = createDataSign(ws.getDataSign(), pars, service);
                                if (sign != null) {
                                    pars.put(CallWS.WS_DATA_SIGN, sign);
                                }
                                for (Map.Entry<String, String> par : paramsAdd.entrySet()) {
                                    String key = par.getKey();
                                    String value = par.getValue();
                                     pars.put(key, value);
                                }

                                long startTime = System.currentTimeMillis();
                                ContentResult result = CallWS.cpWebservice(ws, pars, GlobalConfig.retryWebservice);
                                
                                long durationTime = System.currentTimeMillis() - startTime;
                                boolean cpResponseOk = (result != null) && result.getResult() == ContentResult.RESULT_SUCCESS;
                                boolean kpiOk = durationTime < GlobalConfig.KPI_CP_TIMEOUT;
                                
                                String response = result == null ? "CP Timeout" : result.toString();
                                MyLog.Infor("Call WS (" + ws.getSubServiceName() + "):" + tranID + " -> result: " + response);
                                
                                if (!kpiOk || !(cpResponseOk && isCustomerLostMoneyByCpSecnario)) {
                                    String errorCode = getKpiErrorCode(kpiOk, cpResponseOk, result);
                                    
                                    KpiCpSoap kpiCpSoap = new KpiCpSoap(service.getProvider(), cmd.getSubServiceName(), cmd.getCommand(), tranID,
                                            msisdn,cmd.getChargeCommand(), durationTime, ws.getUrl(),
                                            parseValue(ws.getRawXML(), pars) , response, result == null ? "" :  result.getDesc(), startTime, errorCode, CallWS.WS_MODE_REAL);
                                    KpiCpSoapDao.getInstance().enqueue(kpiCpSoap);
                                }
                
                                if (result != null) {
                                    p.put(CP_RESULT, result.getDesc());
                                    if (result.getResult() == ContentResult.RESULT_SUCCESS) {
                                        msgCode = DBConfiguration.REPLY_MSG_SUCCESS;
                                        contentResultUssd = BusinessCode.SUCCESS;
                                    } else {
                                        msgCode = DBConfiguration.REPLY_MSG_FAILED;
                                        contentResult = DBConst.RESULT_FAILED;
                                    }
                                    
                                    if (result.getDesc() != null) {
                                        contentResultUssd.setDescription(result.getDesc());
                                    }
                                } else {
                                    msgCode = DBConfiguration.REPLY_MSG_FAILED;
                                    contentResult = DBConst.RESULT_FAILED;
                                }
                            } else {
                                msgCode = DBConfiguration.REPLY_MSG_FAILED;
                                contentResult = DBConst.RESULT_FAILED;
                            }
                        }
                    } else {
                        msgCode = DBConfiguration.REPLY_MSG_NOT_ENOUGH_MONEY;
                        contentResult = DBConst.RESULT_FAILED;
                        contentResultUssd = BusinessCode.NOT_ENOUGH_MONEY;
                    }
                } else {
                    p.put(CP_RESULT, checkResult.getDesc());
                    msgCode = DBConfiguration.REPLY_MSG_FAILED;
                    contentResult = DBConst.RESULT_FAILED;
                    contentResultUssd = BusinessCode.FAIL;
                    if (checkResult.getDesc() != null && !checkResult.getDesc().isEmpty()) {
                        contentResultUssd.setDescription(checkResult.getDesc());
                    }
                }
            }
        } else {
            msgCode = DBConfiguration.REPLY_MSG_NOT_ENOUGH_MONEY;
            contentResult = DBConst.RESULT_FAILED;
            contentResultUssd = BusinessCode.NOT_ENOUGH_MONEY;
        }
        // Insert Subscription log, dua va queue, hungtv45        
        SubLogs subLogs = new SubLogs(msisdn, cmd.getSubServiceName(), tranID,
                "FORWARD : " + cmd.getChargeCommand()+" - "+ price,
                contentResult, System.currentTimeMillis(),stepsForward, cmd.getCommand());
        SubLogsInserDao.getInstance().enqueue(subLogs);
                
        // <editor-fold defaultstate="collapsed" desc="Dump CDR">
        //CDR
        if (getCDRDump() != null) {
            CDR cdr = new CDR();
            cdr.addField(msisdn); // MSISDN
            cdr.addField(""); // Ma thue bao tren BCCS
            cdr.addField(""); // Loai thue bao Tra truoc/tra sau
            cdr.addField(service.getServiceName() + "_" + cmd.getSubServiceName()); // Ma dich vu VAS MaHeThong_MaDichVu -> Chua ro
            cdr.addField("13"); // Ma Tac dong -> chua ro
            //1: Dang ky       2: Hủy            3: Gia hạn            4: Kiểm tra
            //5: Hướng dẫn     6: Thêm số        7: Xóa số             8: Hủy gia hạn
            //9: Tải bài hát   10: Xem Video    11: Confirm (Xác nhận) 12: khôi phục

            cdr.addField("3"); // Ma hinh thuc tac dong 
            // 1: SMS, 2: WEB, 3: USSD, 4: WAP, 
            // 5: IVR, 6: He thong xu ly, 7: Websevices, 
            // 8: Nguoi quan tri khai thác, 9: Các kenh tac dong ben ngoai
            cdr.addField(PublicLibs.getCurrentDateTime()); // Thoi gian tac dong
            cdr.addField(msisdn); // 8 So dien thoai tac dong
            cdr.addField(PublicLibs.getCurrentDateTime()); // Thoi gian bat dau hieu luc
            cdr.addField(""); // Thoi gian ket thuc hieu luc
            cdr.addField("" + (contentResult == DBConst.RESULT_SUCCESS ? chargePrice : 0)); // Cuoc
            cdr.addField("" + (contentResult == DBConst.RESULT_SUCCESS ? 0 : -1)); // Trang thai tac dong
            cdr.addField("0"); // Ma loi giao dich
            cdr.addField(""); // Nguyen nhan loi
//            cdr.addField(cmd.getRawSMS()); // Noi dung tin nhan
//            cdr.addField(cmd.getShortCode()); // Dau so

//            cdr.addField(mtContent.replace("\n", "")); // Tin nhan tra ve
            cdr.addField(PublicLibs.getCurrentDateTime()); // Thoi gian tra ve
            cdr.addField(""); // Party code
            cdr.addField(""); // Thong tin du tru 1 -> Luu so luong MT cua tin nhan
            cdr.addField(""); // Thong tin du tru 2 -> Luu Username da ban tin
            cdr.addField(""); // Thong tin du tru 3
            cdr.addField(""); // Thong tin du tru 4
            cdr.addField(""); // Thong tin du tru 5
            cdr.addField(""); // Thong tin du tru 6
            cdr.addField(""); // Thong tin du tru 7
            cdr.addField(""); // Thong tin du tru 8
            cdr.addField(""); // Thong tin du tru 9
            cdr.addField(""); // Thong tin du tru 10

            getCDRDump().addCDR(cdr);
        }    
        // </editor-fold>
        
        if (paramsAdd != null) {
            for (Map.Entry<Object, Object> entry : p.entrySet()) {
                Object key = entry.getKey();
                Object value = entry.getValue();
                if (key != null && value != null) {
                    paramsAdd.put(String.valueOf(key), String.valueOf(value));
                }
            }
        }
        MyLog.Infor("Params: " + tranID + ":" + paramsAdd);
        return contentResultUssd;
    }
    
    
    public boolean isRegisteredSubService(String msisdn, String subService) {
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        Subscriber sub = ussdService.getSubscriber(msisdn, subService);
        int result = DBConst.SERVICE_REGISTERED;
        
        if (sub == null || sub.getStatus() == DBConst.STATUS_INACTIVE) {
            result = DBConst.SERVICE_UNREGISTERED;
        } 
        
        return result == DBConst.SERVICE_REGISTERED;
    }
    
    private BusinessCode processGetMPSPassword(String msisdn, CPService service, Command cmd, PriceCharge price, String tranID, Map<String,String> params, String stepsGetMpsPass) {
        Properties p = new Properties();
//        p.put(SMS_ITEM, cmd.getExtSMS());
        p.put(SUB_NAME, cmd.getSubServiceName());

//        String msgCode = DBConfiguration.REPLY_MSG_FAILED;
//        SimpleDateFormat format = new SimpleDateFormat(GlobalConfig.datetimeFormat);
        int contentResult = DBConst.RESULT_SUCCESS;
        BusinessCode contentResultUssd = BusinessCode.FAIL;
        int chargeResult = CallWS.CHARGE_SUCCESS;
        // MO Fee
        if (price != null && price.getMOPrice() > 0) {
            // Add Charge transaction
            chargeResult = CallWS.chargeUser(GlobalConfig.paymentUrl, msisdn, CallWS.CMD_MO, price.getMOPrice(),
                    service.getProvider(), service.getServiceName(), cmd.getSubServiceName(),
                    tranID, cmd.getCommand());
        }

        if (price != null && chargeResult == CallWS.CHARGE_SUCCESS && price.getCMDPrice() > 0) {
            // Charge
            chargeResult = CallWS.chargeUser(GlobalConfig.paymentUrl, msisdn, cmd.getChargeCommand(), price.getCMDPrice(),
                    service.getProvider(), service.getServiceName(), cmd.getSubServiceName(),
                    tranID, cmd.getCommand());
        }

        if (chargeResult == CallWS.CHARGE_SUCCESS) {
//            Subscriber sub = mDBConnection.getSubscriber(msisdn, cmd.getSubServiceName());
            CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
            Subscriber sub = ussdService.getSubscriber(msisdn, cmd.getSubServiceName());
            if (sub != null && sub.getStatus() != DBConst.STATUS_INACTIVE) {
                // register or pending
                MPSUser user = mDBConnection.getMPSUser(msisdn);
                if (user != null) {
                    String newPassword = PublicLibs.getRandomPassword();
                    user.setPassword(newPassword);
                } else {
                    // Create NewPassword Here
                    user = new MPSUser(msisdn, PublicLibs.getRandomPassword(), DBConst.STATUS_ACTIVE,
                            0, System.currentTimeMillis(), 0);
                }

                mDBConnection.updateMPSUser(user);
                p.put(CP_RESULT, user.getPassword());
//                msgCode = DBConfiguration.REPLY_MSG_SUCCESS;
                contentResult = DBConst.RESULT_SUCCESS;
                contentResultUssd = BusinessCode.SUCCESS;
            } else {
//                msgCode = DBConfiguration.REPLY_MSG_NOT_REGISTER;
                contentResult = DBConst.RESULT_FAILED;
            }
        } else {
//            msgCode = DBConfiguration.REPLY_MSG_NOT_ENOUGH_MONEY;
            contentResult = DBConst.RESULT_FAILED;
        }
        
        if (params != null) {
            for (Map.Entry<Object, Object> entry : p.entrySet()) {
                Object key = entry.getKey();
                Object value = entry.getValue();
                if (key != null && value != null) {
                    params.put(String.valueOf(key), String.valueOf(value));
                }
            }
        }
        // Insert Subscription log, dua va queue, hungtv45
        SubLogs subLogs = new SubLogs(msisdn, cmd.getSubServiceName(), tranID, "REGISTER " + price,
                        contentResult, System.currentTimeMillis(), stepsGetMpsPass, cmd.getCommand());
                    SubLogsInserDao.getInstance().enqueue(subLogs);
        return contentResultUssd;
    }
    
    private boolean cancelSubscriber(Subscriber sub, String desc, String tranID, String channel, String stepsCancelSubcriber){
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        boolean result;
        Subscriber tmp = sub.clone();
        boolean cancelOK = true;
        SubService subService = ussdService.getSubService(sub.getSubServiceName());

        if (subService.getCancelWS() > 0) {
            CPWebservice ws = ussdService.getCPWebService(subService.getCancelWS());
            if (ws != null) {
                // <editor-fold defaultstate="collapsed" desc="Create Parameter">
                Properties pars = new Properties();
                pars.put(CallWS.WS_USERNAME, ws.getUserName());
                pars.put(CallWS.WS_PASSWORD, ws.getPassword());
                pars.put(CallWS.WS_SERVICE_NAME, subService.getSubServiceName());
                pars.put(CallWS.WS_MSISDN, sub.getMsisdn());
                pars.put(CallWS.WS_CHARGE_TIME, PublicLibs.getCurrentDateTime());
                pars.put(CallWS.WS_MODE, CallWS.WS_MODE_REAL);
                pars.put(CallWS.WS_PARAMS, CallWS.SERVICE_UNREGISTER + "");
                pars.put(CallWS.WS_AMOUNT, "0");
                pars.put(CallWS.WS_COMMAND, ""); // Raw SMS
                pars.put(CallWS.WS_TRANS_ID, tranID); // Transaction ID
                pars.put(CallWS.WS_CHANEL, channel);
                // </editor-fold>
                
                long startTime = System.currentTimeMillis();
                ContentResult cpRegisterResult = CallWS.cpRegister(ws, pars, GlobalConfig.retryWebservice);
                
                long durationTime = System.currentTimeMillis() - startTime;
                boolean cpResponseOk = (cpRegisterResult != null) && cpRegisterResult.getResult() == ContentResult.RESULT_SUCCESS;
                boolean kpiOk = durationTime < GlobalConfig.KPI_CP_TIMEOUT;

                if (!kpiOk || !cpResponseOk) {
                    String errorCode = getKpiErrorCode(kpiOk, cpResponseOk, cpRegisterResult);
                    String response = cpRegisterResult == null ? "CP Timeout" : cpRegisterResult.toString();
                    KpiCpSoap kpiCpSoap = new KpiCpSoap(sub.getProvider(), sub.getSubServiceName(), channel, tranID,
                            sub.getMsisdn(),CallWS.CMD_UNREGISTER, durationTime, ws.getUrl(),
                            parseValue(ws.getRawXML(), pars) , response, desc, startTime, errorCode, CallWS.WS_MODE_REAL);
                    KpiCpSoapDao.getInstance().enqueue(kpiCpSoap);
                }
                        
                MyLog.Debug("Call WS " + ws + " -> " + cpRegisterResult);
                if (cpRegisterResult == null || cpRegisterResult.getResult() != CallWS.RESULT_SUCCESS) {
                    cancelOK = false;
                }
            }
        }

        if (cancelOK) {
            tmp.setStatus(DBConst.STATUS_INACTIVE);
            tmp.setCancelTime(System.currentTimeMillis());
            tmp.setDescription(desc);
            SubscriberUpdateDao.getInstance().enqueue(tmp);
            //dua vao queue cho nhanh, hungtv45
            SubLogs subLogs = new SubLogs(sub.getMsisdn(), sub.getSubServiceName(), tranID, "UNREGISTER " + desc,
                        DBConst.RESULT_SUCCESS , System.currentTimeMillis(),stepsCancelSubcriber, channel);
                    SubLogsInserDao.getInstance().enqueue(subLogs);
                    
            result = true;
        } else {
            result = false;
        }
        
        return result;
    }
    
    /**
     * if Command is Check Required, MPS send soap MODE = CHECK to cp
     */
    private ContentResult checkRegisterWS(Command cmd, String msisdn, int action, String transId, String provider, Map<String,String> params) {
        ContentResult checkResult = new ContentResult(ContentResult.RESULT_SUCCESS, "Initial Desc");
        if (!cmd.isCheckRequired()) {
             MyLog.Infor("no check CPWebservice: " + transId);
            return checkResult;
        }
        
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        // Gonna check here
        
        CPWebservice ws = ussdService.getCPWebService(cmd.getWebserviceId());
        if (ws != null) {
            // <editor-fold defaultstate="collapsed" desc="Create Parameter">
            Properties pars = new Properties();
            pars.put(CallWS.WS_USERNAME, ws.getUserName());
            pars.put(CallWS.WS_PASSWORD, ws.getPassword());
            pars.put(CallWS.WS_SERVICE_NAME, cmd.getSubServiceName());
            pars.put(CallWS.WS_MSISDN, msisdn);
            pars.put(CallWS.WS_CHARGE_TIME, PublicLibs.getCurrentDateTime());
            pars.put(CallWS.WS_MODE, CallWS.WS_MODE_CHECK);
            pars.put(CallWS.WS_PARAMS, action + "");
            pars.put(CallWS.WS_AMOUNT, "0"); // amount 0 for test
//                pars.put(CallWS.WS_COMMAND, cmd.getRawSMS()); // Raw SMS
            pars.put(CallWS.WS_TRANS_ID, transId); // Raw SMS
            pars.put(CallWS.WS_CHANEL, cmd.getCommand());
            // </editor-fold>
            if (params != null && !params.isEmpty()){
            for (Map.Entry<String, String> par : params.entrySet()) {
                    String key = par.getKey();
                    String value = par.getValue();
                    pars.put(key, value);
                }
            }

            long startTime = System.currentTimeMillis();
            checkResult = CallWS.cpRegister(ws, pars, GlobalConfig.retryWebservice);

            long durationTime = System.currentTimeMillis() - startTime;
            boolean cpResponseOk = (checkResult != null) && checkResult.getResult() == ContentResult.RESULT_SUCCESS;
            boolean kpiOk = durationTime < GlobalConfig.KPI_CP_TIMEOUT;

            String response = checkResult == null ? "CP Timeout" : checkResult.toString();
            MyLog.Debug("Call WS mode= check: " + ws + " -> " + response);

            if (!kpiOk || !cpResponseOk) {
                String cmdCharge = (action == CallWS.SERVICE_REGISTER) ? CallWS.CMD_REGISTER : CallWS.CMD_UNREGISTER;
                String errorCode = getKpiErrorCode(kpiOk, cpResponseOk, checkResult);
//                    String response = checkResult == null ? "CP Timeout" : checkResult.toString();
                KpiCpSoap kpiCpSoap = new KpiCpSoap(provider, cmd.getSubServiceName(), cmd.getCommand(), transId,
                        msisdn,cmdCharge, durationTime, ws.getUrl(),
                        parseValue(ws.getRawXML(), pars) , response, checkResult.getDesc(), startTime, errorCode, CallWS.WS_MODE_CHECK);
                KpiCpSoapDao.getInstance().enqueue(kpiCpSoap);
            }
            MyLog.Debug("Call WS " + ws + " -> " + checkResult);
        }
        
        return checkResult;
    }
    
    
    /**
     * if Command is Check Required, MPS send soap MODE = CHECK to cp
     */
    private ContentResult sendRequestToCP(int webId, String msisdn, int action, 
            String transId, String provider,
            String subServiceName, String command, String mode,
            Map<String,String> params) {
        ContentResult checkResult = new ContentResult(ContentResult.RESULT_FAILED, "Initial check promotion");
        if (webId < 0) {
             MyLog.Infor("no webId check");
            return checkResult;
        }
        
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        // Gonna check here
        
        CPWebservice ws = ussdService.getCPWebService(webId);
        
        if (ws == null) {
            MyLog.Infor("no webcp to check wsId: " + webId);
            return checkResult;
        }
        
        // <editor-fold defaultstate="collapsed" desc="Create Parameter">
        Properties pars = new Properties();
        pars.put(CallWS.WS_USERNAME, ws.getUserName());
        pars.put(CallWS.WS_PASSWORD, ws.getPassword());
        pars.put(CallWS.WS_SERVICE_NAME, subServiceName);
        pars.put(CallWS.WS_MSISDN, msisdn);
        pars.put(CallWS.WS_CHARGE_TIME, PublicLibs.getCurrentDateTime());
        pars.put(CallWS.WS_MODE, mode);
        pars.put(CallWS.WS_PARAMS, action + "");
        pars.put(CallWS.WS_AMOUNT, "0"); // amount 0 for test
//                pars.put(CallWS.WS_COMMAND, cmd.getRawSMS()); // Raw SMS
        pars.put(CallWS.WS_TRANS_ID, transId); // Raw SMS
        
        // </editor-fold>
        if (params != null && !params.isEmpty()){
        for (Map.Entry<String, String> par : params.entrySet()) {
                String key = par.getKey();
                String value = par.getValue();
                pars.put(key, value);
            }
        }

        long startTime = System.currentTimeMillis();
        checkResult = CallWS.cpRegister(ws, pars, GlobalConfig.retryWebservice);

        long durationTime = System.currentTimeMillis() - startTime;
        boolean cpResponseOk = (checkResult != null) && checkResult.getResult() == ContentResult.RESULT_SUCCESS;
        boolean kpiOk = durationTime < GlobalConfig.KPI_CP_TIMEOUT;

        String response = checkResult == null ? "CP Timeout" : checkResult.toString();
//            MyLog.Debug("Call WS mode= check: " + ws + " -> " + response);

        if (!kpiOk || !cpResponseOk) {
            String cmdCharge = String.valueOf(action);
            String errorCode = getKpiErrorCode(kpiOk, cpResponseOk, checkResult);
            KpiCpSoap kpiCpSoap = new KpiCpSoap(provider, subServiceName, 
                    command, transId,
                    msisdn,cmdCharge, durationTime, ws.getUrl(),
                    parseValue(ws.getRawXML(), pars) , response,
                    command + "|" + mode, startTime, errorCode, mode);
            KpiCpSoapDao.getInstance().enqueue(kpiCpSoap);
        }
        
         MyLog.Infor("Call WS " + transId + ", mode:"+ mode + ", response:" + checkResult 
                + " : (ms) " + durationTime);
        
        return checkResult;
    }

    private static final int RETRY_WS_1_TIMES = 1;
    /**
     *
     * @param cmd
     * @param msisdn
     * @param action
     * @return
     */
    private ContentResult checkOthersWS(Command cmd, String msisdn, int amount,
            CPService service, SubService subService, String transId, Map<String,String> paramsAdd) {
        ContentResult checkResult = new ContentResult(ContentResult.RESULT_SUCCESS, null);
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        // Gonna check here
        if (cmd.isCheckRequired()) {
            CPWebservice ws = ussdService.getCPWebService(cmd.getWebserviceId());
            if (ws != null) {
                // <editor-fold defaultstate="collapsed" desc="Create Parameter">
                Properties pars = new Properties();
                pars.put(CallWS.WS_USERNAME, ws.getUserName());
                pars.put(CallWS.WS_PASSWORD, ws.getPassword());
                pars.put(CallWS.WS_SERVICE_NAME, cmd.getSubServiceName());
                pars.put(CallWS.WS_MSISDN, msisdn);
                pars.put(CallWS.WS_CHARGE_TIME, PublicLibs.getCurrentDateTime());
                pars.put(CallWS.WS_DATETIME, PublicLibs.getCurrentDateTime());
                pars.put(CallWS.WS_MODE, CallWS.WS_MODE_CHECK);
                pars.put(CallWS.WS_AMOUNT, "" + amount); // amount 0 for test
                pars.put(CallWS.WS_TRANS_ID, transId);
                pars.put(CallWS.WS_CHANEL, cmd.getCommand());
                String params = CallWS.parseValue(ws.getParams(), pars);
                pars.put(CallWS.WS_PARAMS, params);
                String sign = createDataSign(ws.getDataSign(), pars, service);
                if (sign != null) {
                    pars.put(CallWS.WS_DATA_SIGN, sign);
                }
                for (Map.Entry<String, String> par : paramsAdd.entrySet()) {
                    String key = par.getKey();
                    String value = par.getValue();
                    pars.put(key, value);
                }
                long startTime = System.currentTimeMillis();
                checkResult = CallWS.cpWebservice(ws, pars, RETRY_WS_1_TIMES);

                long durationTime = System.currentTimeMillis() - startTime;
                boolean cpResponseOk = (checkResult != null) && checkResult.getResult() == ContentResult.RESULT_SUCCESS;
                boolean kpiOk = durationTime < GlobalConfig.KPI_CP_TIMEOUT;
                
                String response = checkResult == null ? "CP Timeout" : checkResult.toString();
                MyLog.Infor("Call WS: " + transId + " -> result: " + response);
                
                if (!kpiOk || !cpResponseOk) {
                    String errorCode = getKpiErrorCode(kpiOk, cpResponseOk, checkResult);
                    
                    KpiCpSoap kpiCpSoap = new KpiCpSoap(service.getProvider(), cmd.getSubServiceName(), cmd.getCommand(), transId,
                            msisdn,cmd.getChargeCommand(), durationTime, ws.getUrl(),
                            parseValue(ws.getRawXML(), pars) , response, checkResult.getDesc(), startTime, errorCode, CallWS.WS_MODE_CHECK);
                    KpiCpSoapDao.getInstance().enqueue(kpiCpSoap);
                }
                
                MyLog.Debug( "Call WS " + ws + " -> " + checkResult);
            }
        }

        return checkResult;
    }
    
    
     /**
     * Check user need to be charged register fee or not
     *
     * @param sub
     * @return
     */
    private boolean needToChargeRegisterFee(Subscriber sub) {
        boolean result = true;
        if (sub != null) {
            if (sub.getStatus() == DBConst.STATUS_ACTIVE) {
                result = false;
            } else {
                if (sub.getNextMonfeeChargeTime() > System.currentTimeMillis()
                        && sub.getChargeStatus() == Subscriber.CHARGE_OK) {
                    MyLog.Infor(" User " + sub.getMsisdn() + " registes ago");
                    result = false;
                }
            }
        }

        return result;
    }
    
    
    private boolean restoreSubscriber(Subscriber sub, String desc, String tranID, String provider, String channel, String stepsRestoreSubscriber) {
        boolean result;
        Subscriber tmp = sub.clone();
        boolean restoreOK = true;
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        SubService subService = ussdService.getSubService(sub.getSubServiceName());

        if (subService.getRegisterWS() > 0) {
            CPWebservice ws = ussdService.getCPWebService(subService.getRegisterWS());
            if (ws != null) {
                // <editor-fold defaultstate="collapsed" desc="Create Parameter">
                Properties pars = new Properties();
                pars.put(CallWS.WS_USERNAME, ws.getUserName());
                pars.put(CallWS.WS_PASSWORD, ws.getPassword());
                pars.put(CallWS.WS_SERVICE_NAME, subService.getSubServiceName());
                pars.put(CallWS.WS_MSISDN, sub.getMsisdn());
                pars.put(CallWS.WS_CHARGE_TIME, PublicLibs.getCurrentDateTime());
                pars.put(CallWS.WS_MODE, CallWS.WS_MODE_REAL);
                pars.put(CallWS.WS_PARAMS, CallWS.SERVICE_REGISTER + "");
                pars.put(CallWS.WS_AMOUNT, "0");
                pars.put(CallWS.WS_COMMAND, "RESTORE"); // Raw SMS
                pars.put(CallWS.WS_TRANS_ID, tranID); // Transaction ID
                pars.put(CallWS.WS_CHANEL, channel);
                // </editor-fold>
                long startTime = System.currentTimeMillis();
                ContentResult cpRegisterResult = CallWS.cpRegister(ws, pars, GlobalConfig.retryWebservice);
                long durationTime = System.currentTimeMillis() - startTime;
                boolean cpResponseOk = (cpRegisterResult != null) && cpRegisterResult.getResult() == ContentResult.RESULT_SUCCESS;
                boolean kpiOk = durationTime < GlobalConfig.KPI_CP_TIMEOUT;

                if (!kpiOk || !cpResponseOk) {
                    String errorCode = getKpiErrorCode(kpiOk, cpResponseOk, cpRegisterResult);
                    String response = cpRegisterResult == null ? "CP Timeout" : cpRegisterResult.toString();
                    KpiCpSoap kpiCpSoap = new KpiCpSoap(sub.getProvider(), sub.getSubServiceName(), channel, tranID,
                            sub.getMsisdn(),CallWS.CMD_REGISTER, durationTime, ws.getUrl(),
                            parseValue(ws.getRawXML(), pars), response, desc, startTime, errorCode, CallWS.WS_MODE_REAL);
                    KpiCpSoapDao.getInstance().enqueue(kpiCpSoap);
                }
                        
                MyLog.Debug( "Call WS " + ws + " -> " + cpRegisterResult);
                if (cpRegisterResult == null || cpRegisterResult.getResult() != CallWS.RESULT_SUCCESS) {
                    restoreOK = false;
                }
            }
        }

        if (restoreOK) {
            SubscriberUpdateDao.getInstance().enqueue(tmp);
            SubLogs subLogs = new SubLogs(sub.getMsisdn(), sub.getSubServiceName(), tranID, "REGISTER - FREE",
                    DBConst.RESULT_SUCCESS , System.currentTimeMillis(),stepsRestoreSubscriber, channel);
            SubLogsInserDao.getInstance().enqueue(subLogs);
            result = true;
        } else {
            result = false;
        }

        return result;
    }
    
    
    private String createDataSign(String dataForm, Properties value, CPService service) {
        String result = "";
        String data = CallWS.parseValue(dataForm, value);
        MyLog.Debug("Data to create sign: " + data);
        if (service.getVTPrivateKey() != null) {
            result = EncryptManager.createMsgSignature(data, service.getVTPrivateKey());
        }
        MyLog.Debug("Sign: " + result);

        return result;
    }    

    @Override
    public BusinessCode checkRegister(String subServiceName, String isdn,Command command) {
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        CPService cpService = ussdService.getCpServiceBySubSerivceName(command.getSubServiceName());
        if (cpService == null) {
            MyLog.Error("register error cpService null");
            return BusinessCode.FAIL;
        }
        PriceCharge priceCharge = ussdService.getPriceCharge(command.getCmdPrice(),command.getMoPrice(), command.getStatus());
        if (priceCharge == null) {
            MyLog.Error("register error priceCharge null");
            return BusinessCode.FAIL;
        }
       return processCheckRegister(isdn, command);
    }
    
    @Override
    public BusinessCode checkRegister(String subServiceName, String isdn) {
        BusinessCode regResultUssd = BusinessCode.NOT_REGISTER;
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        Subscriber sub = ussdService.getSubscriber(isdn, subServiceName);
        if (sub == null || sub.getStatus() == DBConst.STATUS_INACTIVE) {
            regResultUssd = BusinessCode.NOT_REGISTER;
        }
        else{
            regResultUssd = BusinessCode.ALREADY;
        }
        return regResultUssd;
    }
    
//    private void doLog(long startTime, HtmlLog htmlCpInfo) {
//        long endTime = System.currentTimeMillis();
//        String error = htmlCpInfo.getErrorMsg();
//        float duringTime = (endTime - startTime) / 1000;
//        StringBuilder sb = new StringBuilder();
//       
//        sb.append("During : " + duringTime).append("(s)<br/>");
//        if (error != null) {
//            sb.append("Error : " + error + "<br/>");
//        }
//        htmlCpInfo.setErrorMsg(sb.toString());
//
//        if (duringTime > GlobalConfig.KPI_CP_TIMEOUT_MAX) {
//             htmlLogger.log(Level.SEVERE, error, htmlCpInfo);
//        } else {
//            htmlLogger.log(Level.WARNING, error, htmlCpInfo);
//        }
//    }
//    
    @Deprecated
    private String getLogWebservice(CPWebservice webservice, Properties replace) {
        StringBuilder sb = new StringBuilder("URL: ");
        sb.append(webservice.getUrl()).append("<br/>")
                .append("BODY: ");
        String body = parseValue(webservice.getRawXML(), replace);
        body = StringEscapeUtils.escapeXml(body);
        sb.append(body);
        
        return sb.toString();
    }
    
    private String parseValue(String input, Properties replace) {
        if (input == null || input.isEmpty()) {
            return "";
        }
        
        String result = input;
        
        Enumeration<Object> key = replace.keys();
        while (key.hasMoreElements()) {
            String k = (String) key.nextElement();
            String val = replace.getProperty(k);
            result = result.replace(k, val);
        }
        return result;
    }
    
    public void setOracleConnection(OracleConnection oracleConnection) {
        this.mDBConnection = oracleConnection;
    }
    
    public OracleConnection getOracleConnection(){
        return mDBConnection;
    }

    public void setCDRDump(CDRDumper cDRDumper) {
        this.mCDRDumper = cDRDumper;
    }
        
    public CDRDumper getCDRDump() {
         return mCDRDumper;
    }

    /**
     * 
     * Ngoai ra neu da tung dang ky: thi BusinessCode.setData(subscriber), cho
     * nay chi co y nghia voi dich vu chi cho phep 1 goi cuoc
     * @return 
     *  BusinessCode.NOT_REGISTER
     *  BusinessCode.ALREADY
     *  BusinessCode.FAIL
     */
    @Override
    public BusinessCode checkRegisterByServiceName(String serviceName, String isdn) {
        BusinessCode result = BusinessCode.NOT_REGISTER;
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        CPService cpService = ussdService.getCpService(serviceName);
        if (cpService == null) {
            MyLog.Error("unregister error cpService null, " + isdn);
            return BusinessCode.FAIL;
        }
        
        Map<String, SubService> subServices = ussdService.findSubServices(serviceName);
        if (subServices == null) {
            MyLog.Error("could not find subservice, service: " + serviceName);
            return BusinessCode.NOT_REGISTER;
        }
        
        List<Subscriber> subscribers = ussdService.findSubscriberByServiceName(isdn, serviceName);
        
        if (subscribers != null && !subscribers.isEmpty()) {
            for (Subscriber subscriber : subscribers) {
                if (subscriber.getStatus() == DBConst.STATUS_ACTIVE 
                        || subscriber.getStatus() == DBConst.STATUS_PENDING) {
                    result = BusinessCode.ALREADY;
                    result.setData(subscriber);
                    result.setResponse(BusinessCode.ALREADY.getResponse());
                    return result;
                }
            }
        } 
        return result;
    }
    
    /**
     * Neu chua dang ky: BusinessCode.NOT_REGISTER
     * Neu da dang ky: BusinessCode.ALREADY
     * 
     * Ngoai ra neu da tung dang ky: thi BusinessCode.setData(subscriber)
     * @return 
     * BusinessCode.NOT_REGISTER
     * BusinessCode.ALREADY
     * BusinessCode.FAIL
     */
    @Override
    public BusinessCode checkRegisterBySubServiceName(String subServiceName, String isdn) {
        BusinessCode result = BusinessCode.NOT_REGISTER;
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        
        SubService subService = ussdService.getSubService(subServiceName);
        if (subService == null) {
            MyLog.Error("could not find subservice, service: " + subServiceName);
            return BusinessCode.FAIL;
        }
        
        Subscriber subscriber =  ussdService.getSubscriber(isdn, subServiceName);
        
        if (subscriber != null) {
            if (subscriber.getStatus() == DBConst.STATUS_ACTIVE 
                    || subscriber.getStatus() == DBConst.STATUS_PENDING) {
                 result = BusinessCode.ALREADY;
                 result.setData(subscriber);
                 return result;
            }
            result.setData(subscriber);
        }
        
        return result;
    }

    private String getKpiErrorCode(boolean kpiOk, boolean cpResponseOk, ContentResult cpRegisterResult) {
        String response = KpiCpSoap.ERROR_CODE_OK;
        if (!cpResponseOk){
            if (cpRegisterResult == null || cpRegisterResult.getResult() == ContentResult.RESULT_ERROR) {
                return KpiCpSoap.ERROR_CODE_CP_TIMEOUT;
            }
            return response;
        }
        if (!kpiOk) {
             return KpiCpSoap.ERROR_CODE_KPI;
        }
        return response;
    }

    /**
     *
     * @param isdn
     * @param tranID
     * @param params
     * @param serviceName
     * @param subServiceName
     * @param channel
     * @param stepsUnregisterSub
     * @return
     */
        
    @Override
    public BusinessCode unregisterSub(String isdn, String tranID, Map<String, String> params, 
            String subServiceName, String channel, String stepsUnregisterSub) {
        MyLog.Infor(tranID + ",unregister sub-service: " + subServiceName);
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        SubService subService = ussdService.getSubService(subServiceName);
        if (subService == null) {
            MyLog.Error("unregister error subService null");
            return BusinessCode.FAIL;
        }
        
        String serviceName = subService.getServiceName();
        
        CPService cpService = ussdService.getCpService(serviceName);
        if (cpService == null) {
            MyLog.Error("unregister error cpService null");
            return BusinessCode.FAIL;
        }
        
        BusinessCode result = BusinessCode.FAIL;
         
        Command command =  ussdService.getUssdCommand(subServiceName, Command.TYPE_UNREGISTER);
        if (command == null) {
            MyLog.Error("unregister error command null");
            return BusinessCode.FAIL;
        }
        
        if (Command.COMMAND_IVR.equals(channel)) {
            command = ussdService.getIvrCommand(subServiceName, Command.TYPE_UNREGISTER);
        } else if (Command.COMMAND_USSD.equals(channel)) {
            command = ussdService.getUssdCommand(subServiceName, Command.TYPE_UNREGISTER);
        }
        
        if (command == null) {
            result.setDescription(subServiceName);
            return result;
        }
        
        PriceCharge priceCharge = new PriceCharge(command.getCmdPrice(), command.getMoPrice(), command.getStatus());
        result = processUnRegister(isdn, cpService, command, priceCharge, tranID, params, stepsUnregisterSub);
        result.setDescription(subServiceName);
         
        return result;
    }

    /**
     * @author haind25
     * @return 
     * BusinessCode.SUCCESS
     * BusinessCode.FAIL
     * BusinessCode.ALREADY
     * BusinessCode.NOT_REGISTER
     */
    @Override
    public BusinessCode renewBySubServiceName(String msisdn, String subSerivceName,
            String tranID, Map<String,String> params, String channel, String log) {
        long start = System.currentTimeMillis();
        BusinessCode result = BusinessCode.FAIL;
        CachingeService cachingService = ServiceFactory.getBean(CachingeService.class);
        SubService mSubService = cachingService.getSubService(subSerivceName);
       
        if (mSubService == null) {
            result.setDescription("not exist: " + subSerivceName);
            MyLog.Error(tranID + ",renew but sub-service not exist: " + subSerivceName);
            return result;
        }
        
        CPService service = cachingService.getCpService(mSubService.getServiceName());
        
        if (service == null) {
            result.setDescription("not exist service: " + mSubService.getServiceName());
            MyLog.Error(tranID + ",not exist service: " +  mSubService.getServiceName());
            return result;
        }
        
        Subscriber sub = cachingService.getSubscriber(msisdn, subSerivceName);
        
        if (sub == null || Subscriber.STATUS_INACTIVE == sub.getStatus()) {
            MyLog.Error(tranID + ",renew but sub is null");
            return BusinessCode.NOT_REGISTER;
        }
        
        if (Subscriber.STATUS_ACTIVE == sub.getStatus()) {
            MyLog.Error(tranID + ",renew already sub: " + msisdn + "|" + subSerivceName);
            return BusinessCode.ALREADY;
        }
        
        // Gonna charge here
        PriceCharge p = mDBConnection.getMonfeePrice(sub.getMsisdn(), sub.getServiceName(), sub.getSubServiceName());
        int chargeStatus = CallWS.CHARGE_SUCCESS;
        int monfeePrice = 0;
        if (p.getStatus() == PriceCharge.STATUS_OK && p.getCMDPrice() > 0) {
            monfeePrice = p.getCMDPrice();
            // Charge Here
            long startCharge = System.currentTimeMillis();
            chargeStatus = CallWS.chargeMonfeeUser(GlobalConfig.paymentUrl, sub.getMsisdn(),
                    PublicLibs.formatDateTime(sub.getRegisterTime()), monfeePrice,
                    sub.getProvider(), sub.getServiceName(), sub.getSubServiceName(), tranID);
            
            MyLog.Infor(tranID + ", renew MPI response: " + chargeStatus + ", duration (ms):" 
                    + (System.currentTimeMillis() - startCharge));
        } else {
            MyLog.Infor(tranID + ",Invalid price status for " + sub + " " + p);
            if (p.getStatus() != PriceCharge.STATUS_OK) {
                MyLog.Debug(tranID + "-Invalid price status for " + sub + " " + p);
                // Insert Log
                SubLogs subLogs = new SubLogs(sub.getMsisdn(), sub.getSubServiceName(), tranID, "Invalid Fee (" + p.toString() + ")",
                        -2, System.currentTimeMillis());
                SubLogsInserDao.getInstance().enqueue(subLogs);
                MyLog.Infor(tranID + "-Finish Charge for " + sub + " (" + (System.currentTimeMillis() - start) + " ms)");
                result= BusinessCode.FAIL;
                result.setDescription("Invalid price");
                return result;
            }
        }

        if (chargeStatus == CallWS.CHARGE_SUCCESS) {
            // update status
            sub.setStatus(DBConst.STATUS_ACTIVE);
            long addDate = mDBConnection.getAdditonalDateMonfee(sub.getMsisdn(), 
                    sub.getSubType(), sub.getSubServiceName(), sub.getServiceName());
            sub.setLastMonfeeChargeTime(System.currentTimeMillis());
            sub.setNextMonfeeChargeTime(PublicLibs.getFirstMomentOfToday() 
                    + addDate * DBConst.A_DAY_MS);
            sub.setChargeStatus(DBConst.CHARGE_OK);
            // Add success count here
            sub.addMonfeeSuccessCount();
            // Add to Notify Queue
            if (mSubService.isSendNotification()) {
                sub.setMonfeeSuccessCount(0);
                sub.setLastSendNotify(new Timestamp(PublicLibs.getFirstMomentOfToday()));
                // Add to Notification Queue
                MyLog.Debug(tranID + "Add notification for " + sub);

                String content = mSubService.getNotificationContent();
                if (content == null || content.trim().isEmpty()) {
                     MyLog.Error(tranID + ",renew content is null");
                } else {
                    sendMessage(sub.getMsisdn(), content, service, mSubService, sub, p); 
                }
            }
             // Update subscriber
            
            MyLog.Infor("needUpdateDB, sub = " + sub);
            boolean updateDbStatus = mDBConnection.updateSubscriber(sub, 2);
            if (updateDbStatus) {
                result = BusinessCode.SUCCESS;
            }
            
        } else {
            /**
             * khong giong nhu SSE , neu o day charge fail thi ko cap nhat DB
             * lien quan hanh vi khach hang. ho bat lan 1 dang la sub, lan 2 
             * thi khong phai la sub
             */
            if (chargeStatus == CallWS.CHARGE_BALANCE_NOT_ENOUGH ) {
                //neu la trang thai khong du tien
                //thi cap nhat de hien thi thong bao cho phu hop
                result= BusinessCode.NOT_ENOUGH_MONEY;
            }
        }

        SubLogs subLogs = new SubLogs(sub.getMsisdn(), sub.getSubServiceName(),
                tranID, channel + "|MONFEE (" + p.toString() + ")", 
                chargeStatus, System.currentTimeMillis());
        SubLogsInserDao.getInstance().enqueue(subLogs);

        // Need to call WS notification CP                
        if (mSubService.getMonfeeWS() > 0) {
            CPWebservice ws = cachingService.getCPWebService(mSubService.getMonfeeWS());
            int action = (chargeStatus == CallWS.CHARGE_SUCCESS) ?
                    CallWS.CHARGE_SUCCESS : CallWS.CHARGE_FAILED ;
            Properties pars = new Properties();
            pars.put(CallWS.WS_USERNAME, ws.getUserName());
            pars.put(CallWS.WS_PASSWORD, ws.getPassword());
            pars.put(CallWS.WS_SERVICE_NAME, sub.getSubServiceName());
            pars.put(CallWS.WS_MSISDN, sub.getMsisdn());
            pars.put(CallWS.WS_CHARGE_TIME, PublicLibs.getCurrentDateTime());
            pars.put(CallWS.WS_PARAMS, action + "");
            pars.put(CallWS.WS_MODE, CallWS.WS_MODE_REAL + "");
            pars.put(CallWS.WS_AMOUNT, monfeePrice + "");
            pars.put(CallWS.WS_COMMAND, "MONFEE");
            pars.put(CallWS.WS_TRANS_ID, tranID);
            pars.put(CallWS.WS_CHANEL, channel);

            long startTime = System.currentTimeMillis();
            int monfeeResult = CallWS.cpMonFee(ws, pars, 1);

            long durationTime = System.currentTimeMillis() - startTime;
            boolean cpResponseOk = monfeeResult == ContentResult.RESULT_SUCCESS;
            boolean kpiOk = durationTime < GlobalConfig.KPI_CP_TIMEOUT;

            if (!kpiOk || !cpResponseOk) {
                String errorCode = cpResponseOk ? KpiCpSoap.ERROR_CODE_CP_FAIL : KpiCpSoap.ERROR_CODE_CP_TIMEOUT;
                String response = String.valueOf(monfeeResult);
                KpiCpSoap kpiCpSoap = new KpiCpSoap(sub.getProvider(), sub.getSubServiceName(), channel, tranID,
                        sub.getMsisdn(), CallWS.CMD_MONFEE, durationTime, ws.getUrl(),
                        parseValue(ws.getRawXML(), pars), response, "MONFEE", startTime, errorCode, CallWS.WS_MODE_REAL);
                KpiCpSoapDao.getInstance().enqueue(kpiCpSoap);
            }
            MyLog.Infor("Call CP webservice " + ws + ", trans:" + tranID + " -> " + monfeeResult + " , duration (ms): " + durationTime);
        }

        if (getCDRDump() != null) {
            CDR cdr = new CDR();
            cdr.addField(msisdn); // MSISDN
            cdr.addField(""); // Ma thue bao tren BCCS
            cdr.addField(""); // Loai thue bao Tra truoc/tra sau
            cdr.addField(service.getServiceName() + "_" + sub.getSubServiceName()); // Ma dich vu VAS MaHeThong_MaDichVu -> Chua ro
            cdr.addField("RENEW"); // Ma Tac dong -> chua ro
            //1: Dang ky       2: Hủy            3: Gia hạn            4: Kiểm tra
            //5: Hướng dẫn     6: Thêm số        7: Xóa số             8: Hủy gia hạn
            //9: Tải bài hát   10: Xem Video    11: Confirm (Xác nhận) 12: khôi phục

            cdr.addField(channel); // Ma hinh thuc tac dong 
            // 1: SMS, 2: WEB, 3: USSD, 4: WAP, 
            // 5: IVR, 6: He thong xu ly, 7: Websevices, 
            // 8: Nguoi quan tri khai thác, 9: Các kenh tac dong ben ngoai
            cdr.addField(PublicLibs.getCurrentDateTime()); // Thoi gian tac dong
            cdr.addField(msisdn); // 8 So dien thoai tac dong
            cdr.addField(PublicLibs.getCurrentDateTime()); // Thoi gian bat dau hieu luc
            cdr.addField(""); // Thoi gian ket thuc hieu luc
            cdr.addField("" + (chargeStatus == DBConst.RESULT_SUCCESS ? monfeePrice : 0)); // Cuoc
            cdr.addField("" + (chargeStatus == DBConst.RESULT_SUCCESS ? 0 : -1)); // Trang thai tac dong
            cdr.addField(String.valueOf(chargeStatus)); // Ma loi giao dich
            cdr.addField(""); // Nguyen nhan loi
            cdr.addField(PublicLibs.getCurrentDateTime()); // Thoi gian tra ve
            cdr.addField(tranID); // Party code
            cdr.addField(""); // Thong tin du tru 1 -> Luu so luong MT cua tin nhan
            cdr.addField(""); // Thong tin du tru 2 -> Luu Username da ban tin
            cdr.addField(""); // Thong tin du tru 3
            cdr.addField(""); // Thong tin du tru 4
            cdr.addField(""); // Thong tin du tru 5
            cdr.addField(""); // Thong tin du tru 6
            cdr.addField(""); // Thong tin du tru 7
            cdr.addField(""); // Thong tin du tru 8
            cdr.addField(""); // Thong tin du tru 9
            cdr.addField(""); // Thong tin du tru 10

            getCDRDump().addCDR(cdr);
        }
        MyLog.Infor("Finish renew, trans:" + tranID 
                    + ", msisdn:" + msisdn  
                    + ", sub-service:" + sub.getSubServiceName()
                    + ", result:" + result.getResponse()
                    + " , duration (ms): " + (System.currentTimeMillis() - start));
        return result;
    }
    
    private void sendMessage(String msisdn, String content, CPService service, 
            SubService subService, Subscriber sub,PriceCharge p) {
        Properties smsParams = new Properties();
        String softName = subService.getDescription();
        if (softName == null) {
            softName = subService.getSoftName();
        }

        smsParams.put(SUB_NAME, softName);
        smsParams.put(SERVICE_NAME, sub.getServiceName());
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        smsParams.put(SUB_START_DATE, format.format(new Date(sub.getRegisterTime())));
        smsParams.put(SUB_END_DATE, format.format(new Date(sub.getNextMonfeeChargeTime())));
        smsParams.put(CMD_PRICE, (p.getCMDPrice() / DBConst.MONEY_RATE) + "");

        content = CallWS.parseValue(content, smsParams);
        String smsUrl = service.getSMSGWUrl();
        String smsUsername = service.getSMSGWUser();
        String smsPassword = service.getSMSGWPass();
        String smsShortcode = service.getShortCode();

//        String url, String username, String password, 
//                       String msisdn, String content, String shortCode, String alias, String smsType, int retry
        
        CallWS.sendMT(smsUrl, smsUsername, smsPassword,
                msisdn, content, smsShortcode, smsShortcode, "HEX_TEXT", 1);
    }
    
    /**
     * 
     */
    @Override
    public BusinessCode renewByServiceName(String msisdn, String serviceName, 
            String tranID, Map<String,String> params, String channel, String log) {
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        CPService cpService = ussdService.getCpService(serviceName);
        if (cpService == null) {
            MyLog.Error(tranID + ",Abort RENEW " + msisdn + 
                        ", service not found: " + serviceName);
            return BusinessCode.FAIL;
        }
        
        Map<String, SubService> subServices = ussdService.findSubServices(serviceName);
        if (subServices == null) {
            MyLog.Error(tranID + ",Abort RENEW " + msisdn + 
                        ", not found sub-service of sevice: " + serviceName);
            return BusinessCode.FAIL;
        }
        
        BusinessCode result = BusinessCode.FAIL;
        int i = 0;
         
        // get subservice which isdn was register, include active, in_active and pending
        List<Subscriber> subscribers = ussdService.findSubscriberByServiceName(msisdn, serviceName);
        
        if (subscribers == null || subscribers.isEmpty()) {
            MyLog.Error(tranID + ",Abort RENEW " + msisdn + 
                        ", not register: " + serviceName);
            return BusinessCode.NOT_REGISTER;
        }
        
        /**
         * CHECK REGISTER
         * neu da gia han thanh cong 1 trong cac goi cuoc cua dich vu
         * thi coi nhu da gia han, khong cho phep gia han tiep theo
         */
        for (Subscriber subscriber : subscribers) {
            if (subscriber.getStatus() == Subscriber.STATUS_ACTIVE) {
                MyLog.Infor(tranID + ",Abort RENEW " + msisdn + 
                        " is sub of" + subscriber.getSubServiceName());
                return BusinessCode.ALREADY;
            }
        }
        
        /*
            CHECK PENDING
            renew all sub-service of service are pending
        */
        boolean isPending = false;
        for (Subscriber subscriber : subscribers) {
            if (subscriber.getStatus() != DBConst.STATUS_PENDING) {
                //khong phai trang thai pending thi khong duoc gia han
                continue;
            }

            isPending = true;
            MyLog.Infor(tranID + ",RENEW " + msisdn + 
                        "|" + subscriber.getSubServiceName());
            
            BusinessCode resultSubService = renewBySubServiceName(msisdn, 
                    subscriber.getSubServiceName(), tranID, params, channel, log);
            if (i == 0) {
                //default get first process
                result = resultSubService;
                result.setDescription(subscriber.getSubServiceName());
            }
            i++;

            if (BusinessCode.SUCCESS.getValue().equals(resultSubService.getValue())) {
                result = resultSubService;
                result.setDescription(subscriber.getSubServiceName());
            }
        }
        
        /*
            IF PENDING -> RETURN
            if isdn is one sub of Subservice and is pending -> return;
        */ 
        if (isPending) {
            return result;
        }
        
        /*
        * neu da trang thai da ACTIVE hoac PENDING thi dang RENEW o tren
        * con lai deu la INACTIVE, tra ve ma NOT_RGISTER
        */
        MyLog.Error(tranID + ",Abort RENEW " + msisdn + 
                        ", not register: " + serviceName);
        return BusinessCode.NOT_REGISTER;
    }
    
    @Override
    public BusinessCode cancelSilentBySubservice(String msisdn, String subSerivceName,
            String tranID, Map<String,String> params, String channel, String log) {
        MyLog.Infor("START cancle silent by sub-service, trans:" + tranID 
                    + ", msisdn:" + msisdn  
                    + ", channel:" + channel
                    + ", sub-service:" + subSerivceName
                   );
        long start = System.currentTimeMillis();
        BusinessCode result = BusinessCode.FAIL;
        CachingeService cachingService = ServiceFactory.getBean(CachingeService.class);
        SubService mSubService = cachingService.getSubService(subSerivceName);
       
        if (mSubService == null) {
            result.setDescription("not exist sub-service: " + subSerivceName);
            MyLog.Error("ABORT cancel, sub-service not exist: " + subSerivceName + ", trans_id: " + tranID );
            return result;
        }
        
        CPService service = cachingService.getCpService(mSubService.getServiceName());
        
        if (service == null) {
            result.setDescription("not exist service: " + mSubService.getServiceName());
            MyLog.Error("ABORT cancel,service not exist: " +  mSubService.getServiceName() + ", trans_id: " + tranID );
            return result;
        }
        
        Subscriber sub = cachingService.getSubscriber(msisdn, subSerivceName);
        
        if (sub == null || Subscriber.STATUS_INACTIVE == sub.getStatus()) {
            MyLog.Infor("ABORT cancel, not sub: " + msisdn + ", sub-service: " + subSerivceName);
            return BusinessCode.NOT_REGISTER;
        }
        
        // update status
        sub.setStatus(Subscriber.STATUS_INACTIVE);
        sub.setCancelTime(System.currentTimeMillis());
        sub.setChargeStatus(DBConst.CHARGE_FAILED);
        sub.setDescription("AUTO_CANCEL|" + channel);
        // Add success count here
        // Update subscriber
            
        MyLog.Infor("UPDATE sub table:" + sub);
        boolean updateDbStatus = mDBConnection.updateSubscriber(sub, 2);
        if (updateDbStatus) {
            result = BusinessCode.SUCCESS;
        }
        
        // call ws to CP for disable service
        // Send Unregister command to MPI
        int cancelResult = CallWS.chargeUser(GlobalConfig.paymentUrl, sub.getMsisdn(),
                CallWS.CMD_UNREGISTER, 0, sub.getProvider(), sub.getServiceName(),
                sub.getSubServiceName(), tranID, CallWS.CHANNEL_OCS);
        MyLog.Infor(tranID + " Send CANCEL command to MPI -> " + cancelResult);
        
        // Add transaction to DB
        SubLogs subLogs = new SubLogs(sub.getMsisdn(), sub.getSubServiceName(),
                tranID, channel + " (CANCEL)", 
                CallWS.CHARGE_SUB_NOT_EXIST, System.currentTimeMillis());
        subLogs.setTraceStep(log);
        subLogs.setChannel(channel);
        
        SubLogsInserDao.getInstance().enqueue(subLogs);

        // cancel service
        if (mSubService.getCancelWS() > 0) {
            CPWebservice ws = cachingService.getCPWebService(mSubService.getCancelWS());
            
            // <editor-fold defaultstate="collapsed" desc="Create Parameter">
            if (ws != null) {
                Properties pars = new Properties();
                pars.put(CallWS.WS_USERNAME, ws.getUserName()==null ? "" : ws.getUserName());
                pars.put(CallWS.WS_PASSWORD, ws.getPassword()==null ? "" : ws.getPassword());
                pars.put(CallWS.WS_SERVICE_NAME, sub.getSubServiceName());
                pars.put(CallWS.WS_MSISDN, sub.getMsisdn());
                pars.put(CallWS.WS_CHARGE_TIME, PublicLibs.getCurrentDateTime());
                pars.put(CallWS.WS_MODE, CallWS.WS_MODE_REAL);
                pars.put(CallWS.WS_PARAMS, CallWS.SERVICE_UNREGISTER + "");
                pars.put(CallWS.WS_AMOUNT, "0");
                pars.put(CallWS.WS_COMMAND, "AUTO_CANCEL|" + channel);
                pars.put(CallWS.WS_TRANS_ID, tranID);
                pars.put(CallWS.WS_CHANEL, channel);
                if (params != null && !params.isEmpty()) {
                    for (Map.Entry<String, String> entry : params.entrySet()) {
                        String key = entry.getKey();
                        String value = entry.getValue();
                        pars.put(key, value);
                    }
                }
                // </editor-fold>
                long startTime = System.currentTimeMillis();
                ContentResult cpRegisterResult = CallWS.cpRegister(ws, pars, 1);

                long durationTime = System.currentTimeMillis() - startTime;
                boolean cpResponseOk = (cpRegisterResult != null) && (cpRegisterResult.getResult() == ContentResult.RESULT_SUCCESS);
                boolean kpiOk = durationTime < GlobalConfig.KPI_CP_TIMEOUT;

                String errorCode = getKpiErrorCode(kpiOk, cpResponseOk, cpRegisterResult);
                String response = String.valueOf(cpRegisterResult);
                KpiCpSoap kpiCpSoap = new KpiCpSoap(sub.getProvider(), sub.getSubServiceName(), Command.COMMAND_RENEW, tranID,
                        sub.getMsisdn(), CallWS.CMD_UNREGISTER, durationTime, ws.getUrl(),
                        parseValue(ws.getRawXML(), pars), response, "AUTO_CANCEL|" + channel, startTime, errorCode, CallWS.WS_MODE_REAL);
                KpiCpSoapDao.getInstance().enqueue(kpiCpSoap);

                MyLog.Infor("Call WS " + ws.getUrl()
                        + " for CANCEL " + mSubService.getServiceName()
                        + ", msisdn: " + sub.getMsisdn() + " -> " + cpRegisterResult + ", duruation (ms): " + durationTime);
            }
        } else {
            MyLog.Infor("Cancel sub-service: "+ mSubService.getSubServiceName() 
                    + " , for: " + sub.getMsisdn() 
                    + ", but not send to cp, websevice not found");
        }

        if (getCDRDump() != null) {
            CDR cdr = new CDR();
            cdr.addField(msisdn); // MSISDN
            cdr.addField(""); // Ma thue bao tren BCCS
            cdr.addField(""); // Loai thue bao Tra truoc/tra sau
            cdr.addField(service.getServiceName() + "_" + sub.getSubServiceName()); // Ma dich vu VAS MaHeThong_MaDichVu -> Chua ro
            cdr.addField("OCSSUBCHANGE"); // Ma Tac dong -> chua ro
            //1: Dang ky       2: Hủy            3: Gia hạn            4: Kiểm tra
            //5: Hướng dẫn     6: Thêm số        7: Xóa số             8: Hủy gia hạn
            //9: Tải bài hát   10: Xem Video    11: Confirm (Xác nhận) 12: khôi phục

            cdr.addField(channel); // Ma hinh thuc tac dong 
            // 1: SMS, 2: WEB, 3: USSD, 4: WAP, 
            // 5: IVR, 6: He thong xu ly, 7: Websevices, 
            // 8: Nguoi quan tri khai thác, 9: Các kenh tac dong ben ngoai
            cdr.addField(PublicLibs.getCurrentDateTime()); // Thoi gian tac dong
            cdr.addField(msisdn); // 8 So dien thoai tac dong
            cdr.addField(PublicLibs.getCurrentDateTime()); // Thoi gian bat dau hieu luc
            cdr.addField(""); // Thoi gian ket thuc hieu luc
            cdr.addField("0"); // Cuoc
            cdr.addField("-1" ); // Trang thai tac dong
            cdr.addField("401"); // Ma loi giao dich
            cdr.addField("OCS_SUB_CHANGE"); // Nguyen nhan loi
            cdr.addField(PublicLibs.getCurrentDateTime()); // Thoi gian tra ve
            cdr.addField(tranID); // Party code
            cdr.addField(""); // Thong tin du tru 1 -> Luu so luong MT cua tin nhan
            cdr.addField(""); // Thong tin du tru 2 -> Luu Username da ban tin
            cdr.addField(""); // Thong tin du tru 3
            cdr.addField(""); // Thong tin du tru 4
            cdr.addField(""); // Thong tin du tru 5
            cdr.addField(""); // Thong tin du tru 6
            cdr.addField(""); // Thong tin du tru 7
            cdr.addField(""); // Thong tin du tru 8
            cdr.addField(""); // Thong tin du tru 9
            cdr.addField(""); // Thong tin du tru 10

            getCDRDump().addCDR(cdr);
        }
        MyLog.Infor("Finish cancel sub, trans:" + tranID 
                    + ", msisdn:" + msisdn  
                    + ", sub-service:" + sub.getSubServiceName()
                    + ", result:" + result.getResponse()
                    + " , duration (ms): " + (System.currentTimeMillis() - start));
        return result;
    }
    
    @Override
    public BusinessCode cancelSilent(String msisdn, 
            String tranID, Map<String,String> params, String channel,
            String log, Date registerBeforeTime) {
        long start = System.currentTimeMillis();
        MyLog.Infor("START cancle silent all service by msisdn, trans:" + tranID 
                    + ", msisdn:" + msisdn  
                    + ", channel:" + channel
                    + " , subscriber before: " + registerBeforeTime);
        CachingeService cachingService = ServiceFactory.getBean(CachingeService.class);
        BusinessCode result = BusinessCode.FAIL;
       
        // get subservice which isdn was register, include active, in_active and pending
        List<Subscriber> subscribers = cachingService.findSubscriberByMsisdn(msisdn);
        
        if (subscribers == null || subscribers.isEmpty()) {
            MyLog.Infor("END cancel " + msisdn + 
                        ", not register any service, list sub empty, transId: " + tranID);
            return BusinessCode.NOT_REGISTER;
        }
        
        boolean isRegister = false;
        int i = 0;
        for (Subscriber subscriber : subscribers) {
            if (Subscriber.STATUS_ACTIVE != subscriber.getStatus() 
                    && Subscriber.STATUS_PENDING != subscriber.getStatus() ) {
                continue;
            }
            
            if (registerBeforeTime != null 
                    && subscriber.getRegisterTime() > registerBeforeTime.getTime()) {
                MyLog.Infor(tranID 
                        + ",SUBSCRIBER but not cancel, register time < change time: " 
                        + msisdn + 
                        " is sub of: " + subscriber.getSubServiceName());
                continue;
            }
            
            MyLog.Infor(tranID + ",CANCLE " + msisdn + 
                        "|" + subscriber.getSubServiceName());
            isRegister = true;
            
            BusinessCode resultSubService = cancelSilentBySubservice(msisdn, 
                    subscriber.getSubServiceName(), tranID, params, channel, log);
            if (i == 0) {
                //default get first process
                result = resultSubService;
                result.setData(subscriber);
            }
            i++;

            if (BusinessCode.SUCCESS.getValue().equals(resultSubService.getValue())) {
                result = resultSubService;
                result.setData(subscriber);
            }
        }
        
        if (!isRegister) {
            MyLog.Infor("END cancel silent:" + msisdn + 
                        ", not register any service, inactive all sub, transId: " + tranID);
            return BusinessCode.NOT_REGISTER;
        }
        
        MyLog.Infor("END cancel silent, trans:" + tranID 
                    + ", msisdn:" + msisdn  
                    + ", result:" + result.getResponse()
                    + ", duration (ms): " + (System.currentTimeMillis() - start));
        return result;
    }
    
}
