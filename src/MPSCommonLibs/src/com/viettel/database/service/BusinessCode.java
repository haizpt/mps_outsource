/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.service;

/**
 *
 * @author haind25
 */
public enum BusinessCode {

    SUCCESS("SUCCESS"),
    FAIL("FAIL"), CONFLICT("CONFLICT"), ALREADY("ALREADY"),
    NOT_REGISTER("NOT_REGISTER"), NOT_ENOUGH_MONEY("NOT_ENOUGH_MONEY"), SUCCESS_REQUIRE_MPS_ACCOUNT("SUCCESS_REQUIRE_MPS_ACCOUNT");

    private final String value;
    private String description;
    private String response;

    private Object data;

    private BusinessCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }
}
