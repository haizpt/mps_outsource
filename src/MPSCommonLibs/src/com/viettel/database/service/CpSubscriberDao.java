/**
 *
 * handsome boy
 */

package com.viettel.database.service;

import com.viettel.database.datatype.CpSubscirber;
import java.util.List;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Dec 9, 2015
 * @mail haind25@viettel.com.vn
 */
public interface CpSubscriberDao {
    
    CpSubscirber find(String subServiceName, String isdn);
    List<CpSubscirber> find(String isdn);
    
    /**
     * @return include inactive, pending and active sub
     */
    List<CpSubscirber> find(List<String> subServiceNames, String isdn);
}
