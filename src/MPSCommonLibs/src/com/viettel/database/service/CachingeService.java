/**
 *
 * handsome boy
 */

package com.viettel.database.service;

import com.viettel.database.datatype.CPService;
import com.viettel.database.datatype.CPWebservice;
import com.viettel.database.datatype.IvrCommand;
import com.viettel.database.datatype.MOCommand;
import com.viettel.database.datatype.PriceCharge;
import com.viettel.database.datatype.SubService;
import com.viettel.database.datatype.Subscriber;
import com.viettel.database.datatype.UssdCommand;
import java.util.List;
import java.util.Map;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Nov 24, 2015
 * @mail haind25@viettel.com.vn
 */
public interface CachingeService {
    UssdCommand getUssdCommand(String subServiceName, int type);
    
    SubService getSubService(String subServiceName);
    
    CPService getCpService(String serviceName);
    
    CPService getCpServiceBySubSerivceName(String subServiceName);
    
    PriceCharge getPriceCharge(int cmdPrice, int moPrice, int status);
    
    MOCommand getMOCommand(String subServiceName,int type);
    
    IvrCommand getIvrCommand(String subServiceName,int type);
    
    Subscriber getSubscriber(String msisdn, String subServiceName);

    public CPWebservice getCPWebService(int webserviceId);
    
    public CPWebservice getCPWebService(String webserviceName);
    
    public Subscriber checkConflict(String msisdn, String service, String currentSubService) ;
    
     /**
     * @param serviceName
      * @return 
     * key: subServcieName
     * value: subService
     */
    public Map<String, SubService> findSubServices(String serviceName);
    
    List<Subscriber> findSubscriberByServiceName(String msisdn, String serviceName);
    
    List<Subscriber> findSubscriberByMsisdn(String msisdn);
    
}

