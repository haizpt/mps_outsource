/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.database.oracle;

import com.viettel.utilities.MyLog;
import com.viettel.utilities.PublicLibs;
import com.timesten.jdbc.TimesTenDataSource;
import java.sql.PreparedStatement;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author thangpm4
 */
public class TimestenConnection {
    private Connection mConn = null;
//    private String host;
//    private String port;
    private String dsn;
    private String username;
    private String password;
    private String orclPwd;
    private int mId;
    
    /**
     * Create a db importer
     * @param host host
     * @param port port
     * @param sid sid
     * @param username username
     * @param password password
     */
    public TimestenConnection(int id, /*String host, String port,*/
        String dsn, String username, String password, String orclPwd)
    {
//        this.host = host;
//        this.port = port;
        this.dsn = dsn;
        this.username = username;
        this.password = password;
        this.orclPwd = orclPwd;

        mId = id;
    }

    /**
     * connect to the database
     * @return connection
     * @throws SQLException
     */
    public void connect() throws SQLException
    {
        DriverManager.registerDriver(
            new oracle.jdbc.driver.OracleDriver());
        String url = "jdbc:timesten:client:" + dsn;
//        String url = "jdbc:timesten:direct:" + dsn;
        TimesTenDataSource ds = new TimesTenDataSource();
        ds.setUrl(url);
        ds.setUser(username);
        ds.setOraclePassword(orclPwd);
        ds.setPassword(password);
        mConn = ds.getConnection();
//        return mConn;
    }

    public Connection getTimestenConnection() {
        return mConn;
    }
    
    public CallableStatement getTimestenStatement(String strSql) {
        try {
            return mConn.prepareCall(strSql);
        } catch (Exception ex) {
            MyLog.Error("TS Thread " + mId + ": Exception insert MSISDN: " + ex.getMessage());
            MyLog.Error(ex);
            if (!isConnected(mConn)) {
                retry();
            }
            
            return null;
        }
    }
    /**
     * Init DB importer
     */
    public void init()
    {
        try {
//            scheduleAtFixedRate(1000);
            connect();
        } catch (SQLException ex)
        {
            MyLog.Error("TS Thread " + mId + ": Init Timesten error: " + ex.toString());
        }
    }

    /**
     * Retry connection to database
     */
    public void retry() {
        try { mConn.close(); }
        catch (Exception ex) {}

//        if (!isConnected(conn))
        {
            try
            {
                MyLog.Infor("TS Thread " + mId + ": Losted connection to the database,Wait 5 secs before reconnecting.");
                Thread.sleep(5000);
                MyLog.Infor("TS Thread " + mId + ": reconnecting...");
                connect();
                MyLog.Infor("TS Thread " + mId + ": done.");
            } catch (Exception ex)
            {
                MyLog.Error("TS Thread " + mId + ": reconnect error: " + ex.getMessage());
                /*NGPLog.Error("TS Thread " + mId + ": wait 30s before retry.");
                try { Thread.sleep(30000);
                } catch (Exception ep) {}*/
            }
        }
    }

    public void process()
    {
        retry();
    }

    /**
     * Check if the connection is alive
     * @param conn Connection
     * @return boolean
     */
    protected boolean isConnected(Connection conn)
    {
        if (conn == null)
        {
            return false;
        }

        boolean connected = false;
        PreparedStatement stmt = null;

        try
        {
            stmt = conn.prepareStatement("select 1 from dual");
            connected = stmt.execute();
            //stmt.close();
        } catch (Exception ex)
        {
            MyLog.Error("TS Thread " + mId + ": TimestenImporter.isConnected: " + ex.getMessage());
        } finally {
            try { stmt.close(); } catch (Exception ex) {}
        }

        return connected;
    }
}
