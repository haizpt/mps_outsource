/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.oracle;


import com.viettel.database.datatype.KpiCpSoap;
import com.viettel.utilities.MyLog;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author haind25
 * 
 * 
 *  CREATE TABLE KPI_CP_SOAP 
(
  ID VARCHAR2(256) NOT NULL 
, PROVIDER_NAME VARCHAR2(50) 
, SUB_SERVICE_NAME VARCHAR2(100) 
, CHANNEL VARCHAR2(20) 
, TRANS_ID VARCHAR2(256) 
, MSISDN VARCHAR2(20) 
, ACTION VARCHAR2(30) 
, DURATION INT 
, URL VARCHAR2(500) 
, BODY VARCHAR2(1000) ,
, RESPONSE VARCHAR2(1000) 
, SENT_TIME VARCHAR2(20) 
, CREATED TIMESTAMP 
, DESCRIPTION VARCHAR2(500) 
, WS_MODE VARCHAR2(20) 
, CONSTRAINT KPI_CP_SOAP_PK PRIMARY KEY 
  (
    ID 
  )
  ENABLE 
)PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
  TABLESPACE "DATA" 
  PARTITION BY RANGE (CREATED) 
  INTERVAL(NUMTOYMINTERVAL(1, 'MONTH')) 
 (PARTITION pos_data_p2 VALUES LESS THAN (TO_DATE('1-03-2016', 'DD-MM-YYYY'))  TABLESPACE "DATA" )
;
* 
* CREATE INDEX KPI_CP_SOAP_provider_name ON KPI_CP_SOAP (provider_name) LOCAL;
CREATE INDEX KPI_CP_SOAP_trans_id ON KPI_CP_SOAP (trans_id) LOCAL;
create index MPS.KPI_CP_SOAP_trans_id_SUB_SERVICE_NAME_MSISDN on MPS.KPI_CP_SOAP("SUB_SERVICE_NAME","MSISDN") LOCAL;
* 
COMMENT ON COLUMN KPI_CP_SOAP.CHANNEL IS 'USSD, RENEW, IVR, SMS, WEB|WAP';
COMMENT ON COLUMN KPI_CP_SOAP.ACTION IS 'REGISTER,CANCEL, FORWARD, RENEW';
COMMENT ON COLUMN KPI_CP_SOAP.DURATION IS 'milisecond';
 */
public class KpiCpSoapDao implements Runnable{
    /**
     * thong thuong batch size se phai lon hon TPS (mac dinh TPS = 200/node)
     * day la ung dung lon nen de batch size an toan la 500 ~ max TPS
     * nen sau moi lan xu ly he thong se nghi: dua tren feedback phan hoi. max TPS/size of data
     */
    
    private final static int BATCH_SIZE = 500;
    private final static int DEFAULT_SLEEP = 5000;
    
    private final static String SQL_PRO_INSERT = "INSERT INTO KPI_CP_SOAP (" +
                                                " ID, " +
                                                " PROVIDER_NAME," +
                                                " SUB_SERVICE_NAME," +
                                                " CHANNEL," +
                                                " TRANS_ID," +
                                                " MSISDN," +
                                                " ACTION," +
                                                " DURATION," +
                                                " URL," +
                                                " BODY," +
                                                " RESPONSE," +
                                                " REQUEST_TIME," +
                                                " DESCRIPTION," +
                                                " ERROR_CODE," +
                                                " WS_MODE," +
                                                " CREATED" +
                                                " ) VALUES(?,?,?,?,?,?,?,?,?,?,?,?, ?,?,?,sysdate)";
    
    private final LinkedList<KpiCpSoap> queue = new LinkedList<KpiCpSoap>();
    private final Object obj = new Object();
    private DataSource dataSource = null;
    
    private DatabaseConnectionPool databaseConnectionPool = null;
    
    private static KpiCpSoapDao _instance = null;
    
    private int batchSize = BATCH_SIZE;
    
    private KpiCpSoapDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    
    public static KpiCpSoapDao config(DataSource dataSource) {
        if (_instance == null) {
            _instance = new KpiCpSoapDao(dataSource);
            Thread t = new Thread(_instance);
            t.setName("thread-" + KpiCpSoapDao.class.getSimpleName());
            t.start();
        }
        
        return _instance;
    }
    
    private KpiCpSoapDao(DatabaseConnectionPool dataSource) {
        this.databaseConnectionPool = dataSource;
    }

    
    public static KpiCpSoapDao config(DatabaseConnectionPool dataSource) {
        if (_instance == null) {
            _instance = new KpiCpSoapDao(dataSource);
            Thread t = new Thread(_instance);
            t.setName("thread-" + KpiCpSoapDao.class.getSimpleName());
            t.start();
        }
        
        return _instance;
    }

    public int getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }
    
    public static KpiCpSoapDao getInstance() {
        return _instance;
    }
   
    public void enqueue(KpiCpSoap transaction) {
        synchronized (obj) {
            queue.add(transaction);
            MyLog.Infor("enqueue queue: " + transaction);
            MyLog.Infor("size of queue: " + queue.size() );
        }
    }

    private List<KpiCpSoap> dequeue() {
        synchronized (obj) {
            if (queue.isEmpty()) {
                return null;
            }
            List<KpiCpSoap> datas = new ArrayList<KpiCpSoap>(batchSize);
            int i = 0;
            while (queue.size() > 0 && (i < batchSize)) {
                KpiCpSoap transaction = queue.poll();
                datas.add(transaction);
                i++;
            }

            return datas;
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                List<KpiCpSoap> datas = dequeue();
                if (datas == null) {
                     Thread.sleep(DEFAULT_SLEEP);
                     continue;
                }
                
                process(datas);
                int length = datas.size();
                MyLog.Infor("KpiCpSoapInserDao processed bacth queue = " + length);
                datas.clear();
                datas = null;

                int sleep = 1000 * batchSize / length; //m s
                if (sleep > DEFAULT_SLEEP) {
                    sleep = DEFAULT_SLEEP;
                }
                Thread.sleep(sleep);
            } catch (Exception ex) {
                MyLog.Error("KpiCpSoapInserDao.java");
                MyLog.Error(ex);
            }
        }
    }
     
    public Connection getConnection() throws SQLException {
        Connection connection = null;
        if (dataSource != null) {
            connection = dataSource.getConnection();
        }
        
        if (connection == null && databaseConnectionPool != null) {
            connection = databaseConnectionPool.getConnection().getDatabaseConnection();
        }
        
        if (connection == null) {
            MyLog.Error("error can not getConnection");  
        }
        
        return connection;
    }
    
    private void closeConnection(Connection conn) throws SQLException{
        if (dataSource != null) {
            conn.close();
        }
    }
     
    private void process(List<KpiCpSoap> datas) throws SQLException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = getConnection();
            ps = connection.prepareStatement(SQL_PRO_INSERT);
            ps.setQueryTimeout(5);//second
            int i = 0;
            for (KpiCpSoap data : datas) {
                i = 0;
                ps.setString(++i, data.getId());
                ps.setString(++i, data.getProviderName());
                ps.setString(++i, data.getSubServiceName());
                ps.setString(++i, data.getChannel());
                
                ps.setString(++i, data.getTransactionId());
                ps.setString(++i, data.getMsisdn());
                ps.setString(++i, data.getAction());
                ps.setLong(++i, data.getDuration());
                
                ps.setString(++i, data.getUrl());
                ps.setString(++i, data.getBody());
                ps.setString(++i, data.getResponse());
                ps.setTimestamp(++i, data.getRequestTime());
                
                ps.setString(++i, data.getDescription());
                ps.setString(++i, data.getErrorCode());
                ps.setString(++i, data.getMode());
                
                ps.addBatch();
            }

            ps.executeBatch();
            connection.commit();
//            ps = null;
           
        } catch (Exception ex) {
            MyLog.Error("error insert in class KpiCpSoapDao");
            MyLog.Error(ex);            
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                closeConnection(connection);
            } catch (Exception ee) {
            }
        }
    }

   
}

