/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.oracle;


import com.viettel.database.datatype.Subscriber;
import com.viettel.utilities.MyLog;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author hungtv45
 */
public class SubscriberUpdateDao implements Runnable{
     /**
     * thong thuong batch size se phai lon hon TPS (mac dinh TPS = 200/node)
     * day la ung dung lon nen de batch size an toan la 500 ~ max TPS
     * nen sau moi lan xu ly he thong se nghi: dua tren feedback phan hoi. max TPS/size of data
     */
    
    private final static int BATCH_SIZE = 500;
    private final static int DEFAULT_SLEEP = 2000;
    
    private final static String SQL_PRO_UPDATE = "UPDATE SERVICES_SUBSCRIPTIONS " +
                                                " SET STATUS=?," +
                                                " REGISTER_TIME=?," +
                                                " CANCEL_TIME=?," +
                                                " DESCRIPTIONS=?," +
                                                " LAST_MONFEE_CHARGE_TIME=?," +
                                                " NEXT_MONFEE_CHARGE_TIME=?," +
                                                " CHARGE_STATUS=?," +
                                                " IS_PROMOTION=?," +
                                                " MONFEE_SUCCESS_COUNT=?," +
                                                " LAST_SEND_NOTIFY=?" +
                                                " WHERE SUB_SERVICE_NAME=? and MSISDN=? ";
    
    private final LinkedList<Subscriber> queue = new LinkedList<Subscriber>();
    private final Object obj = new Object();
    
    private DataSource dataSource = null;
    
    private int batchSize = BATCH_SIZE;
    
    private static SubscriberUpdateDao _instance = null;
    
    private SubscriberUpdateDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static SubscriberUpdateDao config(DataSource dataSource) {
        if (_instance == null) {
            _instance = new SubscriberUpdateDao(dataSource);
            Thread t = new Thread(_instance);
            t.setName("thread-" + SubscriberUpdateDao.class.getSimpleName());
            t.start();
        }
        
        return _instance;
    }
    
    
    public static SubscriberUpdateDao getInstance() {
        return _instance;
    }

    public int getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }
   
    public void enqueue(Subscriber transaction) {
        synchronized (obj) {
            queue.add(transaction);
            MyLog.Infor("enqueue queue update sub: " + transaction);
            MyLog.Infor("size of update queue: " + queue.size() );
        }
    }

    private  List<Subscriber> dequeue() {
        synchronized (obj) {
            if (queue.isEmpty()) {
                return null;
            }
            List<Subscriber> datas = new LinkedList<Subscriber>();
            int i = 0;
            while (queue.size() > 0 && (i < batchSize)) {
                Subscriber transaction = queue.poll();
                datas.add(transaction);
                i++;
            }

            return datas;
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                List<Subscriber> datas = dequeue();
                if (datas != null) {
                    try {
                        process(datas);
                    } catch (Exception ex) {
                        MyLog.Error("SubscriberUpdateDao = " + ex);
                    }
                    int length = datas.size();
                    MyLog.Debug("SubscriberUpdateDao processed queue = " + length);
                    datas.clear();
                    datas = null;
                   
                        int sleep = 1000 * batchSize / length; // ms
                        if (sleep > DEFAULT_SLEEP) {
                            sleep = DEFAULT_SLEEP;
                        }
                        Thread.sleep(sleep);
                   
                } else {
                    Thread.sleep(DEFAULT_SLEEP);
                }
            } catch (Exception ex) {
                MyLog.Error("SubscriberUpdateDao" + ex);
                MyLog.Error(ex);
            }
        }
    }

    private void process(List<Subscriber> datas) {
        if (dataSource == null) {
            throw new IllegalArgumentException("ERROR connection is null");
        }
        Connection connection = null;
        PreparedStatement ps = null;
        try {            
            connection = dataSource.getConnection();
            ps = connection.prepareStatement(SQL_PRO_UPDATE);
            ps.setQueryTimeout(5);//5 seconds
            for (Subscriber subscriber : datas) {
               // TODO set bien vao day
                ps.setInt(1, subscriber.getStatus());
                ps.setTimestamp(2,  new java.sql.Timestamp(subscriber.getRegisterTime()));
                 if (subscriber.getCancelTime() != 0) {
                    ps.setTimestamp(3, new java.sql.Timestamp(subscriber.getCancelTime()));
                } else {
                    ps.setTimestamp(3, null);
                }
                ps.setString(4, subscriber.getDescription()); 
                ps.setTimestamp(5, new java.sql.Timestamp(subscriber.getLastMonfeeChargeTime()));
                ps.setTimestamp(6, new java.sql.Timestamp(subscriber.getNextMonfeeChargeTime()));
                ps.setInt(7, subscriber.getChargeStatus());
                ps.setInt(8, subscriber.getIsPromotion());
                ps.setInt(9, subscriber.getMonfeeSuccessCount());
                ps.setTimestamp(10, subscriber.getLastSendNotify());
                ps.setString(11, subscriber.getSubServiceName());
                ps.setString(12, subscriber.getMsisdn());
                ps.addBatch();
            }

            ps.executeBatch();
            connection.commit();
            ps.close();
            ps = null;
            
        } catch (Exception ex) {
            MyLog.Error("error insert in class SubscriberUpdateDao");
            MyLog.Error(ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                   MyLog.Error(ex);
                } catch (Exception ex) {
                   MyLog.Error(ex);
                }
                
            }
        }
    }

   
}

