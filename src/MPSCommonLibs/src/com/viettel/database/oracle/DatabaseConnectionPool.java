/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.oracle;

import com.viettel.utilities.MyLog;
import javax.sql.DataSource;

/**
 *
 * @author HoaND6
 */
public class DatabaseConnectionPool {
    private String mConnectionConf;
    private int mNumberOfConnection;
    private OracleConnection[] mConnection;
    private int mIdx;
    /**
     * 
     * @param connectionConf Path to connection String file
     * @param numberOfConnection Number of connection
     */
    public DatabaseConnectionPool(String connectionConf, int numberOfConnection) {
        this.mConnectionConf = connectionConf;
        this.mNumberOfConnection = numberOfConnection;
    }
    
    public DatabaseConnectionPool(){
        //do nothing
    }
    
    /**
     * @author haind25
     * @param datasource
     */
    public void setOracleConnection(DataSource datasource) {
        OracleConnection oracleConnection = new OracleConnection(datasource);
        mNumberOfConnection = 1; // max connection depend on Datasource OracleConnection
        mConnection = new OracleConnection[mNumberOfConnection];
        mConnection[0] = oracleConnection;
    }
    
    public void start() {
        if (mConnection != null) {
            return;
        }
        mConnection = new OracleConnection[mNumberOfConnection];
        for (int i=0; i<mConnection.length; i++) {
            mConnection[i] = new OracleConnection(i, mConnectionConf);
        }
        
        mIdx = 0;
    }
    
    synchronized public OracleConnection getConnection() {
        if (mConnection == null || mConnection.length == 0) {
            return null;
        }
        
        OracleConnection result = null;
        int count = 0;
        while (result == null) {
            mIdx++;
            count++;
            if (mIdx >= mConnection.length) {
                mIdx = 0;
            }
            
            if (mConnection[mIdx] != null && mConnection[mIdx].isConnected()) {
                result = mConnection[mIdx];
            }
            
            if (result == null && (count % mConnection.length == 0)) {
                // Go 1 round, wait 5 sec
                MyLog.Warning("Get database Connection failed, wait 5 sec before continue ...");
                try { 
                    Thread.sleep(5000); 
                }
                catch (Exception ex) {
                    MyLog.Error("getConnection OracleConnection  " + ex);
                }
                
                if (count > mConnection.length * 10) {
                    // try 10 round
                    MyLog.Warning("Wait connection too long, retun NULL for GETTER");
                    break;
                }
            }
        }
        
        return result;
    }
}
