/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.oracle;


import com.viettel.database.datatype.SubLogs;
import com.viettel.utilities.MyLog;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import javax.sql.DataSource;
import org.apache.log4j.Logger;

/**
 *
 * @author hungtv45
 */
public class SubLogsInserDao implements Runnable{
     /**
     * thong thuong batch size se phai lon hon TPS (mac dinh TPS = 200/node)
     * day la ung dung lon nen de batch size an toan la 500 ~ max TPS
     * nen sau moi lan xu ly he thong se nghi: dua tren feedback phan hoi. max TPS/size of data
     */
    
    private final static int BATCH_SIZE = 500;
    private final static int DEFAULT_SLEEP = 5000;
    
    private final static String SQL_PRO_INSERT_LOG = "INSERT INTO subscription_logs(MSISDN, SUB_SERVICE_NAME, TRANID, ACTION, RESULT, DATETIME, TRACE, CHANNEL) " + 
                            "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
    
    private final LinkedList<SubLogs> queue = new LinkedList<SubLogs>();
    private final Object obj = new Object();
    
    private DataSource dataSource = null;
    
    private DatabaseConnectionPool databaseConnectionPool = null;
    private OracleConnection oracleConnection = null;
    
    private static SubLogsInserDao _instance = null;
    
    private final static Logger log = Logger.getLogger(SubLogsInserDao.class);
    
    private SubLogsInserDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static void config(DataSource dataSource) {
        if (_instance == null) {
            _instance = new SubLogsInserDao(dataSource);
            Thread t = new Thread(_instance);
            t.setName("thread-" + SubLogsInserDao.class.getSimpleName());
            t.start();
        }
    }
    
    private SubLogsInserDao(DatabaseConnectionPool dataSource) {
        this.databaseConnectionPool = dataSource;
    }

    public static SubLogsInserDao config(DatabaseConnectionPool dataSource) {
        if (_instance == null) {
            _instance = new SubLogsInserDao(dataSource);
            Thread t = new Thread(_instance);
            t.setName("thread-" + SubLogsInserDao.class.getSimpleName());
            t.start();
        }
        
        return _instance;
    }
    
    private SubLogsInserDao(OracleConnection oracleConnection) {
        this.oracleConnection = oracleConnection;
    }

    public  static SubLogsInserDao config(OracleConnection oracleConnection) {
        if (_instance == null) {
            _instance = new SubLogsInserDao(oracleConnection);
            Thread t = new Thread(_instance);
            t.setName("thread-" + SubLogsInserDao.class.getSimpleName());
            t.start();
        }
        
        return _instance;
    }
    
    public synchronized static SubLogsInserDao getInstance() {
        return _instance;
    }
   
    public void enqueue(SubLogs transaction) {
        synchronized (obj) {
            queue.add(transaction);
            log.info("enqueue queue: " + transaction);
            log.info("size of queue: " + queue.size() );
        }
    }

    private  List<SubLogs> dequeue() {
        synchronized (obj) {
            if (queue.isEmpty()) {
                return null;
            }
             List<SubLogs> datas = new LinkedList<SubLogs>();
            int i = 0;
            while (queue.size() > 0 && (i < BATCH_SIZE)) {
                SubLogs transaction = queue.poll();
                datas.add(transaction);
                i++;
            }

            return datas;
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                List<SubLogs> datas = dequeue();
                if (datas == null) {
                     Thread.sleep(DEFAULT_SLEEP);
                     continue;
                }
                
                process(datas);
                int length = datas.size();
                MyLog.Infor("SubLogInsertDao processed bacth queue = " + length);
                datas.clear();
                datas = null;

                int sleep = 1000 * BATCH_SIZE / length; //m s
                if (sleep > DEFAULT_SLEEP) {
                    sleep = DEFAULT_SLEEP;
                }
                Thread.sleep(sleep);
            } catch (Exception ex) {
                MyLog.Error("SubLogInsertDao.java");
                MyLog.Error(ex);
            }
        }
    }
     
    public Connection getConnection() throws SQLException {
        Connection connection = null;
        if (dataSource != null) {
            connection = dataSource.getConnection();
        }
        
        if (connection == null && databaseConnectionPool != null) {
            connection = databaseConnectionPool.getConnection().getDatabaseConnection();
        }
        
        if (connection == null && oracleConnection != null) {
            connection = oracleConnection.getDatabaseConnection();
        }
        
        if (connection == null) {
            MyLog.Error("error can not getConnection");  
        }
        
        return connection;
    } 
    
    private void closeConnection(Connection conn) throws SQLException{
        if (dataSource != null) {
            conn.close();
        }
    }
    
    private void process(List<SubLogs> datas) {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = getConnection();
            ps = connection.prepareStatement(SQL_PRO_INSERT_LOG);
            ps.setQueryTimeout(5);//5 seconds
            for (SubLogs subLogs : datas) {
               // TODO set bien vao day
//                MSISDN, SUB_SERVICE_NAME, TRANID, ACTION, RESULT, DATETIME, TRACE
                ps.setString(1, subLogs.getLogMsisdn());
                ps.setString(2, subLogs.getLogSubService());
                ps.setString(3, subLogs.getLogTranID());
                ps.setString(4, subLogs.getLogAction());
                ps.setInt(5, subLogs.getLogResult());
                ps.setTimestamp(6, new Timestamp(subLogs.getLogDatetime()));
                ps.setString(7, subLogs.getTraceStep());
                ps.setString(8, subLogs.getChannel());
                ps.addBatch();
            }

            ps.executeBatch();
            connection.commit();
            ps.close();
            ps = null;
//            closeConnection(connection);
        } catch (Exception ex) {
            MyLog.Error("error insert in class SubLogInsertDao");
            MyLog.Error(ex);
        }finally {
            if (connection != null) {
                try {
                    closeConnection(connection);
                } catch (SQLException ex) {
                   MyLog.Error(ex);
                } catch (Exception ex) {
                   MyLog.Error(ex);
                }
                
            }
        }

    }

   
}

