/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.oracle;

import com.viettel.database.datatype.CPService;
import com.viettel.database.datatype.CPWebservice;
import com.viettel.database.datatype.ChargingSubsQueue;
import com.viettel.database.datatype.IvrCommand;
import com.viettel.database.datatype.MOCommand;
import com.viettel.database.datatype.MPSUser;
import com.viettel.database.datatype.PriceCharge;
import com.viettel.database.datatype.PromotionCommand;
import com.viettel.database.datatype.RegisteredSubServices;
import com.viettel.database.datatype.SubService;
import com.viettel.database.datatype.Subscriber;
import com.viettel.database.datatype.SyncQueue;
import com.viettel.database.datatype.SyncTask;
import com.viettel.database.datatype.UssdCommand;
import com.viettel.utilities.MyLog;
import com.viettel.utilities.PublicLibs;
import java.sql.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author hoand
 */
public class DirectConnection implements Runnable {

    public static final int CONNECTION_TIMEOUT = 10000; //ms
    protected static int CHECK_TIME = 1000;
    protected boolean isRunning;
    protected Connection mConnection = null;
    protected int mId;
    protected boolean mIsConnected;
    private long mLastConnect;
    private ConnectionStrManager mConnManager;

    private DataSource dataSource = null;

    public DirectConnection(int id, String poolFile) {
        mId = id;
        isRunning = false;
        mIsConnected = false;
        mLastConnect = 0;
        mConnManager = new ConnectionStrManager(poolFile);

        try {
            DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
        } catch (Exception e) {
            MyLog.Error("DBThread " + mId + "ERROR" + e.getMessage());
            System.exit(0);
        }
    }

    public DirectConnection(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public DirectConnection(Connection connection) {
        this.mConnection = connection;
    }

    protected void init() {
        if (System.currentTimeMillis() - mLastConnect > CONNECTION_TIMEOUT) {
            mLastConnect = System.currentTimeMillis();

            String conn = mConnManager.getConnectionStr();
            if (conn != null) {
                MyLog.Infor("DBThread " + mId + "Connecting to '" + conn + "'");

                if (mConnManager.getUser() != null
                        && mConnManager.getPassword() != null) {
                    mConnection = connectToOraServer(mConnManager.getUrl(), mConnManager.getUser(), mConnManager.getPassword());
                } else {
                    mConnection = connectToOraServer(conn);
                }
                if (mConnection != null) {
                    mIsConnected = true;
                    MyLog.Infor("DBThread " + mId + "CONNECTED."); // to '" + conn + "'");
                } else {
                    MyLog.Error("DBThread " + mId + "CAN NOT Connect to DB"); //'" + conn + "'");
                }
            } else {
                MyLog.Infor("DBThread " + mId + "get Connection String Failed");
            }
        } else {
            MyLog.Error("Wait for timeout .... ");
        }
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        mIsConnected = dataSource != null;
    }

    public boolean isConnected() {
        if (dataSource != null) {
            return true;
        }
        return mIsConnected;
    }

    private void reconnect() {
        /* neu la datasource thi ko can reconnect*/
        if (dataSource != null) {
            return;
        }
        mIsConnected = false;
        try {
            MyLog.Debug("DBThread " + mId + "Wait 5 secs before RECONNECT to DB ....");
            Thread.sleep(5000); // wait 5s before reconnect
            MyLog.Debug("DBThread " + mId + "Reconnecting ...");
            mConnection.close();
        } catch (Exception e) {
        } finally {
            mConnection = null;
            init();
        }
    }

    private String getThreadName() {
        return "DBThread " + mId + ": ";
    }

    protected Connection connectToOraServer(String conn) {
        try {
            Connection co = DriverManager.getConnection(conn);
            MyLog.Infor("DBThread " + mId + "CONNECTED.");
            return co;
        } catch (Exception e) {
            MyLog.Error("DBThread " + mId + "Error connect to Oracle DB server" + e.getMessage());
            return null;
        }
    }

    protected Connection connectToOraServer(String conn, String user, String pass) {
        try {
            Connection co = DriverManager.getConnection(conn, user, pass);

            MyLog.Infor("DBThread " + mId + "CONNECTED.");
            return co;
        } catch (Exception e) {
            MyLog.Error("DBThread " + mId + "Error connect to Oracle DB server" + e.getMessage());
            return null;
        }
    }

    public void startConnect() {
        init();

        isRunning = true;
        Thread t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        while (isRunning && dataSource == null) {
            try {
                Statement s = null;
                try {
                    String strSQL = "select 1 from dual";
                    s = mConnection.createStatement();
                    s.setQueryTimeout(30); // sec
                    java.sql.ResultSet r = s.executeQuery(strSQL);
                    r.next();
                    s.close();
                } catch (Exception ex) {
                    MyLog.Error("DBThread " + mId + "Check connection ERROR" + ex.getMessage() + ", RECONNECTING .... ");
                    reconnect();
                } finally {
                    if (s != null) {
                        s.close();
                    }
                }

                Thread.sleep(CHECK_TIME);

            } catch (Exception e) {
                MyLog.Error("DBThread " + mId + "ERROR" + e.getMessage());
                MyLog.Error(e);
            }
        }
    }

    public CallableStatement getStatement(String strSql) {
        try {
            return getDatabaseConnection().prepareCall(strSql);
        } catch (Exception ex) {
            MyLog.Error("DBThread " + mId + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
            reconnect();
            return null;
        }
    }

    public Connection getDatabaseConnection() {
        if (dataSource == null) {
            return mConnection;
        }

        try {
            return dataSource.getConnection();
        } catch (SQLException ex) {
            MyLog.Error(ex);
        }
        return null;
    }

    public void closeConnection(Connection con) {
        if (dataSource == null || con == null) {
            return;
        }

        try {
            con.close();
        } catch (Exception ex) {
            MyLog.Error(ex);
        }
    }

    public boolean excuteUpdate(String strSQL) {
        MyLog.Debug(getThreadName() + "start execute'" + strSQL + "'");
        boolean result = false;
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        int count = 0;
        Connection conn = null;
        try {
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            // Set timeout
            s.setQueryTimeout(30);
            count = s.executeUpdate();
            result = true;
            s.close();
        } catch (SQLException e) {
            MyLog.Error(getThreadName() + "ERROR" + e.getMessage());
            MyLog.Error(e);
            result = false;
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
            reconnect();
            result = false;
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }
        MyLog.Infor(getThreadName() + " Finish execute '" + strSQL + "'" + result + " ("
                + (System.currentTimeMillis() - start) + " ms) (Count" + count + ")");

        return result;
    }

    public Vector<MOCommand> getAllMOCommand() {
        MyLog.Debug(getThreadName() + "start getAllMOCommand");
        Vector<MOCommand> result = new Vector<MOCommand>();
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "SELECT * FROM v_mo_command ORDER BY short_code";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            // Set timeout
            s.setQueryTimeout(30);
            ResultSet r = s.executeQuery();

            while (r.next()) {
                String cmd = r.getString("CMD");
                String subServiceName = r.getString("SUB_SERVICE_NAME");
                int type = r.getInt("TYPE");
                String successMessage = r.getString("SUCCESS_MESSAGE");
                String failedMessage = r.getString("FAILED_MESSAGE");
                String secondFailedMessage = r.getString("FAILED_MESSAGE_2");
                String notEnoughMoney = r.getString("NOT_ENOUGH_MONEY_MESSAGE");
                int fee = r.getInt("FEE");
                int requireRegisted = r.getInt("REQUIRED_REGISTED");
                String serviceName = r.getString("SERVICE_NAME");
                String serviceCode = r.getString("SUB_SERVICE_CODE");
                String shortCode = r.getString("SHORT_CODE");
                int webserviceID;
                try {
                    webserviceID = r.getInt("WEBSERVICE_ID");
                } catch (Exception ex) {
                    webserviceID = -1;
                }
                String chargeCMD = r.getString("CHARGE_CMD");
                if (chargeCMD == null || chargeCMD.isEmpty()) {
                    chargeCMD = "CHARGE"; // default;
                }

                int checkRequired = r.getInt("CHECK_REQUIRED");
                int checkPrice = r.getInt("CHECK_PRICE");
                int freeSMS = r.getInt("NUMBER_FREE_SMS");
                String countKey = r.getString("COUNT_KEY");
                int countDay = r.getInt("COUNT_DAY");
                int limitCount = r.getInt("LIMIT_COUNT");

                if (shortCode != null && cmd != null) {
                    MOCommand command = new MOCommand(cmd, subServiceName, successMessage, failedMessage, secondFailedMessage, notEnoughMoney,
                            serviceName, serviceCode, shortCode, type, fee, requireRegisted, webserviceID, chargeCMD,
                            checkRequired, checkPrice, freeSMS, limitCount, countDay, countKey);
                    MyLog.Debug(getThreadName() + " Add MOCommand " + command);
                    result.add(command);
                }
            }
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish getAllMOCommand -> " + result.size() + "(" + (System.currentTimeMillis() - start) + " ms)");
        return result;
    }

    /**
     * Get price charge
     *
     * @param shortCode
     * @param subService
     * @param cmd
     * @param type
     * @param category
     * @param msisdn
     * @return
     */
    public PriceCharge getPriceForCMD(String shortCode, String subService, String cmd,
            int type, String category, String msisdn) {
        MyLog.Debug(getThreadName() + "start getPriceForCMD for (ShortCode: " + shortCode + "; MSISDN: " + msisdn
                + "; SubService: " + subService + "; CMD: " + cmd + "; Category: " + category + "; Type: " + type + ")");
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        PriceCharge result = null;
        Connection conn = null;
        try {
            String strSQL = "BEGIN PS_CHECK_BUSINESS_REQ_SMS.PR_CHECK_BUSINESS_REQ_SMS"
                    + "(?, ?, ?, ?, ?, ?, ?, ?, ?); END;";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30);
            s.setString(1, shortCode);
            s.setString(2, subService);
            s.setString(3, cmd);
            s.setInt(4, type);
            s.setString(5, category);
            s.setString(6, msisdn);
            s.registerOutParameter(7, java.sql.Types.INTEGER);
            s.registerOutParameter(8, java.sql.Types.INTEGER);
            s.registerOutParameter(9, java.sql.Types.INTEGER);
            s.execute();

            int realPrice = s.getInt(7);
            int cmdPrice = s.getInt(8);
            int status = s.getInt(9);

            result = new PriceCharge(realPrice, cmdPrice, status);
        } catch (SQLException e) {
            MyLog.Error(getThreadName() + "ERROR" + e.getMessage());
            MyLog.Error(e);
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR3" + ex.getMessage());
            MyLog.Error(ex);
            reconnect();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish getPriceForCMD for (ShortCode: " + shortCode + "; MSISDN: " + msisdn
                + "; SubService: " + subService + "; CMD: " + cmd + "; Category: " + category + "; Type: " + type + ") -> (" + result + ") ("
                + (System.currentTimeMillis() - start) + " ms)");
        return result;
    }

    /**
     * Get Monfee price for subscriber
     *
     * @param msisdn
     * @param serviceName
     * @param subServiceName
     * @return
     */
    public PriceCharge getMonfeePrice(String msisdn, String serviceName, String subServiceName) {
        MyLog.Debug(getThreadName() + "start getMonfeePrice for " + msisdn + " - " + serviceName + " - " + subServiceName);
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        PriceCharge result = null;
        Connection conn = null;
        try {
            String strSQL = "begin ps_check_business_req_sms.PR_CHECK_PRICE_MONFEE(?, ?, ?, ?, ?); end;";

            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            // Set timeout
            s.setQueryTimeout(30);
            s.setString(1, serviceName);
            s.setString(2, subServiceName);
            s.setString(3, msisdn);
            s.registerOutParameter(4, java.sql.Types.INTEGER);
            s.registerOutParameter(5, java.sql.Types.INTEGER);
            s.execute();

            int realPrice = s.getInt(4);
            int cmdPrice = 0;
            int status = s.getInt(5);

            result = new PriceCharge(realPrice, cmdPrice, status);
        } catch (SQLException e) {
            MyLog.Error(getThreadName() + "ERROR" + e.getMessage());
            MyLog.Error(e);
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR3" + ex.getMessage());
            MyLog.Error(ex);
            reconnect();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish getPriceForCMD for " + msisdn + " - " + serviceName + "' - '" + subServiceName + "' -> (" + result + ") ("
                + (System.currentTimeMillis() - start) + " ms)");
        return result;
    }

    /**
     * Get additional date for Service
     *
     * @param msisdn
     * @param subType
     * @param subService
     * @param service
     * @param cmd
     * @param cat
     * @param shortCode
     * @param cmdType
     * @return
     */
    public long getAdditonalRegisterDate(String msisdn, int subType, String subService, String service,
            String cmd, String cat, String shortCode, int cmdType) {
        String log = "getAdditonalRegisterDate (MSISDN: " + msisdn
                + "; SubType: " + subType
                + "; Service: " + service
                + "; SubService: " + subService
                + "; ShortCode: " + shortCode
                + "; CMD: " + cmd
                + "; CAT: " + cat
                + "; CMDType: " + cmdType
                + ")";
        MyLog.Debug(getThreadName() + "start " + log);
        int result = 30;
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "{ ?=call fn_get_add_day_monfee_reg_sms(?, ?, ?, ?, ?, ?, ?, ?) }";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30);
            s.registerOutParameter(1, java.sql.Types.VARCHAR);
            s.setString(2, msisdn);
            s.setInt(3, subType);
            s.setString(4, cmd);
            s.setString(5, cat);
            s.setString(6, subService);
            s.setString(7, service);
            s.setString(8, shortCode);
            s.setInt(9, cmdType);

            s.executeUpdate();

            result = s.getInt(1);
        } catch (SQLException e) {
            MyLog.Error(getThreadName() + "ERROR " + e.getMessage());
            MyLog.Error(e);
            result = 30;
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR " + ex.getMessage());
            MyLog.Error(ex);
            reconnect();
            result = 30;
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish " + log + " -> " + result + "("
                + (System.currentTimeMillis() - start) + " ms)");

        return result;
    }

    public long getAdditonalRegisterDateWS(String msisdn, int subType, String subService, String service) {
        String log = "getAdditonalRegisterDate (MSISDN: " + msisdn
                + "; SubType: " + subType
                + "; Service: " + service
                + "; SubService: " + subService
                + ")";
        MyLog.Debug(getThreadName() + "start getAdditonalRegisterDateWS" + log);
        int result = 30;
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "{ ?=call fn_get_add_day_register_ws(?, ?, ?, ?) }";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30); // sec
            s.registerOutParameter(1, java.sql.Types.VARCHAR);
            s.setString(2, msisdn);
            s.setInt(3, subType);
            s.setString(4, subService);
            s.setString(5, service);

            s.executeUpdate();

            result = s.getInt(1);
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR " + ex.getMessage());
            MyLog.Error(ex);
            result = 30;
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish getAdditonalRegisterDateWS" + log + " -> " + result + "("
                + (System.currentTimeMillis() - start) + " ms)");

        return result;
    }

    /**
     * Get additional day for charge monfee success
     *
     * @param msisdn
     * @param subType
     * @param subService
     * @param service
     * @return
     */
    public long getAdditonalDateMonfee(String msisdn, int subType, String subService, String service) {
        MyLog.Debug(getThreadName() + "start getAdditonalDateMonfee for '" + subType + "' - " + subService + " - " + service);
        int result = 30;
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
//            String strSQL = "{ ?=call FN_GET_ADDITIONAL_DAY(?, ?, ?, ?) }";
            String strSQL = "{ ?=call fn_get_add_day_charge_monfee(?, ?, ?, ?) }";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            // Set timeout
            s.setQueryTimeout(30); // sec
            s.registerOutParameter(1, java.sql.Types.VARCHAR);
            s.setString(2, msisdn);
            s.setInt(3, subType);
            s.setString(4, subService);
            s.setString(5, service);

            s.executeUpdate();
            result = s.getInt(1);
        } catch (SQLException e) {
            MyLog.Error(getThreadName() + "ERROR " + e.getMessage());
            MyLog.Error(e);
            result = 30;
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR " + ex.getMessage());
            MyLog.Error(ex);
            reconnect();
            result = 30;
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish getAdditonalDateMonfee for'" + subType + " - " + subService + " - " + service + " -> " + result + "("
                + (System.currentTimeMillis() - start) + " ms)");

        return result;
    }

    private long getTimeStamp(ResultSet r, String column, long defaultValue) {
        long result = defaultValue;
        try {
            if (r.getTimestamp(column) != null) {
                result = r.getTimestamp(column).getTime();
            }
        } catch (Exception ex) {
            MyLog.Debug(getThreadName() + " get Timestamp for " + column + " failed, use default: " + defaultValue);
            result = defaultValue;
        }

        return result;
    }

    /**
     * Get Subscription information
     *
     * @param msisdn
     * @param subServiceName
     * @return
     */
    public Subscriber getSubscriber(String msisdn, String subServiceName) {
        MyLog.Debug(getThreadName() + "start get getSubScription for '" + msisdn + "' - '" + subServiceName + "'");
        Subscriber result = null;
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "SELECT * FROM v_service_subs WHERE msisdn = ? AND SUB_SERVICE_NAME=?";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            // Set timeout
            s.setQueryTimeout(30);
            s.setString(1, msisdn);
            s.setString(2, subServiceName);

            ResultSet r = s.executeQuery();
            if (r.next()) {
                String provider = r.getString("PROVIDER_NAME");
                String serviceName = r.getString("SERVICE_NAME");
                int status = r.getInt("STATUS");
                long registerTime = getTimeStamp(r, "REGISTER_TIME", 0);
                long cancelTime = getTimeStamp(r, "CANCEL_TIME", 0);
                String description = r.getString("DESCRIPTIONS");
                long lastMonFeeCharge = getTimeStamp(r, "LAST_MONFEE_CHARGE_TIME", 0);
                long nextMonFeeCharge = getTimeStamp(r, "NEXT_MONFEE_CHARGE_TIME", 0);
                int chargeStatus = r.getInt("CHARGE_STATUS");
                int subType = r.getInt("SUBS_TYPE");
                int allowDebFee = r.getInt("DAY_DEBIT_ALLOWED");
                int isPromotion = r.getInt("IS_PROMOTION");
                String subServiceCode = r.getString("SUB_SERVICE_CODE");
                int monfeeCount = r.getInt("MONFEE_SUCCESS_COUNT");
                Timestamp lastNotify = r.getTimestamp("LAST_SEND_NOTIFY");

                result = new Subscriber(msisdn, provider, serviceName, subServiceName, subServiceCode, description,
                        status, chargeStatus, subType, allowDebFee,
                        isPromotion, registerTime, cancelTime,
                        lastMonFeeCharge, nextMonFeeCharge, monfeeCount, lastNotify);
            }
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR " + ex.getMessage());
            MyLog.Error(ex);
            result = null;
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish getSubScription for '" + msisdn + "' - '" + subServiceName + "' -> " + result + "("
                + (System.currentTimeMillis() - start) + " ms)");

        return result;
    }

    //get promo hungtv45 phieu yeu cau VTC
    //check promo for sub_service
    @Deprecated
    public String getPromoSubSerive(String subServiceName) {
        CallableStatement s = null;
        String is_promo = "0";
        Connection conn = null;
        try {
//            SELECT COUNT(*) FROM SUB_SERVICES S, SERVICES_SUBSCRIPTIONS B WHERE B.SUB_SERVICE_NAME='pukeman_daily' AND  S.SUB_SERVICE_NAME='pukeman_daily' AND B.MSISDN= '882080333' AND S.IS_PROMO = 1;
            String strSQL = "SELECT IS_PROMO FROM SUB_SERVICES where SUB_SERVICE_NAME=?";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30);
            s.setString(1, subServiceName);

            ResultSet r = s.executeQuery();
            if (r.next()) {
                is_promo = r.getString("IS_PROMO");
            }
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR " + ex.getMessage());
            MyLog.Error(ex);
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }
        return is_promo;
    }

    //checkSubRegisterFirst

    /**
     * @author hungtv45
     *
     * @deprecated
     */
    public boolean checkSubRegisterFirst(String subServiceName, String msisdn) {
        CallableStatement s = null;
        String check_register_first = "0";
        Connection conn = null;
        try {
//            SELECT case when exists (SELECT 1 from SERVICES_SUBSCRIPTIONS where SUB_SERVICE_NAME = 'pukeman_daily' AND MSISDN='882080333') then '1' else '0' end as check_register_first FROM dual;
            String strSQL = "SELECT case WHEN EXISTS (SELECT 1 from SERVICES_SUBSCRIPTIONS WHERE SUB_SERVICE_NAME = ? AND MSISDN = ?) THEN 1 ELSE 0 END AS CHECKRF FROM dual";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30);
            s.setString(1, subServiceName);
            s.setString(2, msisdn);

            ResultSet r = s.executeQuery();
            if (r.next()) {
                check_register_first = r.getString("CHECKRF");
            }
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR " + ex.getMessage());
            MyLog.Error(ex);
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        if ("1".equals(check_register_first.trim())) {
            return false;
        } else {
            return true;
        }
    }

    public Vector<Subscriber> getAllRegisteredService(String msisdn) {
        MyLog.Debug(getThreadName() + "start getAllRegisteredService for '" + msisdn + "'");
        Vector<Subscriber> result = new Vector<Subscriber>();
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "SELECT * FROM v_service_subs WHERE msisdn=?";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30);
            s.setString(1, msisdn);

            ResultSet r = s.executeQuery();
            while (r.next()) {
//                String msisdn = r.getString("MSISDN");
                String subServiceName = r.getString("SUB_SERVICE_NAME");
                String provider = r.getString("PROVIDER_NAME");
                String serviceName = r.getString("SERVICE_NAME");
                int status = r.getInt("STATUS");
                long registerTime = getTimeStamp(r, "REGISTER_TIME", 0);
                long cancelTime = getTimeStamp(r, "CANCEL_TIME", 0);
                String description = r.getString("DESCRIPTIONS");

                long lastMonFeeCharge = getTimeStamp(r, "LAST_MONFEE_CHARGE_TIME", 0);
                long nextMonFeeCharge = getTimeStamp(r, "NEXT_MONFEE_CHARGE_TIME", 0);
                int chargeStatus = r.getInt("CHARGE_STATUS");
                int subType = r.getInt("SUBS_TYPE");
                int allowDebFee = r.getInt("DAY_DEBIT_ALLOWED");
                int isPromotion = r.getInt("IS_PROMOTION");
                String subServiceCode = r.getString("SUB_SERVICE_CODE");

                int monfeeCount = r.getInt("MONFEE_SUCCESS_COUNT");
                Timestamp lastNotify = r.getTimestamp("LAST_SEND_NOTIFY");

                Subscriber sub = new Subscriber(msisdn, provider, serviceName, subServiceName, subServiceCode, description,
                        status, chargeStatus, subType, allowDebFee,
                        isPromotion, registerTime, cancelTime,
                        lastMonFeeCharge, nextMonFeeCharge, monfeeCount, lastNotify);
                MyLog.Debug("Add Subscriber " + sub);
                result.add(sub);
            }
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR " + ex.getMessage());
            MyLog.Error(ex);
            reconnect();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish getAllRegisteredService for '" + msisdn + "' - '" + result.size() + "' -> " + result + "("
                + (System.currentTimeMillis() - start) + " ms)");

        return result;
    }

    /**
     * hungtv45 - PYC Get All SubService Information
     *
     * @return
     */
    public Map<String, SubService> getAllSubService() {
        MyLog.Debug(getThreadName() + "start getAllSubService");
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Map<String, SubService> result = new Hashtable<String, SubService>();
        Connection conn = null;
        try {
            String strSQL = "SELECT * FROM V_SUB_SERVICES WHERE STATUS=1 AND IS_SUBSCRIPTIONS=1"; // active & subscription enable
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30); // sec            
            ResultSet r = s.executeQuery();

            while (r.next()) {
                String subServiceCode = r.getString("SUB_SERVICE_CODE");
                String subServiceName = r.getString("SUB_SERVICE_NAME");
                String description = r.getString("DESCRIPTIONS");
                String serviceName = r.getString("SERVICE_NAME");

                int isSubscription = r.getInt("IS_SUBSCRIPTIONS");
                int status = r.getInt("STATUS");
                int fixTimeChargeMonFee = r.getInt("FIX_TIME_CHARGE_MONFEE");
                String groupChargeType = r.getString("GROUP_CHARGE_TYPE");

                int subType = r.getInt("SUBS_TYPE");
                String subFee = r.getString("SUBS_PRICE");
                String promoFee = r.getString("PROMO_PRICE");
                long promoDateBegin = getTimeStamp(r, "PROMO_DATETIME_BEGIN", 0);

                long promoDateEnd = getTimeStamp(r, "PROMO_DATETIME_END", 0);
                int dayAllowDebit = r.getInt("DAY_DEBIT_ALLOWED");
                String monfeeHour = r.getString("MONFEE_HOUR");
                int stopOnError = r.getInt("STOP_ON_ERROR");

                int checkConflict = r.getInt("CHECK_CONFLICT");
                int requireMPSUser = r.getInt("REQUIRE_MPS_USER");
                int recheckBeforeMonfee = r.getInt("RECHECK_BEFORE_MONFEE");
                int monfeeWSId = r.getInt("MONFEE_WS_ID");

                int registerWSId = r.getInt("REGISTER_WS_ID");
                int cancelWSId = r.getInt("CANCEL_WS_ID");
                String errorMessage = r.getString("DEFAULT_ERR_MSG");
                int autoCancel = r.getInt("AUTO_CANCEL_CONFILICT");

                int sendNotify = r.getInt("SEND_NOTIFY");
                String notifyContent = r.getString("NOTIFY_CONTENT");
                String softName = r.getString("SOFT_NAME");

                //hungtv45 PYC
                int daySendNotifyBeforeCancel = r.getInt("DAY_SEND_NOTIFY_BEFORE_CANCEL");
                int sendNotifyBeforeCancel = r.getInt("SEND_NOTIFY_BEFORE_CANCEL");
                String notifyBeforeCancel = r.getString("NOTIFY_BEFORE_CANCEL");
                int sendNotifyAutoCancel = r.getInt("SEND_NOTIFY_AUTO_CANCEL");
                String notifyAutoCancel = r.getString("NOTIFY_AUTO_CANCEL");
                
                int renewCheckMode = getInt("RENEW_CHECK_MODE", r , 0);
                
                SubService tmp = new SubService(subServiceCode, subServiceName, description, serviceName,
                        groupChargeType, subFee, promoFee, isSubscription, status,
                        fixTimeChargeMonFee, subType, dayAllowDebit,
                        promoDateBegin, promoDateEnd, PublicLibs.convertHashSetInt(monfeeHour),
                        stopOnError, checkConflict, requireMPSUser, recheckBeforeMonfee, //, warningList);
                        monfeeWSId, registerWSId, cancelWSId,
                        errorMessage, autoCancel == SubService.AUTO_CANCEL_TRUE, sendNotify, notifyContent, softName,
                        daySendNotifyBeforeCancel, sendNotifyBeforeCancel, notifyBeforeCancel, sendNotifyAutoCancel, notifyAutoCancel);
                tmp.setRenewCheckMode(renewCheckMode);
                
//                int daySendNotifyBeforeCancel, int sendNotifyBeforeCancel, String notifyBeforeCancel, int sendNotifyAutoCancel, String notifyAutoCancel
                MyLog.Debug(getThreadName() + "Add subService " + tmp);
                result.put(tmp.getSubServiceName(), tmp);
            }
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
            reconnect();
            result.clear();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish getAllSubService -> " + result.size() + " recs ("
                + (System.currentTimeMillis() - start) + " ms)");

        return result;
    }
    
    private int getInt(String key, ResultSet rs, int defaultValue) throws SQLException {
        ResultSetMetaData rsMeta = rs.getMetaData();
        int metaLength = rsMeta.getColumnCount();
        for (int i = 1; i <= metaLength; i++) {
            String metaColumn = rsMeta.getColumnName(i);
            if (key.equalsIgnoreCase(metaColumn)) {
                return rs.getInt(metaColumn);
            }
        }
        return defaultValue;
    }

    public Map<String, SubService> findSubServices(String serviceName) {
        MyLog.Debug(getThreadName() + "start getAllSubService");
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Map<String, SubService> subServices = new Hashtable<String, SubService>();
        Connection conn = null;
        try {
            String strSQL = "SELECT * FROM V_SUB_SERVICES_VTC WHERE status=1 AND SERVICE_NAME=? "; // active & subscription enable
//            String strSQL = "SELECT * FROM V_SUB_SERVICES WHERE status=1 AND SERVICE_NAME=? "; // active & subscription enable
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30); // sec
            s.setString(1, serviceName);
            ResultSet r = s.executeQuery();
            while (r.next()) {
                String subServiceCode = r.getString("SUB_SERVICE_CODE");
                String subServiceName = r.getString("SUB_SERVICE_NAME");
                String description = r.getString("DESCRIPTIONS");
                int isSubscription = r.getInt("IS_SUBSCRIPTIONS");
                int status = r.getInt("STATUS");
                int fixTimeChargeMonFee = r.getInt("FIX_TIME_CHARGE_MONFEE");
                String groupChargeType = r.getString("GROUP_CHARGE_TYPE");
                int subType = r.getInt("SUBS_TYPE");
                String subFee = r.getString("SUBS_PRICE");
                String promoFee = r.getString("PROMO_PRICE");
                long promoDateBegin = getTimeStamp(r, "PROMO_DATETIME_BEGIN", 0);
                long promoDateEnd = getTimeStamp(r, "PROMO_DATETIME_END", 0);
//                long subServiceCode = r.getString("PROMO_DATETIME_BEGIN");
//                long subServiceCode = r.getString("PROMO_DATETIME_END");
                int dayAllowDebit = r.getInt("DAY_DEBIT_ALLOWED");
                String monfeeHour = r.getString("MONFEE_HOUR");
                int stopOnError = r.getInt("STOP_ON_ERROR");
                int checkConflict = r.getInt("CHECK_CONFLICT");
                int requireMPSUser = r.getInt("REQUIRE_MPS_USER");
                int recheckBeforeMonfee = r.getInt("RECHECK_BEFORE_MONFEE");
//                String warningList = r.getString("WARNING_LIST");
                int monfeeWSId = r.getInt("MONFEE_WS_ID");
                int registerWSId = r.getInt("REGISTER_WS_ID");
                int cancelWSId = r.getInt("CANCEL_WS_ID");

                String errorMessage = r.getString("DEFAULT_ERR_MSG");
                int autoCancel = r.getInt("AUTO_CANCEL_CONFILICT");
                int sendNotify = r.getInt("SEND_NOTIFY");
                String notifyContent = r.getString("NOTIFY_CONTENT");
                String softName = r.getString("SOFT_NAME");

                //hungtv45 PYC
                int daySendNotifyBeforeCancel = r.getInt("DAY_SEND_NOTIFY_BEFORE_CANCEL");
                int sendNotifyBeforeCancel = r.getInt("SEND_NOTIFY_BEFORE_CANCEL");
                String notifyBeforeCancel = r.getString("NOTIFY_BEFORE_CANCEL");
                int sendNotifyAutoCancel = r.getInt("SEND_NOTIFY_AUTO_CANCEL");
                String notifyAutoCancel = r.getString("NOTIFY_AUTO_CANCEL");

                SubService tmp = new SubService(subServiceCode, subServiceName, description, serviceName,
                        groupChargeType, subFee, promoFee, isSubscription, status,
                        fixTimeChargeMonFee, subType, dayAllowDebit,
                        promoDateBegin, promoDateEnd, PublicLibs.convertHashSetInt(monfeeHour),
                        stopOnError, checkConflict, requireMPSUser, recheckBeforeMonfee, //, warningList);
                        monfeeWSId, registerWSId, cancelWSId,
                        errorMessage, autoCancel == SubService.AUTO_CANCEL_TRUE, sendNotify, notifyContent, softName,
                        daySendNotifyBeforeCancel, sendNotifyBeforeCancel, notifyBeforeCancel, sendNotifyAutoCancel, notifyAutoCancel);

                MyLog.Debug(getThreadName() + "Add subService " + tmp);
                subServices.put(tmp.getSubServiceName(), tmp);
            }
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
            reconnect();
            subServices.clear();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish getAllSubService -> " + subServices.size() + " recs ("
                + (System.currentTimeMillis() - start) + " ms)");

        return subServices;
    }

    /**
     * Get MPS User information
     *
     * @param msisdn
     * @return
     */
    public MPSUser getMPSUser(String msisdn) {
        MyLog.Debug(getThreadName() + "start getMPSUser for '" + msisdn + "'");
        MPSUser result = null;
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "SELECT * FROM v_mps_user WHERE msisdn = ?";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30);
            s.setString(1, msisdn);
            ResultSet r = s.executeQuery();
            if (r.next()) {
//                String msisdn = r.getString("MSISDN");
                String password = r.getString("PASSWORD");
                int status = r.getInt("STATUS");
                long registerTime = getTimeStamp(r, "REGISTER_TIME", 0);
                long cancelTime = getTimeStamp(r, "CANCEL_TIME", 0);
                int coin = r.getInt("COIN");
                result = new MPSUser(msisdn, password, status, coin, registerTime, cancelTime);
            }
        } catch (SQLException e) {
            MyLog.Error(getThreadName() + "ERROR" + e.getMessage());
            MyLog.Error(e);
            result = null;
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
            reconnect();
            result = null;
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        if (result != null) {
            MyLog.Infor(getThreadName() + "Finish getMPSUser for '" + msisdn + "' -> " + result + "("
                    + (System.currentTimeMillis() - start) + " ms)");
        } else {
            MyLog.Infor(getThreadName() + "Finish getMPSUser for '" + msisdn + "' -> NULL ("
                    + (System.currentTimeMillis() - start) + " ms)");
        }

        return result;
    }

    /**
     * Update MPS User
     *
     * @param user
     * @return
     */
    public boolean updateMPSUser(MPSUser user) {
        MyLog.Debug(getThreadName() + "start updateMPSUser for " + user);
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        boolean result = true;
        Connection conn = null;
        try {
            String strSQL = "begin PR_MPS_ACCOUNT_IU(?, ?, ?, ?, ?, '', '', sysdate, 0, '', ?); end;";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30);
            s.setString(1, user.getMsisdn());
            s.setString(2, user.getPassword());
            s.setInt(3, user.getStatus());
            s.setTimestamp(4, new java.sql.Timestamp(user.getRegisterTime()));
            s.setTimestamp(5, new java.sql.Timestamp(user.getCancelTime()));
            s.setInt(6, user.getCoin());
            s.executeUpdate();
        } catch (SQLException e) {
            MyLog.Error(getThreadName() + "ERROR" + e.getMessage());
            MyLog.Error(e);
            result = false;
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR3" + ex.getMessage());
            MyLog.Error(ex);
            result = false;
            reconnect();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish updateMPSUser for " + user + "' -> (" + result + ") ("
                + (System.currentTimeMillis() - start) + " ms)");
        return result;
    }

    /**
     * Get webservice to call
     *
     * @param wsId
     * @return
     */
//    public CPWebservice getCPWebService(int wsId, String subService) {
    public CPWebservice getCPWebService(int wsId) {
        MyLog.Debug(getThreadName() + "start getCPWebService (WSID: " + wsId + ")");
        CPWebservice result = null;
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
//            String strSQL = "SELECT * FROM cp_webservice_accounts WHERE webservice_id = ? AND upper(SUB_SERVICE_NAME) = ?";
            String strSQL = "SELECT * FROM cp_webservice_accounts WHERE webservice_id = ?";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            // Set timeout
            s.setQueryTimeout(30);
            s.setInt(1, wsId);
            ResultSet r = s.executeQuery();
            if (r.next()) {
                String subServiceName = r.getString("SUB_SERVICE_NAME");
                String username = r.getString("USER_NAME");
                String password = r.getString("PASSWORD");
                String url = r.getString("URL");
                String params = r.getString("PARAMS");
                String wsType = r.getString("WEBSERVICE_TYPE");
                String soapAction = r.getString("SOAP_ACTION");
                String tagPrefix = r.getString("TAG_PREFIX");
                String rawXML = r.getString("RAW_XML");
                String returnTag = r.getString("RETURN_TAG");
                int timeout = r.getInt("TIMEOUT");
                String signal = r.getString("DATA_SIGN");
                result = new CPWebservice(wsId, subServiceName, username, password, url, params,
                        wsType, soapAction, tagPrefix, rawXML, returnTag, timeout, signal);
            }
        } catch (SQLException e) {
            MyLog.Error(getThreadName() + "ERROR" + e.getMessage());
            MyLog.Error(e);
            result = null;
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
            reconnect();
            result = null;
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "start get getSubService for (WSID: " + wsId + ") -> " + result + "("
                + (System.currentTimeMillis() - start) + " ms)");

        return result;
    }

    /**
     * Update Subscriber for Sub Service
     *
     * @param sub ServiceSubscription
     * @return 
     */
    public boolean updateSubscriber(Subscriber sub) {
        MyLog.Debug(getThreadName() + "start updateSubscriber for " + sub);
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        boolean result = true;
        Connection conn = null;
        try {
            String strSQL = "BEGIN PR_SUBSCRIBER_IU(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); end;";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30);
            s.setString(1, sub.getMsisdn());
            s.setString(2, sub.getSubServiceName());
            s.setInt(3, sub.getStatus());
            s.setTimestamp(4, new java.sql.Timestamp(sub.getRegisterTime()));
            if (sub.getCancelTime() != 0) {
                s.setTimestamp(5, new java.sql.Timestamp(sub.getCancelTime()));
            } else {
                s.setTimestamp(5, null);
            }
            s.setString(6, sub.getDescription());
            s.setTimestamp(7, new java.sql.Timestamp(sub.getLastMonfeeChargeTime()));
            s.setTimestamp(8, new java.sql.Timestamp(sub.getNextMonfeeChargeTime()));
            s.setInt(9, sub.getChargeStatus());
            s.setInt(10, sub.getIsPromotion());
            s.setInt(11, sub.getMonfeeSuccessCount());
            s.setTimestamp(12, sub.getLastSendNotify());

            s.executeUpdate();
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR3" + ex.getMessage());
            MyLog.Error(ex);
            result = false;
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish updateSubscriber for " + sub + "' -> (" + result + ") ("
                + (System.currentTimeMillis() - start) + " ms)");
        return result;
    }

    /**
     * Check User register service or not
     *
     * @param msisdn Phone Number
     * @param subService SubService name
     * @return
     */
    public boolean isRegisteredSubService(String msisdn, String subService) {
        MyLog.Debug(getThreadName() + "start isRegisteredSubService for'" + msisdn + "' - '" + subService + "'");

        Subscriber sub = getSubscriber(msisdn, subService);
        long start = System.currentTimeMillis();
        int result = DBConst.SERVICE_REGISTERED;

        if (sub == null || sub.getStatus() == DBConst.STATUS_INACTIVE) {
            result = DBConst.SERVICE_UNREGISTERED;
        }

        MyLog.Infor(getThreadName() + "Finish registSubService for '" + msisdn + "' - '" + subService + "' -> " + result + "("
                + (System.currentTimeMillis() - start) + " ms)");

        return result == DBConst.SERVICE_REGISTERED;
    }

    /**
     * Insert MO Log to DB
     *
     * @param subService
     * @param shortCode
     * @param msisdn
     * @param content
     * @param tranID
     * @param mt
     */
    public void insertSMS(String subService, String shortCode, String msisdn, String content, String tranID, boolean mt) {
        String smsType = mt ? "MT" : "MO";
        MyLog.Debug(getThreadName() + "start insertSMS (" + smsType + ") for '" + msisdn + "' - '" + content + "' TranID: '" + tranID + "'");
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = mt ? "BEGIN pr_TRANSACTION_MT_I(?, ?, ?, ?, ?, sysdate); end;"
                    : "BEGIN pr_TRANSACTION_MO_I(?, ?, ?, ?, ?, sysdate); end;";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30); // sec
            s.setString(1, subService);
            s.setString(2, shortCode);
            s.setString(3, msisdn);
            s.setString(4, content);
            s.setString(5, tranID);
            s.executeUpdate();
        } catch (SQLException e) {
            MyLog.Error(getThreadName() + "ERROR SQL" + e.getMessage());
            MyLog.Error(e);
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
            reconnect();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish insert " + smsType + " for '" + msisdn + "' - '" + content + "' TranID: '" + tranID + "' ("
                + (System.currentTimeMillis() - start) + " ms)");
    }

    /**
     * Add transaction to log
     *
     * @param msisdn
     * @param service
     * @param subService
     * @param cmd
     * @param content
     * @param price
     * @param tranID
     */
    public void addTransactionLog(String msisdn, String service, String subService, String cmd, String content,
            int price, String tranID) {
        MyLog.Debug(getThreadName() + "start addTransactionLog for '" + msisdn + "' - '" + content + "' TranID: '" + tranID + "'");
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "BEGIN PR_TRANSACTIONS_I(?, ?, ?, ?, sysdate, "
                    + "'', '', '', "
                    + "?, '', sysdate, '', ?, 2, ?, ''); end;";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            // Set timeout
            s.setQueryTimeout(30); // sec
            s.setString(1, msisdn);
            s.setString(2, service);
            s.setString(3, subService);
            s.setString(4, cmd);
            s.setString(5, content);
            s.setInt(6, price);
            s.setString(7, tranID);
            s.executeUpdate();
        } catch (SQLException e) {
            MyLog.Error(getThreadName() + "ERROR SQL" + e.getMessage());
            MyLog.Error(e);
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
            reconnect();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish addTransactionLog for '" + msisdn + "' - '" + content + "' TranID: '" + tranID + "' ("
                + (System.currentTimeMillis() - start) + " ms)");
    }

    /**
     * Load all subscriber for Sub_Services_Name
     *
     * @param queue Store Subscriber
     * @param subService Sub Services Name
     * @return
     */
    public boolean loadChargingSubsFromDB(ChargingSubsQueue queue, String subService) {
        if (queue == null || subService == null) {
            MyLog.Error(getThreadName() + "start loadChargingSubsFromDB for null, return now.");
            return false;
        }

        boolean result = true;
        MyLog.Debug(getThreadName() + "start loadChargingSubsFromDB for SUB SERVICES: " + subService);
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            //hungtv45
            String strSQL = "SELECT * FROM v_active_subs "
                    + "WHERE trunc(next_monfee_charge_time) <= trunc(sysdate) "
                    + "AND sub_service_name = ?";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(300);
//            s.setTimestamp(1, new java.sql.Timestamp(PublicLibs.getFirstMomentOfToday() + 60000)); // add one Mins
            s.setString(1, subService);
            ResultSet rs = s.executeQuery();
            while (rs.next()) {
                String msisdn = rs.getString("MSISDN");
                String provider = rs.getString("PROVIDER_NAME");
                String serviceName = rs.getString("SERVICE_NAME");
                String subServiceName = rs.getString("SUB_SERVICE_NAME");
                int status = rs.getInt("STATUS");
                long registerTime = getTimeStamp(rs, "REGISTER_TIME", System.currentTimeMillis());
                long cancelTime = getTimeStamp(rs, "CANCEL_TIME", 0);
                String description = rs.getString("DESCRIPTIONS");

                long lastMonFeeCharge = getTimeStamp(rs, "LAST_MONFEE_CHARGE_TIME", System.currentTimeMillis());
                long nextMonFeeCharge = getTimeStamp(rs, "NEXT_MONFEE_CHARGE_TIME", System.currentTimeMillis());
                int chargeStatus = rs.getInt("CHARGE_STATUS");
                int subType = rs.getInt("SUBS_TYPE");
                int allowDebFee = rs.getInt("DAY_DEBIT_ALLOWED");
                int isPromotion = rs.getInt("IS_PROMOTION");

                int monfeeWSID = rs.getInt("MONFEE_WS_ID");
                int registerWSID = rs.getInt("REGISTER_WS_ID");
                int cancelWSID = rs.getInt("CANCEL_WS_ID");
                String subServiceCode = rs.getString("SUB_SERVICE_CODE");
                int monfeeCount = rs.getInt("MONFEE_SUCCESS_COUNT");
                Timestamp lastNotify = rs.getTimestamp("LAST_SEND_NOTIFY");

                Subscriber sub = new Subscriber(msisdn, provider, serviceName, subServiceName, subServiceCode, description,
                        status, chargeStatus, subType, allowDebFee,
                        isPromotion, registerTime, cancelTime,
                        lastMonFeeCharge, nextMonFeeCharge, monfeeCount, lastNotify);
                sub.setMonfeeWSID(monfeeWSID);
                sub.setRegisterWSID(registerWSID);
                sub.setCancelWSID(cancelWSID);
                queue.putTask(sub);
            }
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR load sub for: " + subService);
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
            result = false;
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish for SUB SERVICES: " + subService + " -> " + queue.size() + " record(s) ("
                + (System.currentTimeMillis() - start) + " ms)");
        return result;
    }

    public void loadChargingSubsFromDB(ChargingSubsQueue queue) {
        if (queue == null) {
            MyLog.Debug(getThreadName() + "start loadChargingSubsFromDB for null, return now.");
            return;
        }
//        ChargingSubsQueue result = new ChargingSubsQueue();
        MyLog.Debug(getThreadName() + "start loadChargingSubsFromDB");
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "SELECT * FROM v_active_subs WHERE next_monfee_charge_time < ?";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30); // sec
//            s.setTimestamp(1, new java.sql.Timestamp(System.currentTimeMillis()));
            s.setTimestamp(1, new java.sql.Timestamp(PublicLibs.getFirstMomentOfToday() + 60000)); // add one Mins

            ResultSet rs = s.executeQuery();
            while (rs.next()) {
                String msisdn = rs.getString("MSISDN");
                String provider = rs.getString("PROVIDER_NAME");
                String serviceName = rs.getString("SERVICE_NAME");
                String subServiceName = rs.getString("SUB_SERVICE_NAME");
                int status = rs.getInt("STATUS");
                long registerTime = getTimeStamp(rs, "REGISTER_TIME", System.currentTimeMillis());
//                long registerTime = rs.getTimestamp("REGISTER_TIME").getTime();
                long cancelTime = getTimeStamp(rs, "CANCEL_TIME", 0);
                String description = rs.getString("DESCRIPTIONS");
//                long monFeeChargeTime = 0;
//                if (getTimeStamp(r, ""MONFEE_CHARGE_TIME") != null) {
//                    monFeeChargeTime = getTimeStamp(r, ""MONFEE_CHARGE_TIME").getTime();
//                }

                long lastMonFeeCharge = getTimeStamp(rs, "LAST_MONFEE_CHARGE_TIME", System.currentTimeMillis());
                long nextMonFeeCharge = getTimeStamp(rs, "NEXT_MONFEE_CHARGE_TIME", System.currentTimeMillis());
                int chargeStatus = rs.getInt("CHARGE_STATUS");
                int subType = rs.getInt("SUBS_TYPE");
                int allowDebFee = rs.getInt("DAY_DEBIT_ALLOWED");
                int isPromotion = rs.getInt("IS_PROMOTION");

                int monfeeWSID = rs.getInt("MONFEE_WS_ID");
                int registerWSID = rs.getInt("REGISTER_WS_ID");
                int cancelWSID = rs.getInt("CANCEL_WS_ID");
                String subServiceCode = rs.getString("SUB_SERVICE_CODE");
                int monfeeCount = rs.getInt("MONFEE_SUCCESS_COUNT");
                Timestamp lastNotify = rs.getTimestamp("LAST_SEND_NOTIFY");

                Subscriber sub = new Subscriber(msisdn, provider, serviceName, subServiceName, subServiceCode, description,
                        status, chargeStatus, subType, allowDebFee,
                        isPromotion, registerTime, cancelTime,
                        lastMonFeeCharge, nextMonFeeCharge, monfeeCount, lastNotify);
                sub.setMonfeeWSID(monfeeWSID);
                sub.setRegisterWSID(registerWSID);
                sub.setCancelWSID(cancelWSID);
                queue.putTask(sub);
            }
        } catch (SQLException e) {
            MyLog.Error(getThreadName() + "ERROR SQL" + e.getMessage());
            MyLog.Error(e);
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
            reconnect();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish loadChargingSubsFromDB -> " + queue.size() + " record(s) ("
                + (System.currentTimeMillis() - start) + " ms)");
//        return result;
    }

    /**
     * Insert log for subscription
     *
     * @param msisdn
     * @param subService
     * @param tranID
     * @param action
     * @param result
     * @param datetime
     */
    public void insertSubsLog(String msisdn, String subService, String tranID, String action, int result, long datetime) {
        MyLog.Debug(getThreadName() + "start insertSubsLog (" + msisdn + ", " + subService + ", " + action + ", Result: " + result + ")");
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        int count = 0;
        Connection conn = null;
        try {
            String strSQL = "INSERT INTO subscription_logs(MSISDN, SUB_SERVICE_NAME, TRANID, ACTION, RESULT, DATETIME) "
                    + "VALUES(?, ?, ?, ?, ?, ?)";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30);
            s.setString(1, msisdn);
            s.setString(2, subService);
            s.setString(3, tranID);
            s.setString(4, action);
            s.setInt(5, result);
            s.setTimestamp(6, new java.sql.Timestamp(datetime));

            count = s.executeUpdate();
        } catch (SQLException e) {
            MyLog.Error(getThreadName() + "ERROR SQL" + e.getMessage());
            MyLog.Error(e);
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
            reconnect();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish insertSubsLog (" + msisdn + ", " + subService + ", " + action + ", Result: " + result + ") -> "
                + count + " record(s) (" + (System.currentTimeMillis() - start) + " ms)");
    }

    public Map<String, CPService> getAllService() {
        MyLog.Debug(getThreadName() + "start getAllService");
        Map<String, CPService> result = new Hashtable<String, CPService>();
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "SELECT * FROM v_services";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30); // sec
            java.sql.ResultSet r = s.executeQuery();

            while (r.next()) {
                String providerName = r.getString("PROVIDER_NAME");
                String serviceName = r.getString("SERVICE_NAME");
                String serviceCode = r.getString("SERVICE_CODE");
                String shortCode = r.getString("SHORT_CODE");
                String sms = r.getString("DEFAULT_SMS");
                int price = r.getInt("DEFAULT_SMS_PRICE");
                String vtPrivateKey = r.getString("PRIVATE_VT_CP");
                String vtPublicKey = r.getString("PUBLIC_VT_CP");
                String cpPrivateKey = r.getString("PRIVATE_CP");
                String cpPublicKey = r.getString("PUBLIC_CP");
                String smsgwUrl = r.getString("SMSGW_URL");
                String smsgwUser = r.getString("SMSGW_USER");
                String smsgwPass = r.getString("SMSGW_PASSWORD");

                if (serviceName != null) {
                    CPService ser = new CPService(providerName, serviceCode, serviceName, shortCode, sms, price,
                            vtPublicKey, vtPrivateKey, cpPublicKey, cpPrivateKey,
                            smsgwUrl, smsgwUser, smsgwPass);
                    MyLog.Debug(getThreadName() + " Add Services " + ser);
                    result.put(serviceName, ser);
                }
            }
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR3" + ex.getMessage());
            MyLog.Error(ex);
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish getAllService -> " + result.size() + "("
                + (System.currentTimeMillis() - start) + " ms)");

        return result;
    }

    //hungtv45 add since 25 11 2015
    public CPService getCpService(String serviceName) {
        MyLog.Debug(getThreadName() + "start getCpService");
        CPService result = null;
        PreparedStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "SELECT * FROM v_services where SERVICE_NAME = ?";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            // Set timeout
            s.setString(1, serviceName);
            java.sql.ResultSet r = s.executeQuery();

            if (r.next()) {
                String providerName = r.getString("PROVIDER_NAME");
                String serviceCode = r.getString("SERVICE_CODE");
                String shortCode = r.getString("SHORT_CODE");
                String sms = r.getString("DEFAULT_SMS");
                int price = r.getInt("DEFAULT_SMS_PRICE");
                String vtPrivateKey = r.getString("PRIVATE_VT_CP");
                String vtPublicKey = r.getString("PUBLIC_VT_CP");
                String cpPrivateKey = r.getString("PRIVATE_CP");
                String cpPublicKey = r.getString("PUBLIC_CP");
                String smsgwUrl = r.getString("SMSGW_URL");
                String smsgwUser = r.getString("SMSGW_USER");
                String smsgwPass = r.getString("SMSGW_PASSWORD");

                result = new CPService(providerName, serviceCode, serviceName, shortCode, sms, price,
                        vtPublicKey, vtPrivateKey, cpPublicKey, cpPrivateKey,
                        smsgwUrl, smsgwUser, smsgwPass);
            }
            r.close();
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR3" + ex.getMessage());
            MyLog.Error(ex);
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish getCpService for serviceName: " + serviceName + "' -> " + result + "("
                + (System.currentTimeMillis() - start) + " ms)");

        return result;
    }

    /**
     * Get UssdCommand information
     *
     * @param typeCommand
     * @since 25 11 2015 hungtv45
     * @param subServiceName
     * @return
     */
    public UssdCommand getUssdCommand(String subServiceName, int typeCommand) {

        MyLog.Debug("start get UssdCommand for" + subServiceName);
        UssdCommand result = null;
        CallableStatement s = null;
        ResultSet r = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "SELECT * FROM USSD_COMMAND WHERE SUB_SERVICE_NAME=? AND TYPE = ?";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30);
            s.setString(1, subServiceName);
            s.setInt(2, typeCommand);
            r = s.executeQuery();
            if (r.next()) {
                String id = r.getString("ID");
                String description = r.getString("DESCRIPTION");
                String successMessage = r.getString("SUCCESS_MESSAGE");
                String failMessage = r.getString("FAILED_MESSAGE");
                String notEnoughMoney = r.getString("NOT_ENOUGH_MONEY_MESSAGE");
                String chargeCommand = r.getString("CHARGE_CMD");
                String confirmMessage = r.getString("CONFIRM_MESSAGE");
                String alreadyRegisterMessage = r.getString("ALREADY_REGISTER_MESSAGE");
                String notRegisterMessage = r.getString("NOT_REGISTER_MESSAGE");
                int status = r.getInt("STATUS");
                int type = r.getInt("TYPE");
                int webServiceID = r.getInt("WEBSERVICE_ID");
                int moPrice = r.getInt("MO_PRICE");
                int requiredRegisted = r.getInt("REQUIRED_REGISTED");
                int checkRequired = r.getInt("CHECK_REQUIRED");
                int checkPrice = r.getInt("CHECK_PRICE");
                int messageChannel = r.getInt("MESSAGE_CHANNEL");
                int cmdPrice = r.getInt("CMD_PRICE");
                result = new UssdCommand(id, subServiceName, description, successMessage, failMessage, notEnoughMoney,
                        chargeCommand, status, type, webServiceID,
                        moPrice, requiredRegisted, checkRequired,
                        checkPrice, messageChannel, confirmMessage, alreadyRegisterMessage, notRegisterMessage, cmdPrice);
            }
//            r.close();
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR " + ex.getMessage());
            MyLog.Error(ex);
            result = null;
        } finally {
            if (r != null) {
                try {
                    r.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DirectConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish getSubScription for " + subServiceName + "' -> " + result + "("
                + (System.currentTimeMillis() - start) + " ms)");
        return result;
    }

    /**
     * Get SubService information
     *
     * @since 25 11 2015 hungtv45
     * @param subServiceName
     * @return
     */
    public SubService getSubService(String subServiceName) {

        MyLog.Debug(getThreadName() + "start getAllSubService");
        SubService result = null;
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "SELECT * FROM V_SUB_SERVICES WHERE status=1 AND IS_SUBSCRIPTIONS=1 AND SUB_SERVICE_NAME = ?";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30);
            s.setString(1, subServiceName);
            ResultSet r = s.executeQuery();
            while (r.next()) {
                String subServiceCode = r.getString("SUB_SERVICE_CODE");
                String description = r.getString("DESCRIPTIONS");
                String serviceName = r.getString("SERVICE_NAME");
                int isSubscription = r.getInt("IS_SUBSCRIPTIONS");
                int status = r.getInt("STATUS");
                int fixTimeChargeMonFee = r.getInt("FIX_TIME_CHARGE_MONFEE");
                String groupChargeType = r.getString("GROUP_CHARGE_TYPE");
                int subType = r.getInt("SUBS_TYPE");
                String subFee = r.getString("SUBS_PRICE");
                String promoFee = r.getString("PROMO_PRICE");
                long promoDateBegin = getTimeStamp(r, "PROMO_DATETIME_BEGIN", 0);
                long promoDateEnd = getTimeStamp(r, "PROMO_DATETIME_END", 0);
                int dayAllowDebit = r.getInt("DAY_DEBIT_ALLOWED");
                String monfeeHour = r.getString("MONFEE_HOUR");
                int stopOnError = r.getInt("STOP_ON_ERROR");
                int checkConflict = r.getInt("CHECK_CONFLICT");
                int requireMPSUser = r.getInt("REQUIRE_MPS_USER");
                int recheckBeforeMonfee = r.getInt("RECHECK_BEFORE_MONFEE");
                int monfeeWSId = r.getInt("MONFEE_WS_ID");
                int registerWSId = r.getInt("REGISTER_WS_ID");
                int cancelWSId = r.getInt("CANCEL_WS_ID");
                String errorMessage = r.getString("DEFAULT_ERR_MSG");
                int autoCancel = r.getInt("AUTO_CANCEL_CONFILICT");
                int sendNotify = r.getInt("SEND_NOTIFY");
                String notifyContent = r.getString("NOTIFY_CONTENT");
                String softName = r.getString("SOFT_NAME");
                int is_promo = r.getInt("IS_PROMO");
                result = new SubService(subServiceCode, subServiceName, description, serviceName,
                        groupChargeType, subFee, promoFee, isSubscription, status,
                        fixTimeChargeMonFee, subType, dayAllowDebit,
                        promoDateBegin, promoDateEnd, PublicLibs.convertHashSetInt(monfeeHour),
                        stopOnError, checkConflict, requireMPSUser, recheckBeforeMonfee,
                        monfeeWSId, registerWSId, cancelWSId,
                        errorMessage, autoCancel == SubService.AUTO_CANCEL_TRUE, sendNotify, notifyContent, softName);
                result.setPromotion(is_promo);
            }
            r.close();
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }
        MyLog.Infor(getThreadName() + "Finish getSubScription for " + subServiceName + "' -> " + result + "("
                + (System.currentTimeMillis() - start) + " ms)");

        return result;

    }

    public Subscriber checkConflict(String msisdn, String service, String currentSubService) {
        MyLog.Debug(getThreadName() + "start checkConflict for '" + msisdn + "' - '" + service + "'");
        Subscriber result = null;
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "SELECT * FROM v_service_subs "
                    + "WHERE msisdn=? "
                    + "AND (status=1 OR status=2) " + // Active or pending
                    "AND sub_service_name IN ("
                    + "SELECT sub_service_name FROM V_SUB_SERVICES "
                    + "WHERE service_name=? "
                    + "AND check_conflict=1 "
                    + "AND status=1 "
                    + "AND sub_service_name <> ? "
                    + ")";

            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);            
            s.setQueryTimeout(30); // sec
            s.setString(1, msisdn);
            s.setString(2, service);
            s.setString(3, currentSubService);

            ResultSet r = s.executeQuery();
            if (r.next()) {
//                String msisdn = r.getString("MSISDN");
                String subServiceName = r.getString("SUB_SERVICE_NAME");
                String provider = r.getString("PROVIDER_NAME");
                String serviceName = r.getString("SERVICE_NAME");
                int status = r.getInt("STATUS");
                long registerTime = getTimeStamp(r, "REGISTER_TIME", 0);
                long cancelTime = getTimeStamp(r, "CANCEL_TIME", 0);
                String description = r.getString("DESCRIPTIONS");
//                long monFeeChargeTime = 0;
//                if (getTimeStamp(r, ""MONFEE_CHARGE_TIME") != null) {
//                    monFeeChargeTime = getTimeStamp(r, ""MONFEE_CHARGE_TIME").getTime();
//                }

                long lastMonFeeCharge = getTimeStamp(r, "LAST_MONFEE_CHARGE_TIME", 0);
                long nextMonFeeCharge = getTimeStamp(r, "NEXT_MONFEE_CHARGE_TIME", 0);
                int chargeStatus = r.getInt("CHARGE_STATUS");
                int subType = r.getInt("SUBS_TYPE");
                int allowDebFee = r.getInt("DAY_DEBIT_ALLOWED");
                int isPromotion = r.getInt("IS_PROMOTION");
                String subServiceCode = r.getString("SUB_SERVICE_CODE");
                int monfeeCount = r.getInt("MONFEE_SUCCESS_COUNT");
                Timestamp lastNotify = r.getTimestamp("LAST_SEND_NOTIFY");

                result = new Subscriber(msisdn, provider, serviceName, subServiceName, subServiceCode, description,
                        status, chargeStatus, subType, allowDebFee,
                        isPromotion, registerTime, cancelTime,
                        lastMonFeeCharge, nextMonFeeCharge, monfeeCount, lastNotify);
            }
            r.close();
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR " + ex.getMessage());
            MyLog.Error(ex);
            result = null;
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish checkConflict for '" + msisdn + "' - '" + service + "' -> " + result + "("
                + (System.currentTimeMillis() - start) + " ms)");

        return result;
    }

    public String getMessage(String subService, String cmd, String key) {
        String name = "(Sub: " + subService + "; CMD: " + cmd + "; Key: " + key + ")";
        MyLog.Debug(getThreadName() + "start getMessage for " + name);
        String result = null;
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "SELECT content FROM mo_reply_content "
                    + "WHERE sub_service_name=? "
                    + "AND CMD=? " + // Active or pending
                    "AND KEY=?";

            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            // Set timeout
            s.setQueryTimeout(30); // sec
            // -----------------
            s.setString(1, subService);
            s.setString(2, cmd);
            s.setString(3, key);

            ResultSet r = s.executeQuery();
            if (r.next()) {
                result = r.getString("CONTENT");
            }
            r.close();
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR " + ex.getMessage());
            MyLog.Error(ex);
            result = null;
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish getMessage for " + name + " -> " + result + "("
                + (System.currentTimeMillis() - start) + " ms)");

        return result;
    }

    public void loadListFromDB(SyncQueue queue) {
        if (queue == null || queue.size() > 0) {
            MyLog.Debug(getThreadName() + "start loadListFromDB null or Queue is not Empty, return now.");
            return;
        }

        MyLog.Debug(getThreadName() + "start loadListFromDB");
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "SELECT msisdn, sub_service_name, status, register_time "
                    + "FROM v_active_subs "
                    + //"WHERE msisdn in ('983929641', '963768900', '985888280', '963700099') " + // Test
                    "ORDER BY msisdn, register_time";

            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            // Set timeout
            s.setQueryTimeout(120); // sec
            ResultSet rs = s.executeQuery();

            String msisdn = null;
            Hashtable<String, RegisteredSubServices> registered = new Hashtable<String, RegisteredSubServices>();

            while (rs.next()) {
                String newMsisdn = rs.getString("MSISDN");
                String subServiceName = rs.getString("SUB_SERVICE_NAME");
                int status = rs.getInt("STATUS");
                long registerTime = getTimeStamp(rs, "REGISTER_TIME", System.currentTimeMillis());

                if (msisdn == null || !msisdn.equals(newMsisdn)) {
                    // new
                    if (msisdn != null) {
                        SyncTask task = new SyncTask(msisdn, registered);
                        MyLog.Debug("Add SyncTask " + task);
                        queue.putTask(task);
                    }

                    msisdn = newMsisdn;
                    registered = new Hashtable<String, RegisteredSubServices>();
                }
                // add
                registered.put(subServiceName, new RegisteredSubServices(subServiceName, status, registerTime));
            }
            rs.close();

        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish loadListFromDB -> " + queue.size() + " record(s) ("
                + (System.currentTimeMillis() - start) + " ms)");
    }

    public String getCountType(int type) {
        switch (type) {
            case DBConst.RATE_TYPE_ALL:
                return "ALL";
            case DBConst.RATE_TYPE_FREE:
                return "FREE";
            case DBConst.RATE_TYPE_NOT_FREE:
                return "NOT FREE";
        }

        return "UNKNOW";
    }

    public int getRateCount(String msisdn, String countKey, int countDay, int countType) {
        String log = "(MSISDN: " + msisdn + "; Key: " + countKey + "; Count Day: " + countDay
                + "; CountType: " + getCountType(countType) + ")";
        MyLog.Debug(getThreadName() + "start getRateCount for " + log);
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        int result = 0;
        try {
            String strSQL = "SELECT count(*) "
                    + "FROM v_mo_rate "
                    + "WHERE msisdn = ? "
                    + "AND COUNT_KEY = ? ";
            if (countType == DBConst.RATE_TYPE_FREE) {
                strSQL += "AND is_free = 0 ";
            } else if (countType == DBConst.RATE_TYPE_NOT_FREE) {
                strSQL += "AND is_free = 1 ";
            }

            if (countDay > 1) {
                strSQL += "AND datetime >= trunc(sysdate - " + (countDay - 1);
            } else {
                strSQL += "AND datetime >= trunc(sysdate)";
            }

            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setString(1, msisdn);
            s.setString(2, countKey);
            // Set timeout
            s.setQueryTimeout(120); // sec
            ResultSet rs = s.executeQuery();
            if (rs.next()) {
                result = rs.getInt(1);
            }
            rs.close();
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish getRateCount for " + log + " -> " + result + " ("
                + (System.currentTimeMillis() - start) + " ms)");
        return result;
    }

    public void insertRateCount(String msisdn, String countKey, String moContent, boolean isFree) {
        String log = "(MSISDN: " + msisdn + "; Key: " + countKey + "; Content: " + moContent + "; IsFree: " + isFree + ")";
        MyLog.Debug(getThreadName() + "start insertRateCount for " + log);
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "begin pr_MO_RATE_I(?, ?, ?, ?, sysdate); end;";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            // Set timeout
            s.setQueryTimeout(30);
            s.setString(1, msisdn);
            s.setString(2, countKey);
            s.setString(3, moContent);
            s.setInt(4, (isFree ? DBConst.RATE_FREE : DBConst.RATE_NOT_FREE));

            s.executeUpdate();
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish insertRateCount for " + log + " ("
                + (System.currentTimeMillis() - start) + " ms)");
    }

    public int updateConfiguration(String key, String value) {
        MyLog.Debug(getThreadName() + "start updateConfiguration for (" + key + "; " + value + ")");
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        int result = 2;
        Connection conn = null;
        try {
            String strSQL = "begin PR_SYSTEM_CONFIGURATIONS_IU(?, ?, ?); end;";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setString(1, key);
            s.setString(2, value);

            s.registerOutParameter(3, java.sql.Types.INTEGER);
            s.execute();
            result = s.getInt(3);
        } catch (SQLException e) {
            MyLog.Error(getThreadName() + "ERROR " + e.getMessage());
            MyLog.Error(e);
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR " + ex.getMessage());
            MyLog.Error(ex);
            reconnect();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish updateConfiguration for (" + key + "; " + value + ") -> (" + result + ") ("
                + (System.currentTimeMillis() - start) + " ms)");
        return result;
    }

    /**
     * Get Configuration from DB.
     *
     * @param table
     * @return value
     */
    public Map<String, String> getConfiguration(final String table) {
        long start = System.currentTimeMillis();
        CallableStatement s = null;
        Map<String, String> result = new Hashtable();
        Connection conn = null;
        try {
            String strSQL = "SELECT * FROM " + table;
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);

            ResultSet r = s.executeQuery();
            while (r.next()) {
                String k = r.getString("KEY");
                String v = r.getString("VALUE");
                MyLog.Debug(getThreadName() + "Got (key: " + k + "; Value: " + v);
                result.put(k, v);
            }
            r.close();
        } catch (SQLException e) {
            MyLog.Error(getThreadName() + "get configurations from " + table + " error: " + e.getMessage());
            MyLog.Error(e);
            result.clear();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception e) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Debug(getThreadName() + "Finish get configurations from " + table + " -> " + result.size()
                + " (" + (System.currentTimeMillis() - start) + " ms)");
        return result;
    }

    public int getAirtimeConsume(String msisdn) {
        String log = msisdn;
        MyLog.Debug(getThreadName() + "start getAirtimeConsume for " + log);
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        int result = 0;
        try {
            String strSQL = "SELECT * "
                    + "FROM SUBSCRIBERS_CONSUME_ONLINE "
                    + "WHERE msisdn = ?";

            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setString(1, msisdn);
            // Set timeout
            s.setQueryTimeout(120); // sec
            ResultSet rs = s.executeQuery();
            if (rs.next()) {
                result = rs.getInt("MOBILE");
            }
            rs.close();
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish getAirtimeConsume for " + log + " -> " + result + " ("
                + (System.currentTimeMillis() - start) + " ms)");
        return result;
    }

    public CPService getCpServiceBySubSerivceName(String subServiceName) {
        MyLog.Debug(getThreadName() + "start getCpServiceBySubSerivceName: " + subServiceName);
        CPService result = null;
        PreparedStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "SELECT a.* FROM v_services a  "
                    + "where a.SERVICE_NAME in "
                    + " (select b.Service_Name from sub_services b where b.sub_service_name = ?)";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            // Set timeout
            s.setString(1, subServiceName);
            java.sql.ResultSet r = s.executeQuery();

            if (r.next()) {
                String providerName = r.getString("PROVIDER_NAME");
                String serviceCode = r.getString("SERVICE_CODE");
                String serviceName = r.getString("SERVICE_NAME");
                String shortCode = r.getString("SHORT_CODE");
                String sms = r.getString("DEFAULT_SMS");
                int price = r.getInt("DEFAULT_SMS_PRICE");
                String vtPrivateKey = r.getString("PRIVATE_VT_CP");
                String vtPublicKey = r.getString("PUBLIC_VT_CP");
                String cpPrivateKey = r.getString("PRIVATE_CP");
                String cpPublicKey = r.getString("PUBLIC_CP");
                String smsgwUrl = r.getString("SMSGW_URL");
                String smsgwUser = r.getString("SMSGW_USER");
                String smsgwPass = r.getString("SMSGW_PASSWORD");

                result = new CPService(providerName, serviceCode, serviceName, shortCode, sms, price,
                        vtPublicKey, vtPrivateKey, cpPublicKey, cpPrivateKey,
                        smsgwUrl, smsgwUser, smsgwPass);
            }
            r.close();
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR3" + ex.getMessage());
            MyLog.Error(ex);
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish service for sub_service: " + subServiceName + "' -> " + result + "("
                + (System.currentTimeMillis() - start) + " ms)");

        return result;
    }

    public MOCommand getMOCommand(String subServiceName, int type) {
        MOCommand command = null;
        PreparedStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "SELECT * FROM v_mo_command where SUB_SERVICE_NAME =? and type = ? ORDER BY short_code";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setString(1, subServiceName);
            s.setInt(2, type);
            ResultSet r = s.executeQuery();

            if (r.next()) {
                String cmd = r.getString("CMD");
                String successMessage = r.getString("SUCCESS_MESSAGE");
                String failedMessage = r.getString("FAILED_MESSAGE");
                String secondFailedMessage = r.getString("FAILED_MESSAGE_2");
                String notEnoughMoney = r.getString("NOT_ENOUGH_MONEY_MESSAGE");
                int fee = r.getInt("FEE");
                int requireRegisted = r.getInt("REQUIRED_REGISTED");
                String serviceName = r.getString("SERVICE_NAME");
                String serviceCode = r.getString("SUB_SERVICE_CODE");
                String shortCode = r.getString("SHORT_CODE");
                int webserviceID;
                try {
                    webserviceID = r.getInt("WEBSERVICE_ID");
                } catch (Exception ex) {
                    webserviceID = -1;
                }
                String chargeCMD = r.getString("CHARGE_CMD");
                if (chargeCMD == null || chargeCMD.isEmpty()) {
                    chargeCMD = "CHARGE"; // default;
                }

                int checkRequired = r.getInt("CHECK_REQUIRED");
                int checkPrice = r.getInt("CHECK_PRICE");
                int freeSMS = r.getInt("NUMBER_FREE_SMS");
                String countKey = r.getString("COUNT_KEY");
                int countDay = r.getInt("COUNT_DAY");
                int limitCount = r.getInt("LIMIT_COUNT");

                command = new MOCommand(cmd, subServiceName, successMessage, failedMessage, secondFailedMessage, notEnoughMoney,
                        serviceName, serviceCode, shortCode, type, fee, requireRegisted, webserviceID, chargeCMD,
                        checkRequired, checkPrice, freeSMS, limitCount, countDay, countKey);

            }
            r.close();
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor("Finish getAllMOCommand -> " + subServiceName + "(" + (System.currentTimeMillis() - start) + " ms)");
        return command;
    }

    public IvrCommand getIvrCommand(String subServiceName, int type) {
        MyLog.Debug("start get UssdCommand for" + subServiceName);
        IvrCommand result = null;
        PreparedStatement s = null;
        ResultSet r = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "SELECT * FROM IVR_COMMAND WHERE SUB_SERVICE_NAME=? AND TYPE = ?";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            s.setQueryTimeout(30);
            s.setString(1, subServiceName);
            s.setInt(2, type);
            r = s.executeQuery();
            if (r.next()) {
                String id = r.getString("ID");
                String description = r.getString("DESCRIPTION");
                String successMessage = r.getString("SUCCESS_MESSAGE");
                String failMessage = r.getString("FAILED_MESSAGE");
                String notEnoughMoney = r.getString("NOT_ENOUGH_MONEY_MESSAGE");
                String chargeCommand = r.getString("CHARGE_CMD");
                String confirmMessage = r.getString("CONFIRM_MESSAGE");
                String alreadyRegisterMessage = r.getString("ALREADY_REGISTER_MESSAGE");
                String notRegisterMessage = r.getString("NOT_REGISTER_MESSAGE");
                int status = r.getInt("STATUS");
                int webServiceID = r.getInt("WEBSERVICE_ID");
                int moPrice = r.getInt("MO_PRICE");
                int requiredRegisted = r.getInt("REQUIRED_REGISTED");
                int checkRequired = r.getInt("CHECK_REQUIRED");
                int checkPrice = r.getInt("CHECK_PRICE");
                int messageChannel = r.getInt("MESSAGE_CHANNEL");
                int cmdPrice = r.getInt("CMD_PRICE");
                result = new IvrCommand(id, subServiceName, description, successMessage, failMessage, notEnoughMoney,
                        chargeCommand, status, type, webServiceID,
                        moPrice, requiredRegisted, checkRequired,
                        checkPrice, messageChannel, confirmMessage, alreadyRegisterMessage, notRegisterMessage, cmdPrice);
            }
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR " + ex.getMessage());
            MyLog.Error(ex);
            result = null;
        } finally {
            if (r != null) {
                try {
                    r.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DirectConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "Finish getSubScription for " + subServiceName + "' -> " + result + "("
                + (System.currentTimeMillis() - start) + " ms)");
        return result;
    }
    
    
    public List<PromotionCommand> getCurrentPromotioncommands(String sub_service_name){
        long start = System.currentTimeMillis();
        Connection conn = null;
        List<PromotionCommand> result = new ArrayList<PromotionCommand>();
        CallableStatement calStat = null;
        ResultSet rs = null;
        try {
            conn = getDatabaseConnection();
            if (conn == null) {
                MyLog.Error("can not connect to database");
                return null;
            }
            
            String sql = "select * FROM promotion_command ";
            sql += " WHERE status = 1 and sub_service_name=? ";
            sql += " and from_date <= sysdate ";
            sql += " and (to_date is null or sysdate < to_date) ";
            sql += " order by created desc ";
            
            calStat = conn.prepareCall(sql);
            calStat.setString(1, sub_service_name);
            calStat.setQueryTimeout(30);
            rs = calStat.executeQuery();
            while (rs.next()) {
                PromotionCommand promotionBO = new PromotionCommand();
                promotionBO.setStatus(rs.getInt("Status"));
                promotionBO.setDescription(rs.getString("Description"));
                promotionBO.setSubServiceName(rs.getString("SUB_SERVICE_NAME"));
                
                promotionBO.setType(rs.getString("Type"));
                promotionBO.setSuccessMessage(rs.getString("Success_message"));
                promotionBO.setFailMessage(rs.getString("Failed_message"));
                promotionBO.setWebserviceId(rs.getInt("Webservice_id"));
                
                promotionBO.setChargeCommand(rs.getString("Charge_cmd"));
                promotionBO.setCheckRequired(rs.getInt("Check_required"));
                promotionBO.setCmdPrice(rs.getInt("Cmd_price"));
                promotionBO.setMessageChannel(rs.getInt("Message_channel"));
                promotionBO.setId(rs.getString("ID"));
                
                promotionBO.setFromDate(rs.getDate("FROM_DATE"));
                promotionBO.setToDate(rs.getDate("TO_DATE"));
                promotionBO.setCode(rs.getString("CODE"));
                promotionBO.setFreeDay(rs.getInt("FREE_DAY"));
                promotionBO.setPromotionChannel(rs.getString("Promotion_Channel"));
                
                result.add(promotionBO);
            }
            
        } catch (Exception ex) {
             MyLog.Error(ex);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (calStat != null) {
                    calStat.close();
                }
            } catch (Exception ee) {
            }
            closeConnection(conn);
        }
        MyLog.Infor(getThreadName() + "Finish PromotionCommand for " + sub_service_name + "' -> " + result.size() + "("
                + (System.currentTimeMillis() - start) + " ms)");
        return result;
    }

    public CPWebservice getCPWebService(String webserviceName) {
        MyLog.Debug(getThreadName() + "start getCPWebService (webservice_name: " + webserviceName + ")");
        CPWebservice result = null;
        CallableStatement s = null;
        long start = System.currentTimeMillis();
        Connection conn = null;
        try {
            String strSQL = "SELECT * FROM cp_webservice_accounts WHERE webservice_name = ?";
            conn = getDatabaseConnection();
            s = conn.prepareCall(strSQL);
            // Set timeout
            s.setQueryTimeout(30);
            s.setString(1, webserviceName);
            ResultSet r = s.executeQuery();
            if (r.next()) {
                String subServiceName = r.getString("SUB_SERVICE_NAME");
                String username = r.getString("USER_NAME");
                String password = r.getString("PASSWORD");
                String url = r.getString("URL");
                String params = r.getString("PARAMS");
                String wsType = r.getString("WEBSERVICE_TYPE");
                String soapAction = r.getString("SOAP_ACTION");
                String tagPrefix = r.getString("TAG_PREFIX");
                String rawXML = r.getString("RAW_XML");
                String returnTag = r.getString("RETURN_TAG");
                int wsId = r.getInt("WEBSERVICE_ID");
                int timeout = r.getInt("TIMEOUT");
                String signal = r.getString("DATA_SIGN");
                result = new CPWebservice(wsId, subServiceName, username, password, url, params,
                        wsType, soapAction, tagPrefix, rawXML, returnTag, timeout, signal);
            }
            r.close();
        } catch (SQLException e) {
            MyLog.Error(getThreadName() + "ERROR" + e.getMessage());
            MyLog.Error(e);
        } catch (Exception ex) {
            MyLog.Error(getThreadName() + "ERROR" + ex.getMessage());
            MyLog.Error(ex);
            reconnect();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (Exception ee) {
                }
            }
            closeConnection(conn);
        }

        MyLog.Infor(getThreadName() + "start get getSubService for (webservice_name: " 
                + webserviceName + ") -> " + result + "("
                + (System.currentTimeMillis() - start) + " ms)");

        return result;
    }
}
