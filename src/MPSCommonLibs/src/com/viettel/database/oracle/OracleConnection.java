/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.database.oracle;

import com.viettel.database.datatype.CPService;
import com.viettel.database.datatype.CPWebservice;
import com.viettel.database.datatype.ChargingSubsQueue;
import com.viettel.database.datatype.IvrCommand;
import com.viettel.database.datatype.MOCommand;
import com.viettel.database.datatype.MPSUser;
import com.viettel.database.datatype.PriceCharge;
import com.viettel.database.datatype.PromotionCommand;
import com.viettel.database.datatype.SubService;
import com.viettel.database.datatype.Subscriber;
import com.viettel.database.datatype.SyncQueue;
import com.viettel.database.datatype.UssdCommand;
import com.viettel.hlr.HLR;
import com.viettel.utilities.MyLog;
import com.viettel.utilities.TextSecurity;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import javax.sql.DataSource;

/**
 *
 * @author hoand
 */
public class OracleConnection {
    private int mId;
    private DirectConnection mDirectConnection;
    private TimestenConnection mTimestenConnection;

    private String mTimestenDSN,
                mTimestenUSER,
                mTimestenPASS,
                mTimestenOraclePass;

    private String mOraclePoolFile;
    private HLR mHlr;

    public OracleConnection(int number, String confFile) {
        mId = number;

        MyLog.Infor(showVersion());
        try {
            Properties prop = new Properties();
            FileInputStream gencfgFile = new FileInputStream(confFile);
            prop.load(gencfgFile);

            // Oracle DB
            mOraclePoolFile = prop.getProperty("connection_file");
            MyLog.Infor("Thread " + mId + ": Connection Pool: '" + mOraclePoolFile + "'");
            // Oracle
            mDirectConnection = new DirectConnection(mId, mOraclePoolFile);
            mDirectConnection.startConnect();
            
            // Thang PM TimesTen
            mTimestenDSN = prop.getProperty("mTimestenDSN");
            if (mTimestenDSN != null && !mTimestenDSN.trim().isEmpty()) {
                mTimestenUSER = prop.getProperty("mTimestenUSER");
                mTimestenPASS = prop.getProperty("mTimestenPASS");
                mTimestenOraclePass = prop.getProperty("mTimestenOraclePass");

                MyLog.Infor("Thread " + mId + ": Oracle Timesten: " + 
                                                     mTimestenDSN + ":" +
                                                     mTimestenUSER + ":" +
                                                     mTimestenPASS + ":" +
                                                     mTimestenOraclePass);
                // Decrypt pass
                TextSecurity sec = TextSecurity.getInstance();
                mTimestenPASS = sec.Decrypt(mTimestenPASS);
                mTimestenOraclePass = sec.Decrypt(mTimestenOraclePass);

                try {
                    MyLog.Infor("Thread " + mId + ": init Timesten ... ");
                    mTimestenConnection = new TimestenConnection(mId, mTimestenDSN, mTimestenUSER, mTimestenPASS, mTimestenOraclePass);
                    mTimestenConnection.connect();
                    MyLog.Infor("Thread " + mId + ": done.");
                } catch (Exception ex) {
                    MyLog.Error("Thread " + mId + ": init importer failed (" + ex.getMessage() + ")");
                    mTimestenConnection = null;
                }
            }
            
            // HLR Configuration
//            String hlrConfig = prop.getProperty("hlrconf");
//            MyLog.Infor("HLR Confing file: " + hlrConfig);
//            mHlr = HLR.getInstance();
//            mHlr.setConfigFile(hlrConfig);

            gencfgFile.close();
        } catch (Exception e) {
            MyLog.Error("Thread " + mId + ": can not init Oracle Connection.");
            MyLog.Error(e);
        }
    }
    
    public OracleConnection(DataSource dataSource) {
        mDirectConnection = new DirectConnection(dataSource);
    }
    
    public void setDataSource(DataSource dataSource) {
        mDirectConnection.setDataSource(dataSource);
    }
    
    public DataSource getDataSource() {
        return mDirectConnection.getDataSource();
    }
    
    public String showVersion() {
        StringBuilder result = new StringBuilder();
        
        try {
            InputStream is = getClass().getResourceAsStream("version.properties");
            Properties p = new Properties();
            p.load(is);

            Enumeration<Object> key = p.keys();
            result.append("MPSCommonLibs Build Information: \n");
            while (key.hasMoreElements()) {
                String k = (String) key.nextElement();
                result.append(k).append(": ").append(p.getProperty(k)).append("\n");
            }
            is.close();
        } catch (Exception ex) {
            
        }
        
        return result.toString();
    }
    
    public boolean isConnected() {
        return mDirectConnection.isConnected();
    }
    
    public CallableStatement getTimestenStatement(String strSql) {
        return mTimestenConnection.getTimestenStatement(strSql);
    }
    
    public CallableStatement getDatabaseStatement(String strSql) {
        return mDirectConnection.getStatement(strSql);
    }
    
    public Connection getTimestenConnection() {
        return mTimestenConnection.getTimestenConnection();
    }
    
    public Connection getDatabaseConnection() {
        return mDirectConnection.getDatabaseConnection();
    }
    
    public boolean executeDBUpdate(String strSQL) {
        return mDirectConnection.excuteUpdate(strSQL);
    }
    
//    public MOCommand getMOCommand(String shortCode, String sms) {
//        return mDirectConnection.getMOCommand(shortCode, sms);
//    }
    
    /**
     * Get price charge
     * @param shortCode
     * @param subService
     * @param cmd
     * @param type
     * @param category
     * @param msisdn
     * @return 
     */
    public PriceCharge getPriceForCMD(String shortCode, String subService, String cmd, 
            int type, String category, String msisdn) {
        return mDirectConnection.getPriceForCMD(shortCode, subService, cmd, type, category, msisdn);
    }
    
   
    /**
     * Get Subscription information
     * @param msisdn
     * @param subServiceName
     * @return 
     */
    public Subscriber getSubscriber(String msisdn, String subServiceName) {
        return mDirectConnection.getSubscriber(msisdn, subServiceName);
    }
    
    /**
     * Check User register service or not
     * @param msisdn Phone Number
     * @param subService SubService name
     * @return 
     */
    public boolean isRegisteredSubService(String msisdn, String subService) {
        return mDirectConnection.isRegisteredSubService(msisdn, subService);
    }
    
    /**
     * Get webservice to call
     * @param wsId
     * @return 
     */
    public CPWebservice getCPWebService(int wsId) {
        return mDirectConnection.getCPWebService(wsId);
    }
    @Deprecated
    public String getPromoSubSerive(String subService){
        return mDirectConnection.getPromoSubSerive(subService);
    }
    
    @Deprecated
    public boolean checkSubRegisterFirst(String subService, String msisdn){
        return mDirectConnection.checkSubRegisterFirst(subService,msisdn);
    }
    
    /**
     * Insert MO Log to DB
     * @param subService
     * @param shortCode
     * @param msisdn
     * @param content
     * @param tranID 
     */
    public void insertMO(String subService, String shortCode, String msisdn, String content, String tranID) {
        mDirectConnection.insertSMS(subService, shortCode, msisdn, content, tranID, false);
    }
    
    /**
     * Insert MT Log to DB
     * @param subService
     * @param shortCode
     * @param msisdn
     * @param content
     * @param tranID 
     */
    public void insertMT(String subService, String shortCode, String msisdn, String content, String tranID) {
        mDirectConnection.insertSMS(subService, shortCode, msisdn, content, tranID, true);
    }
    
    /**
     * Add transaction to log
     * @param msisdn
     * @param service
     * @param subService
     * @param cmd
     * @param content
     * @param price
     * @param tranID 
     */
    public void addTransactionLog(String msisdn, String service, String subService, String cmd, String content, 
                                   int price, String tranID) {
        mDirectConnection.addTransactionLog(msisdn, service, subService, cmd, content, price, tranID);
    }
    
    /**
     * Load all subscriber need to be charged 
     * @param queue
     * @param subServices
     * @return 
     */
//    public void loadChargingSubsFromDB(ChargingSubsQueue queue) {
//        mDirectConnection.loadChargingSubsFromDB(queue);
//    }
    
    public boolean loadChargingSubsFromDB(ChargingSubsQueue queue, String subServices) {
        return mDirectConnection.loadChargingSubsFromDB(queue, subServices);
    }
    
    /**
     * Get Monfee price for subscriber
     * @param msisdn
     * @param serviceName
     * @param subServiceName
     * @return 
     */
    public PriceCharge getMonfeePrice(String msisdn, String serviceName, String subServiceName) {
        return mDirectConnection.getMonfeePrice(msisdn, serviceName, subServiceName);
    }
    
    /**
     * Get additional date for Service
     * @param msisdn
     * @param subType
     * @param subService
     * @param service
     * @return 
     */
    public long getAdditonalDateWS(String msisdn, int subType, String subService, String service) {
        return mDirectConnection.getAdditonalRegisterDateWS(msisdn, subType, subService, service);
    }
    
    public long getAdditonalRegisterDate(String msisdn, int subType, String subService, String service,
                                         String cmd, String cat, String shortCode, int cmdType) {
        return mDirectConnection.getAdditonalRegisterDate(msisdn, subType, subService, service, cmd, cat, shortCode, cmdType);
    }
    
    /**
     * Get additional day for user with charged monfee success
     * @param msisdn
     * @param subType
     * @param subService
     * @param service
     * @return 
     */
    public long getAdditonalDateMonfee(String msisdn, int subType, String subService, String service) {
        return mDirectConnection.getAdditonalDateMonfee(msisdn, subType, subService, service);
    }
    
    /**
     * Update Subscriber for Sub Service
     * @param sub ServiceSubscription
     * @param retryTime
     * @return boolean
     */
    public boolean updateSubscriber(Subscriber sub, int retryTime) {
        boolean result = false;
        
        if (retryTime <=0) {
            retryTime = 1;
        }
        
        for (int i=0; i<retryTime; i++) {
            MyLog.Debug("updateSubscriber (Retry: " + i + ") " + sub);
            result = mDirectConnection.updateSubscriber(sub);
            if (result) {
                break;
            }
        }
        
        return result;
    }
    
    /**
     * Insert log for subscription
     * @param msisdn
     * @param subService
     * @param tranID
     * @param action
     * @param result
     * @param datetime 
     */
    public void insertSubsLog(String msisdn, String subService, String tranID, String action, int result, long datetime) {
        mDirectConnection.insertSubsLog(msisdn, subService, tranID, action, result, datetime);
    }
    
    /**
     * Get SubService Information
     * @param subServiceName
     * @return 
     */
//    public SubService getSubService(String subServiceName) {
//        return mDirectConnection.getSubService(subServiceName);
//    }
    
    /**
     * Get MPS User information
     * @param msisdn
     * @return 
     */
    public MPSUser getMPSUser(String msisdn) {
        return mDirectConnection.getMPSUser(msisdn);
    }
    
    /**
     * Update MPS User
     * @param user 
     */
    public void updateMPSUser(MPSUser user) {
        mDirectConnection.updateMPSUser(user);
    }
    
    /**
     * 
     * @return 
     */
    public Map<String, SubService> getAllSubService() {
        return mDirectConnection.getAllSubService();
    }
    
    public Map<String, CPService> getAllServices() {
        return mDirectConnection.getAllService();
    }
    
    public Vector<MOCommand> getAllMOCommand() {
        return mDirectConnection.getAllMOCommand();
    }
    
    public Subscriber checkConflict(String msisdn, String service, String currentSubService) {
        return mDirectConnection.checkConflict(msisdn, service, currentSubService);
    }
    
    public String getMessage(String subService, String cmd, String key) {
        return mDirectConnection.getMessage(subService, cmd, key);
    }
    
    public Vector<Subscriber> getAllRegisteredService(String msisdn) {
        return mDirectConnection.getAllRegisteredService(msisdn);
    }
    
    public void loadListFromDB(SyncQueue queue) {
        mDirectConnection.loadListFromDB(queue);
    }
    
    public int getRateCount(String msisdn, String countKey, int countDay, int rateType) {
        return mDirectConnection.getRateCount(msisdn, countKey, countDay, rateType);
    }
    
    public void insertRateLog(String msisdn, String countKey, String moContent, boolean isFree) {
        mDirectConnection.insertRateCount(msisdn, countKey, moContent, isFree);
    }
    
    public Map<String,String> getConfiguration(String table) {
        return mDirectConnection.getConfiguration(table);
    }
    
    public int getAirtimeConsume(String msisdn) {
        return mDirectConnection.getAirtimeConsume(msisdn);
    }

    public UssdCommand getUssdCommand(String subServiceName, int type) {
        return mDirectConnection.getUssdCommand(subServiceName, type);
        
    }

    public SubService getSubService(String subServiceName) {
        return mDirectConnection.getSubService(subServiceName);
    }

    public CPService getCpService(String serviceName) {
        return mDirectConnection.getCpService(serviceName);
    }

    public CPService getCpServiceBySubSerivceName(String serviceName) {
        return mDirectConnection.getCpServiceBySubSerivceName(serviceName);
    }
    
    public MOCommand getMOCommand(String subServiceName,int type) {
        return mDirectConnection.getMOCommand(subServiceName, type);
    }

    public IvrCommand getIvrCommand(String subServiceName, int type) {
        return mDirectConnection.getIvrCommand(subServiceName, type);
    }

    public Map<String, SubService> findSubServices(String serviceName) {
        return mDirectConnection.findSubServices(serviceName);
    }

    public List<PromotionCommand> getPromotionCommands(String subServiceName) {
        return mDirectConnection.getCurrentPromotioncommands(subServiceName);
    }

    public CPWebservice getCPWebService(String webserviceName) {
       return mDirectConnection.getCPWebService(webserviceName);
    }
}
