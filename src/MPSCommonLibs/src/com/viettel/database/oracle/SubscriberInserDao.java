/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.oracle;


import com.viettel.database.datatype.Subscriber;
import com.viettel.utilities.MyLog;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author hungtv45
 */
public class SubscriberInserDao implements Runnable{
    /**
     * thong thuong batch size se phai lon hon TPS (mac dinh TPS = 200/node)
     * day la ung dung lon nen de batch size an toan la 500 ~ max TPS
     * nen sau moi lan xu ly he thong se nghi: dua tren feedback phan hoi. max TPS/size of data
     */
    
    private final static int BATCH_SIZE = 500;
    private final static int DEFAULT_SLEEP = 1000;
    
    private final static String SQL_PRO_INSERT = "INSERT INTO SERVICES_SUBSCRIPTIONS (" +
                                                " MSISDN, " +
                                                " SUB_SERVICE_NAME," +
                                                " STATUS," +
                                                " REGISTER_TIME," +
                                                " CANCEL_TIME," +
                                                " DESCRIPTIONS," +
                                                " LAST_MONFEE_CHARGE_TIME," +
                                                " NEXT_MONFEE_CHARGE_TIME," +
                                                " CHARGE_STATUS," +
                                                " IS_PROMOTION," +
                                                " MONFEE_SUCCESS_COUNT," +
                                                " LAST_SEND_NOTIFY" +
                                                " ) VALUES(?,?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?)";
    
    private final LinkedList<Subscriber> queue = new LinkedList<Subscriber>();
    private final Object obj = new Object();
    private DataSource dataSource = null;
    private static SubscriberInserDao _instance = null;
    
    private int batchSize = BATCH_SIZE;
    
    private SubscriberInserDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    
    public static SubscriberInserDao config(DataSource dataSource) {
        if (_instance == null) {
            _instance = new SubscriberInserDao(dataSource);
            Thread t = new Thread(_instance);
            t.setName("thread-" + SubscriberInserDao.class.getSimpleName());
            t.start();
        }
        
        return _instance;
    }

    public int getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }
    
    public static SubscriberInserDao getInstance() {
        return _instance;
    }
   
    public void enqueue(Subscriber transaction) {
        synchronized (obj) {
            queue.add(transaction);
            MyLog.Infor("enqueue queue: " + transaction);
            MyLog.Infor("size of queue: " + queue.size() );
        }
    }

    private  List<Subscriber> dequeue() {
        synchronized (obj) {
            if (queue.isEmpty()) {
                return null;
            }
            List<Subscriber> datas = new ArrayList<Subscriber>(batchSize);
            int i = 0;
            while (queue.size() > 0 && (i < batchSize)) {
                Subscriber transaction = queue.poll();
                datas.add(transaction);
                i++;
            }

            return datas;
        }
    }

    @Override
     public void run() {
        while (true) {
            List<Subscriber> datas = dequeue();
            if (datas != null) {
                try{
                    process(datas);
                } catch(Exception ex){
                    MyLog.Error("SubcriberInserDao.java = " + ex);
                }
                int length = datas.size();
                MyLog.Infor("SubscriberInserDao processed bacth queue = " + length);
                datas.clear();
                datas = null;
                try {
                    int sleep = 1000 * batchSize / length; // ms
                    if (sleep > DEFAULT_SLEEP) {
                        sleep = DEFAULT_SLEEP;
                    }
                    Thread.sleep(sleep);
                } catch (InterruptedException ex) {
                    MyLog.Error("error insert in class SubcriberInserDao.java = " + ex);
                }
            } else {
                try {
                    Thread.sleep(DEFAULT_SLEEP);
                } catch (InterruptedException ex) {
                    MyLog.Error("error insert in class SubcriberInserDao.java = " + ex);
                }
            }
        }
    }

    private void process(List<Subscriber> datas) {
        if (dataSource == null) {
            throw new IllegalArgumentException("ERROR connection is null");
        }
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = dataSource.getConnection();
            ps = connection.prepareStatement(SQL_PRO_INSERT);
            ps.setQueryTimeout(5);//5 seconds
            for (Subscriber subscriber : datas) {
               // TODO set bien vao day
                ps.setString(1, subscriber.getMsisdn());
                ps.setString(2, subscriber.getSubServiceName());
                ps.setInt(3, subscriber.getStatus());
                ps.setTimestamp(4, new java.sql.Timestamp(subscriber.getRegisterTime()));
                
                 if (subscriber.getCancelTime() != 0) {
                    ps.setTimestamp(5, new java.sql.Timestamp(subscriber.getCancelTime()));
                } else {
                    ps.setTimestamp(5, null);
                }
                                
                ps.setString(6, subscriber.getDescription());
                ps.setTimestamp(7, new java.sql.Timestamp(subscriber.getLastMonfeeChargeTime()));
                ps.setTimestamp(8, new java.sql.Timestamp(subscriber.getNextMonfeeChargeTime()));
                ps.setInt(9, subscriber.getChargeStatus());
                ps.setInt(10, subscriber.getIsPromotion());
                ps.setInt(11, subscriber.getMonfeeSuccessCount());
                ps.setTimestamp(12, subscriber.getLastSendNotify());
                ps.addBatch();
            }

            ps.executeBatch();
            connection.commit();
            ps.close();
            ps = null;
//            connection.close();
        } catch (Exception ex) {
            MyLog.Error("error insert in class SubscriberInserDao");
            MyLog.Error(ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                   MyLog.Error(ex);
                } catch (Exception ex) {
                   MyLog.Error(ex);
                }
                
            }
        }

    }

   
}

