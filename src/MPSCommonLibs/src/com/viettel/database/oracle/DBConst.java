/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.database.oracle;

/**
 *
 * @author hoand
 */
public class DBConst {
    public static final int MONEY_RATE = 10000;
    
    public static final long A_DAY_MS = 86400000;
    
    public static final int CANCEL_SERVICE_RESULT_NORMAL = 0;
    public static final int CANCEL_SERVICE_RESULT_EXIST = 1;
    public static final int CANCEL_SERVICE_RESULT_FAILED = 2;
    
    public static final int SERVICE_REGISTERED = 0;
    public static final int SERVICE_UNREGISTERED = 1;

    public static final int STATUS_INACTIVE = 0;
    public static final int STATUS_ACTIVE = 1;
    public static final int STATUS_PENDING = 2;
    
    public static final int CHARGE_OK = 1;
    public static final int CHARGE_FAILED = 0;
    
    public static final int RESULT_SUCCESS = 1;
    public static final int RESULT_FAILED = 0;
    
    public static final int PROMOTION_OUT = 0;
    public static final int PROMOTION_IN = 1;
    
    public static final int REGISTER_SUCCESS = 0;
    public static final int REGISTER_FAILED = 1;
    public static final int REGISTER_ALREADY_REGISTERED = 2;
    public static final int REGISTER_IN_CHARGE_PERIOD = 3;
    
    public static final int UNREGISTER_NOT_EXIST = 4;
    public static final int UNREGISTER_SUCCESS = 5;
    public static final int UNREGISTER_IN_CHARGE_PERIOD = 6;
    
    public static final int RATE_FREE = 0;
    public static final int RATE_NOT_FREE = 1;
    
    public static final int RATE_TYPE_ALL = 0;
    public static final int RATE_TYPE_FREE = 1;
    public static final int RATE_TYPE_NOT_FREE = 2;
    
    public static String MODULE_CODE_EBP = "020";
    public static String MODULE_CODE_SMS_HANDLER = "070";
    public static String MODULE_CODE_CMSCP = "060";
    public static String MODULE_CODE_CMSMPS = "050";
    public static String MODULE_CODE_SSE = "040";
    public static String MODULE_CODE_UAL = "030";
    public static String MODULE_CODE_MPI = "010";
    public static String MODULE_CODE_SPI = "001";

}
