/**
 *
 * handsome boy
 */
package com.viettel.database.datatype;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Dec 12, 2015
 * @mail haind25@viettel.com.vn
 */
public class SmsData {

    private String url, user, pass, msisdn, content, shortCode, alias, smsType;
    private long sendTime; //affter this time, System will send sms

    public SmsData(String url, String user, String pass, String msisdn, String content, String shortCode, String alias, String smsType) {
        this.url = url;
        this.user = user;
        this.pass = pass;
        this.msisdn = msisdn;
        this.content = content;
        this.shortCode = shortCode;
        this.alias = alias;
        this.smsType = smsType;
        sendTime = System.currentTimeMillis();
    }

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public String getContent() {
        return content;
    }

    public String getShortCode() {
        return shortCode;
    }

    public String getAlias() {
        return alias;
    }

    public String getSmsType() {
        return smsType;
    }

    public long getSendTime() {
        return sendTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(msisdn)
                .append("|").append(shortCode)
                .append("|").append(smsType)
                .append("|").append(user)
                .append("|").append(content);
        return sb.toString();
    }
}
