/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.datatype;

import java.util.Calendar;

/**
 *
 * @author HoaND6
 */
public class SendingTime {
    private TimeRange[] mTimeRange;
    
    public SendingTime(String time) {
        mTimeRange = parseSentTime(time);
    }
    
    /**
     * Parse Sent time
     * @param inp
     * @return 
     */
    public static TimeRange[] parseSentTime(String inp) {
        if (inp == null || inp.isEmpty()) {
            return null;
        }
        
        String[] time = inp.split(",");
        TimeRange[] result = new TimeRange[time.length];
        for (int i=0; i<time.length; i++) {
            if (time[i] != null && !time[i].isEmpty()) {
                result[i] = TimeRange.parse(time[i].trim());
            } else {
                result[i] = null;
            }
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        if (mTimeRange == null) {
            return "";
        }
        StringBuilder b = new StringBuilder();
        for (int i=0; i<mTimeRange.length; i++) {
            if (mTimeRange[i] != null) {
                b.append(mTimeRange[i]).append(", ");
            }
        }
        
        return b.toString();
    }
    
    public boolean isInSentTime(long millis) {
        if (mTimeRange == null) {
            return false;
        }
        
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(millis);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int min = c.get(Calendar.MINUTE);
        
        for (int i=0; i<mTimeRange.length; i++) {
            if (mTimeRange[i].isInSentTime(hour, min)) {
                return true;
            }
        }
        
        return false;
    }
}
