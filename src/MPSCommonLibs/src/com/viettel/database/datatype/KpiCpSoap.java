/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.database.datatype;

import java.sql.Timestamp;
import java.util.UUID;

/**
 *
 * @author haind25
 */
public class KpiCpSoap {
    public static final String ERROR_CODE_OK = "OK";
    public static final String ERROR_CODE_KPI = "KPI";
    public static final String ERROR_CODE_CP_FAIL = "CP_FAIL";
    public static final String ERROR_CODE_CP_TIMEOUT = "TIMEOUT";

    private String id;
    private String providerName;
    private String subServiceName;
    private String channel;
    private String transactionId;
    private String msisdn;
    private String action;
    private long duration;
    private String url;
    private String body;
    private String response;
    private Timestamp requestTime;
    private String description;
    private String errorCode;
    private String mode;



    public KpiCpSoap(String providerName, String subServiceName, String channel,
            String transactionId, String msisdn, 
            String action, long duration, 
            String url, String body, String response, String description, long requestTime,String errorCode, String mode) {
        this.providerName = providerName;
        this.subServiceName = subServiceName;
        this.channel = channel;
        this.transactionId = transactionId;
        this.msisdn = msisdn;
        this.action = action;
        this.duration = duration;
        this.url = url;
        this.body = body;
        this.response = response;
        this.description = description;
        UUID uuid = UUID.randomUUID();
        id = uuid.toString();
        this.requestTime = new Timestamp(requestTime);
        this.mode = mode;
        this.errorCode = errorCode;
    }

    public String getId() {
        return id;
    }

    public String getProviderName() {
        return providerName;
    }
    
    public String getSubServiceName() {
        return subServiceName;
    }

    public String getChannel() {
        return channel;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public String getAction() {
        return action;
    }

    public long getDuration() {
        return duration;
    }

    public String getUrl() {
        return url;
    }

    public String getBody() {
        return body;
    }

    public String getResponse() {
        return response;
    }

    public Timestamp getRequestTime() {
        return requestTime;
    }

    public String getDescription() {
        return description;
    }

    public String getErrorCode() {
        return errorCode;
    }
    
    public String getMode() {
        return mode;
    }

    @Override
    public String toString() {
        return getTransactionId()+"|"+getMsisdn()+"|" + getAction()+"|"+getSubServiceName();
    }
    
    
}
