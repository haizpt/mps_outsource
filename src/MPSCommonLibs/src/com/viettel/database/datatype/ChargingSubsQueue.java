/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.datatype;

import java.util.LinkedList;

/**
 *
 * @author hoand
 */
public class ChargingSubsQueue {
//    public static ChargingSubsQueue mMe = null;
    
    private LinkedList<Subscriber> mQueue;
    private final Object mLock = new Object();
    
    
    public ChargingSubsQueue() {
        mQueue = new LinkedList<Subscriber>();
    }
    
//    public static ChargingSubsQueue getInstance() {
//        if (mMe == null) {
//            mMe = new ChargingSubsQueue();
//        }
//        
//        return mMe;
//    }
    
    public Subscriber getNextTask() {
        synchronized (mLock) {
            if (mQueue.size() > 0) {
                Subscriber s = mQueue.poll();
                return s;
            } else {
                return null;
            }
        }
    }
    
    public void putTask(Subscriber task) {
        synchronized (mLock) {
            mQueue.add(task);
        }
    }
    
    public void clearList() {
        synchronized (mLock) {
            mQueue.clear();
        }
    }
    
    public int size() {
        synchronized (mLock) {
            return mQueue.size();
        }
    }
}
