/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.datatype;

/**
 *
 * @author hoand
 */
public class CPService {
    private String mProvider, mServiceCode, mServiceName, mShortCode, mDefaultSMS;
    private int mDefaultPrice;
    private String mVTPublicKey, mVTPrivateKey, mCPPublicKey, mCPPrivateKey;
    private String mSMSGWUrl, mSMSGWUser, mSMSGWPass;

    public CPService(String provider, String mServiceCode, String mServiceName, String mShortCode, 
                     String mDefaultSMS, int price, 
                     String vtPublicKey, String vtPrivateKey, String cpPublicKey, String cpPrivateKey,
                     String smsgwUrl, String smsgwUser, String smsgwPass) {
        mProvider = provider;
        this.mServiceCode = mServiceCode;
        this.mServiceName = mServiceName;
        this.mShortCode = mShortCode;
        this.mDefaultSMS = mDefaultSMS;
        mDefaultPrice = price;
        mVTPublicKey = vtPublicKey;
        mVTPrivateKey = vtPrivateKey;
        mCPPublicKey = cpPublicKey;
        mCPPrivateKey = cpPrivateKey;
        
        mSMSGWUrl = smsgwUrl;
        mSMSGWUser = smsgwUser;
        mSMSGWPass = smsgwPass;
    }

    public String getServiceCode() {
        return mServiceCode;
    }

    public String getServiceName() {
        return mServiceName;
    }

    public String getShortCode() {
        return mShortCode;
    }

    public String getDefaultSMS() {
        return mDefaultSMS;
    }

    public int getDefaultPrice() {
        return mDefaultPrice;
    }

    public String getProvider() {
        return mProvider;
    }

    public String getVTPublicKey() {
        return mVTPublicKey;
    }

    public String getVTPrivateKey() {
        return mVTPrivateKey;
    }

    public String getCPPublicKey() {
        return mCPPublicKey;
    }

    //    public String getCPPrivateKey() {
    //        return mCPPrivateKey;
    //    }
    public String getSMSGWUrl() {
        return mSMSGWUrl;
    }

    public String getSMSGWUser() {
        return mSMSGWUser;
    }

    public String getSMSGWPass() {
        return mSMSGWPass;
    }
    
    @Override
    public String toString() {
        return "(ShortCode: " + getShortCode() + "; Provicer: " + getProvider() + "; ServiceName: " + getServiceName() + 
                "; ServiceCode: " + getServiceCode() + "; Price: " + getDefaultPrice() + "; SMS: " + getDefaultSMS() + ")";
    }
}
