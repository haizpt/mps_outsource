/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.datatype;

import java.util.Vector;

/**
 *
 * @author hoand
 */
public class SyncQueue {
//    public static RequestQueue mMe = null;
    
    private Vector<SyncTask> mQueue;
    private final Object mLock = new Object();
    
    public SyncQueue() {
        mQueue = new Vector<SyncTask>();
    }
//    private RequestQueue() {
//        mQueue = new Vector<SMSTask>();
//    }
    
//    public static RequestQueue getInstance() {
//        if (mMe == null) {
//            mMe = new RequestQueue();
//        }
//        
//        return mMe;
//    }
    
    public SyncTask getNextTask() {
        synchronized (mLock) {
            if (mQueue.size() > 0) {
                SyncTask s = mQueue.firstElement();
                mQueue.remove(0);
                return s;
            } else {
                return null;
            }
        }
    }
    
    public void putTask(SyncTask task) {
        synchronized (mLock) {
            mQueue.add(task);
        }
    }
    
    public void clearList() {
        synchronized (mLock) {
            mQueue.clear();
        }
    }
    
    public int size() {
        synchronized (mLock) {
            return mQueue.size();
        }
    }
}
