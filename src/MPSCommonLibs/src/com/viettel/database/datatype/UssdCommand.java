/**
 *
 * handsome boy
 */

package com.viettel.database.datatype;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Nov 24, 2015
 * @mail haind25@viettel.com.vn
 */
public class UssdCommand extends Command{
    
    
    
//    private String mRawSMS;

    
    public UssdCommand(String id, String subServiceName, String description, String successMessage, String failMessage, String notEnoughMoney, 
                        String chargeCommand, int status, int type, int webServiceID, int moPrice, int requiredRegisted, 
                        int checkRequired, int checkPrice, int messageChannel, String confirmMessage,String alreadyRegisterMessage, String notRegisterMessage, int chargePrice) {
        super(Command.COMMAND_USSD , id, subServiceName, description, successMessage, failMessage, notEnoughMoney, chargeCommand, 
                status, type, webServiceID, moPrice, requiredRegisted, checkRequired, checkPrice, 
                messageChannel, confirmMessage, alreadyRegisterMessage, notRegisterMessage,chargePrice);
    }
    
    
}
