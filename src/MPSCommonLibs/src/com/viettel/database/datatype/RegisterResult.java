/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.datatype;

/**
 *
 * @author hoand
 */
public class RegisterResult {
    private int mResult;
    private Subscriber mSub;

    public RegisterResult(int mResult, Subscriber mSub) {
        this.mResult = mResult;
        this.mSub = mSub;
    }

    public int getResult() {
        return mResult;
    }

    public Subscriber getSub() {
        return mSub;
    }
}
