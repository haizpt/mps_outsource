/**
 *
 * handsome boy
 */

package com.viettel.database.datatype;

import java.util.Hashtable;
import java.util.Map;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Nov 27, 2015
 * @mail haind25@viettel.com.vn
 */
public class TransactionInfo{
    private String transactionId;
    private String msidn;
    private String subServcieName;
    private Long created;
    private Map<String,String> params;

    public TransactionInfo(String transactionId, String msidn) {
        this.transactionId = transactionId;
        this.msidn = msidn;
        created = System.currentTimeMillis();
        params = new Hashtable<String, String>();
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getMsidn() {
        return msidn;
    }

    public void setMsidn(String msidn) {
        this.msidn = msidn;
    }
    
    public void putParam(String key, String value) {
        params.put(key, value);
    }

    public Map<String, String> getParams() {
        return params;
    }

    public Long getCreated() {
        return created;
    }

    public String getSubServcieName() {
        return subServcieName;
    }

    public void setSubServcieName(String subServcieName) {
        this.subServcieName = subServcieName;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("transactionId: ").append(getTransactionId());
        sb.append(", msidn: ").append(getMsidn());
        return sb.toString();
    }
    
    public String paresContext(String message) {
        if (params == null || message == null 
                || message.isEmpty()) {
            return message;
        }
        String result = message;
//        logger.debug("Begin Parse content : " + result);
        while (result.contains("#") && checkIncludeContext(result, params)){
            for (Map.Entry<String, String> entry : params.entrySet()) {
                String k = entry.getKey();
                String val = entry.getValue();
                result = result.replace(k, val);
//                logger.debug("-- Key:" + k + " Value: " + val);
            }
        }
        
//        logger.debug("End Parse content : " + result);
        return result;
    }
    
    private boolean checkIncludeContext(String message, Map<String, String> context){
        if (message == null || context == null) {
            return false;
        }
        for (Map.Entry<String, String> entry : context.entrySet()) {
            String string = entry.getKey();
            if (message.contains(string)) {
                return true;
            }
        }
        return false;
    }
    
}
