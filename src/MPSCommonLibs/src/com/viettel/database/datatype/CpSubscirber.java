/**
 *
 * handsome boy
 */

package com.viettel.database.datatype;

import java.sql.Date;
import java.sql.Timestamp;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Dec 9, 2015
 * @mail haind25@viettel.com.vn
 */
public class CpSubscirber {
    public static final int STATUS_INACTIVE = 0;
    public static final int STATUS_ACTIVE = 1;
    public static final int STATUS_PENDING = 2;
    
    public static final int CHARGE_OK = 1;
    public static final int CHARGE_FAILED = 0;
    
    public static final int SUB_TYPE_MONTHLY = 3;
    public static final int SUB_TYPE_WEEKLY = 2;
    public static final int SUB_TYPE_DAILY = 1;
    
    private String mMsisdn, mSubServiceName, mDescription;
    private int mStatus, mChargeStatus, mIsPromotion,  mAllowDebFee;
    private long mRegisterTime, mCancelTime, mLastMonfeeChargeTime, mNextMonfeeChargeTime;

    private int mMonfeeSuccessCount;
    private Timestamp mLastSendNotify;
    

    public CpSubscirber(String mMsisdn, String mSubServiceName,  String description, 
                               int mStatus, int mChargeStatus,
                               int mIsPromotion, long mRegisterTime, long mCancelTime, 
                               long mLastMonfeeChargeTime, long mNextMonfeeChargeTime, 
                               int monfeeSuccessCount, Timestamp lastNotify) {
        this.mMsisdn = mMsisdn;
        this.mSubServiceName = mSubServiceName;
       
        this.mDescription = description;
        this.mStatus = mStatus;
        this.mChargeStatus = mChargeStatus;
        this.mIsPromotion = mIsPromotion;
        this.mRegisterTime = mRegisterTime;
        this.mCancelTime = mCancelTime;
        this.mLastMonfeeChargeTime = mLastMonfeeChargeTime;
        this.mNextMonfeeChargeTime = mNextMonfeeChargeTime;

        mMonfeeSuccessCount = monfeeSuccessCount;
        mLastSendNotify = lastNotify;
    }

    public String getMsisdn() {
        return mMsisdn;
    }

    public String getSubServiceName() {
        return mSubServiceName;
    }

    public String getDescription() {
        return mDescription;
    }
    
    public int getStatus() {
        return mStatus;
    }

    public int getChargeStatus() {
        return mChargeStatus;
    }

    public int getIsPromotion() {
        return mIsPromotion;
    }

    public long getRegisterTime() {
        return mRegisterTime;
    }

    public long getCancelTime() {
        return mCancelTime;
    }

    public long getLastMonfeeChargeTime() {
        return mLastMonfeeChargeTime;
    }

    public long getNextMonfeeChargeTime() {
        return mNextMonfeeChargeTime;
    }

    public void setMsisdn(String mMsisdn) {
        this.mMsisdn = mMsisdn;
    }

    public void setSubServiceName(String mSubServiceName) {
        this.mSubServiceName = mSubServiceName;
    }

    public void setStatus(int mStatus) {
        this.mStatus = mStatus;
    }

    public void setChargeStatus(int mChargeStatus) {
        this.mChargeStatus = mChargeStatus;
    }

    public void setIsPromotion(int mIsPromotion) {
        this.mIsPromotion = mIsPromotion;
    }

    public void setRegisterTime(long mRegisterTime) {
        this.mRegisterTime = mRegisterTime;
    }

    public void setCancelTime(long mCancelTime) {
        this.mCancelTime = mCancelTime;
    }

    public void setLastMonfeeChargeTime(long mLastMonfeeChargeTime) {
        this.mLastMonfeeChargeTime = mLastMonfeeChargeTime;
    }

    public void setNextMonfeeChargeTime(long mNextMonfeeChargeTime) {
        this.mNextMonfeeChargeTime = mNextMonfeeChargeTime;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public void addMonfeeSuccessCount() {
        mMonfeeSuccessCount++;
    }
    
    public void setMonfeeSuccessCount(int mMonfeeSuccessCount) {
        this.mMonfeeSuccessCount = mMonfeeSuccessCount;
    }

    public void setLastSendNotify(Timestamp mLastSendNotify) {
        this.mLastSendNotify = mLastSendNotify;
    }

    public int getMonfeeSuccessCount() {
        return mMonfeeSuccessCount;
    }

    public Timestamp getLastSendNotify() {
        return mLastSendNotify;
    }

    public int getAllowDebFee() {
        return mAllowDebFee;
    }

    
    @Override
    public String toString() {
        return "(MSISDN: " + getMsisdn() + "; " + 
               "SubServiceName: " + getSubServiceName() + "; " + 
               "LastMonfee: " + (new Date(getLastMonfeeChargeTime())) + "; " + 
               "NextMonfee: " + (new Date(getNextMonfeeChargeTime())) + "; " + 
               "ChargeStatus: " + getChargeStatus() + "; " + 
               "Promo: " + getIsPromotion() + "; " + 
               "Status: " + getStatus() + ")";
    }
}
