/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.datatype;
/**
 *
 * @author hungtv45
 */
public class SubLogs {
    
    private String logMsisdn;
    private String logSubService;
    private String logTranID;
    private String logAction;
    private int logResult;
    private long logDatetime;
    private String traceStep;
    private String channel;
    public SubLogs() {
        
    }
    
    public SubLogs(String logMsisdn,String logSubService, String logTranID, String logAction, int logResult, long logDatetime, String traceStep, String channel) {
        this.logMsisdn = logMsisdn;
        this.logSubService = logSubService;
        this.logTranID = logTranID;
        this.logAction = logAction;
        this.logResult = logResult;
        this.logDatetime = logDatetime;
        this.traceStep = traceStep;
        this.channel = channel;
        
    }
    
    public SubLogs(String logMsisdn, String logSubService, String logTranID, String logAction, int logResult, long logDatetime) {
        this.logMsisdn = logMsisdn;
        this.logSubService = logSubService;
        this.logTranID = logTranID;
        this.logAction = logAction;
        this.logResult = logResult;
        this.logDatetime = logDatetime;       
        
    }

    @Override
    public SubLogs clone() {
        SubLogs result = new SubLogs(logMsisdn, logSubService, logTranID, logAction, 
                                           logResult, logDatetime,traceStep, channel);        
        return result;
    }
    

    public String getLogMsisdn() {
        return logMsisdn;
    }

    public void setLogMsisdn(String logMsisdn) {
        this.logMsisdn = logMsisdn;
    }

    public String getLogSubService() {
        return logSubService;
    }

    public void setLogSubService(String logSubService) {
        this.logSubService = logSubService;
    }

    public String getLogTranID() {
        return logTranID;
    }

    public void setLogTranID(String logTranID) {
        this.logTranID = logTranID;
    }

    public String getLogAction() {
        return logAction;
    }

    public void setLogAction(String logAction) {
        this.logAction = logAction;
    }

    public int getLogResult() {
        return logResult;
    }

    public void setLogResult(int logResult) {
        this.logResult = logResult;
    }

    public long getLogDatetime() {
        return logDatetime;
    }

    public void setLogDatetime(long logDatetime) {
        this.logDatetime = logDatetime;
    }

    public String getTraceStep() {
        return traceStep;
    }

    public void setTraceStep(String traceStep) {
        this.traceStep = traceStep;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    @Override
    public String toString() {
        return "SubLogs{" + "logMsisdn=" + logMsisdn + ", logSubService=" + logSubService + ", logTranID=" + logTranID + ", logAction=" + logAction + ", logResult=" + logResult + ", logDatetime=" + logDatetime + ", traceStep=" + traceStep + ", channel=" + channel + '}';
    }
    
    
 
  
}
