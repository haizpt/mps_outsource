/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.datatype;

import java.sql.Date;
import java.sql.Timestamp;

/**
 *
 * @author hoand
 */
public class Subscriber {
    public static final int STATUS_INACTIVE = 0;
    public static final int STATUS_ACTIVE = 1;
    public static final int STATUS_PENDING = 2;
    public static final int STATUS_UNDEFINE = -1;
    
    public static final int CHARGE_OK = 1;
    public static final int CHARGE_FAILED = 0;

    public static final int PROMOTION_Y = 1;
    public static final int PROMOTION_N = 0;
    
    public static final int SUB_TYPE_MONTHLY = 3;
    public static final int SUB_TYPE_WEEKLY = 2;
    public static final int SUB_TYPE_DAILY = 1;
    
    
    
    private String mMsisdn, mProvider, mSubServiceName, mDescription, mServiceName, mSubServiceCode;
    private int mStatus, mChargeStatus, mIsPromotion, mSubType, mAllowDebFee;
    private long mRegisterTime, mCancelTime, mLastMonfeeChargeTime, mNextMonfeeChargeTime;

    private int mMonfeeWSID, mRegisterWSID, mCancelWSID, mMonfeeSuccessCount;
    private Timestamp mLastSendNotify;
    
    public Subscriber() {
        
    }
    
    public Subscriber(String mMsisdn, String providerName, String serviceName, String mSubServiceName, String subServiceCode, String description, 
                               int mStatus, int mChargeStatus, int subType, int allowDebFee,
                               int mIsPromotion, long mRegisterTime, long mCancelTime, 
                               long mLastMonfeeChargeTime, long mNextMonfeeChargeTime, 
                               int monfeeSuccessCount, Timestamp lastNotify) {
        this.mMsisdn = mMsisdn;
        mProvider = providerName;
        mServiceName = serviceName;
        this.mSubServiceName = mSubServiceName;
        mSubServiceCode = subServiceCode;
        this.mDescription = description;
        this.mStatus = mStatus;
        this.mChargeStatus = mChargeStatus;
        mSubType = subType;
        mAllowDebFee = allowDebFee;
        this.mIsPromotion = mIsPromotion;
        this.mRegisterTime = mRegisterTime;
        this.mCancelTime = mCancelTime;
        this.mLastMonfeeChargeTime = mLastMonfeeChargeTime;
        this.mNextMonfeeChargeTime = mNextMonfeeChargeTime;
        mMonfeeWSID = mRegisterWSID = mCancelWSID = -1;
        mMonfeeSuccessCount = monfeeSuccessCount;
        mLastSendNotify = lastNotify;
    }
    
    @Override
    public Subscriber clone() {
        Subscriber result = new Subscriber(mMsisdn, mProvider, mServiceName, mSubServiceName, 
                                           mSubServiceCode, mDescription, mStatus, mChargeStatus, mSubType, 
                                           mAllowDebFee, mIsPromotion, mRegisterTime, mCancelTime, 
                                           mLastMonfeeChargeTime, mNextMonfeeChargeTime, 
                                           mMonfeeSuccessCount, mLastSendNotify);
        mMonfeeWSID = mRegisterWSID = mCancelWSID = -1;
        return result;
    }
    
    public String getMsisdn() {
        return mMsisdn;
    }

    public String getSubServiceName() {
        return mSubServiceName;
    }

    public String getDescription() {
        return mDescription;
    }
    
    public int getStatus() {
        return mStatus;
    }

    public int getChargeStatus() {
        return mChargeStatus;
    }

    public int getIsPromotion() {
        return mIsPromotion;
    }

    public long getRegisterTime() {
        return mRegisterTime;
    }

    public long getCancelTime() {
        return mCancelTime;
    }

    public long getLastMonfeeChargeTime() {
        return mLastMonfeeChargeTime;
    }

    public long getNextMonfeeChargeTime() {
        return mNextMonfeeChargeTime;
    }

    public void setMsisdn(String mMsisdn) {
        this.mMsisdn = mMsisdn;
    }

    public void setSubServiceName(String mSubServiceName) {
        this.mSubServiceName = mSubServiceName;
    }

    public void setStatus(int mStatus) {
        this.mStatus = mStatus;
    }

    public void setChargeStatus(int mChargeStatus) {
        this.mChargeStatus = mChargeStatus;
    }

    public void setIsPromotion(int mIsPromotion) {
        this.mIsPromotion = mIsPromotion;
    }

    public void setRegisterTime(long mRegisterTime) {
        this.mRegisterTime = mRegisterTime;
    }

    public void setCancelTime(long mCancelTime) {
        this.mCancelTime = mCancelTime;
    }

    public void setLastMonfeeChargeTime(long mLastMonfeeChargeTime) {
        this.mLastMonfeeChargeTime = mLastMonfeeChargeTime;
    }

    public void setNextMonfeeChargeTime(long mNextMonfeeChargeTime) {
        this.mNextMonfeeChargeTime = mNextMonfeeChargeTime;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public void addMonfeeSuccessCount() {
        mMonfeeSuccessCount++;
    }
    
    public void setMonfeeSuccessCount(int mMonfeeSuccessCount) {
        this.mMonfeeSuccessCount = mMonfeeSuccessCount;
    }

    public void setLastSendNotify(Timestamp mLastSendNotify) {
        this.mLastSendNotify = mLastSendNotify;
    }

    public int getMonfeeSuccessCount() {
        return mMonfeeSuccessCount;
    }

    public Timestamp getLastSendNotify() {
        return mLastSendNotify;
    }
    
    public String getServiceName() {
        return mServiceName;
    }

    public int getSubType() {
        return mSubType;
    }

    public int getAllowDebFee() {
        return mAllowDebFee;
    }

    public int getMonfeeWSID() {
        return mMonfeeWSID;
    }

    public int getRegisterWSID() {
        return mRegisterWSID;
    }

    public int getCancelWSID() {
        return mCancelWSID;
    }

    public void setMonfeeWSID(int mMonfeeWSID) {
        this.mMonfeeWSID = mMonfeeWSID;
    }

    public void setRegisterWSID(int mRegisterWSID) {
        this.mRegisterWSID = mRegisterWSID;
    }

    public void setCancelWSID(int mCancelWSID) {
        this.mCancelWSID = mCancelWSID;
    }

    public String getSubServiceCode() {
        return mSubServiceCode;
    }

    public String getProvider() {
        return mProvider;
    }
   
    @Override
    public String toString() {
        return "(MSISDN: " + getMsisdn() + "; " + 
               "SubServiceName: " + getSubServiceName() + "; " + 
               "LastMonfee: " + (new Date(getLastMonfeeChargeTime())) + "; " + 
               "NextMonfee: " + (new Date(getNextMonfeeChargeTime())) + "; " + 
               "ChargeStatus: " + getChargeStatus() + "; " + 
               "Promo: " + getIsPromotion() + "; " + 
               "Status: " + getStatus() + ")";
    }
}
