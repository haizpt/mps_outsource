/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.datatype;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author HoaND6
 */
public class TimeRange {
    private Moment mStart, mEnd;

    public TimeRange(Moment mStart, Moment mEnd) {
        this.mStart = mStart;
        this.mEnd = mEnd;
    }
    
    public boolean isInSentTime(Moment moment) {
        return (mStart.before(moment) && mEnd.after(moment));
    }
    
    public boolean isInSentTime(int hour, int min) {
        return (mStart.before(hour, min) && mEnd.after(hour, min));
    }

    @Override
    public String toString() {
        return "(" + mStart + " -> " + mEnd + ')';
    }
    
    
    /**
     * Parse format 
     * @param inp
     * @return 
     */
    public static TimeRange parse(String inp) {
        if (inp == null || inp.isEmpty()) {
            return null;
        }
        try {
            String[] time = inp.split("-");
            if (time.length == 2) {
                SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                Calendar c = Calendar.getInstance();
                c.setTime(format.parse(time[0]));
                Moment start = new Moment(c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE));
                
                c.setTime(format.parse(time[1]));
                Moment end = new Moment(c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE));
                
                return new TimeRange(start, end);
            } else {
                return null;
            } 
        } catch(Exception ex) {
            return null;
        }
        
    }
}
