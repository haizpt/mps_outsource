/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.datatype;

import java.util.HashSet;

/**
 *
 * @author hoand
 */
public class SubService {
    public static final int AUTO_CANCEL_TRUE = 1;
    public static final int AUTO_CANCEL_FALSE = 0;
    
    public static final int SEND_NOTIFICATION_TRUE = 1;
    public static final int SEND_NOTIFICATION_FALSE = 0;
    
    public static final int SEND_NOTIFY_BEFORE_CANCEL_TRUE = 1;
    public static final int SEND_NOTIFY_BEFORE_CANCEL_FALSE = 0;
    
    public static final int SEND_NOTIFY_AUTO_CANCEL_TRUE = 1;
    public static final int SEND_NOTIFY_AUTO_CANCEL_FALSE = 0;
    
    private String mSubServiceCode, mSubServiceName, mDescription, mServiceName, mGroupChargeType, mSubFee, mPromoFee;
    private int mIsSubscription, mStatus, mFixTimeChargeMonfee, mSubType, mDayDebitAllowed;
    private long mPromoBegin, mPromoEnd;
    private HashSet<Integer> mMonfeeHour;
    private boolean mStopOnError, mCheckConflict, mRequireMPSAccount, mRecheckBeforeMonfee;
    private int mMonfeeWS, mRegisterWS, mCancelWS;
    private String mErrorMessage;
    private boolean mAutoCancel;
    private int mSendNotification;
    private String mNotificationContent, mSoftName;
    //hungtv45 PYC
    private int mSendNotifyBeforeCancel;
    private int mDaySendNotifyBeforeCancel;
    private String mNotifyBeforeCancel;
    private int mSendNotifyAutoCancel;
    private String mNotifyAutoCancel;
    private int promotion;
    private int renewCheckMode;

    public SubService(String mSubServiceCode, String mSubServiceName, String mDescription, String mServiceName, 
                      String mGroupChargeType, String mSubFee, String mPromoFee, int mIsSubscription, int mStatus, 
                      int mFixTimeChargeMonfee, int mSubType, int mDayDebitAllowed, long mPromoBegin, long mPromoEnd,
                      HashSet<Integer> monfeeHour, 
                      int stopOnError, int checkConflict, int requireMPSAccount, int recheckBeforeMonfee, //, String warningList
                      int monfeeWS, int registerWS, int cancelWS, 
                      String errorMsg, boolean autoCancel, int sendNotification, String notificationContent, String softName
            ) {
        this.mSubServiceCode = mSubServiceCode;
        this.mSubServiceName = mSubServiceName;
        this.mDescription = mDescription;
        this.mServiceName = mServiceName;
        this.mGroupChargeType = mGroupChargeType;
        this.mSubFee = mSubFee;
        this.mPromoFee = mPromoFee;
        this.mIsSubscription = mIsSubscription;
        this.mStatus = mStatus;
        this.mFixTimeChargeMonfee = mFixTimeChargeMonfee;
        this.mSubType = mSubType;
        this.mDayDebitAllowed = mDayDebitAllowed;
        this.mPromoBegin = mPromoBegin;
        this.mPromoEnd = mPromoEnd;
        mMonfeeHour = monfeeHour;
        mStopOnError = (stopOnError == 1); // 0: False, 1: true
        mCheckConflict = (checkConflict == 1); // 0: False, 1: true
        mRequireMPSAccount = (requireMPSAccount == 1); // 0: False, 1: true
        mRecheckBeforeMonfee = (recheckBeforeMonfee == 1); // 0: False, 1: true
        mMonfeeWS = monfeeWS;
        mRegisterWS = registerWS;
        mCancelWS = cancelWS;
        mErrorMessage = errorMsg;
        mAutoCancel = autoCancel;
        mSendNotification = sendNotification;
        mNotificationContent = notificationContent;
        mSoftName = softName;
    }
    //hungtv45 PYC
    public SubService(String mSubServiceCode, String mSubServiceName, String mDescription, String mServiceName, 
                      String mGroupChargeType, String mSubFee, String mPromoFee, int mIsSubscription, int mStatus, 
                      int mFixTimeChargeMonfee, int mSubType, int mDayDebitAllowed, long mPromoBegin, long mPromoEnd,
                      HashSet<Integer> monfeeHour, 
                      int stopOnError, int checkConflict, int requireMPSAccount, int recheckBeforeMonfee, //, String warningList
                      int monfeeWS, int registerWS, int cancelWS, String errorMsg, boolean autoCancel, 
                      int sendNotification, String notificationContent, String softName,
                      int daySendNotifyBeforeCancel, int sendNotifyBeforeCancel, String notifyBeforeCancel, int sendNotifyAutoCancel, String notifyAutoCancel
            ) {
        this(mSubServiceCode, mSubServiceName, mDescription, mServiceName, mGroupChargeType, 
                mSubFee, mPromoFee, mIsSubscription, mStatus, mFixTimeChargeMonfee, mSubType, 
                mDayDebitAllowed, mPromoBegin, mPromoEnd, monfeeHour, stopOnError, checkConflict, 
                requireMPSAccount, recheckBeforeMonfee, monfeeWS, registerWS, cancelWS, errorMsg, 
                autoCancel, sendNotification, notificationContent, softName);
        
        mSendNotifyBeforeCancel = sendNotifyBeforeCancel;
        mDaySendNotifyBeforeCancel = daySendNotifyBeforeCancel;
        mNotifyBeforeCancel = notifyBeforeCancel;
        mSendNotifyAutoCancel = sendNotifyAutoCancel;
        mNotifyAutoCancel = notifyAutoCancel;
    }

    public String getSubServiceCode() {
        return mSubServiceCode;
    }

    public String getSubServiceName() {
        return mSubServiceName;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getServiceName() {
        return mServiceName;
    }

    public String getGroupChargeType() {
        return mGroupChargeType;
    }

    public String getSubFee() {
        return mSubFee;
    }

    public String getPromoFee() {
        return mPromoFee;
    }

    public int getIsSubscription() {
        return mIsSubscription;
    }

    public int getStatus() {
        return mStatus;
    }

    public int getFixTimeChargeMonfee() {
        return mFixTimeChargeMonfee;
    }

    public int getSubType() {
        return mSubType;
    }

    public int getDayDebitAllowed() {
        return mDayDebitAllowed;
    }

    public long getPromoBegin() {
        return mPromoBegin;
    }

    public long getPromoEnd() {
        return mPromoEnd;
    }

    public HashSet<Integer> getMonfeeHour() {
        return mMonfeeHour;
    }

    public boolean isStopOnError() {
        return mStopOnError;
    }

    public boolean isCheckConflict() {
        return mCheckConflict;
    }

    public boolean isRequireMPSAccount() {
        return mRequireMPSAccount;
    }

    public boolean isRecheckBeforeMonfee() {
        return mRecheckBeforeMonfee;
    }

    public int getMonfeeWS() {
        return mMonfeeWS;
    }

    public int getRegisterWS() {
        return mRegisterWS;
    }

    public int getCancelWS() {
        return mCancelWS;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }

    public boolean isAutoCancel() {
        return mAutoCancel;
    }
    
    public boolean isSendNotification() {
        return mSendNotification == SEND_NOTIFICATION_TRUE;
    }

    public String getNotificationContent() {
        return mNotificationContent;
    }

    public String getSoftName() {
        if (mSoftName != null && !mSoftName.isEmpty()) {
            return mSoftName;
        } else {
            return getSubServiceName();
        }
    }

    public boolean isSendNotifyBeforeCancel() {
        return mSendNotifyBeforeCancel == SEND_NOTIFY_BEFORE_CANCEL_TRUE;
    }
    
    public boolean isSendNotifyAutoCancel() {
        return mSendNotifyAutoCancel == SEND_NOTIFY_AUTO_CANCEL_TRUE;
    }
    
    public String getNotifyBeforeCancel() {
        return mNotifyBeforeCancel;
    }

    public int getmDaySendNotifyBeforeCancel() {
        return mDaySendNotifyBeforeCancel;
    }
    
    public String getNotifyAutoCancel() {
        return mNotifyAutoCancel;
    }

    public int getPromotion() {
        return promotion;
    }

    public void setPromotion(int promotion) {
        this.promotion = promotion;
    }
    
    public void setRenewCheckMode(int renewCheckMode) {
        this.renewCheckMode = renewCheckMode;
    }
    
    public int getRenewCheckMode() {
        return renewCheckMode;
    }
    
    @Override
    public String toString() {
        return "(SubServiceName: " + getSubServiceName() + "; " +
               "SoftName: " + getSoftName()+ "; " +
               "ServiceName: " + getServiceName() + "; " +
               "SubFee: " + getSubFee() + "; " +
               "PromoFee: " + getPromoFee() + "; " +
               "SubType: " + getSubType() + "; " +
               "Monfee Hour: " + getMonfeeHour() + "; " +
//               "MonfeeWS: " + getMonfeeWS() + "; " +
//               "RegisterWS: " + getRegisterWS() + "; " +
//               "CancelWS: " + getCancelWS() + "; " +
               "Recheck Before Monfee: " + isRecheckBeforeMonfee() + "; " +
               "Require MPS Account: " + isRequireMPSAccount() + "; " +
               "Notify Monfee: " + isSendNotification() + "; " +
                "Notify before cancel: " + isSendNotification() + "; " +
                "Notify Monfee: " + isSendNotification() + "; " +
               "Status: " + getStatus() + ")";
    }
}
