/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.datatype;

/**
 *
 * @author hoand
 */
public class PriceCharge {
    public static final int STATUS_OK = 0;
    public static final int STATUS_IVALID = 1;
    public static final int STATUS_NOT_FOUND = 2;
    
    private int mCMDPrice, mMOPrice, mStatus;

    public PriceCharge(int cmdPrice, int moPrice, int mStatus) {
        this.mCMDPrice = cmdPrice;
        this.mMOPrice = moPrice;
        this.mStatus = mStatus;
    }

    public int getCMDPrice() {
        return mCMDPrice;
    }

    public int getMOPrice() {
        return mMOPrice;
    }

    public int getStatus() {
        return mStatus;
    }

    public void setCMDPrice(int mCMDPrice) {
        this.mCMDPrice = mCMDPrice;
    }

    public void setMOPrice(int mMOPrice) {
        this.mMOPrice = mMOPrice;
    }

    public void setStatus(int mStatus) {
        this.mStatus = mStatus;
    }
    
    @Override
    public String toString() {
        return "(Price: " + + getCMDPrice() + "; Mo: " + getMOPrice() + "; Status: " + getStatus() + ")";
    }
}
