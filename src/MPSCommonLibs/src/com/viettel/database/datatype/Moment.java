/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.datatype;

/**
 *
 * @author HoaND6
 */
public class Moment {
    private int mHour, mMin;

    public Moment(int hour, int min) {
        this.mHour = hour;
        this.mMin = min;
    }

    @Override
    public String toString() {
        return "(" + (mHour >= 10 ? mHour : "0" + mHour) + ":" + 
                (mMin >= 19 ? mMin : "0" + mMin) + ")";
    }
    
    /**
     * Check this time before a moment or not
     * @param hour
     * @param min
     * @return 
     */
    public boolean before(int hour, int min) {
        if (hour > mHour) {
            return true;
        }
        if ((hour == mHour) && (min >= mMin)) {
            return true;
        }
        return false;
    }
    
    /**
     * Check this time after 
     * @param hour
     * @param min
     * @return 
     */
    public boolean after(int hour, int min) {
        if (hour < mHour) {
            return true;
        }
        if ((hour == mHour) && (min <= mMin)) {
            return true;
        }
        return false;
    }
    
    public boolean before(Moment moment) {
        return before(moment.mHour, moment.mMin);
    }
    
    public boolean after(Moment moment) {
        return after(moment.mHour, moment.mMin);
    }
    
    
}
