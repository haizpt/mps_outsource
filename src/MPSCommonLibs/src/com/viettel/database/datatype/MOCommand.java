/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.datatype;

/**
 *
 * @author hoand
 */
public class MOCommand {
    public static final int TYPE_REGISTER = 1;
    public static final int TYPE_UNREGISTER = 2;
    public static final int TYPE_OTHERS = 3;
    public static final int TYPE_MPS_GET_PASS = 4;
    public static final int TYPE_MPS_CHANGE_PASS = 5;
    public static final int TYPE_RESTORE = 6;
    public static final int TYPE_RATE = 7;
    
    public static final int REQIRED_REGISTED = 1;
    public static final int NOT_REQIRED_REGISTED = 0;
    
    public static final int CHECK_REQIRED = 1;
    public static final int CHECK_NOT_REQIRED = 0;
    
    public static final int CHECK_PRICE_DEFAULT = 0;
    public static final int CHECK_PRICE_FROM_DB = 1;
    public static final int CHECK_PRICE_FROM_CP = 2;
    
    private String mCMD, mSubServiceName, mSuccessMessage, mFailedMessage, mSecondFailedMessage, mNotEnoughMoney,
                    mServiceName, mSubServiceCode, mShortCode;
    private String mExtSMS;
    private int mCMDType, mFee, mRequiredRegisted, mWebserviceID, mCheckRequired;
    private String mChargeCMD;
    private int mCheckPrice;
    private String mRawSMS;
    private int mFreeSMS, mLimitSMS, mCountDay;
    private String mCountKey;

    public MOCommand(String mCMD, String mSubServiceName, String mSuccessMessage, 
                     String mFailedMessage, String mSecondFailedMessage, String notEnoughMoney, String mServiceName, 
                     String subServiceCode, String mShortCode, 
                     int mCMDType, int mFee, int mRequiredRegisted, int mWebserviceID, String chargeCMD, 
                     int checkRequired, int checkPrice, int freeSMS, int limitSMS, int countDay, String countKey) {
        this.mCMD = mCMD;
        this.mSubServiceName = mSubServiceName;
        this.mSuccessMessage = mSuccessMessage;
        this.mFailedMessage = mFailedMessage;
        this.mSecondFailedMessage = mSecondFailedMessage;
        mNotEnoughMoney = notEnoughMoney;
        this.mServiceName = mServiceName;
        this.mShortCode = mShortCode;
        this.mCMDType = mCMDType;
        this.mFee = mFee;
        this.mRequiredRegisted = mRequiredRegisted;
        this.mWebserviceID = mWebserviceID;
        mSubServiceCode = subServiceCode;
        mExtSMS = "";
        mChargeCMD = chargeCMD;
        mCheckRequired = checkRequired;
        mCheckPrice = checkPrice;
        mFreeSMS = freeSMS;
        mCountDay = countDay;
        mCountKey = countKey;
        mLimitSMS = limitSMS;
    }

    public MOCommand duplicated() {
        return new MOCommand(mCMD, mSubServiceName, mSuccessMessage, mFailedMessage, 
                             mSecondFailedMessage, mNotEnoughMoney, mServiceName, 
                             mSubServiceCode, mShortCode, 
                             mCMDType, mFee, mRequiredRegisted, mWebserviceID, mChargeCMD, 
                             mCheckRequired, mCheckPrice, mFreeSMS, mLimitSMS, mCountDay, mCountKey);
    }
    
    public String getRawSMS() {
        return mRawSMS;
    }
    
    public void setRawSMS(String sms) {
        mRawSMS = sms;
    }
    
    public String getCMD() {
        return mCMD;
    }

    public String getSubServiceName() {
        return mSubServiceName;
    }

    public String getSuccessMessage() {
        return mSuccessMessage;
    }

//    public String getFailedMessage() {
//        return mFailedMessage;
//    }

    public String getServiceName() {
        return mServiceName;
    }

    public String getShortCode() {
        return mShortCode;
    }

    public int getCMDType() {
        return mCMDType;
    }

    public int getMOPrice() {
        return mFee;
    }

    public int getRequiredRegisted() {
        return mRequiredRegisted;
    }

    public int getWebserviceID() {
        return mWebserviceID;
    }

    public String getSecondFailedMessage() {
        return mSecondFailedMessage;
    }

    public String getExtSMS() {
        return mExtSMS;
    }

    public void setExtSMS(String mExtSMS) {
        this.mExtSMS = mExtSMS;
    }

    public String getSubServiceCode() {
        return mSubServiceCode;
    }

//    public String getNotEnoughMoney() {
//        return mNotEnoughMoney;
//    }

    public String getChargeCMD() {
        return mChargeCMD;
    }
    
    public boolean isCheckRequired() {
        return mCheckRequired == CHECK_REQIRED;
    }
    
    public int getCheckPriceType() {
        return mCheckPrice;
    }

    public int getFreeSMS() {
        return mFreeSMS;
    }

    public int getLimitSMS() {
        return mLimitSMS;
    }

    public int getCountDay() {
        return mCountDay;
    }

    public String getCountKey() {
        return mCountKey;
    }
    
    private String checkPriceFrom() {
        switch (mCheckPrice) {
            case 0: return "DEFAULT 0";
            case 1: return "Database";
            case 2: return "CP";
            default: return "Unknown";
        }
    }
    
    @Override
    public String toString() {
        if (getCMDType() == TYPE_RATE) {
            return rateString();
        }
        
        return "(CMD: " + getCMD() + "; " +
               "Type: " + getCMDType() + "; " +
               "SubService: " + getSubServiceName() + "; " +
               "ChargeCMD: " + getChargeCMD() + "; " +
               "Require Registed: " + getRequiredRegisted() + "; " +
               "WS ID: " + getWebserviceID() + "; " +
               "Check Reqired: " + isCheckRequired() + "; " +
               "Check Price From: " + checkPriceFrom()+ "; " +
               "ShortCode: " + getShortCode() + ")";
    }
    
    private String rateString() {
        return "(CMD: " + getCMD() + "; " +
               "Type: " + getCMDType() + "; " +
               "SubService: " + getSubServiceName() + "; " +
               "ChargeCMD: " + getChargeCMD() + "; " +
               "Require Registed: " + getRequiredRegisted() + "; " +
               "WS ID: " + getWebserviceID() + "; " +
               "Check Reqired: " + isCheckRequired() + "; " +
               "Check Price: " + checkPriceFrom() + "; " +
               "ShortCode: " + getShortCode() + "; " + 
               "Count Key: " + getCountKey() + "; " + 
               "Count Day: " + getCountDay() + "; " + 
               "Free SMS: " + getFreeSMS() + "; " + 
               "Limit SMS: " + getLimitSMS() + "; " + 
               "ShortCode: " + getShortCode() + ")"; 
    }

    public String getAlreadyRegisterMessage() {
        return mSecondFailedMessage;
    }

    public String getNotEnoughMoney() {
        return mNotEnoughMoney;
    }

    public String getFailMessage() {
        return mFailedMessage;
    }

}
