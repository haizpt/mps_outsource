/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.datatype;

import java.sql.Date;

/**
 *
 * @author HoaND6
 */
public class RegisteredSubServices {
    private String mSubServiceName;
    private int mStatus;
    private long mRegisterTime;

    public RegisteredSubServices(String mSubServiceName, int mStatus, long mRegisterTime) {
        this.mSubServiceName = mSubServiceName;
        this.mStatus = mStatus;
        this.mRegisterTime = mRegisterTime;
    }

    public String getmSubServiceName() {
        return mSubServiceName;
    }

    public int getmStatus() {
        return mStatus;
    }

    public long getmRegisterTime() {
        return mRegisterTime;
    }

    @Override
    public String toString() {
        return "(SubServiceName: "  + mSubServiceName + 
               "; mStatus: "  + mStatus + 
               "; mRegisterTime: "  + (new Date(mRegisterTime)) + ')';
    }
    
    
}
