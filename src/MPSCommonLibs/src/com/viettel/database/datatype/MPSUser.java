/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.datatype;

/**
 *
 * @author hoand
 */
public class MPSUser {
    private String mMsisdn, mPassword;
    private int mStatus, mCoin;
    private long mRegisterTime, mCancelTime;

    public MPSUser(String mMsisdn, String mPassword, int mStatus, int mCoin, long mRegisterTime, long mCancelTime) {
        this.mMsisdn = mMsisdn;
        this.mPassword = mPassword;
        this.mStatus = mStatus;
        this.mCoin = mCoin;
        this.mRegisterTime = mRegisterTime;
        this.mCancelTime = mCancelTime;
    }

    public String getMsisdn() {
        return mMsisdn;
    }

    public String getPassword() {
        return mPassword;
    }

    public int getStatus() {
        return mStatus;
    }

    public int getCoin() {
        return mCoin;
    }

    public long getRegisterTime() {
        return mRegisterTime;
    }

    public long getCancelTime() {
        return mCancelTime;
    }

    public void setMsisdn(String mMsisdn) {
        this.mMsisdn = mMsisdn;
    }

    public void setPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public void setStatus(int mStatus) {
        this.mStatus = mStatus;
    }

    public void setCoin(int mCoin) {
        this.mCoin = mCoin;
    }

    public void setRegisterTime(long mRegisterTime) {
        this.mRegisterTime = mRegisterTime;
    }

    public void setCancelTime(long mCancelTime) {
        this.mCancelTime = mCancelTime;
    }
    
    
    public String toString() {
        return "(MSISDN: " + getMsisdn() + "; " +
               "Pass: " + getPassword() + "; " +
               "Coin: " + getCoin() + "; " +
               "Status: " + getStatus() + ")" ;
    }
}
