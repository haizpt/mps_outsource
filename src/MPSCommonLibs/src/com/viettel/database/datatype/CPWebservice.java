/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.datatype;

/**
 *
 * @author hoand
 */
public class CPWebservice {
    private int mWebserviceID;
    private String mSubserviceName, mUserName, mPassword, mUrl, mParams, mWebserviceType, mSoapAction, mPrefix;
    private String mRawXML, mReturnTag, mDataSign;
    private int mTimeout;

    public CPWebservice(int mWebserviceID, String mSubserviceName, String mUserName, String mPassword, String mUrl, 
                        String mParams, String mWebserviceType, String soapAction, String tagPrefix, 
                        String rawXML, String returnTag, int timeout, String dataSign) {
        this.mWebserviceID = mWebserviceID;
        this.mSubserviceName = mSubserviceName;
        this.mUserName = mUserName;
        this.mPassword = mPassword;
        this.mUrl = mUrl;
        this.mParams = mParams;
        this.mWebserviceType = mWebserviceType;
        mSoapAction = soapAction;
        mPrefix = tagPrefix;
        
        if (mPrefix == null) {
            mPrefix = "";
        }
        
        mRawXML = rawXML;
        mReturnTag = returnTag;
        mTimeout = timeout;
        if (mTimeout < 1000) {
            mTimeout = 30000; // 30s
        }
        
        mDataSign = dataSign;
    }

    public int getWebserviceID() {
        return mWebserviceID;
    }

    public String getSubServiceName() {
        return mSubserviceName;
    }

    public String getUserName() {
        return mUserName;
    }

    public String getPassword() {
        return mPassword;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getParams() {
        return mParams;
    }

    public String getWebserviceType() {
        return mWebserviceType;
    }

    public String getSoapAction() {
        return mSoapAction;
    }

    public String getDataSign() {
        return mDataSign;
    }

//    public String getPrefix() {
//        return mPrefix;
//    }

    public String getRawXML() {
        return mRawXML;
    }

    public String getReturnTag() {
        return mReturnTag;
    }

    public int getTimeout() {
        return mTimeout;
    }
    
    public void setTimeout(int timeout) {
        mTimeout = timeout;
    }
    
    @Override
    public String toString() {
        return "SubService: " + getSubServiceName() + "; " +
               "URL: " + getUrl() + "; " ;
              
    }
}
