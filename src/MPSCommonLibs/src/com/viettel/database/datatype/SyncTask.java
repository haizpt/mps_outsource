/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.database.datatype;

import java.util.Hashtable;

/**
 *
 * @author HoaND6
 */
public class SyncTask {
    private String mMSISDN;
    private Hashtable<String, RegisteredSubServices> mRegistered;

    public SyncTask(String mMSISDN, Hashtable<String, RegisteredSubServices> registered) {
        this.mMSISDN = mMSISDN;
        mRegistered = registered;
    }

    public String getMSISDN() {
        return mMSISDN;
    }

    public Hashtable<String, RegisteredSubServices> getRegistered() {
        return mRegistered;
    }


    @Override
    public String toString() {
        return "(MSISDN: " + mMSISDN + 
               "; Registered: " + mRegistered + ')';
    }
    
    
}
