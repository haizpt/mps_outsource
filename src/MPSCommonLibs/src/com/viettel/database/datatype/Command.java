/**
 *
 * handsome boy
 */

package com.viettel.database.datatype;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Dec 3, 2015
 * @mail haind25@viettel.com.vn
 */
public class Command {
    public static final String COMMAND_ALL = "ALL";
    public static final String COMMAND_SMS = "SMS";
    public static final String COMMAND_USSD = "USSD";
    public static final String COMMAND_WEB = "WEB";
    public static final String COMMAND_WAP = "WAP";
    public static final String COMMAND_IVR = "IVR";
    public static final String COMMAND_SYS = "SYS";
    public static final String COMMAND_RENEW = "RENEW";
    
    public static final int MESSAGE_CHANNEL_USSD_ONLY = 0;
    public static final int MESSAGE_CHANNEL_SMS_ONLY = 1;
    public static final int MESSAGE_CHANNEL_BOTH = 2;
    public static final int MESSAGE_CHANNEL_MULTI = 3;
    public static final int MESSAGE_CHANNEL_IVR_ONLY = 4;
    public static final int MESSAGE_CHANNEL_IVR_SMS = 5;
    
    public static final String MESSAGE_TYPE_SUCCESS = "SUCCESS";
    public static final String MESSAGE_TYPE_FAIL = "FAIL";
    public static final String MESSAGE_TYPE_CONFLICT = "CONFLICT";
    public static final String MESSAGE_TYPE_ALREADY = "ALREADY";
    public static final String MESSAGE_TYPE_NOT_ENOUGH_MONEY = "NOT_ENOUGH_MONEY";
    public static final String MESSAGE_TYPE_SUCCESS_REQUIRE_MPS_ACCOUNT = "SUCCESS_REQUIRE_MPS_ACCOUNT";
    public static final String MESSAGE_CONFIRM = "CONFIRM";
    public static final String NOT_REGISTER = "NOT_REGISTER";
    
    public static final int CHECK_REQIRED = 1;
    public static final int CHECK_NOT_REQIRED = 0;
    
    public static final int TYPE_REGISTER = 1;
    public static final int TYPE_UNREGISTER = 2;
    public static final int TYPE_OTHERS = 3;
    public static final int TYPE_MPS_GET_PASS = 4;
    public static final int TYPE_MPS_CHANGE_PASS = 5;
    public static final int TYPE_RESTORE = 6;
    public static final int TYPE_RATE = 7;
    
    private String command;
    
    private String id;
    private String subServiceName; //SUB_SERVICE_NAME
    private String description;
    private String successMessage;
    private String failMessage;
    private String confirmMessage;
    private String alreadyRegisterMessage;
    private String notRegisterMessage;
    private String notEnoughMoney;
    private String chargeCommand;
   
    
    private int status;
    
    private int type;
    private int webserviceId;
    private int moPrice;
    private int requiredRegisted;// REQUIRED_REGISTED;
    private int checkRequired;// CHECK_REQUIRED;
    private int checkPrice;// CHECK_PRICE;
    private int cmdPrice;//CMD_Price;
    private int messageChannel;
    
     public Command(String command, String id, String subServiceName, String description, String successMessage, String failMessage, String notEnoughMoney, 
                        String chargeCommand, int status, int type, int webServiceID, int moPrice, int requiredRegisted, 
                        int checkRequired, int checkPrice, int messageChannel, String confirmMessage, String alreadyRegisterMessage, String notRegisterMessage, int cmdPrice) {
        this.command = command;
        this.id=id;
        this.subServiceName=subServiceName;
        this.description=description;
        this.successMessage = successMessage;
        this.failMessage = failMessage;
        this.notEnoughMoney = notEnoughMoney;
        this.chargeCommand = chargeCommand;
        this.status = status;
        this.type = type;
        this.webserviceId = webServiceID;
        this.moPrice = moPrice;
        this.requiredRegisted = requiredRegisted;
        this.checkPrice = checkPrice;
        this.messageChannel = messageChannel;
        this.confirmMessage = confirmMessage;
        this.alreadyRegisterMessage = alreadyRegisterMessage;
        this.notRegisterMessage = notRegisterMessage;
        this.cmdPrice = cmdPrice;
        this.checkRequired = checkRequired;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubServiceName() {
        return subServiceName;
    }

    public void setSubServiceName(String subServiceName) {
        this.subServiceName = subServiceName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public String getFailMessage() {
        return failMessage;
    }

    public void setFailMessage(String failMessage) {
        this.failMessage = failMessage;
    }

    public String getConfirmMessage() {
        return confirmMessage;
    }

    public void setConfirmMessage(String confirmMessage) {
        this.confirmMessage = confirmMessage;
    }

    public String getAlreadyRegisterMessage() {
        return alreadyRegisterMessage;
    }

    public void setAlreadyRegisterMessage(String alreadyRegisterMessage) {
        this.alreadyRegisterMessage = alreadyRegisterMessage;
    }

    public String getNotRegisterMessage() {
        return notRegisterMessage;
    }

    public void setNotRegisterMessage(String notRegisterMessage) {
        this.notRegisterMessage = notRegisterMessage;
    }

    public String getNotEnoughMoney() {
        return notEnoughMoney;
    }

    public void setNotEnoughMoney(String notEnoughMoney) {
        this.notEnoughMoney = notEnoughMoney;
    }

    public String getChargeCommand() {
        return chargeCommand;
    }

    public void setChargeCommand(String chargeCommand) {
        this.chargeCommand = chargeCommand;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getWebserviceId() {
        return webserviceId;
    }

    public void setWebserviceId(int webserviceId) {
        this.webserviceId = webserviceId;
    }

    public int getMoPrice() {
        return moPrice;
    }

    public void setMoPrice(int moPrice) {
        this.moPrice = moPrice;
    }

    public boolean isCheckRequired() {
        return checkRequired == CHECK_REQIRED;
    }

    public void setRequiredRegisted(int requiredRegisted) {
        this.requiredRegisted = requiredRegisted;
    }

    public int getCheckRequired() {
        return checkRequired;
    }

    public int getRequiredRegisted() {
        return requiredRegisted;
    }
    
    public boolean isRequiredRegisted() {
        return requiredRegisted == 1;
    }

    public void setCheckRequired(int checkRequired) {
        this.checkRequired = checkRequired;
    }

    public int getCheckPrice() {
        return checkPrice;
    }

    public void setCheckPrice(int checkPrice) {
        this.checkPrice = checkPrice;
    }

    public int getCmdPrice() {
        return cmdPrice;
    }

    public void setCmdPrice(int cmdPrice) {
        this.cmdPrice = cmdPrice;
    }

    public int getMessageChannel() {
        return messageChannel;
    }

    public void setMessageChannel(int messageChannel) {
        this.messageChannel = messageChannel;
    }
     
    public String getMessage(String type) {
        if (MESSAGE_TYPE_SUCCESS.equals(type)) {
            return  getSuccessMessage();
        }
        else if(MESSAGE_TYPE_ALREADY.equals(type)){
            return getAlreadyRegisterMessage();
        }
        else if(MESSAGE_TYPE_NOT_ENOUGH_MONEY.equals(type)){
            return getNotEnoughMoney();
        }
        else if(MESSAGE_TYPE_SUCCESS_REQUIRE_MPS_ACCOUNT.equals(type)){
            return getSuccessMessage();
        }
        else if(MESSAGE_CONFIRM.equals(type)){
            return getConfirmMessage();
        }
        else if(NOT_REGISTER.equals(type)){
            return getNotRegisterMessage();
        }
        return getFailMessage();
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }
    
}
