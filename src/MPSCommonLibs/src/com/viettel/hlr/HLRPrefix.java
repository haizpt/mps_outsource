/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.hlr;

import com.viettel.utilities.MyLog;

/**
 *
 * @author Hoa
 */
public class HLRPrefix {
    protected String[] mPrefix;

    public HLRPrefix(String initStr) {
        MyLog.Infor("CREATE HLRPrefix: " + initStr);

        mPrefix = initStr.split(",");

        if (mPrefix != null) {
            MyLog.Infor("TOTAL: " + mPrefix.length);
            for (int i=0; i<mPrefix.length; i++) {
                mPrefix[i] = mPrefix[i].trim();
                MyLog.Infor("Add: '" + mPrefix[i] + "'");
            }
        }
    }

    public boolean match(String msisdn) {
        if (mPrefix != null) {
            for (int i=0; i<mPrefix.length; i++)
                if (msisdn.indexOf(mPrefix[i]) == 0)
                    return true;

            return false;
        } else {
            return false;
        }
    }
}
