/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.hlr;

import com.viettel.utilities.MyLog;
import java.io.FileInputStream;
import java.util.Properties;

/**
 *
 * @author hoand
 */
public class HLR {
    private static HLR mMe = null;
    private String mConfig;

    private int mNumberOfHLR;
    private HLRPrefix mHLR[];

    private HLR() {
    }

    public static HLR getInstance() {
        if (mMe == null) {
            mMe = new HLR();
        }

        return mMe;
    }

    public int getNumberOfHLR() {
        return mNumberOfHLR;
    }
    
    public void setConfigFile(String conf) {
        if (conf.equals(mConfig)) {
            mConfig = conf;
            reloadConfig();
        }
    }

    public void reloadConfig() {
        try {
            mNumberOfHLR = 0;
            mHLR = null;

            Properties prop = new Properties();
            FileInputStream gencfgFile = new FileInputStream(mConfig);
            prop.load(gencfgFile);

//            try {
//                mHLRGWServer = prop.getProperty("hlrgw_server");
//                mHLRGWUser = prop.getProperty("hlrgw_user");
//                mHLRGWPass = prop.getProperty("hlrgw_pass");
//            } catch (Exception e) {
//                mHLRGWServer = "10.58.32.210:9997";
//                mHLRGWUser = "vstore";
//                mHLRGWPass = "vstore123";
//            }
//
//            MyLog.Infor("HLRGW: '" + mHLRGWUser + "'/'" + mHLRGWPass + "'@" + mHLRGWServer);
//            TextSecurity sec = TextSecurity.getInstance();
//            mHLRGWPass = sec.Decrypt(mHLRGWPass);

            mNumberOfHLR = Integer.parseInt(prop.getProperty("number_of_hlr"));
            MyLog.Infor("NUMBER OF HLR: " + mNumberOfHLR);
            
            mHLR = new HLRPrefix[ mNumberOfHLR ];
            for (int i=0; i<mNumberOfHLR; i++) {
                try {
                    String tmp = prop.getProperty("HLR" + (i + 1));
                    MyLog.Infor("Got HLR" + (i+1) + ": " + tmp);
                    mHLR[i] = new HLRPrefix(tmp);
                } catch (Exception ex) {
                    MyLog.Error("Error loading HLR" + (i + 1));
                }
            }

            gencfgFile.close();
        } catch (Exception ex) {
            MyLog.Error("Error while loading HLR Config file '" + mConfig + "': " + ex.getMessage());
            mHLR = null;
            mNumberOfHLR = 0;
        }
    }

    public int getHLRNo(String msisdn) {
        String newMSISDN = "84" + nomalizeMSISDN(msisdn);
        for (int i=0; i<mHLR.length; i++)
                if (mHLR[i].match(newMSISDN)) {
                    return i + 1;
                }

        return -1;
    }

    public static String nomalizeMSISDN(String msisdn) {
        if (msisdn.startsWith("0")) {
            return msisdn.substring(1);
        }
        if (msisdn.startsWith("84")) {
            return msisdn.substring(2);
        }
        if (msisdn.startsWith("+84")) {
            return msisdn.substring(3);
        }

        return msisdn;
    }
}
