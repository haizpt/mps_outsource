/**
 *
 * handsome boy
 */

package com.viettel.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Apr 27, 2015
 * @mail haind25@viettel.com.vn
 */
public class CountryCode {
    
    public final static String COUNTRY_CODE = "country_code";
    public final static String REPLACE_MOBILE_PREFIX = "replace_mobile_prefix";
    public final static String VIETTEL_RULES = "vietel_mobile_number_rules";
    
    private static CountryCode countryCode;
    
    private static final Object lock = new Object();
//    private final Map<Object,Object> values;
    
    private String _code ; //viet nam is 84
    private List<String> _replaceMobilePrefixCodes; //viet nam is 84,0, +84
    public List<String> _viettelRules; //viet nam is 84,0, +84
    
    private CountryCode(Map<Object,Object> values) {
//        this.values = values;
        Object tm = values.get(COUNTRY_CODE);
        if (null != tm && !"".equals(tm)) {
            _code = String.valueOf(tm);
        }
        
        tm = values.get(REPLACE_MOBILE_PREFIX);
        if (null != tm && !"".equals(tm)) {
            String _tm = String.valueOf(tm);
            List<String> codes = Arrays.asList(_tm.split(","));
            _replaceMobilePrefixCodes = codes;
        }
        
        tm = values.get(VIETTEL_RULES);
        if (null != tm && !"".equals(tm)) {
            String _tm = String.valueOf(tm);
            List<String> codes = Arrays.asList(_tm.split(","));
            _viettelRules = codes;
        }
        
    }
    
    public static void config(final Map<Object,Object> values) {
        if (countryCode == null) {
            countryCode = new CountryCode(values);
        } 
    }
    
    public static void config(String configFilePath) {
        FileInputStream gencfgFile;
        try {
            Properties prop = new Properties();
            gencfgFile = new FileInputStream(configFilePath);
            prop.load(gencfgFile);
            countryCode = new CountryCode(prop);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CountryCode.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CountryCode.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private static CountryCode getInstance() {
        synchronized(lock) {
            if (countryCode == null) {
               throw new IllegalArgumentException("please config values for Country code");
            }

            return countryCode;
        }
    }
    
    public synchronized static String getCountryCode() {
        if (getInstance()._code == null) {
           throw new IllegalArgumentException("please config values for Country code");
        }
        return getInstance()._code;
    }
    
    public synchronized static List<String> getReplacePrefixCodes() {
        if (getInstance()._replaceMobilePrefixCodes == null) {
           throw new IllegalArgumentException("please config values for Country code");
        }
        return getInstance()._replaceMobilePrefixCodes;
    }
    
    public synchronized static List<String> getViettelRules() {
        if (getInstance()._viettelRules == null) {
           throw new IllegalArgumentException("please config values for Country code");
        }
        return getInstance()._viettelRules;
    }
      
    public synchronized static String formatMobile(String msisdn) {
        if(msisdn == null || "".equals(msisdn) ) {
            return msisdn;
        }
        
        String value = msisdn;
        
        List<String> replacePrefixs = getReplacePrefixCodes();
        for (String replacePrefix : replacePrefixs) {
            if (value.startsWith(replacePrefix)) {
                value = value.substring(replacePrefix.length());
                break;
            }
        }
        
        return value;
    }
    
    public synchronized static boolean isViettelNumber(String msisdn) {
        String _msisdn = formatMobile(msisdn);
        
        List<String> viettelRules = getViettelRules();
        
        for(String viettelRule : viettelRules) {
            if (_msisdn.matches(viettelRule)) {
                return true;
            }
        }
        
        return false;
    }
    
    
}
