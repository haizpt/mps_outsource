package com.viettel.utilities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;


public class HtmlFormater extends Formatter {
	
	public static final String SERVERE_CSS = "servere";
	public static final String WARNING_CSS = "warning";
	public static final String NORNAL_CSS = "normal";
	
	// This method is called for every log records
	public String format(LogRecord rec) {
		StringBuffer buf = new StringBuffer(1000);
		
		String cssClass = null;
		
		if (rec.getLevel().intValue() == Level.SEVERE.intValue()) {
			cssClass = SERVERE_CSS;
		} else if (rec.getLevel().intValue() >= Level.WARNING.intValue()) {
			cssClass = WARNING_CSS;
		} else {
			cssClass = NORNAL_CSS;
		}
		
                HtmlLog cpInfo = null;
                if (rec.getParameters() != null && rec.getParameters().length > 0){
                    Object obj = rec.getParameters()[0];
                    if (obj instanceof HtmlLog) {
                        cpInfo = (HtmlLog) obj;
                    }
                }
                
                
		buf.append("<tr class=\"").append(cssClass).append("\">");
		buf.append("<td>\n");
		buf.append(rec.getLevel());
		buf.append("\n</td>\n");

		buf.append("<td>\n");
		buf.append(calcDate(rec.getMillis()));
		buf.append("\n</td>\n<td>\n");
                if(cpInfo != null) {
                    buf.append(cpInfo.getCpName());
                    buf.append("</td>\n<td>\n");
                    buf.append(cpInfo.getChannel());
                    buf.append("</td>\n<td>\n");
                    buf.append(cpInfo.getAction());
                    buf.append("</td>\n<td>\n");
                    buf.append(cpInfo.getIsdn());
                    buf.append("</td>\n<td>\n");
                    buf.append(cpInfo.getTransactionId());
                    buf.append("</td>\n<td>\n");
//                    buf.append(StringEscapeUtils.escapeXml(cpInfo.getErrorMsg()));
                    buf.append(cpInfo.getErrorMsg());
                } else {
                    buf.append("\n</td>\n<td>\n");
                    buf.append("\n</td>\n<td>\n");
                    buf.append("\n</td>\n<td>\n");
                    buf.append("\n</td>\n<td>\n");
                    buf.append("\n</td>\n<td>\n");
                    buf.append(formatMessage(rec));
                }
                
		buf.append("\n</td>\n");
		
		buf.append("</tr>\n");
		return buf.toString();
	}
	
	public String format(Level level, String mes) {
		StringBuffer buf = new StringBuffer(1000);
		
		String cssClass = null;
		
		if (level.intValue() == Level.SEVERE.intValue()) {
			cssClass = SERVERE_CSS;
		} else if (level.intValue() >= Level.WARNING.intValue()) {
			cssClass = WARNING_CSS;
		} else {
			cssClass = NORNAL_CSS;
		}
		
		buf.append("<tr class=\"").append(cssClass).append("\">");
		buf.append("<td>");
		buf.append(level);
		buf.append("</td>");

		buf.append("<td>");
		buf.append(calcDate(System.currentTimeMillis()));
		buf.append("</td>\n<td>\n");
                buf.append("</td>\n<td>\n");
                buf.append("</td>\n<td>\n");
                buf.append("</td>\n<td>\n");
                buf.append("</td>\n<td>\n");
                buf.append("</td>\n<td>\n");
		buf.append(mes);
		buf.append('\n');
		buf.append("</td>");
		
		buf.append("</tr>\n");
		return buf.toString();
	}

	private String calcDate(long millisecs) {
		SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date resultdate = new Date(millisecs);
		return date_format.format(resultdate);
	}

	// This method is called just after the handler using this
	// formatter is created
	public String getHead(Handler h) {
		return "<HTML>\n<HEAD>\n" 
                        + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n"
                        + "<style>\n.servere {font-weight: bold; color: red}\n"
                        + ".warning {font-weight: bold; color: rgb(133, 67, 15)}\n"
                        + ".normal {color: green}\n"
                        + "</style>\n"
                        + (new Date()) + "\n</HEAD>\n<BODY>\n<PRE>\n"
                        + "<table width=\"100%\" border>\n  " + "<tr><th>Level</th>"
                        + "<th>Time</th>" 
                        + "<th>CP Code</th>" 
                        + "<th>Channel</th>" 
                        + "<th>Action</th>" 
                        + "<th>Isdn</th>"
                        + "<th>transactionId</th>"
                        + "<th>Log Message</th>" + "</tr>\n";
	}

	// This method is called just after the handler using this
	// formatter is closed
	public String getTail(Handler h) {
		return "</table>\n  </PRE></BODY>\n</HTML>\n";
	}
}