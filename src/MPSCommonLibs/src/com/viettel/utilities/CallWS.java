/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.utilities;

import com.viettel.database.datatype.CPWebservice;
import com.wbxml.WbxmlLibs;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.lang3.StringEscapeUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author hoand
 */
public class CallWS {
    private static final int RETRY_CHARGE = 1;
    
    public static final int SERVICE_REGISTER = 0;
    public static final int SERVICE_UNREGISTER = 1;
    public static final int SERVICE_PENDING = 2;
    public static final int SERVICE_RESTORE = 3;
    
    public static final int RESULT_SUCCESS = 0;
    public static final int RESULT_FAILED = 1;
    public static final int RESULT_TIMEOUT = -1;
    
//    public static final int CHARGE_SUCCESS = 0;
    public static final int CHARGE_FAILED = 1;
    
    public static final int CHARGE_SUCCESS = 0;
    public static final int CHARGE_INVALID_PARAMS = 201;
    public static final int CHARGE_VAS_REQUEST_FAILED = 202;
    public static final int CHARGE_BALANCE_NOT_ENOUGH = 401;
    public static final int CHARGE_SUB_NOT_REGISTER = 402;
    public static final int CHARGE_SUB_NOT_EXIST = 403;
    public static final int CHARGE_MSISDN_NOT_AVAILABLE = 404;
    public static final int CHARGE_TWO_WAY_BLOCK = 409;
    public static final int CHARGE_GENERAL_ERROR = 440;
    public static final int CHARGE_MSISDN_CHANGE_OWNER = 405;
    public static final int CHARGE_MSISDN_NOT_SYNC = 501;
    public static final int CHARGE_MSISDN_TIME_OUT = -1;
    
//    public static final int SERVICE_UNREGISTER = 1;
//    public static final int SERVICE_UNREGISTER = 1;
    public static final String CMD_REGISTER = "REGISTER";
    public static final String CMD_UNREGISTER = "CANCEL";
    public static final String CMD_MO = "MO";
    public static final String CMD_CHARGE = "CHARGE";
    public static final String CMD_MONFEE = "MONFEE";
    
    public static final String WS_MODE_CHECK = "CHECK";
    public static final String WS_MODE_REAL = "REAL";
    public static final String WS_MODE_PROMOTION = "PROMOTION";
    
    public static final String CHANNEL_SMS = "SMS";
    public static final String CHANNEL_USSD = "USSD";
    public static final String CHANNEL_WEB = "WEB";
    public static final String CHANNEL_WAP = "WAP";
    public static final String CHANNEL_IVR = "IVR";
    public static final String CHANNEL_SYS = "SYS";
    public static final String CHANNEL_CMS = "CMS";
    
    /*danh cho OCS huy sub, gui cdr cho MPS*/
    public static final String CHANNEL_OCS = "OCS"; 
    //    
    // ***********************************************************************
    // Replacement for calling CP Webservices
    // ***********************************************************************
    public static final String WS_USERNAME = "#USERNAME";
    public static final String WS_PASSWORD = "#PASSWORD";
    public static final String WS_SERVICE_NAME = "#SERVICE_NAME";
    public static final String WS_MSISDN = "#MSISDN";
    public static final String WS_CHARGE_TIME = "#CHARGE_TIME";
    public static final String WS_MODE = "#MODE";
    public static final String WS_PARAMS = "#PARAMS";
    public static final String WS_AMOUNT = "#AMOUNT";
    public static final String WS_COMMAND = "#FULLSMS"; // SMS la noi dung nguoi dung nhan len, doi IVR, SMS la phim vua bam
    public static final String WS_SMS_ITEM = "#SMS_ITEM";
    public static final String WS_TRANS_ID = "#TRANS_ID";
    public static final String WS_PROMOTION = "#PROMOTION";
    public static final String WS_SUBSCRIPTION_STATUS = "#SUBSCRIPTION_STATUS"; // check trang thai la sub hay chua, danh cho ivr va ussd
    public static final String WS_CHANEL = "#CHANNEL"; // check trang thai la sub hay chua, danh cho ivr va ussd
    
    public static final String WS_DATETIME = "#yyyyMMddHHmmss";
    public static final String WS_EXTSMS = "#EXTSMS";
    public static final String WS_DATA_SIGN = "#DATA_SIGN";
    
    public static final int THIRTY_SECOND_TIMEOUT = 30 * 1000; // ms
    public static final int CHARGING_TIMEOUT = 120 * 1000; //ms
    
    public static void disableSslVerification() {
        try {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    @Override
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }
            };

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            MyLog.Error(e);
        } catch (KeyManagementException e) {
            MyLog.Error(e);
        }
    }

    public static String callHttpsWS(String wsURL, String inputXML, String soapAction, int retry, int timeout) {
        String result = "";
        String responseString;
        
        disableSslVerification();
        
        int i=0;
        for (i = 0; i<retry; i++) {
            MyLog.Debug("Try " + i + " Call WS (" + wsURL + ") got XML input: " + inputXML);
            
            try {
                URL url = new URL(wsURL);
                URLConnection connection = url.openConnection();
                HttpsURLConnection httpConn = (HttpsURLConnection) connection;
                ByteArrayOutputStream bout = new ByteArrayOutputStream();
                bout.write(inputXML.getBytes());

                byte[] b = bout.toByteArray();
                // Set the appropriate HTTP parameters.
                httpConn.setReadTimeout(timeout); // ms
                httpConn.setConnectTimeout(timeout); // ms
                httpConn.setRequestProperty("Content-Length", String.valueOf(b.length));
                httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
                httpConn.setRequestProperty("SOAPAction", soapAction);

                httpConn.setRequestMethod("POST");
                httpConn.setDoOutput(true);
                httpConn.setDoInput(true);
                DataOutputStream out = new DataOutputStream(httpConn.getOutputStream());
                //Write the content of the request to the outputstream of the HTTP Connection.
                out.write(b);
                out.close();
                //Ready with sending the request.
                //Read the response.
                InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
                BufferedReader in = new BufferedReader(isr);

                //Write the SOAP message response to a String.
                while ((responseString = in.readLine()) != null) {
                    result += responseString;
                }
            } catch (Exception ex) {
                MyLog.Error(ex);
                result = "";
            }
            
            if (result != null && !result.isEmpty()) {
                break;
            }
        }
        
        return result;
    }
    
    public static String callHttpWS(String wsURL, String inputXML, String soapAction, int retry, int timeout) {
        String result = "";
        String responseString;
        
        int i=0;
        for (i = 0; i<retry; i++) {
            MyLog.Debug("Try " + i + " Call WS (" + wsURL + ") got XML input: " + inputXML);
            
            try {
                URL url = new URL(wsURL);
                URLConnection connection = url.openConnection();
                HttpURLConnection httpConn = (HttpURLConnection)connection;
                ByteArrayOutputStream bout = new ByteArrayOutputStream();
                bout.write(inputXML.getBytes());

                byte[] b = bout.toByteArray();
                // Set the appropriate HTTP parameters.
                httpConn.setReadTimeout(timeout); // ms
                httpConn.setConnectTimeout(timeout); // ms
                httpConn.setRequestProperty("Content-Length", String.valueOf(b.length));
                httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
                httpConn.setRequestProperty("SOAPAction", soapAction);

                httpConn.setRequestMethod("POST");
                httpConn.setDoOutput(true);
                httpConn.setDoInput(true);
                OutputStream out = httpConn.getOutputStream();
                //Write the content of the request to the outputstream of the HTTP Connection.
                out.write(b);
                out.close();
                //Ready with sending the request.
                //Read the response.
                InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
                BufferedReader in = new BufferedReader(isr);

                //Write the SOAP message response to a String.
                while ((responseString = in.readLine()) != null) {
                    result += responseString;
                }
            } catch (Exception ex) {
                MyLog.Error(ex);
                result = "";
            }
            
            if (result != null && !result.isEmpty()) {
                break;
            }
        }
        MyLog.Debug("Try " + i + " Call WS got Response: '" + result + "'");
        
        return result;
    }
    
    public static String callWS(String wsURL, String inputXML, String soapAction, int retry, int timeout) {
        int newRetry = retry;
        if (newRetry <= 0) {
            newRetry = 1;
        }
        
        if (wsURL.toUpperCase().startsWith("HTTPS")) {
            return callHttpsWS(wsURL, inputXML, soapAction, newRetry, timeout);
        } else {
            return callHttpWS(wsURL, inputXML, soapAction, newRetry, timeout);
        }
    }
    
    public static String callSoap(String wsURL, String inputXML, String soapAction, int retry, int timeout, String returnTag) {
        int newRetry = retry;
        if (newRetry <= 0) {
            newRetry = 1;
        }
        
        if (wsURL.toUpperCase().startsWith("HTTPS")) {
            String tmp = null;
            try {
                String returnValue = callHttpsWS(wsURL, inputXML, soapAction, newRetry, timeout);
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                //parse using builder to get DOM representation of the XML file
                InputStream inp = new ByteArrayInputStream(returnValue.getBytes());
                Document doc = db.parse(inp);
                
                tmp = doc.getElementsByTagName(returnTag).item(0).getTextContent();
            } catch (ParserConfigurationException ex) {
                Logger.getLogger(CallWS.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SAXException ex) {
                Logger.getLogger(CallWS.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(CallWS.class.getName()).log(Level.SEVERE, null, ex);
            }
            return tmp;
        } else {
            return callHttpSoap(wsURL, inputXML, newRetry, timeout, returnTag);
        }
    }

    private static String callHttpSoap(String wsURL,String inputXML, int retry, int timeout,String returnTag ){
        int i=0;
        String result = null;
        for (i = 0; i< retry; i++) {
            MyLog.Debug("Try " + i + " Call WS (" + wsURL + ") got XML input: " + inputXML);
            SoapClient client = new SoapClient(wsURL.trim());
            Map<String,String> datas = client.sendRequest(inputXML, timeout);
            MyLog.Debug("inputXML = = " +  inputXML);
            MyLog.Debug("datas = " +  datas);
            if (datas != null && datas.size() >0) {
                if (datas.containsKey(returnTag)){
                    result = datas.get(returnTag);
                } else {
                    MyLog.Error("have result from cp, but do not have tag: " + returnTag);
                    for (Map.Entry<String, String> entry : datas.entrySet()) {
                            String string = entry.getKey();
                            String string1 = entry.getValue();
                            MyLog.Error(string + " => " + string1);
                    }
                }
            }
            
            if (result != null && !result.isEmpty()) {
                break;
            }
        }
        
        return result;
    }
    
    /**
     * @param url
     * @param username
     * @param password
     * @param msisdn
     * @param content
     * @param shortCode
     * @param retry
     * @param alias
     * @param smsType
     * @return 0 - success, -1 fail
     */
    public static int sendMT(String url, String username, String password, 
                       String msisdn, String content, String shortCode, String alias, String smsType, int retry) {
        boolean usedHex = "HEX_TEXT".equals(smsType);//xxx
        int result = -1;
        if (content == null || content.isEmpty()) {
            MyLog.Warning("SendMT is called with Empty SMS");
            return result;
        }
        
        long start = System.currentTimeMillis();
        String soapAction = url;
        String newContent = StringEscapeUtils.escapeXml(content);
        
        if (usedHex) {
            newContent = convertContent(content, true);
        }
        
        String xmlInput = 
                "<?xml version=\"1.0\" ?>" + 
                "<S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\">" + 
                "<S:Body>" + 
                    "<smsRequest xmlns=\"http://smsws/xsd\">" + 
                        "<username>" + username + "</username>" + 
                        "<password>" + password + "</password>" + 
                        "<msisdn>" + msisdn + "</msisdn>" + 
                        "<content>" + newContent + "</content>" + 
                        "<shortcode>" + shortCode + "</shortcode>" + 
                        "<alias>" + alias + "</alias>" + 
                        "<params>" + smsType + "</params>" + 
                    "</smsRequest>" + 
                "</S:Body>" + 
                "</S:Envelope>";
        try {
            String xmlOut = callWS(url, xmlInput, soapAction, retry, THIRTY_SECOND_TIMEOUT);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            //parse using builder to get DOM representation of the XML file
            InputStream inp = new ByteArrayInputStream(xmlOut.getBytes());
            Document doc = db.parse(inp);
            
            result = Integer.parseInt(doc.getElementsByTagName("return").item(0).getTextContent());
            MyLog.Infor("Send SMS to '" + msisdn + "' -> " + result + 
                    " (" + (System.currentTimeMillis() - start) + " ms)");
        } catch (Exception e) {
            MyLog.Error(e);
            result = -1;
        }
        return result;
    }
    
    /**
     * Auto convert content to HEX or not
     *
     * @param content
     * @return
     */
    private static String convertContent(String content, boolean useHex) {
        if (useHex) {
            String charSet = CharsetDetector.detectCharsetStr(content);
            if (charSet != null && !charSet.equals("ASCII")) {
                try {
                    return WbxmlLibs.byteArrToHexStr(content.getBytes(charSet));
                } catch (Exception ex) {
                    return WbxmlLibs.byteArrToHexStr(content.getBytes());
                }
            } else {
                return WbxmlLibs.byteArrToHexStr(content.getBytes());
            }
        } else {
            return content;
        }
    }
    
    public static int chargeUser(String url, String msisdn, String command, int amount, 
                                 String provider, String service, String subService, String tranId) {
        return chargeUser(url,msisdn, command,amount,provider,service,subService,tranId, null);
    }
    
    /**
     * Charge user with amount
     * @param url
     * @param msisdn
     * @param command
     * @param amount
     * @param provider
     * @param service
     * @param subService
     * @param tranId
     * @param channel
     * @return 
     */
    public static int chargeUser(String url, String msisdn, String command, int amount, 
                                 String provider, String service, String subService, String tranId, String channel) {
        MyLog.Debug("Charge User '" + msisdn + "' amount " + amount + "(" + subService + ")");
        long start = System.currentTimeMillis();
        String soapAction = "";
        int result = CHARGE_MSISDN_TIME_OUT;
        String m_channel = channel == null? "" : channel;
        
        String xmlInput = 
//                "<?xml version=\"1.0\" ?>" + 
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws.payment.mps.viettel.com/\">" +
                "<soapenv:Header/>" + 
                "<soapenv:Body>" + 
                   "<ws:doCharge>" + 
                      "<msisdn>" + msisdn + "</msisdn>" + 
                      "<amount>" + amount + "</amount>" + 
                      "<serviceName>" + service + "</serviceName>" + 
                      "<providerName>" + provider + "</providerName>" + 
                      "<channel>" + m_channel + "</channel>" + 
                      "<subCpName></subCpName>" + 
                      "<category></category>" + 
                      "<item></item>" + 
                      "<command>" + command + "</command>" + 
                      "<subService>" + subService + "</subService>" + 
                      "<transId>" + tranId + "</transId>" + 
                   "</ws:doCharge>" + 
                "</soapenv:Body>" + 
             "</soapenv:Envelope>";
//        MyLog.Debug(xmlInput);
        try {
            String xmlOut = callWS(url, xmlInput, soapAction, RETRY_CHARGE, CHARGING_TIMEOUT);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            //parse using builder to get DOM representation of the XML file
            InputStream inp = new ByteArrayInputStream(xmlOut.getBytes());
            Document doc = db.parse(inp);
            
            result = Integer.parseInt(doc.getElementsByTagName("return").item(0).getTextContent());
        } catch (Exception e) {
            MyLog.Error(e);
        }

        MyLog.Infor("Finish Charge User '" + msisdn + "' amount " + amount + "(" + subService + ") -> " + result + 
                    " (" + (System.currentTimeMillis() - start) + " ms)");
        return result;
    }
    
     public static int chargeMonfeeUser(String url, String msisdn, String regTime, int amount, 
                                       String provider, String service, String subService, String tranId) {
         return chargeMonfeeUser(url, msisdn, regTime,amount,provider,service,subService,tranId, "RENEW", null);
     }
    
    /**
     * Charge monfee for user
     * @param url
     * @param msisdn
     * @param regTime Register time in yyyyMMddHHmmss format
     * @param amount
     * @param provider
     * @param service
     * @param subService
     * @param tranId
     * @return 
     */
    public static int chargeMonfeeUser(String url, String msisdn, String regTime, int amount, 
                                       String provider, String service, String subService, String tranId, String cmd,String channel) {
        MyLog.Debug("Charge User '" + msisdn + "' amount " + amount + "(" + subService + ")");
        long start = System.currentTimeMillis();
        String soapAction = "";
        String m_cmd = "MONFEE";
        if (cmd != null) {
            m_cmd = cmd;
        }
        //int result = CHARGE_BALANCE_NOT_ENOUGH;
        int result = CHARGE_MSISDN_TIME_OUT;
        String m_channel = channel == null? "SYS" : channel;
        
        String xmlInput = 
//                "<?xml version=\"1.0\" ?>" + 
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws.payment.mps.viettel.com/\">" +
                "<soapenv:Header/>" + 
                "<soapenv:Body>" + 
                   "<ws:doCharge>" + 
                      "<msisdn>" + msisdn + "</msisdn>" + 
                      "<amount>" + amount + "</amount>" + 
                      "<serviceName>" + service + "</serviceName>" + 
                      "<providerName>" + provider + "</providerName>" +
                      "<channel>" + m_channel + "</channel>" + 
                      "<subCpName></subCpName>" + 
                      "<category></category>" + 
                      "<item></item>" + 
                      "<registertime>" + regTime + "</registertime>" + 
                      "<command>" +m_cmd +"</command>" + 
                      "<subService>" + subService + "</subService>" + 
                      "<transId>" + tranId + "</transId>" + 
                   "</ws:doCharge>" + 
                "</soapenv:Body>" + 
             "</soapenv:Envelope>";
//        MyLog.Debug(xmlInput);
        try {
            String xmlOut = callWS(url, xmlInput, soapAction, RETRY_CHARGE, CHARGING_TIMEOUT);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            //parse using builder to get DOM representation of the XML file
            InputStream inp = new ByteArrayInputStream(xmlOut.getBytes());
            Document doc = db.parse(inp);
            
            result = Integer.parseInt(doc.getElementsByTagName("return").item(0).getTextContent());
        } catch (Exception e) {
            MyLog.Error(e);
        }

        MyLog.Infor("Finish Charge User '" + msisdn + "' amount " + amount + "(" + subService + ") -> " + result + 
                    " (" + (System.currentTimeMillis() - start) + " ms)");
        return result;
    }
    
    
//    public static String callCPWebservice() {
////        MyLog.Debug("Charge User '" + msisdn + "' amount " + amount);
//        MyLog.Debug("Call CP Webservice");
//        return "Can goi Webservice cua CP de lay noi dung.";
//    }
    
    private static String getRegisterStr(int action) {
        if (action == SERVICE_REGISTER) {
            return "Register";
        } else {
            return "Unregister";
        }
    }
    
    /**
     * Replace values to variable in POST XML
     * @param input
     * @param replace
     * @return 
     */
    public static String parseValue(String input, Properties replace) {
        if (input == null || input.isEmpty()) {
            return "";
        }
        
        String result = input;
        
        Enumeration<Object> key = replace.keys();
        while (key.hasMoreElements()) {
            String k = (String) key.nextElement();
            String val = replace.getProperty(k);
            result = result.replace(k, val);
        }
        return result;
    }
    
    /**
     * Call webservice from CP to register or unregister service
     * @param ws
     * @param params
     * @param retryTime
     * @return 
     */
    public static ContentResult cpRegister(CPWebservice ws, Properties params, int retryTime) {
        MyLog.Debug("Call CP Register Webservice " + ws + " for " + params);
        ContentResult result = new ContentResult(ContentResult.RESULT_ERROR, "CP Timeout");;
        int code = RESULT_TIMEOUT;
        long start = System.currentTimeMillis();
        String xmlInput = parseValue(ws.getRawXML().trim(), params);
        MyLog.Debug("========>>>  "+xmlInput);

        try {
            String tmp = callHttpSoap(ws.getUrl().trim(), xmlInput, retryTime, ws.getTimeout(), ws.getReturnTag().trim());
            if (tmp == null) {
                MyLog.Error("Can not send to CP");
                MyLog.Error(ws.getUrl().trim());
                MyLog.Error(xmlInput);
                return result;
            }
            StringTokenizer tok = new StringTokenizer(tmp, "|");
            code = Integer.parseInt(tok.nextToken());
            String desc = "";
            if (tok.countTokens() > 0) {
                desc = tok.nextToken();
            }
            result = new ContentResult(code, desc);
        } catch (Exception e) {
            MyLog.Error("Can not send to CP");
            MyLog.Error(e);
        }
        if (code == RESULT_SUCCESS) {
             MyLog.Infor("Call Register Webservice " + ws  + " -> ok," + (System.currentTimeMillis() - start) + " ms)");
        } else {
            MyLog.Infor("Call Register time " + ws.getSubServiceName() + " -> error,(" + (System.currentTimeMillis() - start) + " ms)");
            MyLog.Infor("Call Register getUrl " + ws.getUrl() + ", body: " + xmlInput);
//            MyLog.Infor("Call Register pramse " + ws + " for " + params);
        }
        
        MyLog.Infor("ket qua khi goi ws cp:result = " +result);
        return result;
    }
    
    /**
     * Call Monfee webservice from CP    
     * @param ws
     * @param params
     * @param retryTime
     * @return 
     */
    public static int cpMonFee(CPWebservice ws, Properties params, int retryTime) {
        MyLog.Debug("call CP MonFee Webservice " + ws + " with " + params);
        int result = RESULT_TIMEOUT;
        
        long start = System.currentTimeMillis();
        String xmlInput = parseValue(ws.getRawXML(), params);
        try {
            
            String tmp = callHttpSoap(ws.getUrl(), xmlInput, retryTime, ws.getTimeout(), ws.getReturnTag());
            if (tmp != null) {
                if (tmp.startsWith(RESULT_SUCCESS + "|") 
                        ||String.valueOf(RESULT_SUCCESS).equals(tmp)){
                    result = RESULT_SUCCESS;
                } else {
                    result = RESULT_FAILED;
                    MyLog.Error("send renew to cp but received not true: " + tmp);
                }
            } else {
                MyLog.Error("Send renew result to cp got error, may be can not connect or return tag incorrect ");
            }
        } catch (Exception e) {
            MyLog.Error("ERROR: can not send to cp");
            MyLog.Error(e);
        }
        
        MyLog.Debug("callCPMonFeeWebservice '" + ws + "' (" + params + ") -> " + result + " (" + (System.currentTimeMillis() - start) + " ms)");
        return result;
    }
    
    /**
     * hungtv45, thuc hien goi ws CP da khai bao len he thong MPS
     * Call webservice from CP to get Content
     * @param ws
     * @param params
     * @param retryTimes
     * @return 
     */
    public static ContentResult cpWebservice(CPWebservice ws, Properties params, int retryTimes) {
//        MyLog.Debug("Call CP Contents Webservice " + ws + " with " + params);
        ContentResult result = null;
        
        long start = System.currentTimeMillis();
        String xmlInput = parseValue(ws.getRawXML(), params);
        try {
            
            MyLog.Infor("starting call CP:" + ws.getUrl() + ",body: " + xmlInput );
            String tmp = callHttpSoap(ws.getUrl(), xmlInput, retryTimes, ws.getTimeout(), ws.getReturnTag());
            if (tmp == null) {
                return null;
            }
            StringTokenizer tok = new StringTokenizer(tmp, "|");
            int code = Integer.parseInt(tok.nextToken());
            String desc = "";
            if (tok.countTokens() > 0) {
                desc = tok.nextToken();
            }
            int moPrice = 0;
            int cmdPrice = 0;
            
            if (tok.countTokens() >= 2) {
                moPrice = Integer.parseInt(tok.nextToken());
                cmdPrice = Integer.parseInt(tok.nextToken());
            }
            
            result = new ContentResult(code, desc, moPrice, cmdPrice);
        } catch (Exception e) {
            MyLog.Error(e);
        }
        
        MyLog.Debug("GetContent '" + params + "' -> " + result + " (" + (System.currentTimeMillis() - start) + " ms)");
        return result;
    }
    
    public static void main(String[] args) {
        String tmp = "1|SUCCESS";
        StringTokenizer tok = new StringTokenizer(tmp, "|");
        int code = Integer.parseInt(tok.nextToken());
        System.out.println("CODE: "+ code);
    }
}
