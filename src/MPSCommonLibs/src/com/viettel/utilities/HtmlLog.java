/**
 *
 * handsome boy
 */

package com.viettel.utilities;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Nov 20, 2015
 * @mail haind25@viettel.com.vn
 */
public class HtmlLog {
   public static final String ACTION_REGISTER = "REGISTER";
   public static final String ACTION_CANCEL = "CANCEL";
   public static final String ACTION_GETPASSWORD = "GETPASSWORD";
   public static final String ACTION_OTHER = "OTHER";
   
   public static final String CHANNEL_SMS = "SMS";
   public static final String CHANNEL_EBP = "WEB|WAP";
   public static final String CHANNEL_RENEW = "RENEW";
   public static final String CHANNEL_IVR = "IVR";
   public static final String CHANNEL_USSD = "USSD";
    
   private String cpName;
   private String channel;
   private String action;
   private String errorMsg;
    private String isdn;
    private String transactionId;

    public HtmlLog(String cpName, String channel, String action, String isdn, String transactionId, String errorMsg) {
        this.cpName = cpName;
        this.channel = channel;
        this.action = action;
        this.isdn = isdn;
        this.transactionId = transactionId;
        this.errorMsg = errorMsg;
    }

    public String getCpName() {
        return cpName;
    }

    public String getChannel() {
        return channel;
    }

    public String getAction() {
        return action;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
    
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
    
    public String getIsdn() {
        return isdn;
    }

    public String getTransactionId() {
        return transactionId;
    }
}
