/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hungtv45
 */
public class GlobalConfig {
    private static Boolean FEATURE_PROMOTION_ENABLE = null;
    
    public static String FEATURE_ENABLE = "1";
    public static String FEATURE_DISABLE = "0";
    
    public static String paymentUrl = "http://127.0.0.1:9186/MPI/Payment?wsdl";
    //hungtv45 test
//    public static String paymentUrl = "http://10.30.165.147:9186/MPI/Payment?wsdl";
    public static String datetimeFormat="dd/MM/yyyy";
    public static int retryWebservice = 1;
    public static String cdr_dumper = "cdrdumper.conf";
    public static int KPI_CP_TIMEOUT = 3000;
    public static int KPI_CP_TIMEOUT_MAX = 10000; // 10s
    public final static String CONFIG_CDR_DUMPER = "cdr_dumper";
    public static String HTTP_PORTS = "9500"; // http ports
    
     public static String  sms_ws_url;
     public static String  sms_ws_username;
     public static String  sms_ws_password;
     public static String  sms_ws_shortcode;
    
    
    public static ResourceBundle resourceBundle;
    public final static Object lock = new Object();
    
    public static void config(InputStream inputStream) {
        synchronized(lock) {
            try {
                resourceBundle = new PropertyResourceBundle(inputStream);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(GlobalConfig.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(GlobalConfig.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(GlobalConfig.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public static void config(String configFile) {
        FileInputStream fis;
        try {
            fis = new FileInputStream(configFile);
            config(fis);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GlobalConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static String get(String key) {
        synchronized(lock) {
            try {
                return resourceBundle.getString(key);
            } catch (Exception ex) {
            }
            return null;
        }
    }
    
    /**
     * @author: haind25
     * @since  22/02/2017
     */
    public static boolean ENABLE_PROMOTION() {
        if (FEATURE_PROMOTION_ENABLE != null) {
            return FEATURE_PROMOTION_ENABLE;
        }
        
        String enable = get("FEATURE_PROMOTION_ENABLE");
        FEATURE_PROMOTION_ENABLE = enable != null 
                && FEATURE_ENABLE.equals(enable);
        
        return FEATURE_PROMOTION_ENABLE;
    }
}
