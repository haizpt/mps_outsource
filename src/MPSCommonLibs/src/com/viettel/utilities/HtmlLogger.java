package com.viettel.utilities;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <b>BUG FIXES:</b>
 * <ul>
 * <li>Synchronize when contruct new logger -- by <b>thanhnv60</b></li>
 * <li>Using WeakHashMap to make weak referent to Logger(s). This allow JVM to
 * auto GC when Logger(s) no longer used --by <b>thanhnv60</b></li>
 * </ul>
 *
 * @author thanhnv60
 *
 */
public class HtmlLogger {

    private static Logger logger = null;
    private LoggerInfo info;

    private HtmlFormater formatter;
    private static Map<Logger, LoggerInfo> s_loggers = new WeakHashMap<Logger, LoggerInfo>();
    private static String toDay;

    public static String HOME_FOLDER = "log" + File.separator;

    public HtmlLogger(String name) {
        this(name, Level.ALL);
    }

    public HtmlLogger(String name, Level level) {
        synchronized (s_loggers) {
            if (logger == null) {
                logger = Logger.getLogger("CP_KPI_");
            }

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd");
            String _toDay = sdf.format(new Timestamp(System.currentTimeMillis()));

            if (!s_loggers.containsKey(logger)) {
                createHandler(level, _toDay);
            } else {
                if (!_toDay.equals(HtmlLogger.toDay)) {
                    HtmlLogger.toDay = _toDay;
                    info = s_loggers.get(logger);
                    logger.removeHandler(info.handler);
                    createHandler(level, _toDay);
                } else {
                    info = s_loggers.get(logger);
                }
            }
        }
    }

    /**
     * Create file handler
     *
     * @param level
     * @param toDay String represent to day using to create log file name
     */
    private void createHandler(Level level, String toDay) {
        try {

            String fileName = HOME_FOLDER + toDay + "_%g.html";
            FileHandler handler = new FileHandler(fileName, true);

            logger.addHandler(handler);
            logger.setLevel(level);

            formatter = new HtmlFormater();
            handler.setFormatter(formatter);
            handler.setEncoding("UTF-8");

            info = new LoggerInfo(handler, fileName);

            s_loggers.put(logger, info);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    public void e(String message) {
//        logger.severe(message);
//    }
//
//    public void w(String message) {
//        logger.warning(message);
//    }
//
//    public void i(String message) {
//        logger.info(message);
//    }
//
//    public void f(String message) {
//        logger.finest(message);
//    }

    public void log(Level level, String msg) {
        logger.log(level, msg);
    }

    public void log(Level level, String msg, Object param1) {
        logger.log(level, msg, param1);
    }
    
    public void log(Level level, String msg, HtmlLog cpInfo) {
        logger.log(level, msg, cpInfo);
    }

//    public void log(Level level, String msg, Object params[]) {
//        logger.log(level, msg, params);
//    }
//
//    public void log(Level level, String msg, Throwable thrown) {
//        logger.log(level, msg, thrown);
//    }
//
//    public void flush() {
//        info.getHandler().flush();
//    }

    public String getFileName() {
        return info.getFileName();
    }

    class LoggerInfo {

        private FileHandler handler;
        private String fileName;

        public LoggerInfo() {

        }

        public LoggerInfo(FileHandler handler, String fileName) {
            this.handler = handler;
            this.fileName = fileName;
        }

        public FileHandler getHandler() {
            return handler;
        }

        public void setHandler(FileHandler handler) {
            this.handler = handler;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }
    }
}
