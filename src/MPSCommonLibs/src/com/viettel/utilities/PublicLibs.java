/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.viettel.utilities;

import com.viettel.database.oracle.DBConst;
import java.sql.Date;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.Properties;
import java.util.Random;
import java.util.Vector;
import java.util.regex.Pattern;

/**
 *
 * @author hoand
 */
public class PublicLibs {
    
    public static String nomalizeMSISDN(String msisdn) {
        return CountryCode.formatMobile(msisdn);
    }

    public static String internationalMSISDN(String msisd) {
        return CountryCode.getCountryCode() + CountryCode.formatMobile(msisd);
    }
    
    public static Vector<String> split(String src, String regex) {
        MyLog.Debug("Split String '" + src + "' by '" + regex + "'");
        Vector<String> result = new Vector<String>();
        StringBuilder b = new StringBuilder(src);
        while (b.length() > 0) {
            int idx = b.indexOf(regex);
            
            if (idx >= 0) {
                String s = b.substring(0, idx);
//                NGPLog.Debug("Got next word: '" + s + "'" );
                if (s == null) s = "";
                result.add(s);
            } else {
                // end, put the rest to queue
                result.add(b.toString());
            }
            // delete
            b.delete(0, idx + regex.length());
//            NGPLog.Debug("String left: '" + b.toString() + "'");
            
            idx = b.indexOf(regex);
            if (idx < 0) {
                // end, put the rest to string
                result.add(b.toString());
                break;
            }
        }
        
        return result;
    }
    
    public static String convertUnicodeToASCII(String s) {
        String s2 = s;
        try {
            String s1 = Normalizer.normalize(s, Normalizer.Form.NFKD);
            String regex = Pattern.quote("[\\p{InCombiningDiacriticalMarks}\\p{IsLm}\\p{IsSk}]+");
            s2 = new String(s1.replaceAll(regex, "").getBytes("ascii"), "ascii");

    //        System.out.println(s2);
    //        System.out.println(s.length() == s2.length());
//            return s2;
        } catch (Exception ex) {
            MyLog.Error(ex);
//            return s;
        }
        
        //-----------------
//        StringBuilder sb = new StringBuilder();
//        
//        for (int i = 0; i < s2.length(); i++) {
//            char ch = s2.charAt(i);
//            if (ch <= 0xFF) {
//                sb.append(ch);
//            }
//        }
        return s2.replace("?", "");
        
//        Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
//        string strFormD = s.Normalize(System.Text.NormalizationForm.FormD);
//        return regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
    }
    /**
     * 
     * @return yyyyMMddHHmmss
     */
    public static String getCurrentDateTime() {
//        Calendar c = Calendar.getInstance();
//        c.setTimeInMillis(System.currentTimeMillis());
        SimpleDateFormat f = new SimpleDateFormat("yyyyMMddHHmmss");
        return f.format(new Date(System.currentTimeMillis()));
    }
    
    /**
     * 
     * @return yyyyMMddHHmmss
     */
//    public static String getCurrentDateTimeMillis() {
////        Calendar c = Calendar.getInstance();
////        c.setTimeInMillis(System.currentTimeMillis());
//        SimpleDateFormat f = new SimpleDateFormat("yyyyMMddHHmmssSSS");
//        return f.format(new Date(System.currentTimeMillis()));
//    }
//    
    /**
     * 
     * @return yyyyMMddHHmmss
     */
    public static String formatDateTime(long time) {
        SimpleDateFormat f = new SimpleDateFormat("yyyyMMddHHmmss");
        return f.format(new Date(time));
    }
    
    public static String getRandomNumber() {
        Random r = new Random(System.currentTimeMillis());
        String result = r.nextInt(999) + "";
        
        for (int i=0; i< 3 - result.length(); i++) {
            result = "0" + result;
        }
        
        return result;
    }
    
    public static String getRandomPassword() {
        Random r = new Random(System.currentTimeMillis());
        String result = r.nextInt(999999) + "";
        
        for (int i=0; i< 6 - result.length(); i++) {
            result = "0" + result;
        }
        
        return result;
    }
    
    public static String getTranID(String moduleCode, String code) {
        return moduleCode + code + getCurrentDateTime() + getRandomNumber();
    }
    
    public static long getFirstMomentOfToday() {
        long current = System.currentTimeMillis();
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(current);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        
        return c.getTimeInMillis();
    }
    
    /**
     * count day
     * @param startDate
     * @param endDate
     * @return 
     */
    public static long getDayBetween(long startDate, long endDate) {
        long ms = endDate - startDate;
        long count = ms / DBConst.A_DAY_MS;
        count += (ms % DBConst.A_DAY_MS) > 0 ? 1 : 0;
        return count;
    }
    
    /**
     * Convert String to hashset
     * @param value
     * @return 
     */
    public static HashSet<String> convertHashSet(String value) {
        HashSet<String> result = new HashSet<String>();
        Collections.synchronizedSet(result);
        try {
            String temp[] = value.trim().split(",");
            for (int i=0; i<temp.length; i++) {
                if (temp[i] != null && !temp[i].trim().isEmpty()) {
                    result.add(temp[i].toUpperCase().trim());
                }
            }
        } catch (Exception ex) {
            MyLog.Warning("return empty Hashset");
        }
        return result;
    }
    
    public static HashSet<Integer> convertHashSetInt(String value) {
        HashSet<Integer> result = new HashSet<Integer>();
        Collections.synchronizedSet(result);
        try {
            String temp[] = value.trim().split(",");
            for (int i=0; i<temp.length; i++) {
                if (temp[i] != null) {
                    try {
                        int tmp = Integer.parseInt(temp[i].toUpperCase().trim());
                        result.add(tmp);
                    } catch (Exception ex) {}
                }
            }
        } catch (Exception ex) {
            MyLog.Warning("return empty Hashset");
        }
        return result;
    }
    
    public static String loadString(Properties p, String key, String defaultValue) {
        String result = defaultValue;
        if (p.getProperty(key) != null) {
            result = p.getProperty(key);
        }
        if (result != null) {
            return result.trim();
        } else {
            return result;
        }
    }
    
    public static int loadInt(Properties p, String key, int defaultValue) {
        int result = defaultValue;
        try {
            result = Integer.parseInt(p.getProperty(key).trim());
        } catch (Exception ex) {
            MyLog.Warning(key + " Does not exist, use default: " + defaultValue);
        }
        return result;
    }
    
    public static boolean loadBoolean(Properties p, String key, boolean defaultValue) {
        boolean result = defaultValue;
        try {
            result = Boolean.parseBoolean(p.getProperty(key).trim());
        } catch (Exception ex) {
            MyLog.Warning(key + " Does not exist, use default: " + defaultValue);
        }
        return result;
    }
    
    public static String[] loadArray(Properties p, String key) {
        String[] result = null;
        try {
            result = p.getProperty(key).trim().split(",");
            for (int i=0; i<result.length; i++) {
                if (result[i] != null) {
                    result[i] = result[i].toUpperCase().trim();
                }
            }
        } catch (Exception ex) {
            MyLog.Warning(key + " Does not exist, use empty Array");
        }
        return result;
    }
    
    public static String arrayToString(String[] arr) {
        StringBuilder b = new StringBuilder();
        
        if (arr != null) {
            for (int i=0; i<arr.length; i++) {
                b.append(arr[i]).append("', '");
            }
        }
        
        if (b.length() > 2) {
            b.delete(b.length() - 2, b.length());
        }
        
        return b.toString();
    }
    
    public static String[] parseArray(String input) {
        String[] result = null;
        if (input != null && !input.trim().isEmpty()) {
            result = input.split(",");
            for (int i=0; i<result.length; i++) {
                if (result[i] != null) {
                    result[i] = result[i].trim();
                }
            }
        }
        return result;
    }
}
