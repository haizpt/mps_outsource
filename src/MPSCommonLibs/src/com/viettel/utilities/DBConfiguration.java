/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.utilities;

import com.viettel.database.datatype.CPService;
import com.viettel.database.datatype.MOCommand;
import com.viettel.database.datatype.SubService;
import com.viettel.database.oracle.OracleConnection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.sql.DataSource;

/**
 *
 * @author HoaND6
 */
public class DBConfiguration implements Runnable {
    public static final String REPLY_MSG_SUCCESS = "SUCCESS_MESSAGE";
    public static final String REPLY_MSG_FAILED = "FAILED_MESSAGE";
    public static final String REPLY_MSG_REGISTER_CONFLICT = "REGISTER_CONFLICT";
    public static final String REPLY_MSG_ALREADY_REGISTER = "ALREADY_REGISTER";
    public static final String REPLY_MSG_NOT_REGISTER = "NOT_REGISTER";
    public static final String REPLY_MSG_NOT_ENOUGH_MONEY = "NOT_ENOUGH_MONEY";
    public static final String REPLY_MSG_LIMIT_SMS = "LIMIT_SMS";
    public static final String REPLY_MSG_END_FREE_SMS = "END_FREE_SMS";
    
//    public static final String REPLY_MSG_CP_ERROR = "CP_ERROR";
//    public static final String REPLY_MSG_FAILED_MESSAGE = "FAILED_MESSAGE";
//    public static final String REPLY_MSG_FAILED_MESSAGE = "FAILED_MESSAGE";
//    public static final String REPLY_MSG_FAILED_MESSAGE = "FAILED_MESSAGE";
//    public static final String REPLY_MSG_FAILED_MESSAGE = "FAILED_MESSAGE";
    
    
    public static int LOAD_SEQUENCE = 600 * 1000; //ms 10 minutes
    public static int SLEEP_TIME = 30 * 1000; //sleep 5 minutes
    
    protected static DBConfiguration mMe = null;
    private boolean mIsRunning;
    protected long mLastLoadConfig;
    protected OracleConnection mDBConnection;
    protected Map<String, CPService> mService;
    protected Map<String, SubService> mSubService;
    protected Vector<MOCommand> mMOCommand;
    private boolean mIsLoadSubservice, mIsLoadService, mIsLoadMOCommand;

    public static DBConfiguration getInstance(String config) {
        if (mMe == null) {
            mMe = new DBConfiguration(config);
            mMe.start();
        }
        return mMe;
    }
    
    protected DBConfiguration(String connection) {
        mIsRunning = false;
        mLastLoadConfig = 0;
        
        mIsLoadSubservice = mIsLoadService = mIsLoadMOCommand = true;
        mService = new Hashtable();
        mMOCommand = new Vector();
        mSubService = new Hashtable();
        mDBConnection = new OracleConnection(110, connection);
    }
    
    /**
     * @author haind25
     * @param datasource
     * @return 
     */
    public static DBConfiguration getInstance(DataSource datasource) {
        if (mMe == null) {
            mMe = new DBConfiguration(datasource);
            mMe.start();
        }
        return mMe;
    }
    
    /**
     * @author haind25
     * @param datasource
     */
    protected DBConfiguration(DataSource datasource) {
        mIsRunning = false;
        mLastLoadConfig = 0;
        
        mIsLoadSubservice = mIsLoadService = mIsLoadMOCommand = true;
        mService = new Hashtable();
        mMOCommand = new Vector();
        mSubService = new Hashtable();
        mDBConnection = new OracleConnection(datasource);
    }

    public boolean isLoadSubservice() {
        return mIsLoadSubservice;
    }

    public boolean isLoadService() {
        return mIsLoadService;
    }

    public boolean isLoadMOCommand() {
        return mIsLoadMOCommand;
    }

    public void setIsLoadSubservice(boolean mIsLoadSubservice) {
        this.mIsLoadSubservice = mIsLoadSubservice;
    }

    public void setIsLoadService(boolean mIsLoadService) {
        this.mIsLoadService = mIsLoadService;
    }

    public void setIsLoadMOCommand(boolean mIsLoadMOCommand) {
        this.mIsLoadMOCommand = mIsLoadMOCommand;
    }
    
    public void start() {
        if (!mIsRunning) {
            mIsRunning = true;
            Thread t = new Thread(this);
            t.start();
        }
    }

    public void stop() {
        mIsRunning = false;
    }
    
    public void reloadConfig() {
        if (System.currentTimeMillis() - mLastLoadConfig > LOAD_SEQUENCE) {
            // load here
            if (isLoadService()) {
                Map<String, CPService> tmp = mDBConnection.getAllServices();
                if (tmp != null && !tmp.isEmpty()) {
                    mService = tmp;
                }
            }
            
            if (isLoadMOCommand()) {
                Vector<MOCommand> tmp = mDBConnection.getAllMOCommand();
                if (tmp != null && !tmp.isEmpty()) {
                    mMOCommand = tmp;
                }
            }
            
            if (isLoadService()) {
                Map<String, SubService> tmp = mDBConnection.getAllSubService();
                if (tmp != null && !tmp.isEmpty()) { 
                    mSubService = tmp;
                }
            }
            //
            mLastLoadConfig = System.currentTimeMillis();
        }
    }
    
    /**
     * Get CPServices information
     * @param shortCode
     * @return 
     * 
     * Den day roi mai check tiep ko load dc cau hinh
     */
    public CPService getCPServices(String shortCode) {
//        waitLoading();
        for (Map.Entry<String, CPService> entry : mService.entrySet()) {
            CPService cPService = entry.getValue();            
            MyLog.Debug("shortCode.equals(cPService.getShortCode()) = " + shortCode.equals(cPService.getShortCode()));
            if (shortCode.equals(cPService.getShortCode())) {
                return cPService;
            }
        }
        return null;
    }
    
   /**
    * Get SubServices 
    * @param subServiceName
    * @return 
    */
    public SubService getSubServices(String subServiceName) {
        if (mSubService.containsKey(subServiceName)) {
            return mSubService.get(subServiceName);
        } else {
            return null;
        }
    }
    
    /**
     * Get Service
     * @param serviceName
     * @return 
     */
    public CPService getServices(String serviceName) {
//        waitLoading();
        for (String key : mService.keySet()) {
            CPService ser = mService.get(key);
            if (ser.getServiceName().equals(serviceName)) {
                return ser;
            }
        }
        
        return null;
    }
    
    
    /**
     * Get MO Command
     * @param shortCode
     * @param sms
     * @return 
     */
    public MOCommand getMOCommand(String shortCode, String sms) {
//        waitLoading();
        MOCommand result = null; 
        String cap = sms.toUpperCase();
        
        Enumeration<MOCommand> e = mMOCommand.elements();
        while (e.hasMoreElements()) {
            MOCommand cmd = e.nextElement();
            if (cmd.getShortCode().equals(shortCode)){
                if(cap.equals(cmd.getCMD())
                    || cap.matches(cmd.getCMD())) {
                    result = cmd.duplicated();
                    break;
            }
            }
        }

        MyLog.Debug("Get MOCommand for " + sms + " (" + shortCode + ") -> " + result);
        return result;
    }
    
    /**
     * @author haind25
     * @param shortCode
     * @param moType
     * @return 
     * @since 05.10.2015 (dd/MM/yyyy)
     */
    public List<MOCommand> getMOCommands(String shortCode, int moType) {
        List<MOCommand> commands = new LinkedList<MOCommand>();
        
        for (MOCommand mOCommand : mMOCommand) {
            if (mOCommand.getShortCode().equals(shortCode) 
                    && mOCommand.getCMDType() == moType) {
                commands.add(mOCommand);
            }
                    
        }
        
        return commands;
    }
    
    /**
     * Get Reply Message base on key
     * @param subServices
     * @param moCommand
     * @param msgKey
     * @return 
     */
    public String getMoReplyMessage(String subServices, String moCommand, String msgKey) {
        return mDBConnection.getMessage(subServices, moCommand, msgKey);
//        return "Dummy Message";
    }
    
    public String getStatistics() {
        StringBuilder result = new StringBuilder();
        
        result.append("CPServices On System\n");
        
        for (String tmp: mService.keySet()) {
            CPService val = mService.get(tmp);
            result.append("CPServices: ").append(tmp).append(" ").append(val).append("\n");
        }
        
        result.append("\nMOCommand On System\n");
        Enumeration<MOCommand> moList = mMOCommand.elements();
        while (moList.hasMoreElements()) {
            MOCommand mo = moList.nextElement();
            result.append("MOCommand: ").append(mo).append("\n");
        }
        
        result.append("SubServices On System\n");
       
        for (String tmp : mSubService.keySet()) {
            SubService val = mSubService.get(tmp);
            result.append("SubServices: ").append(tmp).append(" ").append(val).append("\n");
        }
        
        return result.toString();
    }
    
    @Override
    public void run() {
        while (mIsRunning) {
            try {
                reloadConfig();
                Thread.sleep(SLEEP_TIME);
            } catch (Exception ex) {
                MyLog.Error(ex);
            }
        }
    }
}
