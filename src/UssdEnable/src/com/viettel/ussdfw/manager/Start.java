/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.manager;

import java.io.File;

/**
 *
 * @author TuanNS3_S
 * @since 14/05/2013
 *
 */
public class Start {
    
    public static  String HOME_CONFIG = "../etc/";
    
    public static void main(String[] args) throws Exception {
        String tmHomeconfig = System.getProperty("USSD_HOME");
        if (tmHomeconfig != null) {
            HOME_CONFIG = tmHomeconfig;
        }
        if (!HOME_CONFIG.endsWith("/")) {
            HOME_CONFIG = HOME_CONFIG + File.separator;
        }

        System.out.println("USS_HOMME: " + HOME_CONFIG);
        // Khoi dong MoProcess
        AppManager.getInstance().start();
//        String filePath = "config/etc/global.cfg";
//        GlobalConfig.config(filePath);
    }
}
