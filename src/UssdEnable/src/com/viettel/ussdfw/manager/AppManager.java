/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.manager;

//import com.viettel.database.oracle.OracleConnection;
//import com.viettel.ussdfw.database.ConnectionPoolManager;
import com.viettel.cdrdumper.CDRDumper;
import com.viettel.database.oracle.KpiCpSoapDao;
import com.viettel.database.oracle.OracleConnection;
import com.viettel.database.oracle.SubLogsInserDao;
import com.viettel.database.oracle.SubscriberInserDao;
import com.viettel.database.oracle.SubscriberUpdateDao;
import com.viettel.database.service.BeanFactory;
import com.viettel.database.service.BussinessService;
import com.viettel.database.service.CachingeService;
import com.viettel.database.service.CpSubscriberDao;
import com.viettel.database.service.impl.PromotionManager;
import com.viettel.database.service.impl.ServiceFactory;
import com.viettel.soap.HttpSoap;
import com.viettel.ussdfw.database.ConnectionPoolManager;
import com.viettel.ussdfw.process.ProcessManager;
import com.viettel.ussdfw.utils.BussinessServiceFactory;
import com.viettel.ussdfw.utils.CpSubscriberServiceFactory;
import com.viettel.ussdfw.utils.UssdServiceFactory;
import com.viettel.utilities.CountryCode;
import com.viettel.utilities.GlobalConfig;
import com.viettel.utilities.HtmlLogger;
import com.viettel.utility.PropertiesUtils;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author TuanNS3_S
 * @since 14/05/2013
 *
 */
public class AppManager {

    private static AppManager instance;
    //
    private final Logger logger;
//    private int moManagerId;
    private ProcessManager processManager;
    // Config in app.conf
    public static String appId;
    public static int breakQuery;
    public static int queryDbTimeout;
    // config in loglevel.conf
    public static long[] timesDbLevel;
    public static long minTimeDb;
    public static HashMap loggerDbMap;
    //config log
    public static boolean logToDatabase;
    public static int numThreadLogVasgw;
    public static int numThreadLogTrans;
    public static String mainDatabaseId;
    
    public static String databaseTimestend;
    
    public static String logDatabaseConf;
    public static int numThreadSync;
    private final StringBuffer br = new StringBuffer();
    private boolean exception = false;
    
    public final static Object lock = new Object();
    
    private CDRDumper cDRDumper;
    
    public static AppManager getInstance() throws Exception {
        synchronized (lock) {
            if (instance == null) {
                instance = new AppManager();
            }

            return instance;
        }
    }
    
    public CDRDumper getCDRDumper() {
        if (cDRDumper == null) {
            cDRDumper = new CDRDumper();
//            cDRDumper.loadConfig(GlobalConfig.cdr_dumper);
            String cdrDuper = GlobalConfig.get(GlobalConfig.CONFIG_CDR_DUMPER);
            cDRDumper.loadConfig(cdrDuper);
            cDRDumper.start();
        }
        return cDRDumper;
    }

    public AppManager() throws Exception {
        PropertyConfigurator.configure(Start.HOME_CONFIG + "log.conf");
        logger = Logger.getLogger(AppManager.class);
        getConfig();
        loadLogLevelWarnning();
        initProcess();
    }

    private void getConfig() throws Exception {
        // Get appId
        String config = Start.HOME_CONFIG + "app.conf";
        FileReader fileReader = null;
        fileReader = new FileReader(config);
        Properties pro = new Properties();
        pro.load(fileReader);
        
        CountryCode.config(config);
        
        GlobalConfig.config(config);
        
        if (GlobalConfig.get("HTTP_PORTS") != null) {
            GlobalConfig.HTTP_PORTS = GlobalConfig.get("HTTP_PORTS");
        }
        
        if (GlobalConfig.get("LOG_KPI_DIRECTOR") != null) {
            String kpiHome = GlobalConfig.get("LOG_KPI_DIRECTOR");
            if (!kpiHome.endsWith(File.separator)) {
                kpiHome = kpiHome + File.separator;
            }
            HtmlLogger.HOME_FOLDER = kpiHome ;
        }
        if (GlobalConfig.get("KPI_TIMEOUT") != null) {
            try {
                GlobalConfig.KPI_CP_TIMEOUT
                        = Integer.parseInt(GlobalConfig.get("KPI_TIMEOUT").trim());
            } catch (NumberFormatException ex) {
            }
        }
        
        if (GlobalConfig.get("KPT_TIMEOUT_MAX") != null) {
            try {
                GlobalConfig.KPI_CP_TIMEOUT_MAX
                        = Integer.parseInt(GlobalConfig.get("KPT_TIMEOUT_MAX").trim());
            } catch (NumberFormatException ex) {
            }
        }
        //hungtv45
        if (GlobalConfig.get("paymentUrl") != null){
            GlobalConfig.paymentUrl = GlobalConfig.get("paymentUrl").trim();
        }
        else{
            br.append("don't find configure for paymentUrl in app.conf, get paymentUrl default = http://10.79.123.14:9186/MPI/Payment?wsdl");
        }
        if (GlobalConfig.get("retryWebservice") != null){
            GlobalConfig.retryWebservice = Integer.parseInt(GlobalConfig.get("retryWebservice").trim());
        }
        else{
            br.append("don't find configure for retryWebservice in app.conf, get paymentUrl default = 1");
        }
        
        GlobalConfig.sms_ws_url = GlobalConfig.get("sms_ws_url");
        GlobalConfig.sms_ws_username = GlobalConfig.get("sms_ws_username");
        GlobalConfig.sms_ws_password = GlobalConfig.get("sms_ws_password");
        GlobalConfig.sms_ws_shortcode = GlobalConfig.get("sms_ws_shortcode");
        
        br.setLength(0);
        // Ma ung dung
        try {
            AppManager.appId = pro.getProperty("APP_ID").toUpperCase();
        } catch (Exception ex) {
            br.append("APP_ID not found in app.conf\n");
            exception = true;
        }
        try {
            AppManager.queryDbTimeout =
                    Integer.parseInt(pro.getProperty("QUERY_DB_TIMEOUT"));
        } catch (Exception ex) {
            br.append("QUERY_DB_TIMEOUT not found in app.conf\n");
            exception = true;
        }
        try {
            AppManager.breakQuery =
                    Integer.parseInt(pro.getProperty("BREAK_QUERY")) * 1000;
        } catch (Exception ex) {
            br.append("BREAK_QUERY not found in app.conf\n");
            exception = true;
        }
        try {
            AppManager.numThreadLogVasgw =
                    Integer.parseInt(pro.getProperty("NUM_THREAD_LOG_VASGW"));
            if (AppManager.numThreadLogVasgw <= 0) {
                br.append("NUM_THREAD_LOG_VASGW need config in app.conf\n");
                AppManager.numThreadLogVasgw = 1;
            }
        } catch (Exception ex) {
            br.append("NUM_THREAD_LOG_VASGW not found in app.conf,default=1\n");
            AppManager.numThreadLogVasgw = 1;
        }
        try {
            AppManager.numThreadLogTrans =
                    Integer.parseInt(pro.getProperty("NUM_THREAD_LOG_TRANS"));
            if (AppManager.numThreadLogTrans <= 0) {
                br.append("NUM_THREAD_LOG_TRANS need config in app.conf\n");
                AppManager.numThreadLogVasgw = 1;
            }
        } catch (Exception ex) {
            br.append("NUM_THREAD_LOG_TRANS not found in app.conf,default=1\n");
            AppManager.numThreadLogTrans = 1;
        }

        try {
            AppManager.logToDatabase =
                    Boolean.parseBoolean(pro.getProperty("LOG_TO_DATABASE"));
        } catch (Exception ex) {
            br.append("LOG_TO_DATABASE not found in app.conf, default=false \n");
            logToDatabase = false;
//            exception = true;
        }
        if (AppManager.logToDatabase) {

            try {
                AppManager.mainDatabaseId =
                        pro.getProperty("LOG_TO_DATABASE_ID");
                if (AppManager.mainDatabaseId == null || AppManager.mainDatabaseId.length() <= 0) {
                    br.append("LOG_TO_DATABASE_ID need config in app.conf\n");
                    exception = true;
                }
            } catch (Exception ex) {
                br.append("LOG_TO_DATABASE_ID not found in app.conf\n");
                exception = true;
            }

            try {
                AppManager.logDatabaseConf =
                        pro.getProperty("LOG_DATABASE_CONF");
                if (AppManager.logDatabaseConf == null || AppManager.logDatabaseConf.length() <= 0) {
                    br.append("LOG_DATABASE_CONF need config in app.conf\n");
                    exception = true;
                }
            } catch (Exception ex) {
                br.append("LOG_DATABASE_CONF not found in app.conf\n");
                exception = true;
            }

        }
        
        AppManager.databaseTimestend =  pro.getProperty("DATABASE_TIMESTEND");
        
        try {
            AppManager.numThreadSync =
                    Integer.parseInt(pro.getProperty("NUM_THREAD_SYNC"));
            if (AppManager.numThreadSync <= 0) {
                br.append("NUM_THREAD_SYNC need config in app.conf\n");
                AppManager.numThreadSync = 1;
            }
        } catch (Exception ex) {
            br.append("NUM_THREAD_SYNC not found in app.conf,default=1\n");
            AppManager.numThreadSync = 1;
        }
    
        if (exception) {
            throw new Exception(br.toString());
        } else {
            if (br.length() > 0) {
                logger.warn(br);
            }
        }

    }

    private void initProcess() throws Exception {
//        System.out.println("=====================================================: " );
//        String libpath = System.getProperty("java.library.path");
//        libpath = "C:\\TimesTen\\tt1122_64\\bin;" + libpath;
//        System.setProperty("java.library.path", libpath);
//        System.out.println("libpath: " + System.getProperty("java.library.path"));
        
        processManager = new ProcessManager();
        processManager.loadConnector(Start.HOME_CONFIG + "connector.xml");
        processManager.loadLogThread();
        
        //load dabatabase
        ConnectionPoolManager.loadConfig(Start.HOME_CONFIG + "database.xml");
        DataSource dataSource =  ConnectionPoolManager.getDataSource(mainDatabaseId);
        
        OracleConnection oracleConnection = null;
        if (dataSource != null) {
            oracleConnection = new OracleConnection(dataSource);
            ConnectionPoolManager.setOracleConnection(oracleConnection);
            logger.info("OK - OracleConnection inited success!");
            //hungtv45 get dataSource
            SubscriberInserDao.config(dataSource);
            SubscriberUpdateDao.config(dataSource);
            SubLogsInserDao.config(dataSource);
            KpiCpSoapDao.config(dataSource);
            PromotionManager.config(oracleConnection);
        }
        
        getCDRDumper();
        
        Map<Class<?>, BeanFactory<?>> clazzs = new Hashtable<Class<?>, BeanFactory<?>>();
        
        if (oracleConnection != null) {
            BussinessServiceFactory bussinessService = new BussinessServiceFactory();
            clazzs.put(BussinessService.class, bussinessService);

            UssdServiceFactory ussdService = new UssdServiceFactory();
            clazzs.put(CachingeService.class, ussdService);
        }
        
        DataSource dataSourceTimestend = null;
        
        if (databaseTimestend != null ) {
            dataSourceTimestend =  ConnectionPoolManager.getDataSource(databaseTimestend);
        }
        
        if (dataSourceTimestend == null) {
            CpSubscriberServiceFactory serviceFactory = new CpSubscriberServiceFactory(dataSource, true);
            clazzs.put(CpSubscriberDao.class, serviceFactory);
            logger.info("Don't have timestend!");
        } else {
            CpSubscriberServiceFactory serviceFactory = new CpSubscriberServiceFactory(dataSourceTimestend, false);
            clazzs.put(CpSubscriberDao.class, serviceFactory);
            logger.info("Using have timestend!");
        }
        
        ServiceFactory.setBeanMapper(clazzs);

        logger.info("HTTP Port,  starting ..... " );
        logger.info("HTTP Port, port: " + GlobalConfig.HTTP_PORTS );
        HttpSoap handle = new HttpSoap(GlobalConfig.HTTP_PORTS);
        handle.start();
    } 
    
    /**
     * Doc thong tin file loglevel.conf
     */
    private void loadLogLevelWarnning() throws Exception {
        PropertiesUtils pros = new PropertiesUtils();
        pros.loadProperties(Start.HOME_CONFIG +  "loglevel.conf", false);
        try {
            String[] dbTimes = pros.getProperty("DB_TIMES").split(",");
            String[] dbKey = pros.getProperty("DB_MESSAGE_KEY").split(",");

            loggerDbMap = new HashMap();
            timesDbLevel = new long[dbTimes.length];
            minTimeDb = Long.parseLong(dbTimes[0].trim());
            for (int i = 0; i < dbTimes.length; i++) {
                timesDbLevel[i] = Long.parseLong(dbTimes[i].trim());
                loggerDbMap.put(i, dbKey[i].trim());
            }
        } catch (Exception ex) {
            logger.error("error get info DB_TIMES, DB_MESSAGE_KEY in loglevel.conf");
            loggerDbMap = null;
            throw ex;
        }
    }

    /**
     * Log cham database
     *
     * @param times
     * @return
     */
    public static String getTimeLevelDb(long times) {
        if (loggerDbMap != null) {
            int key = Arrays.binarySearch(timesDbLevel, times);
            if (key < 0) {
                key = -key - 2;
            }

            String label = (String) loggerDbMap.get(key);

            return (label == null) ? "-" : label;
        }
        return null;
    }

    public void start() {
        logger.info("+++ START VAS +++ ");
        processManager.start();
        logger.info("+++  SYSTEM IS STARTING  +++");
    }

    public void stop() {
        processManager.stop();
        logger.info("+++  SYSTEM IS STOPPING  +++");
    }
}
