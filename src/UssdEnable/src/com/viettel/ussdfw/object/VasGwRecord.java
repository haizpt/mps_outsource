/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.object;

import com.viettel.application.ussdapp.common.VasRequest;
import com.viettel.ussdfw.utils.DateUtils;
import java.util.Date;

/**
 *
 * @author TuanNS3_S
 */
public class VasGwRecord {

    private long vasGwLogId;
    private String msisdn;
    private String imsi;
    private long sid;
    private long cpId;
    private long bizid;
    private String request;
    private Date requestTime;
    private String response;
    private Date responseTime;
    private String transactionId;
    private int errorCode;
    private String errorDescription;

    public VasGwRecord(VasRequest req) {
        this.msisdn = req.getMsisdn();
        this.imsi = req.getImsi();
        this.cpId = req.getConnectorId();
        this.bizid = req.getBizId();
        this.transactionId = req.getTransId();
        this.request = req.getParams();
        this.requestTime = new Date();
        this.requestTime.setTime(req.getSendRecvTime());
    }

    public VasGwRecord() {
    }

    public long getBizid() {
        return bizid;
    }

    public void setBizid(long bizid) {
        this.bizid = bizid;
    }

    public long getCpId() {
        return cpId;
    }

    public void setCpId(long cpId) {
        this.cpId = cpId;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public Date getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Date responseTime) {
        this.responseTime = responseTime;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public long getSid() {
        return sid;
    }

    public void setSid(long sid) {
        this.sid = sid;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public long getVasGwLogId() {
        return vasGwLogId;
    }

    public void setVasGwLogId(long vasGwLogId) {
        this.vasGwLogId = vasGwLogId;
    }

    @Override
    public String toString() {
        return "VasGwRecord{" + "msisdn=" + msisdn + ", imsi=" + imsi + ", sid=" + sid + ", cpId=" + cpId + ", bizid=" + bizid + ", request=" + request + ", requestTime=" + requestTime + ", response=" + response + ", responseTime=" + responseTime + ", transactionId=" + transactionId + ", errorCode=" + errorCode + ", errorDescription=" + errorDescription + '}';
    }

    public String toLog(String separator) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getMsisdn() == null ? "" : this.getMsisdn());
        sb.append(separator);
        sb.append(this.getImsi() == null ? "" : this.getImsi());
        sb.append(separator);
        sb.append(this.getSid());
        sb.append(separator);
        sb.append(this.getCpId());
        sb.append(separator);
        sb.append(this.getBizid());
        sb.append(separator);
        sb.append(this.getRequest() == null ? "" : this.getRequest());
        sb.append(separator);
        sb.append(this.getRequestTime() == null ? "" : DateUtils.convertDateToString(this.getRequestTime(), "dd/MM/yyyy HH:mm:ss.FF3"));
        sb.append(separator);
        sb.append(this.getResponse() == null ? "" : this.getResponse());
        sb.append(separator);
        sb.append(this.getResponseTime() == null ? "" : DateUtils.convertDateToString(this.getResponseTime(), "dd/MM/yyyy HH:mm:ss.FF3"));
        sb.append(separator);
        sb.append(this.getTransactionId() == null ? "" : this.getTransactionId());
        sb.append(separator);
        sb.append(this.getErrorCode());
        sb.append(separator);
        sb.append(this.getErrorDescription() == null ? "" : this.getErrorDescription());
        return sb.toString().replaceAll("\n", "#@n@#").replaceAll("\t", "#@t@#");
    }
}
