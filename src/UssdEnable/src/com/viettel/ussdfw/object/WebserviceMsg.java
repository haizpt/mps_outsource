/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.object;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author TuanNS3_S
 */
public class WebserviceMsg {

    private String msisdn;
    private String request;
    private String response;
    private Element dcElement;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Element getDcElement() {
        return dcElement;
    }

    public void setDcElement(Element dcElement) {
        this.dcElement = dcElement;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getProperty(String name) {
        String result = null;
        if (dcElement != null) {
            NodeList list = dcElement.getElementsByTagName(name);
            Element ele = (Element) list.item(0);
            if (ele != null) {
                result = ele.getTextContent();
            }
        }
        return result;
    }
}
