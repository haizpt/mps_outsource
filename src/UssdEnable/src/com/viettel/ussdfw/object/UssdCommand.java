/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.object;

/**
 *
 * @author tungnt64
 */
public class UssdCommand {
    
    private String id;
    private String subServiceName; //SUB_SERVICE_NAME
    private String description;
    private String successMessage;
    private String failMessage;
    private String confirmMessage;
    private String alreadyRegisterMessage;
    private String notRegisterMessage;
    private String notEnoughMoney;
    private String chargeCommand;
    
    private int status;
    
    private int type;
    private int webserviceId;
    private int moPrice;
    private int requiredRegisted;// REQUIRED_REGISTED;
    private int checkRequired;// CHECK_REQUIRED;
    private int checkPrice;// CHECK_PRICE;
    private int cmdPrice;//CMD_Price;
    private int messageChannel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubServiceName() {
        return subServiceName;
    }

    public void setSubServiceName(String subServiceName) {
        this.subServiceName = subServiceName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public String getFailMessage() {
        return failMessage;
    }

    public void setFailMessage(String failMessage) {
        this.failMessage = failMessage;
    }

    public String getConfirmMessage() {
        return confirmMessage;
    }

    public void setConfirmMessage(String confirmMessage) {
        this.confirmMessage = confirmMessage;
    }

    public String getAlreadyRegisterMessage() {
        return alreadyRegisterMessage;
    }

    public void setAlreadyRegisterMessage(String alreadyRegisterMessage) {
        this.alreadyRegisterMessage = alreadyRegisterMessage;
    }

    public String getNotRegisterMessage() {
        return notRegisterMessage;
    }

    public void setNotRegisterMessage(String notRegisterMessage) {
        this.notRegisterMessage = notRegisterMessage;
    }

    public String getNotEnoughMoney() {
        return notEnoughMoney;
    }

    public void setNotEnoughMoney(String notEnoughMoney) {
        this.notEnoughMoney = notEnoughMoney;
    }

    public String getChargeCommand() {
        return chargeCommand;
    }

    public void setChargeCommand(String chargeCommand) {
        this.chargeCommand = chargeCommand;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getWebserviceId() {
        return webserviceId;
    }

    public void setWebserviceId(int webserviceId) {
        this.webserviceId = webserviceId;
    }

    public int getMoPrice() {
        return moPrice;
    }

    public void setMoPrice(int moPrice) {
        this.moPrice = moPrice;
    }

    public int getRequiredRegisted() {
        return requiredRegisted;
    }

    public void setRequiredRegisted(int requiredRegisted) {
        this.requiredRegisted = requiredRegisted;
    }

    public int getCheckRequired() {
        return checkRequired;
    }

    public void setCheckRequired(int checkRequired) {
        this.checkRequired = checkRequired;
    }

    public int getCheckPrice() {
        return checkPrice;
    }

    public void setCheckPrice(int checkPrice) {
        this.checkPrice = checkPrice;
    }

    public int getCmdPrice() {
        return cmdPrice;
    }

    public void setCmdPrice(int cmdPrice) {
        this.cmdPrice = cmdPrice;
    }

    public int getMessageChannel() {
        return messageChannel;
    }

    public void setMessageChannel(int messageChannel) {
        this.messageChannel = messageChannel;
    }
    
}
