/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.object;

import com.viettel.ussdfw.utils.DateUtils;
import java.sql.Timestamp;

/**
 *
 * @author TuanNS3_S
 */
public class TransactionLog {

    private long transactionId;
    private int type;
    private String msisdn;
    private String request;
    private String response;
    private String command;
    private Timestamp requestTime;
    private Timestamp responseTime;
    private String channel;
    private String errorCode;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public Timestamp getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Timestamp requestTime) {
        this.requestTime = requestTime;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Timestamp getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Timestamp responseTime) {
        this.responseTime = responseTime;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "TransactionLog{" + "type=" + type + ", msisdn=" + msisdn + ", request=" + request + ", response=" + response + ", command=" + command + ", requestTime=" + requestTime + ", responseTime=" + responseTime + ", channel=" + channel + ", errorCode=" + errorCode + '}';
    }

    public String toLog(String separator) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getType());
        sb.append(separator);
        sb.append(this.getMsisdn() == null ? "" : this.getMsisdn());
        sb.append(separator);
        sb.append(this.getRequest() == null ? "" : this.getRequest());
        sb.append(separator);
        sb.append(this.getResponse() == null ? "" : this.getResponse());
        sb.append(separator);
        sb.append(this.getCommand() == null ? "" : this.getCommand());
        sb.append(separator);
        sb.append(this.getRequestTime() == null ? "" : DateUtils.convertDateToString(this.getRequestTime(), "dd/MM/yyyy HH:mm:ss.FF3"));
        sb.append(separator);
        sb.append(this.getResponseTime() == null ? "" : DateUtils.convertDateToString(this.getResponseTime(), "dd/MM/yyyy HH:mm:ss.FF3"));
        sb.append(separator);
        sb.append(this.getChannel() == null ? "" : this.getChannel());
        sb.append(separator);
        sb.append(this.getErrorCode());
        return sb.toString().replaceAll("\n", "#@n@#").replaceAll("\t", "#@t@#");
    }
}
