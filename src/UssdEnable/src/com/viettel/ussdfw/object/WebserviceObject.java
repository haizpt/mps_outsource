/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.object;

/**
 *
 * @author TuanNS3_S
 */
public class WebserviceObject {
    
    public static final String WS_ID = "WS_ID";
    public static final String WS_NAME = "WS_NAME";
    public static final String WS_CODE = "WS_CODE";
    public static final String WSDL = "WSDL";
    public static final String MSG_TEMPLATE = "MSG_TEMPLATE";
    public static final String STATUS = "STATUS";
    public static final String DESCRIPTION = "DESCRIPTION";    
    
    private long wsId;
    private String wsName;
    private String wsCode;
    private String wsdl;
    private String msgTemplate;
    private int status;
    private String description;
    private boolean Active = true;
    private int countWarning = 0;
    

    public int getCountWarning() {
        return countWarning;
    }

    public void setCountWarning(int countWarning) {
        this.countWarning = countWarning;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean Active) {
        this.Active = Active;
    }
    

    public String getMsgTemplate() {
        return msgTemplate;
    }

    public void setMsgTemplate(String msgTemplate) {
        this.msgTemplate = msgTemplate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getWsCode() {
        return wsCode;
    }

    public void setWsCode(String wsCode) {
        this.wsCode = wsCode;
    }

    public long getWsId() {
        return wsId;
    }

    public void setWsId(long wsId) {
        this.wsId = wsId;
    }

    public String getWsName() {
        return wsName;
    }

    public void setWsName(String wsName) {
        this.wsName = wsName;
    }

    public String getWsdl() {
        return wsdl;
    }

    public void setWsdl(String wsdl) {
        this.wsdl = wsdl;
    }        
    
}
