/**
 *
 * handsome boy
 */

package com.viettel.ussdfw.utils;

import com.viettel.cdrdumper.CDRDumper;
import com.viettel.database.service.BeanFactory;
import com.viettel.database.service.BussinessService;
import com.viettel.database.service.impl.BussinessServiceImpl;
import com.viettel.ussdfw.database.ConnectionPoolManager;
import com.viettel.ussdfw.manager.AppManager;
import org.openide.util.Exceptions;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Dec 4, 2015
 * @mail haind25@viettel.com.vn
 */
public class BussinessServiceFactory implements BeanFactory<BussinessService>{

    @Override
    public BussinessService getBean() {
        BussinessServiceImpl bussinessService = new BussinessServiceImpl();
        CDRDumper m_cDRDumper = null;
        try {
            m_cDRDumper = AppManager.getInstance().getCDRDumper();
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
        bussinessService.setOracleConnection(ConnectionPoolManager.getOracleConnection());
        bussinessService.setCDRDump(m_cDRDumper);
        return bussinessService;
    }
    
}
