/**
 *
 * handsome boy
 */

package com.viettel.ussdfw.utils;

import com.viettel.database.service.BeanFactory;
import com.viettel.database.service.CpSubscriberDao;
import com.viettel.database.service.impl.CpSubscriberDaoImpl;
import javax.sql.DataSource;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Dec 9, 2015
 * @mail haind25@viettel.com.vn
 */
public class CpSubscriberServiceFactory implements BeanFactory<CpSubscriberDao>{
    
    private final DataSource dataSource;
    private boolean connectionCloseable = true;
    
    public CpSubscriberServiceFactory(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    public CpSubscriberServiceFactory(DataSource dataSource, boolean connectionCloseable) {
        this.dataSource = dataSource;
        this.connectionCloseable = connectionCloseable;
    }
    
    @Override
    public CpSubscriberDao getBean() {
        if (dataSource == null) {
            throw new IllegalArgumentException("not int datasource!!!");
        }
        CpSubscriberDaoImpl cpSubscriberDaoImpl = new CpSubscriberDaoImpl(dataSource);
        cpSubscriberDaoImpl.setConnectionCloseable(connectionCloseable);
        return cpSubscriberDaoImpl;
    }

}
