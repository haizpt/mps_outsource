/**
 *
 * handsome boy
 */

package com.viettel.ussdfw.utils;

import com.viettel.database.service.BeanFactory;
import com.viettel.database.service.CachingeService;
import com.viettel.database.service.impl.CachingServiceImpl;
import com.viettel.ussdfw.database.ConnectionPoolManager;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Dec 4, 2015
 * @mail haind25@viettel.com.vn
 */
public class UssdServiceFactory implements BeanFactory<CachingeService>{

    @Override
    public CachingeService getBean() {
        CachingServiceImpl ussdService = new CachingServiceImpl();
        ussdService.setOracleConnection(ConnectionPoolManager.getOracleConnection());
        return ussdService;
    }

}
