/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.webservice;

import com.viettel.ussdfw.object.WebserviceObject;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author TuanNS3_S
 */
public class WebserviceGroup {

    private List<WebserviceObject> listWs;
    private int count = 0;
    private static Logger logger = Logger.getLogger(WebserviceGroup.class);

    public List<WebserviceObject> getListWs() {
        return listWs;
    }

    public void setListWs(List<WebserviceObject> listWs) {
        this.listWs = listWs;
    }

    public synchronized WebserviceObject getWebservice() {
        WebserviceObject wsObj = null;
        int getAcount = 0;

        try {
            while (true) {
                if (getAcount > listWs.size() - 1) {
                    logger.info("All WS is inactive => return null");
//                    //Tat ca cac Channel trong list deu bi block. Thuc hien giai phong 
//                    for (int i = 0; i < listWs.size(); i++) {
//                        ((WebserviceObject) listWs.get(i)).setActive(true);
//                    }                    
                    getAcount = 0;
                    break;
                }

                if (count > listWs.size() - 1) {
                    //Kiem tra count co lon hon so luong trong list khong.
                    count = 0;
                }

                if (((WebserviceObject) listWs.get(count)).isActive()) {
                    //Lay ws ra, tang count roi return
                    wsObj = (WebserviceObject) listWs.get(count);
                    count++;
                    break;
                } else {
                    getAcount++;
                    count++;
                }
            }
        } catch (Exception ex) {
            logger.error("Get ws in group list fails, return Null:.........", ex);
        }

        return wsObj;
    }

    public synchronized void setInActiveWS(WebserviceObject obj) {
        for (WebserviceObject tmp : listWs) {
            if (obj.getWsId() == tmp.getWsId()) {
                if (tmp.isActive()) {
                    tmp.setActive(false);
                }
                WebserviceActiveProcessor.checkActive(tmp);
            }
        }
    }

    public synchronized void setActiveWS(WebserviceObject obj) {
        for (WebserviceObject tmp : listWs) {
            if (obj.getWsId() == tmp.getWsId()) {
                if (!tmp.isActive()) {
                    tmp.setActive(true);
                }
            }
        }
    }
}
