/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.webservice;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.log4j.Logger;

/**
 *
 * @author TuanNS3_S
 */
public class HttpClientManager {

    private static Logger logger = Logger.getLogger(HttpClientManager.class);
    private List<HttpClient> listHttpclient;
    private int channelCount = 0;

    public HttpClientManager(int size, int connectionTimeout) {
        listHttpclient = new ArrayList<HttpClient>();
        for (int i = 0; i < size; i++) {
            //init httpclient
            HttpClient httpclient = new HttpClient(new MultiThreadedHttpConnectionManager());
            httpclient.getParams().setSoTimeout(connectionTimeout);
            httpclient.getParams().setConnectionManagerTimeout(connectionTimeout);
            httpclient.getHttpConnectionManager().getParams().setConnectionTimeout(connectionTimeout);
            //
            listHttpclient.add(httpclient);
        }
    }

    public synchronized HttpClient getChannel() {
        try {
            if (channelCount > listHttpclient.size() - 1) {
                //Kiem tra exchCount co lon hon so luong chanle trong list khong.
                channelCount = 0;
            }
            //Lay channel ra, tang Count roi return
            HttpClient channel = (HttpClient) listHttpclient.get(channelCount);
            channelCount++;
            return channel;
        } catch (Exception ex) {
            logger.error("Get channel in http channel list fails, return Null:.........", ex);
            return null;
        }
    }
}
