/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.webservice;

import com.viettel.mmserver.base.ProcessThreadMX;
import com.viettel.ussdfw.manager.AppManager;
import com.viettel.ussdfw.object.WebserviceObject;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import utils.BlockQueue;

/**
 *
 * @author TuanNS3_S
 */
public class WebserviceActiveProcessor extends ProcessThreadMX {

//    private static List<WebserviceObject> listWsInactive;
    private static BlockQueue queue;
    private HttpClient httpclient;
    private int CONNECTION_TIME_OUT;
    private int WS_ACTIVE_IDLE;

    public WebserviceActiveProcessor(String threadName, int connectionTimeout, int wsActiveIdle) {
        super(threadName);
        try {
            registerAgent(AppManager.appId + ":type=WebserviceActiveProcessor,name=" + threadName);
            //init httpclient
            httpclient = new HttpClient(new MultiThreadedHttpConnectionManager());
            httpclient.getParams().setSoTimeout(CONNECTION_TIME_OUT);
            httpclient.getParams().setConnectionManagerTimeout(CONNECTION_TIME_OUT);
            httpclient.getHttpConnectionManager().getParams().setConnectionTimeout(CONNECTION_TIME_OUT);
            //
            CONNECTION_TIME_OUT = connectionTimeout;
            WS_ACTIVE_IDLE = wsActiveIdle;
            //
//            listWsInactive = new ArrayList<WebserviceObject>();
            queue = new BlockQueue(0, 1000);
        } catch (Exception ex) {
            logger.error("error when init thread active ws", ex);
        }
    }

    public static void checkActive(WebserviceObject obj) {
        synchronized (queue) {
            queue.enqueue(obj);
        }
    }

    @Override
    protected void process() {
        if (queue.size() > 0) {
            logger.info("Check ws is running.....");
            //Quet trong list nhung ws dang o trang thai inactive            
            WebserviceObject wsObj = (WebserviceObject) queue.dequeue();
            //check ws dang o trang thai inactive
            String url = wsObj.getWsdl();
            if (callWs(url, wsObj.getMsgTemplate())) {
                //ws da active
                //chuyen trang thai 
                wsObj.setActive(true);
                WebserviceFW.setActiveWS(wsObj);
            } else {
                synchronized (queue) {
                    queue.enqueue(wsObj);
                }
            }
            try {
                Thread.sleep(WS_ACTIVE_IDLE);
            } catch (InterruptedException ex) {
            }
        } else {
            try {
                Thread.sleep(WS_ACTIVE_IDLE);
            } catch (InterruptedException ex) {
            }
        }
    }

    private boolean callWs(String url, String reqContent) {
        boolean active = false;
        String result = "";
        String response = "";
        //create an instance PostMethod
        PostMethod post = new PostMethod(url);
        try {
            logger.info("WSDL:" + url);
            RequestEntity entity = new StringRequestEntity(reqContent, "text/xml", "UTF-8");
            post.setRequestEntity(entity);
            httpclient.executeMethod(post);
            result = post.getResponseBodyAsString();
            //ws khong ton tai => throw exception
            if (result.equals("\u0000")) {
                throw new Exception("Webservice not avaiable " + url);
            }
            logger.debug("\n*********************\n");
            logger.debug("Post :" + reqContent);
            logger.debug("Resut :" + result);
            logger.debug("\n*********************\n");
            logger.info("ws : " + url + "is active");
            active = true;
        } catch (Exception e) {
            active = false;
            logger.info("ws:" + url + "is inactive");
            e.printStackTrace();
        }
        post.releaseConnection();
        post = null;
        return active;
    }
}
