/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.webservice;

import com.viettel.ussdfw.common.Common;
//import com.viettel.ussdfw.database.DbProcessorFW;
import com.viettel.ussdfw.log.ProcessTransLog;
import com.viettel.ussdfw.object.TransactionLog;
import com.viettel.ussdfw.object.WebserviceMsg;
import com.viettel.ussdfw.object.WebserviceObject;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Properties;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.w3c.dom.Element;

/**
 *
 * @author TuanNS3_S
 */
public class WebserviceFW {

    protected HttpClientManager httpclientManager;
    protected Logger logger;
    protected StringBuffer br = new StringBuffer();
    //
    private String loggerLabel = "WebserviceFW: ";
    private long timeSt;
    private long timeEx;
    private int CONNECTION_TIME_OUT = 60000;
    private int WS_ACTIVE_IDLE = 10000;
    private int NUMER_HTTPCLIENT = 2;
//    private DbProcessorFW dbProcessor;
//    private String dbId;
    private DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
    private DocumentBuilder db = df.newDocumentBuilder();
    //
    private static HashMap<String, WebserviceGroup> mapWsCode = null;
    private static WebserviceActiveProcessor wsActive = null;

    public WebserviceFW(Logger logger, String pathWSConfig, String pathDatabaseConfig) throws Exception {
        this.logger = logger;
        logger.info("Init Webservice");
        //
        loadConfig(pathWSConfig);
        //khoi tao db
//        loadConfigDb(pathDatabaseConfig);
        //init httpclient
        httpclientManager = new HttpClientManager(NUMER_HTTPCLIENT, CONNECTION_TIME_OUT);
        //
        if (wsActive == null) {
            wsActive = new WebserviceActiveProcessor("WSActive", CONNECTION_TIME_OUT, WS_ACTIVE_IDLE);
            wsActive.start();
        }
    }

    public WebserviceFW(Logger logger, String pathWSConfig) throws Exception {
        this.logger = logger;
        logger.info("Init Webservice");
        //
        loadConfig(pathWSConfig);
        //khoi tao db
//        loadConfigDb("../config/etc/database.xml");
        //init httpclient        
        httpclientManager = new HttpClientManager(NUMER_HTTPCLIENT, CONNECTION_TIME_OUT);
        //
        if (wsActive == null) {
            wsActive = new WebserviceActiveProcessor("WSActive", CONNECTION_TIME_OUT, WS_ACTIVE_IDLE);
            wsActive.start();
        }
    }

    private void loadConfig(String path) throws Exception {
        Properties pro = new Properties();
        FileReader fileReader = null;
        fileReader = new FileReader(path);
        pro.load(fileReader);
        try {
            CONNECTION_TIME_OUT = Integer.parseInt(pro.getProperty("CONNECTION_TIME_OUT", "60000"));
            WS_ACTIVE_IDLE = Integer.parseInt(pro.getProperty("WS_ACTIVE_IDLE", "10000"));
            NUMER_HTTPCLIENT = Integer.parseInt(pro.getProperty("NUMER_HTTPCLIENT", "2"));
        } catch (Exception ex) {
            logger.error("ERROR when read " + path, ex);
        }
//        try {
//            dbId = pro.getProperty("DATABASE_ID");
//            if (dbId == null || dbId.length() <= 0) {
//                logger.error("DATABASE_ID need config in " + path + "\n");
//            }
//        } catch (Exception ex) {
//            logger.error("DATABASE_ID not found in " + path + "\n");
//            throw ex;
//        }
    }

//    private void loadConfigDb(String path) throws Exception {
//        //khoi tao ghi log vao db
////        ConnectionPoolManager.loadConfig(path);
//        if (mapWsCode == null) {
//            dbProcessor = new DbProcessorFW(logger);
//            mapWsCode = dbProcessor.iGetWebservice();
//        }
//    }

    private void setInactiveWS(WebserviceObject wsObj) {
        WebserviceGroup wsGroup = mapWsCode.get(wsObj.getWsCode());
        if (wsGroup != null) {
            logger.info("set inactive WS id:" + wsObj.getWsId());
            wsGroup.setInActiveWS(wsObj);
        }
    }

    public static void setActiveWS(WebserviceObject wsObj) {
        WebserviceGroup wsGroup = mapWsCode.get(wsObj.getWsCode());
        if (wsGroup != null) {
            wsGroup.setActiveWS(wsObj);
        }
    }

    /**
     *
     * @param wsCode
     * @return
     */
    public WebserviceObject getWebservice(String wsCode) {
        WebserviceObject wsObj = null;
        //lay thong tin ws group bang wsCode
        WebserviceGroup wsGroup = mapWsCode.get(wsCode);
        if (wsGroup == null) {
            logger.warn("Don't exits ws_code: " + wsCode + " => return null");
        } else {
            //lay ws trong ws group
            wsObj = wsGroup.getWebservice();
        }
        //
        return wsObj;
    }

    /**
     *
     * @param request
     * @param wsObj
     * @return
     */
    public WebserviceMsg send(WebserviceMsg request, WebserviceObject wsObj) {

        WebserviceMsg wsMsg = new WebserviceMsg();
        wsMsg.setRequest(request.getRequest());
        wsMsg.setResponse(Common.ErrorCode.ERROR);
        if (wsObj == null) {
            logger.error("Webservice is null");
            return wsMsg;
        }
        if (request.getMsisdn() == null || request.getMsisdn().length() == 0) {
            logger.error("MSISDN is null");
            return wsMsg;
        }
        if (request.getRequest() == null || request.getRequest().length() == 0) {
            logger.error("request is null");
            return wsMsg;
        }
        //
        TransactionLog log = new TransactionLog();
        log.setMsisdn(request.getMsisdn());
        log.setType(Common.TransType.WEBSERVICE);
        log.setRequest(request.getRequest());
        log.setCommand(wsObj.getWsCode());
        log.setErrorCode(Common.ErrorCode.ERROR);
        log.setChannel(wsObj.getWsdl());
        PostMethod post = null;
        try {
            setTimeSt(System.currentTimeMillis());
            log.setRequestTime(new Timestamp(System.currentTimeMillis()));
            logger.info("WS: " + wsObj.getWsdl());
            logger.info("REQUEST: " + request.getRequest());
            //
            post = new PostMethod(wsObj.getWsdl());
            RequestEntity entity = new StringRequestEntity(request.getRequest(), "text/xml", "UTF-8");
            post.setRequestEntity(entity);
            httpclientManager.getChannel().executeMethod(post);
            String response = post.getResponseBodyAsString();
            wsMsg.setResponse(response);

            //ws khong ton tai => throw exception
            if (response.equals("\u0000")) {
                throw new Exception("Webservice not avaiable " + wsObj.getWsdl());
            }
            //parse xml tra ve
            InputSource is = new InputSource(new StringReader(response));
            Document dc = db.parse(is);
            Element root = dc.getDocumentElement();
            wsMsg.setDcElement(root);
            //
            log.setResponse(response.length() >= 4000 ? response.substring(0, 3999) : response);
            log.setResponseTime(new Timestamp(System.currentTimeMillis()));
            log.setErrorCode(wsMsg.getProperty("errorCode"));
            logger.info("RESPONSE: " + response);

        } catch (IOException ex) {
            logger.error("IOException when send to ws: " + wsObj.getWsdl(), ex);
            log.setResponseTime(new Timestamp(System.currentTimeMillis()));
            log.setErrorCode(Common.ErrorCode.TIMEOUT);
            wsMsg.setResponse(Common.ErrorCode.TIMEOUT);
            setInactiveWS(wsObj);
        } catch (Exception ex) {
            logger.error("Exception when send to ws: " + wsObj.getWsdl(), ex);
            log.setResponseTime(new Timestamp(System.currentTimeMillis()));
            log.setErrorCode(Common.ErrorCode.ERROR);
            setInactiveWS(wsObj);
        } finally {
            logTime("Time to " + wsObj.getWsCode());
            ProcessTransLog.enqueue(log);
            post.releaseConnection();
        }
        return wsMsg;
    }

    public void setTimeSt(long timeSt) {
        this.timeSt = timeSt;
    }

    public void logTime(String strLog) {
        timeEx = System.currentTimeMillis() - timeSt;
        br.setLength(0);
        br.append(loggerLabel).
                append(strLog).
                append(": ").
                append(timeEx).
                append(" ms");
        logger.info(br);
    }
}
