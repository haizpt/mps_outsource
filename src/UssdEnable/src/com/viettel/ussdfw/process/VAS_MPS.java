/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.process;

import com.viettel.application.ussdapp.common.VasRequest;
import com.viettel.cache.HCache;
import com.viettel.database.datatype.CPService;
import com.viettel.database.datatype.CPWebservice;
import com.viettel.database.datatype.Command;
import static com.viettel.database.datatype.Command.MESSAGE_TYPE_ALREADY;
import static com.viettel.database.datatype.Command.MESSAGE_TYPE_NOT_ENOUGH_MONEY;
import static com.viettel.database.datatype.Command.MESSAGE_TYPE_SUCCESS;
import static com.viettel.database.datatype.Command.MESSAGE_TYPE_SUCCESS_REQUIRE_MPS_ACCOUNT;
import com.viettel.database.datatype.MOCommand;
import com.viettel.database.datatype.MPSUser;
import com.viettel.database.datatype.SmsData;
import com.viettel.database.datatype.SubLogs;
import com.viettel.database.datatype.Subscriber;
import com.viettel.database.datatype.TransactionInfo;
import com.viettel.database.datatype.UssdCommand;
import com.viettel.database.oracle.DBConst;
import com.viettel.database.oracle.SubLogsInserDao;
import com.viettel.database.service.BusinessCode;
import com.viettel.database.service.BussinessService;
import com.viettel.database.service.CachingeService;
import com.viettel.database.service.impl.ServiceFactory;
import com.viettel.process.SendSmsProcess;
import com.viettel.ussdfw.database.ConnectionPoolManager;
import com.viettel.ussdfw.object.VasGwRecord;
import com.viettel.utilities.CallWS;
import com.viettel.utilities.CountryCode;
import com.viettel.utilities.GlobalConfig;
import com.viettel.utilities.PublicLibs;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author tungnt64
 */
public class VAS_MPS extends ConnectorProcessThreadAbstract {
    public static final String CHOOSE_PACKAGE = "CHOOSE_PACKAGE";
    
    public static final String REGISTER = "REGISTER";
    public static final String CANCEL = "CANCEL";
    public static final String CANCELSUB = "CANCELSUB";
    public static final String CANCEL_SUB = "CANCEL_SUB";
    public static final String CONFIRM = "CONFIRM";
    public static final String CONFIRM_REGISTER = "CONFIRM_REGISTER";
    public static final String CONFIRM_CANCEL = "CONFIRM_CANCEL";
    public static final String CONFIRM_OTHER = "CONFIRM_OTHER";
    public static final String SAVE = "SAVE";
    public static final String STATUS = "STATUS";
    public static final String STATUS_SUB = "STATUS_SUB";
    public static final String OTHER = "OTHER";
    
    // KEY COMMAND
    public static final String END_LINE = "\n";
    public static final String CMD_AND = "^";
    public static final String CMD_CONTEXT = "#";
    public static final String CMD_START = "@";
    public static final String CMD_SQL = "@SQL";
    public static final String CMD_WS = "@WS";
    public static final String CMD_SMS = "@SMS";
    public static final String CMD_NUMBER = "@NUMBER";
    public static final String CMD_DATE = "@DATE";
    public static final String CMD_CHAT = "@CHAT";
    //  KEY COMMAND
    public static final String CMD_REGISTER = "@REGISTER";
    public static final String CMD_CANCEL = "@CANCEL";
    public static final String CMD_CANCEL_SUB = "@CANCELSUB";
    public static final String CMD_CANCEL_SUB2 = "@CANCEL_SUB";
    public static final String CMD_CONFIRM = "@CONFIRM";
    public static final String CMD_SAVE = "@SAVE";
    public static final String CMD_STATUS = "@STATUS";//dang la sub hay khong
    public static final String CMD_STATUS_SUB = "@STATUS_SUB"; //dang la sub hay khong
    public static final String CMD_STATUS_CHARGE = "@STATUS_CHARGE"; //trang thai pending, huy va dang ky bang dich vu
    public static final String CMD_STATUS_CHARGE_SUB = "@STATUS_CHARGE_SUB"; //trang thai pending, huy va dang ky bang goi cuoc
    public static final String CMD_OTHER = "@OTHER";
    public static final String CMD_RENEW = "@RENEW";
    public static final String CMD_RENEW_SUB = "@RENEW_SUB";
    
    // STATUS USSD RESPONSE
    public static final String RESPONSE_OK = "OK";
    public static final String RESPONSE_NOK = "NOK";
    public static final String PARAMS_SMS_HEX_TEXT = "HEX_TEXT";
    public static final String PARAMS_SMS_TEXT = "TEXT";
    // REPONSE SUCCESS
    public static final String SUCCESS_REQUIRE_MPS_ACCOUNT = "SUCCESS_REQUIRE_MPS_ACCOUNT";
    private static HCache<String, TransactionInfo> transactionInfos = new HCache("USSDMPS", 1000, HCache.NO_MAX_CAPACITY, 5);
    //CONFIG KEY GLOBAL CONFIG
    public static final String IS_USE_HEX_SMS = "use_hex_for_sms";
    
    
    private String subService;
     
    @Override
    protected void initBeforeStart() throws Exception {
        logger = Logger.getLogger(VAS_MPS.class);

    }

    @Override
    protected VasGwRecord processRequest(VasRequest req) throws Exception {
        long start = System.currentTimeMillis();
        
        VasGwRecord vas = new VasGwRecord(req);
        String transId = req.getTransId();
        String msisdn = req.getMsisdn();
        msisdn = CountryCode.formatMobile(msisdn);
        TransactionInfo transactionInfo = transactionInfos.get(transId);
        if (transactionInfo == null) {
            transactionInfo = new TransactionInfo(transId, msisdn);
            transactionInfos.put(transId, transactionInfo);
            transactionInfo.putParam(CallWS.WS_SUBSCRIPTION_STATUS, 
                    String.valueOf(Subscriber.STATUS_UNDEFINE));
            transactionInfo.putParam(CallWS.WS_MSISDN, msisdn);
            transactionInfo.putParam(CallWS.WS_TRANS_ID, transId);
        }
        transactionInfo.putParam(CallWS.WS_COMMAND, req.getParams());
        
        logger.info(getThreadName() + ",transaction: " + transId 
                + ",msisdn: " + msisdn + ",param: " + req.getParams());
        logger.info(getThreadName() + ",transaction: " + transId 
                + ",msisdn: " + msisdn + ",content: " + req.getContent());
        
        
        String content = req.getContent();
        if (content == null || content.isEmpty()) {
            vas.setResponse(RESPONSE_NOK);
            return vas;
        }

        if (content.contains(CMD_AND)) {
            String[] cmds = content.split("\\" + CMD_AND);
            for (String cmd : cmds) {
                cmd = cmd.trim();
                vas = processCommand(transactionInfo, req, cmd, vas);
                if (vas.getResponse() == null
                        || (!RESPONSE_OK.equals(vas.getResponse()) 
                        && !vas.getResponse().startsWith(RESPONSE_OK)
                        && !cmd.startsWith(CMD_STATUS)
                        && !vas.getResponse().startsWith(RESPONSE_OK))
                        ) {
                    //neu la ko phai check sub thi chi hoat dong tiep
                    //khi cac lenh truoc thanh cong
                    break;
                }
            }
        } else {
            vas = processCommand(transactionInfo, req, content, vas);
        }
        
        logger.info(getThreadName() + ",transaction: " + transId 
                + ",msisdn: " + msisdn + ",Response: " + vas.getResponse() 
                + ", duration(ms): " + (System.currentTimeMillis() - start));
        return vas;
    }
    
    private VasGwRecord processCommand(TransactionInfo transactionInfo,
            VasRequest req, String content, VasGwRecord vas) {
        String cmd = "";
        String firstContent = null;
        String secondContent = null;
        
        int type = UssdCommand.TYPE_OTHERS; //for confirm
        String tmpContent = content.replace("\n", "").trim();
        if (tmpContent.startsWith(CMD_CONTEXT)) {
            content = tmpContent;
            cmd = CMD_SAVE;
            if (content.contains(":")) {
                //loai tu cau hinh
                content = content.trim();
                String[] tmp = content.split(":");
                firstContent = tmp[0].trim();
                secondContent = tmp[1].trim();
            } else {
                // loai nguoi dung nhap vao
                firstContent = content;
                firstContent = firstContent.trim();
                secondContent = req.getParams();
            }
        } else if(tmpContent.startsWith(CMD_START)){
             content = tmpContent;
            String[] tmp = content.split(":");
            if (tmp.length > 1) {
                secondContent = tmp[1];
                secondContent = secondContent.trim();
            } else {
                secondContent = req.getParams();
            }

            cmd = tmp[0].trim();
            
            if (content.startsWith(CMD_CONFIRM)) {
                cmd = CMD_CONFIRM;
                if (content.contains(CANCEL)) {
                    type = UssdCommand.TYPE_UNREGISTER;
                } else if (content.contains(REGISTER)) {
                    type = UssdCommand.TYPE_REGISTER;
                }
            }
        } else {
            content = transactionInfo.paresContext(content);
            vas.setResponse(content);
            logger.info(getThreadName()  
                + " parse context only,msisdn: " + transactionInfo.getMsidn() 
                + "," + cmd + " response => " + vas.getResponse() );
            return vas;
        }
        
        logger.info(getThreadName() + ",cmd: " + cmd + ", secondContent: " + secondContent );
        
//        if (CMD_SQL.equals(cmd)) {
//            vas = callSql(transactionInfo, vas, secondContent);
//        } else if (CMD_SAVE.equals(cmd)) {
//            String value = req.getParams();
//            if (value == null
//                    || value.trim().isEmpty()) {
//                vas.setResponse(RESPONSE_NOK);
//                return vas;
//            }
//            // XXX check truong hop gender : 1 MALE 2 : FEMALE
//            if ("1".equals(value) && "#GENDER".equals(firstContent)) {
//                secondContent = "MALE";
//            } else if ("2".equals(value) && "#GENDER".equals(firstContent)) {
//                secondContent = "FEMALE";
//            }
//            secondContent = transactionInfo.paresContext(secondContent);
//            logger.info(getThreadName() + ", save: key {" + firstContent + ": value " + secondContent + "}");
//
//            transactionInfo.putParam(firstContent, secondContent);
//
//            vas.setResponse(RESPONSE_OK);
//        } else if (CMD_STATUS_CHARGE.equals(cmd)) {
//            vas = checkChargeStatusByServiceName(transactionInfo, vas, secondContent);
//        } else if (CMD_STATUS_CHARGE_SUB.equals(cmd)) {
//            vas = checkChargeStatusBySubServiceName(transactionInfo, vas, secondContent);
//        } else if (CMD_STATUS.equals(cmd)) {
//            vas = checkSubStatusByServiceName(transactionInfo, vas, secondContent);
//        } else if (CMD_STATUS_SUB.equals(cmd)) {
//            vas = checkSubStatusBySubServiceName(transactionInfo, vas, secondContent);
//        } else if (CMD_WS.equals(cmd)) {
//            vas = callSoap(transactionInfo, vas, secondContent);
//        } else if (CMD_REGISTER.equals(cmd)) {
//            vas = register(transactionInfo, vas, secondContent, req);
//        } else if (CMD_CANCEL.equals(cmd)) {
//            vas = cancel(transactionInfo, vas, secondContent, req);
//        } else if (CMD_CANCEL_SUB.equals(cmd) || CMD_CANCEL_SUB2.equals(cmd)) {
//            vas = cancelBySubService(transactionInfo, vas, secondContent, req);
//        } else if (CMD_OTHER.equals(cmd)) {
//            vas = processForward(transactionInfo, vas, secondContent, req);
//        } else if (CMD_CONFIRM.equals(cmd)) {
//            vas = confirm(transactionInfo, vas, secondContent, req, type);
//        } else if (CMD_RENEW.equals(cmd)) {
//            vas = renewService(transactionInfo, vas, secondContent, req);
//        } else if (CMD_RENEW_SUB.equals(cmd)) {
//            vas = renewSubService(transactionInfo, vas, secondContent, req);
//        }
//        
        switch (cmd) {
            case CMD_SQL:
                vas = callSql(transactionInfo, vas, secondContent);
                break;
            case CMD_SAVE:
                //tungnt64
                String value = req.getParams();
                if (value == null || 
                        value.trim().isEmpty()) {
                    vas.setResponse(RESPONSE_NOK);
                    return vas;
                }
                // check truong hop gender : 1 MALE 2 : FEMALE
                if ("1".equals(value) && "#GENDER".equals(firstContent)) {
                    secondContent = "MALE";
                } else if ("2".equals(value) && "#GENDER".equals(firstContent)) {
                    secondContent = "FEMALE";
                }
                secondContent = transactionInfo.paresContext(secondContent);
                logger.info(getThreadName() + ", save: key {" + firstContent + ": value " + secondContent + "}");
                
                transactionInfo.putParam(firstContent, secondContent);
                
                vas.setResponse(RESPONSE_OK);
                break;
                
            case CMD_STATUS_CHARGE:
                vas = checkChargeStatusByServiceName(transactionInfo, vas, secondContent);
                break;
            case CMD_STATUS_CHARGE_SUB:
                vas = checkChargeStatusBySubServiceName(transactionInfo, vas, secondContent);
                break;
            case CMD_STATUS:
                vas = checkSubStatusByServiceName(transactionInfo, vas, secondContent);
                break;
            case CMD_STATUS_SUB:
                vas = checkSubStatusBySubServiceName(transactionInfo, vas, secondContent);
                break;
            case CMD_WS:
                vas = callSoap(transactionInfo, vas, secondContent);
                break;
            case CMD_SMS:
                vas = sendSMS(transactionInfo, vas, secondContent);
                break;
            case CMD_REGISTER:
                vas = register(transactionInfo, vas, secondContent, req);
                break;
            case CMD_CANCEL:
                vas = cancel(transactionInfo, vas, secondContent, req);
                break;
            case CMD_CANCEL_SUB:
            case CMD_CANCEL_SUB2:
                vas = cancelBySubService(transactionInfo,  vas,  secondContent, req);
                break;
            case CMD_OTHER:
                vas = processForward(transactionInfo, vas, secondContent, req);
                break;
            case CMD_CONFIRM:
                vas = confirm(transactionInfo, vas, secondContent, req, type);
                break;
            case CMD_RENEW:
                vas = renewService(transactionInfo, vas, secondContent, req);
                break;
            case CMD_RENEW_SUB:
                vas = renewSubService(transactionInfo, vas, secondContent, req);
                break;
        }
        logger.info(getThreadName()  
                + ",msisdn: " + transactionInfo.getMsidn() 
                + "," + cmd + " response => " + vas.getResponse() );
        return vas;
    }
    
    private VasGwRecord callSql(TransactionInfo transactionInfo, VasGwRecord vas, String sql) {
        vas.setResponse(RESPONSE_NOK);
        sql = transactionInfo.paresContext(sql);
//        sql = "CALL PR_VOTING('A1470766885278','VIETNAM_LAO','84973718628',0,2,2,3)";
//        sql = sql.replace("'", "''");
        sql = sql.replace("\\", "'");
        Connection connection  = null;
        Statement sta = null;
        ResultSet rs = null;
        try {
            connection = ConnectionPoolManager.getDefaultConnection();
            sta = connection.createStatement();
            sta.setQueryTimeout(30);//30 seconds
            rs  = sta.executeQuery(sql);
            String value = null;
            if (rs.next()) {
                value = rs.getString(1);
                vas.setResponse(value);
            }
        } catch (Exception ex) {
            logger.error("error when run: " + sql, ex);
        } finally {
            ConnectionPoolManager.closeQuite(rs, sta, connection);
        }
        
        return vas;
    }
    
    private VasGwRecord register(TransactionInfo transactionInfo, VasGwRecord vas, String subService, VasRequest req) {
       // neu subService ko co gia tri --> lay goi cuoc da confirm dang ky trc do
        BusinessCode businessCode = BusinessCode.FAIL;
        Map<String, String> mapConfirm = transactionInfo.getParams();

        if (subService == null || subService.isEmpty()) {
            subService = transactionInfo.getSubServcieName();
        }

        if (mapConfirm != null && subService == null) {
            subService = mapConfirm.get(CHOOSE_PACKAGE);
        }

        if (subService == null || subService.isEmpty()) {
            logger.error(transactionInfo.toString() + ", error - not config SubserviceName");
            vas.setResponse(RESPONSE_NOK);
            return vas;
        }

        logger.info(getThreadName() +"," + transactionInfo.toString() + "  register SUB_SERVICE: " + subService);

        // goi toi API dang ky
        BussinessService bussinessService = ServiceFactory.getBean(BussinessService.class);
        CachingeService ussdService = ServiceFactory.getBean(CachingeService.class);
        UssdCommand ussdCommand = ussdService.getUssdCommand(subService, UssdCommand.TYPE_REGISTER);
        if (ussdCommand == null) {
            logger.error(getThreadName() +"," + "register error ussdCommand null");
            businessCode = BusinessCode.FAIL;
            vas.setResponse(RESPONSE_NOK);
            return vas;
        }
        this.subService = subService;
        logger.info(getThreadName() + "," +transactionInfo.toString()
                + " call register: " + subService 
                + ", msisdn: " + transactionInfo.getMsidn()
                + ", service: " + subService);
        //test  tai. gia lap thanh cong . Comment lai, khi nao test xong thi bo comment di
        businessCode = bussinessService.register(subService, transactionInfo.getMsidn(), 
                transactionInfo.getTransactionId(), transactionInfo.getParams(),
                ussdCommand, req.getSteps());
        logger.info(getThreadName() + "," + transactionInfo.toString() + " response: " + businessCode);

//                businessCode = BusinessCode.SUCCESS;
        String contentMessage = null ;
        if (businessCode.getResponse() != null && !businessCode.getResponse().isEmpty()) {
            contentMessage = businessCode.getResponse();
            logger.info(getThreadName() + "," + transactionInfo.toString() + ", business response: " + contentMessage);
        } else {
            contentMessage = ussdCommand.getMessage(businessCode.getValue());
            logger.info(getThreadName() + "," + transactionInfo.toString() + " response: " + businessCode);
        }

        if (contentMessage == null || "".equals(contentMessage)) {
            logger.error(getThreadName() + "," + "No content in USSD_COMMAND");
            vas.setResponse(RESPONSE_NOK);
            return vas;
        }

        if (SUCCESS_REQUIRE_MPS_ACCOUNT.equals(businessCode)) {
            logger.info(getThreadName() + "," + transactionInfo.toString() + " response: " + businessCode);
            CPService cpService = ussdService.getCpServiceBySubSerivceName(subService);
            String passwordContent = registerMPSAccount(transactionInfo.getMsidn()
                    , cpService.getServiceName(), transactionInfo.getTransactionId(), req.getSteps());
            contentMessage = contentMessage + passwordContent;
        }

//                contentMessage = paresContext(contentMessage, transactionInfo.getParams());
        contentMessage = transactionInfo.paresContext(contentMessage);
        int messageChannel = ussdCommand.getMessageChannel();
        // send noi dung
        sendMessageToCustomer(vas, contentMessage, messageChannel, UssdCommand.TYPE_REGISTER, businessCode.getValue());
        // nhan ket qua tra ve
        vas.setResponse(contentMessage);
        return vas;
    }
    /**
     * @author haind25
     * @since 03/12/2015
     * @description channel: SMS Only, USSD Only, Both, MESSAGE_CHANNEL_MULTI
     */
    private void sendMessageToCustomer(VasGwRecord vas, String message, int channel, int type, String businessCode) {
          logger.info(getThreadName() + "," + vas.getTransactionId() 
                  + ", message channel: " + channel 
                  + ", type: " + type
                  + ", businessCode: " + businessCode
                  + ", message: " + message
          );
        switch (channel) {
            case UssdCommand.MESSAGE_CHANNEL_USSD_ONLY:
                vas.setResponse(message);
                break;

            case UssdCommand.MESSAGE_CHANNEL_SMS_ONLY:
                sendMt(message, subService, vas.getMsisdn(), type);
                break;
            case UssdCommand.MESSAGE_CHANNEL_BOTH:
                vas.setResponse(message);
                sendMt(message, subService, vas.getMsisdn(), type);
                break;
            case UssdCommand.MESSAGE_CHANNEL_MULTI:
                vas.setResponse(message);
                CachingeService ussdServiceMulti = ServiceFactory.getBean(CachingeService.class);
                MOCommand moCommand = ussdServiceMulti.getMOCommand(subService, type);
                if (moCommand != null) {
                    String smsContent = getMessage(moCommand, businessCode);
                    sendMt(smsContent, moCommand.getServiceName(), vas.getMsisdn());
                } else {
                    logger.error(getThreadName() + "," + "not found SMS config for: " + subService + ", type: " + type);
                }
                break;
        }
    }
    
    
    public String getMessage(MOCommand moCommand, String type) {
        if (MESSAGE_TYPE_SUCCESS.equals(type)) {
            return  moCommand.getSuccessMessage();
        }
        else if(MESSAGE_TYPE_ALREADY.equals(type)){
            return  moCommand.getAlreadyRegisterMessage();
        }
        else if(MESSAGE_TYPE_NOT_ENOUGH_MONEY.equals(type)){
            return  moCommand.getNotEnoughMoney();
        }
        else if(Command.NOT_REGISTER.equals(type)){
            return  moCommand.getSecondFailedMessage();
        }
        else if(MESSAGE_TYPE_SUCCESS_REQUIRE_MPS_ACCOUNT.equals(type)){
            return  moCommand.getSuccessMessage();
        }
        
        return  moCommand.getFailMessage();
    }

    private void sendMt(String content, String serviceName, String msisdn) {
        boolean useHex = Boolean.valueOf(GlobalConfig.get(IS_USE_HEX_SMS));
        String smsType = (useHex ? PARAMS_SMS_HEX_TEXT : PARAMS_SMS_TEXT);

        String url = GlobalConfig.get("sms_ws_url");
        String user = GlobalConfig.get("sms_ws_username");
        String pass = GlobalConfig.get("sms_ws_password");
        String shortcode = GlobalConfig.get("sms_ws_shortcode");
        
        CPService cPService = ServiceFactory.getBean(CachingeService.class).getCpService(serviceName);

        if (cPService != null) {
            if (cPService.getSMSGWUrl() != null && !cPService.getSMSGWUrl().isEmpty()) {
                url = cPService.getSMSGWUrl();
                user = cPService.getSMSGWUser();
                pass = cPService.getSMSGWPass();
                shortcode = cPService.getShortCode();
            }
        }

        SmsData sms = new SmsData(url, user, pass, msisdn, content, shortcode, "", smsType);
        SendSmsProcess.getInstance().enqueue(sms);
    }

    private void sendMt(String content, String subServiceName, String msisdn, int type) {
        boolean useHex = Boolean.valueOf(GlobalConfig.get(IS_USE_HEX_SMS));
        String smsType = (useHex ? PARAMS_SMS_HEX_TEXT : PARAMS_SMS_TEXT);

        String url = GlobalConfig.get("sms_ws_url");
        String user = GlobalConfig.get("sms_ws_username");
        String pass = GlobalConfig.get("sms_ws_password");
        String shortcode = GlobalConfig.get("sms_ws_shortcode");
        
        CPService cPService = null;
        if (type == UssdCommand.TYPE_UNREGISTER) {
            //luc nay subServiceName la serviceName
            cPService = ServiceFactory.getBean(CachingeService.class).getCpService(subServiceName);
        }
        
        if (cPService == null) {
            cPService = ServiceFactory.getBean(CachingeService.class).getCpServiceBySubSerivceName(subServiceName);
        }

        if (cPService != null) {
            if (cPService.getSMSGWUrl() != null && !cPService.getSMSGWUrl().isEmpty()) {
                url = cPService.getSMSGWUrl();
                user = cPService.getSMSGWUser();
                pass = cPService.getSMSGWPass();
                shortcode = cPService.getShortCode();
            }
        } else {
            logger.error(getThreadName() +",not found cpService: " + subServiceName + ", type: " + type);
        }

        SmsData sms = new SmsData(url, user, pass, msisdn, content, shortcode, "", smsType);
        SendSmsProcess.getInstance().enqueue(sms);
    }

    private String registerMPSAccount(String msisdn, String serviceName, String tranID, String stepsRegisterMPSAccount) {
        // Check MPS Account first
        MPSUser user = ConnectionPoolManager.getOracleConnection().getMPSUser(msisdn);
        boolean needUpdate;
        if (user == null) {
            user = new MPSUser(msisdn, PublicLibs.getRandomPassword(), DBConst.STATUS_ACTIVE, 0, System.currentTimeMillis(), 0);
            needUpdate = true;
        } else {
            user.setStatus(DBConst.STATUS_ACTIVE);
            user.setPassword(PublicLibs.getRandomPassword());
            needUpdate = true;
        }

        if (needUpdate) {
            ConnectionPoolManager.getOracleConnection().updateMPSUser(user);
            // Insert Subscription log
            SubLogs subLogs = new SubLogs(msisdn, serviceName, tranID, "REGISTER MPS Account (FREE)",
                    DBConst.RESULT_SUCCESS, System.currentTimeMillis(), stepsRegisterMPSAccount, "SMS");

            SubLogsInserDao.getInstance().enqueue(subLogs);
//            ConnectionPoolManager.getOracleConnection().insertSubsLog(msisdn, serviceName, tranID, "REGISTER MPS Account (SMS) (FREE)", DBConst.RESULT_SUCCESS, System.currentTimeMillis());
        }

        return parseMPSPasswordContent(GlobalConfig.get("mps_password_sms"), user.getPassword());
    }

    private String parseMPSPasswordContent(String content, String password) {
        if (content != null) {
            String result = content.replace("#PASSWORD", password);
            return result;
        } else {
            return "";
        }
    }

    private VasGwRecord checkSubStatusByServiceName(TransactionInfo transactionInfo, VasGwRecord vas, String serviceName) {
        logger.info("check status by service: service name: " + serviceName + ", msisdn: " + transactionInfo.getMsidn());
        BussinessService bussinessServiceStatus = ServiceFactory.getBean(BussinessService.class);
        if (bussinessServiceStatus == null) {
            vas.setResponse(RESPONSE_NOK);
            logger.info("error when get BussinessService: please check database");
            return vas;
        }
        BusinessCode    businessCode = bussinessServiceStatus.checkRegisterByServiceName(serviceName, transactionInfo.getMsidn());
        if (BusinessCode.ALREADY.getValue().equals(businessCode.getValue())) {
            vas.setResponse(RESPONSE_OK);
            transactionInfo.putParam(CallWS.WS_SUBSCRIPTION_STATUS,
                    String.valueOf(Subscriber.STATUS_ACTIVE));//luu trang thai cua sub
        } else {
            vas.setResponse(RESPONSE_NOK);
            transactionInfo.putParam(CallWS.WS_SUBSCRIPTION_STATUS,
                    String.valueOf(Subscriber.STATUS_INACTIVE));
        }
        return vas;
    }

    private VasGwRecord callSoap(TransactionInfo transactionInfo, VasGwRecord vas, String webserviceName) {
        CachingeService cachingeService = ServiceFactory.getBean(CachingeService.class);
        CPWebservice ws = cachingeService.getCPWebService(webserviceName);
        if (ws == null) {
            vas.setResponse(RESPONSE_NOK);
            logger.error(getThreadName() +",not found cpWebService: " + webserviceName);
            return vas;
        }
        Properties pars = new Properties();
        pars.put(CallWS.WS_USERNAME, ws.getUserName());
        pars.put(CallWS.WS_PASSWORD, ws.getPassword());
        pars.put(CallWS.WS_CHARGE_TIME, PublicLibs.getCurrentDateTime());
        String xml = transactionInfo.paresContext(ws.getRawXML());
        xml = CallWS.parseValue(xml, pars);
        String response = CallWS.callSoap(ws.getUrl(), xml, ws.getSoapAction(), 1, ws.getTimeout(), ws.getReturnTag());
        vas.setResponse(response);
        
        //XXX KPI??
        
        return vas;
    }

   /**
     * mac dinh tra ve thanh cong
     */
    private VasGwRecord sendSMS(TransactionInfo transactionInfo, VasGwRecord vas, String content) {
        vas.setResponse(RESPONSE_NOK);
        
        if (GlobalConfig.sms_ws_url == null 
                || GlobalConfig.sms_ws_username == null
                || GlobalConfig.sms_ws_password == null
                || GlobalConfig.sms_ws_shortcode == null
        ) {
            logger.error(getThreadName() +",can not send, check config sms");
            return vas;
        }
        content = transactionInfo.paresContext(content);
        String url = GlobalConfig.sms_ws_url;
        String user = GlobalConfig.sms_ws_username;
        String pass = GlobalConfig.sms_ws_password;
        String shortCode = GlobalConfig.sms_ws_shortcode;
        String alias = shortCode;

        SmsData sms = new SmsData(url, user, pass, transactionInfo.getMsidn(), 
                content, shortCode, alias, "TEXT");
        SendSmsProcess.getInstance().enqueue(sms);

        vas.setResponse(RESPONSE_OK);
        return vas;
    }

    private VasGwRecord cancel(TransactionInfo transactionInfo, VasGwRecord vas, String serviceName, VasRequest req) {
        String subServiceCancel = null;
        BussinessService bussinessServiceCancel = ServiceFactory.getBean(BussinessService.class);
        CachingeService ussdServiceCancle = ServiceFactory.getBean(CachingeService.class);
        String msisdn = transactionInfo.getMsidn();
        String transId = transactionInfo.getTransactionId();
        BusinessCode businessCode = bussinessServiceCancel.unregisterAllSub(msisdn, transId,
                transactionInfo.getParams(), serviceName, Command.COMMAND_USSD, req.getSteps());
        // lay ra subservice cua goi cuoc da huy
        if (businessCode.getDescription() != null) {
            subServiceCancel = businessCode.getDescription();
        }
        if (subServiceCancel == null || "".equals(subServiceCancel)) {
            logger.error(getThreadName() + "Not get subservice cancel");
            vas.setResponse(RESPONSE_NOK);
            return vas;
        }

        UssdCommand ussdCommandCancel = ussdServiceCancle.getUssdCommand(subServiceCancel, UssdCommand.TYPE_UNREGISTER);

        if (ussdCommandCancel == null) {
            logger.error(getThreadName() + "register error ussdCommand null");
            businessCode = BusinessCode.FAIL;
        }
        logger.info(transactionInfo.toString() + " response: " + businessCode);
        String contentMessage = ussdCommandCancel.getMessage(businessCode.getValue());
        if (contentMessage == null || "".equals(contentMessage)) {
            logger.error(getThreadName() + "No content in USSD_COMMAND");
            vas.setResponse(RESPONSE_NOK);
            return vas;
        }
//                contentMessage = paresContext(contentMessage, transactionInfo.getParams());
        contentMessage = transactionInfo.paresContext(contentMessage);
        int messageChannel = ussdCommandCancel.getMessageChannel();
        // send noi dung
        subService = subServiceCancel;
        sendMessageToCustomer(vas, contentMessage, messageChannel, UssdCommand.TYPE_UNREGISTER, businessCode.getValue());
        return vas;
    }

    private VasGwRecord processForward(TransactionInfo transactionInfo, 
            VasGwRecord vas, String subService,
            VasRequest req) {
        
        BussinessService bussinessServiceOther = ServiceFactory.getBean(BussinessService.class);
        CachingeService ussdServiceOther = ServiceFactory.getBean(CachingeService.class);
        
        String realSubService = subService;
        realSubService = transactionInfo.paresContext(realSubService);
        
        UssdCommand ussdCommandOther = ussdServiceOther.getUssdCommand(realSubService, UssdCommand.TYPE_OTHERS);
        if (ussdCommandOther == null) {
            logger.error(getThreadName() + " no ussd command: " + realSubService
                                        + " type: " + UssdCommand.TYPE_OTHERS);
            vas.setResponse(RESPONSE_NOK);
            return vas;
        }
        
        String msisdn = transactionInfo.getMsidn();
        String transId = transactionInfo.getTransactionId();
        
        BusinessCode businessCode = bussinessServiceOther.forward(realSubService, msisdn, transId, 
                transactionInfo.getParams(), ussdCommandOther, req.getSteps());
        logger.info(getThreadName() + "," + "BUSINESS_CODE: " + businessCode);
        
        String contentMessage = null;
        if (businessCode.getDescription() != null 
                && !businessCode.getDescription().trim().isEmpty()) {
            contentMessage = businessCode.getDescription();
            logger.info(getThreadName() + transId + ",message from cp: " + contentMessage);
        } else {
            contentMessage = ussdCommandOther.getMessage(businessCode.getValue());
            if (contentMessage == null || "".equals(contentMessage)) {
                logger.error(getThreadName() + "No content in USSD_COMMAND");
                vas.setResponse(RESPONSE_NOK);
                return vas;
            }
            logger.info(getThreadName() + transId + ",message from system: " + contentMessage);
        }
        this.subService = subService;
        contentMessage = transactionInfo.paresContext(contentMessage);
        logger.info(getThreadName() + transId + ",after pass context: " + contentMessage);
        // send noi dung
        sendMessageToCustomer(vas, contentMessage, ussdCommandOther.getMessageChannel(), 
                UssdCommand.TYPE_OTHERS, businessCode.getValue());
        return vas;
    }

    private VasGwRecord confirm(TransactionInfo transactionInfo, VasGwRecord vas,
            String subServices, VasRequest req, int type) {
        
        CachingeService ussdServiceConfirm = ServiceFactory.getBean(CachingeService.class);
        UssdCommand ussdCommandConfirm = null;
        String subService = null;
        
        String[] listSubService = subServices.split("\\|");
        if (listSubService.length > 1) {
            String choose = req.getParams();
            String digit = "\\d";
            boolean flag = choose.matches(digit);
            int index = -1;
            if (flag) {
                index = Integer.parseInt(choose) - 1;
            }
            
            if (flag && index > 0 && index <= listSubService.length ) {
                subServices = listSubService[Integer.parseInt(choose) - 1];
            } else {
                logger.info(getThreadName() 
                        + ",out of range confirm list:" + subService 
                + ", must choose: 1-" + listSubService.length 
                + ",but choose:" + choose);
                vas.setResponse(RESPONSE_NOK);
                return vas;
            }
        } else {
            subService =  subServices;
        }
        
        if (subService != null) {
            transactionInfo.putParam(CHOOSE_PACKAGE, subServices);
        }
        
        if (subServices != null) {
            transactionInfo.setSubServcieName(subService);
        }
        logger.info(getThreadName() 
                + ", get ussd command, " + subService 
                + ", type:" + type);
        
        if (subService != null) {
            ussdCommandConfirm = ussdServiceConfirm.getUssdCommand(subService, type);
        }
        
        if (ussdCommandConfirm == null) {
            logger.error(getThreadName() + ",Error Can get ussdCommandConfirm" + subService);
            vas.setResponse(RESPONSE_NOK);
            return vas;
        }

        String contentMessage = ussdCommandConfirm.getMessage(CONFIRM);

        if (contentMessage == null || "".equals(contentMessage)) {
            logger.error("No content in USSD_COMMAND, please check: " + subService);
            vas.setResponse(RESPONSE_NOK);
            return vas;
        }

        contentMessage = transactionInfo.paresContext(contentMessage);

        vas.setResponse(contentMessage);
        return vas;
    }

    private VasGwRecord cancelBySubService(TransactionInfo transactionInfo, VasGwRecord vas, String subService, VasRequest req) {
        BussinessService bussinessServiceCancelSub = ServiceFactory.getBean(BussinessService.class);
        BusinessCode businessCode = BusinessCode.FAIL;
        String msisdn = transactionInfo.getMsidn();
        String transId = transactionInfo.getTransactionId();
        
        UssdCommand ussdCommandCancelSub = null;
        
        CachingeService ussdServiceCancleSub = ServiceFactory.getBean(CachingeService.class); 
        
        if (subService.contains(",")) {
            String[] serviceInfo = subService.split(",");
            String serviceName = serviceInfo[0];
            String subServiceName = serviceInfo[1];
            ussdCommandCancelSub = ussdServiceCancleSub.getUssdCommand(subServiceName, UssdCommand.TYPE_UNREGISTER);
            if (ussdCommandCancelSub == null) {
                logger.error(getThreadName() + "," + "register error ussdCommand null");
                vas.setResponse(RESPONSE_NOK);
                return vas;
            }
            businessCode = bussinessServiceCancelSub.unregisterSub(msisdn, transId, transactionInfo.getParams(), 
                    serviceName, subServiceName, Command.COMMAND_USSD, req.getSteps());
        } else {
            ussdCommandCancelSub = ussdServiceCancleSub.getUssdCommand(subService, UssdCommand.TYPE_UNREGISTER);
            if (ussdCommandCancelSub == null) {
                logger.error(getThreadName() + "," + "register error ussdCommand null");
                vas.setResponse(RESPONSE_NOK);
                return vas;
            }
            businessCode = bussinessServiceCancelSub.unregisterSub(msisdn, transId, 
                    transactionInfo.getParams(), subService, Command.COMMAND_USSD, req.getSteps());
        }
        
        String contentMessage = ussdCommandCancelSub.getMessage(businessCode.getValue());
        if (contentMessage == null || "".equals(contentMessage)) {
            logger.error("No content in USSD_COMMAND");
            vas.setResponse(RESPONSE_NOK);
            return vas;
        }
        contentMessage = transactionInfo.paresContext(contentMessage);
        int messageChannel = ussdCommandCancelSub.getMessageChannel();
        // send noi dung
        this.subService =  subService;
        sendMessageToCustomer(vas, contentMessage, messageChannel, UssdCommand.TYPE_UNREGISTER, businessCode.getValue());
        return vas;
    }

    private VasGwRecord checkSubStatusBySubServiceName(TransactionInfo transactionInfo, VasGwRecord vas, String subServiceName) {
        BussinessService bussinessServiceStatus = ServiceFactory.getBean(BussinessService.class);
        
        BusinessCode    businessCode = bussinessServiceStatus.checkRegisterBySubServiceName(subServiceName, transactionInfo.getMsidn());
        logger.error("check sub by sub-service:" + transactionInfo.getMsidn() 
                + ", subServiceName:" + subServiceName + "=> " + businessCode.getValue());
        
        if (BusinessCode.ALREADY.getValue().equals(businessCode.getValue())) {
            vas.setResponse(RESPONSE_OK);
            transactionInfo.putParam(CallWS.WS_SUBSCRIPTION_STATUS,
                    String.valueOf(Subscriber.STATUS_ACTIVE));//luu trang thai cua sub
        } else {
            vas.setResponse(RESPONSE_NOK);
            transactionInfo.putParam(CallWS.WS_SUBSCRIPTION_STATUS,
                    String.valueOf(Subscriber.STATUS_INACTIVE));
        }
        return vas;
    }

    /**
     * kiem tra trang thai cua thue bao: active, pending, inactive 
     * dau vao: Service-Name
     */
    private VasGwRecord checkChargeStatusByServiceName(TransactionInfo transactionInfo, VasGwRecord vas, String serviceName) {
        BussinessService bussinessServiceStatus = ServiceFactory.getBean(BussinessService.class);
        BusinessCode   businessCode = bussinessServiceStatus.checkRegisterByServiceName(serviceName, transactionInfo.getMsidn());
        if (BusinessCode.ALREADY.getValue().equals(businessCode.getValue())) {
            Object data = businessCode.getData();
            vas.setResponse(String.valueOf(Subscriber.STATUS_ACTIVE));
            if (data != null && data instanceof Subscriber) {
                 Subscriber sub = (Subscriber) data;
                 vas.setResponse(String.valueOf(sub.getStatus()));
            }
            
            transactionInfo.putParam(CallWS.WS_SUBSCRIPTION_STATUS,
                    String.valueOf(Subscriber.STATUS_ACTIVE));//luu trang thai cua sub
        } else {
            vas.setResponse(String.valueOf(Subscriber.STATUS_INACTIVE));
            transactionInfo.putParam(CallWS.WS_SUBSCRIPTION_STATUS,
                    String.valueOf(Subscriber.STATUS_INACTIVE));
        }
        return vas;
    }
    
    
   /**
     * kiem tra trang thai cua thue bao: active, pending, inactive 
     * dau vao: Sub-Service-Name
     */
    private VasGwRecord checkChargeStatusBySubServiceName(TransactionInfo transactionInfo,
            VasGwRecord vas, String subServiceName) {
        BussinessService bussinessServiceStatus = ServiceFactory.getBean(BussinessService.class);
        BusinessCode   businessCode 
                = bussinessServiceStatus.checkRegisterBySubServiceName(subServiceName,
                        transactionInfo.getMsidn());
        
        if (BusinessCode.ALREADY.getValue().equals(businessCode.getValue())) {
            Object data = businessCode.getData();
            vas.setResponse(String.valueOf(Subscriber.STATUS_ACTIVE));
            if (data != null && data instanceof Subscriber) {
                 Subscriber sub = (Subscriber) data;
                 vas.setResponse(String.valueOf(sub.getStatus()));
            }
            
            transactionInfo.putParam(CallWS.WS_SUBSCRIPTION_STATUS,
                    String.valueOf(Subscriber.STATUS_ACTIVE));//luu trang thai cua sub
        } else {
            vas.setResponse(String.valueOf(Subscriber.STATUS_INACTIVE));
            transactionInfo.putParam(CallWS.WS_SUBSCRIPTION_STATUS,
                    String.valueOf(Subscriber.STATUS_INACTIVE));
        }
        
        return vas;
    }

    private VasGwRecord renewService(TransactionInfo transactionInfo, VasGwRecord vas, String serviceName, VasRequest req) {
        BussinessService bussinessService = ServiceFactory.getBean(BussinessService.class);
        //String msisdn, String serviceName, String tranID, Map<String,String> params, String channel, String log
        String msisdn = transactionInfo.getMsidn();
        String tranId = transactionInfo.getTransactionId();
        
        BusinessCode  businessCode = bussinessService.renewByServiceName(msisdn, serviceName, tranId, 
                transactionInfo.getParams(),Command.COMMAND_USSD, req.getSteps());
        
        if (BusinessCode.SUCCESS.getValue().equals(businessCode.getValue())) {
            vas.setResponse(RESPONSE_OK);
        } else {
            vas.setResponse(RESPONSE_NOK);
        }
        
        return vas;
    }
    
    
    private VasGwRecord renewSubService(TransactionInfo transactionInfo, VasGwRecord vas, String subServiceName, VasRequest req) {
        BussinessService bussinessService = ServiceFactory.getBean(BussinessService.class);
        //String msisdn, String serviceName, String tranID, Map<String,String> params, String channel, String log
        String msisdn = transactionInfo.getMsidn();
        String tranId = transactionInfo.getTransactionId();
        
        BusinessCode  businessCode = bussinessService.renewBySubServiceName(msisdn, subServiceName, tranId, 
                transactionInfo.getParams(),Command.COMMAND_USSD, req.getSteps());
        
        if (BusinessCode.SUCCESS.getValue().equals(businessCode.getValue())) {
            vas.setResponse(RESPONSE_OK);
        } else {
            vas.setResponse(RESPONSE_NOK);
        }
        
        return vas;
    }
}
