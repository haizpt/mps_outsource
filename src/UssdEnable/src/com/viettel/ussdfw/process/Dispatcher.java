/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.ussdfw.process;

import com.viettel.application.ussdapp.common.VasRequest;
import com.viettel.application.ussdapp.common.VasResponse;
import com.viettel.application.ussdapp.vasinterface.tcp.TCPConnector;
import com.viettel.ussdfw.object.VasGwRecord;
import java.util.HashMap;
import java.util.Map;
import utils.BlockQueue;

/**
 *
 * @author minhnh@viettel.com.vn
 * @since Dec 10, 2012
 * @version 1.0
 */
public class Dispatcher implements VasRequestTimeoutListener{
    private final static int DEFAULT_QUEUE_SIZE = 10000;
//    private int id;
    private String name;
    private HashMap<String, String> fileConfig;
    private HashMap<Integer, TCPConnector> mapConnector;
    private BlockQueue queue;
    private static HashMap<String, Dispatcher> hmInstance = new HashMap<String, Dispatcher>();

    private Dispatcher(String name, HashMap<String, String> listFileConfig) throws Exception {
        this.name = name;
        this.fileConfig = listFileConfig;
        queue = new SafeQueue();
        mapConnector = new HashMap<Integer, TCPConnector>();

        //khoi tao list connector
//        int index = 0;
        for (Map.Entry<String, String> entry : listFileConfig.entrySet()) {
            String fileConfig = entry.getValue();
            String cpId = entry.getKey();
            TCPConnector connector = new TCPConnector(Integer.parseInt(cpId), name + "_" + cpId, fileConfig);
            connector.setQueue(queue);
            mapConnector.put(Integer.parseInt(cpId), connector);
//            index++;
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public synchronized VasRequest getRequest() {
        return (VasRequest) queue.dequeue();
    }

    public void sendResponse(VasResponse vasResponse) throws Exception {
        TCPConnector connector = mapConnector.get(vasResponse.getConnectorId());
        connector.sendResponse(vasResponse);
    }

    public static HashMap<String, Dispatcher> getHmInstance() {
        return hmInstance;
    }

    public static void setHmInstance(HashMap<String, Dispatcher> hmInstance) {
        Dispatcher.hmInstance = hmInstance;
    }

    public HashMap<Integer, TCPConnector> getMapConnector() {
        return mapConnector;
    }

    public void setMapConnector(HashMap<Integer, TCPConnector> mapConnector) {
        this.mapConnector = mapConnector;
    }

    public BlockQueue getQueue() {
        return queue;
    }

    public void setQueue(BlockQueue queue) {
        this.queue = queue;
    }

    public static Dispatcher getInstance(String name, HashMap<String, String> fileConfig) throws Exception {
        if (!hmInstance.containsKey(name)) {
            hmInstance.put(name, new Dispatcher(name, fileConfig));
        }
        return hmInstance.get(name);
    }

    public static synchronized void start() {
        for (Map.Entry<String, Dispatcher> entry : hmInstance.entrySet()) {
            Dispatcher dispatcher = entry.getValue();
            HashMap<Integer, TCPConnector> mapConnector = dispatcher.getMapConnector();
            for (Map.Entry<Integer, TCPConnector> entryCon : mapConnector.entrySet()) {
                TCPConnector connector = entryCon.getValue();
                connector.start();
            }
            BlockQueue queue = dispatcher.getQueue();
            if (queue instanceof SafeQueue) {
                SafeQueue s = (SafeQueue) queue;
                s.start();
            }
        }

    }

    public static synchronized void stop() {
        for (Map.Entry<String, Dispatcher> entry : hmInstance.entrySet()) {
            Dispatcher dispatcher = entry.getValue();
            HashMap<Integer, TCPConnector> mapConnector = dispatcher.getMapConnector();
            for (Map.Entry<Integer, TCPConnector> entryCon : mapConnector.entrySet()) {
                TCPConnector connector = entryCon.getValue();
                connector.stop();
            }
            
            BlockQueue queue = dispatcher.getQueue();
            if (queue instanceof SafeQueue) {
                SafeQueue s = (SafeQueue) queue;
                s.stop();
            }
        }
    }

    @Override
    public VasGwRecord onEventTimeout(VasRequest req) {
        VasGwRecord vas = new VasGwRecord(req);
        vas.setErrorCode(1);
        vas.setErrorDescription("Timeout after 30 seconde");
        return vas;
    }
}
