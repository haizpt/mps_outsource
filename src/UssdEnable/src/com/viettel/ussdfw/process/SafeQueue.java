/**
 *
 * handsome boy
 */

package com.viettel.ussdfw.process;

import com.viettel.application.ussdapp.common.VasRequest;
import com.viettel.ussdfw.log.ProcessVasgwLog;
import com.viettel.ussdfw.object.VasGwRecord;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import utils.BlockQueue;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Nov 21, 2015
 * @mail haind25@viettel.com.vn
 * 
 * @param <T>
 */
public class SafeQueue extends BlockQueue implements Runnable{
    
    protected static final Logger logger = Logger.getLogger(SafeQueue.class);
    /*
     * queue size default duoc tinh dua vao TPS * default_timeout
     * 1 connector cho phep xu ly 200TPS * 30s (timeout) = 6000,
     * he so an toan: de khoang 10.000
     */
    public final static int QUEUE_SIZE_DEFAULT = 10000; // size 10000
    
    public final static int QUEUE_SIZE_UNLIMIT = 0; 
    
    /*
     * cac request tu khi dua vao queue, sau thoi gian timeout ma khong duoc
     * xu ly se bi loai bo khoi queue
     */
    public final static int TIMEOUT_DEFAULT = 30000;//30s
    
    /*
     * timewait: neu khi dequeue ma khong co du lieu, thi cac tien trinh se
     * phai cho them 1 khoang thoi gian, goi la timewait
     */
    public final static int TIMEWAIT_UNLIMIT = 0;//
    public final static int TIMEWAIT_DEFAULT = 2000; //2s
    /**
     * bien lock dien khien multithread de tranh xung dot
     *
     */
    
    public final static int STATE_START = 1;
    public final static int STATE_STOP = 0;
    
    protected final LinkedHashMap<String,VasRequest> queue = new LinkedHashMap<String,VasRequest>();
    protected final Object lock = new Object();
    
    private VasRequestTimeoutListener timeoutListener;
    private int timemout;
    protected int timewait;
    
    private int state;
    
    public SafeQueue() {
        this(TIMEWAIT_DEFAULT);
    }

    public SafeQueue(int timewait) {
        this(timewait, QUEUE_SIZE_DEFAULT, TIMEOUT_DEFAULT);
    }

    public SafeQueue(int timewait, int maxSize) {
        this(timewait, maxSize, TIMEOUT_DEFAULT);
    }
    
    public SafeQueue(int timewait, int maxSize, int timeout) {
        this.timewait = timewait;
        this.maxQueueSize = maxSize;
        this.timemout = timeout;
    }
    
    @Override
    public void enqueue(Object obj) {
        synchronized (lock) {
            int size = this.queue.size();
            if ((this.maxQueueSize > 0) && (size >= this.maxQueueSize)) {
                throw new IndexOutOfBoundsException("Queue is full. Element not added.");
            }
            VasRequest t = (VasRequest) obj;
            if (queue.containsKey(t.getTransId())) {
                throw new ExceptionInInitializerError("TransactionId is existed: " + t.getTransId());
            }
            this.queue.put(t.getTransId(), t);
            logger.info("receive one USSDApp request: " + t);
            logger.info("size queue: " + (size + 1));
//            this.lock.notifyAll();
        }
    }
  
    @Override
    public Object dequeue() {
        synchronized (lock) {
            Object first = null;
            if (!this.queue.isEmpty()) {
                return removeFist();
            } 
//            else {
//                try {
//                    if (this.timewait == 0) {
//                        while (isEmpty()) {
//                            lock.wait();
//                        }
//                        first = removeFist();
//                    } else {
//                        this.lock.wait(this.timewait);
//                        if (size() > 0) {
//                            first = removeFist();
//                        }
//                    }
//                } catch (InterruptedException ex) {
//                }
//            }
                
            return first;
        }
    }
    
    private Object removeFist(){
        for (Map.Entry<String, VasRequest> entry : queue.entrySet()) {
            String transid = entry.getKey();
            VasRequest t = entry.getValue();
            queue.remove(transid);
            return t;
        }
        return null;
    }
    
    @Override
    public int size() {
        synchronized (this.lock) {
            return this.queue.size();
        }
    }

    @Override
    public boolean isEmpty() {
        synchronized (this.lock) {
            return this.queue.isEmpty();
        }
    }

    @Override
    public Object dequeue(Object obj) {
        synchronized (this.lock) {
            VasRequest t = (VasRequest) obj;
            if (!queue.containsKey(t.getTransId())) {
                return null;
            }
            
            return this.queue.remove(t.getTransId());
        }
    }

    @Override
    public Object find(Object obj) {
        synchronized (this.lock) {
            VasRequest t = (VasRequest) obj;
            return queue.get(t.getTransId());
        }

    }

    @Override
    public Object[] toArray() {
        return this.queue.values().toArray();
    }
    
    private List<VasRequest> getTimeoutRequest(){
        synchronized(lock) {
            if (this.timemout == 0 || queue.isEmpty()) {
                return null;
            }
            
            List<VasRequest> timeoutRequest = new LinkedList<VasRequest>();
            for (Map.Entry<String, VasRequest> entry : queue.entrySet()) {
                VasRequest t = entry.getValue();
                if (System.currentTimeMillis() - t.getSendRecvTime() > this.timemout){
                    timeoutRequest.add(t);
                }
            }
            
            for(VasRequest t: timeoutRequest ) {
                queue.remove(t.getTransId());
                StringBuilder sb = new StringBuilder("removed timeout request: transId ");
                sb.append(t.getTransId())
                        .append(" ,isdn: ").append(t.getMsisdn())
                        .append(" ,stateId: ").append(t.getStateId())
                        .append(" ,param: ").append(t.getParams());
                logger.info(sb.toString());
            }
            
            return timeoutRequest;
        }
    }

    @Override
    public void run() {
        
        try {
            Thread.sleep(this.timemout);
        } catch (InterruptedException ex) {
            logger.equals(ex);
        }
        
        while(state == STATE_START) {
            try {
                List<VasRequest> timeoutRequest = getTimeoutRequest();
                if (timeoutRequest == null || timeoutRequest.isEmpty()) {
                    try {
                        logger.debug("not have timeout request");
                        Thread.sleep(3000);//sleep 3s
                        continue;
                    } catch (InterruptedException ex) {
                        logger.error("STOP process timeoutqueue!!!");
                    }
                }

                logger.warn("process timeout queue size: " + timeoutRequest.size());

                for (VasRequest t : timeoutRequest) {
                    VasGwRecord log = null;
                    try {
                        if (timeoutListener != null) {
                            log = timeoutListener.onEventTimeout(t);
                        }
                    } catch(Exception ex) {
                        logger.error(ex);
                    } finally {
                        if (log != null) {
                            log.setResponseTime(new Date());
                            log.setErrorDescription("Timeout, over(ms) " + this.timemout);
                            ProcessVasgwLog.enqueue(log);
                        }
                    }
                }
            } catch (Exception ex) {
                logger.error("STOP process timeoutqueue!!!", ex);
            }
        }
    }

    public void setVasRequestTimeoutListener(VasRequestTimeoutListener timoutListener) {
        this.timeoutListener = timoutListener;
    }
    
    public void start(){
        state = STATE_START;
        Thread thread = new Thread(this);
        thread.setName("thread-" + SafeQueue.class.getSimpleName());
        thread.start();
    }
    
    public void stop(){
        state = STATE_STOP;
    }
}

interface VasRequestTimeoutListener{
     VasGwRecord onEventTimeout(VasRequest t);
}