/*
 * Copyright (C) 2010 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.viettel.ussdfw.process;

import com.viettel.application.ussdapp.common.VasRequest;
import com.viettel.application.ussdapp.common.VasResponse;
import com.viettel.mmserver.base.ProcessThreadMX;
import com.viettel.ussdfw.log.ProcessVasgwLog;
import com.viettel.ussdfw.manager.AppManager;
import com.viettel.ussdfw.object.VasGwRecord;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author minhnh@viettel.com.vn
 * @since Nov 30, 2012
 * @version 1.0
 */
public abstract class ConnectorProcessThreadAbstract extends ProcessThreadMX {

    private static long DEFAULT_DELAY_TIME = 1L;
    private static String DEFAULT_KEY_RETURN = "1";
    private String returnKey = null;
    private long delayTimeResp = 0L;
    private String name;
    private Dispatcher dispatcher;

    public ConnectorProcessThreadAbstract() {
        super("ConnectorProcessThreadAbstract");
    }

    public void config(String connectorName, String instanceName, HashMap<String, String> listFileConfig) throws Exception {
        logger.info("Config:" + instanceName);
        registerAgent(AppManager.appId + ":type=ConnectorProcessThreadAbstract,name=" + instanceName);
        this.name = instanceName;
        dispatcher = Dispatcher.getInstance(connectorName, listFileConfig);

        setDelayTimeResp(DEFAULT_DELAY_TIME);
        setReturnKey(DEFAULT_KEY_RETURN);
        initBeforeStart();
    }

    @Override
    public void start() {
        super.start();
        logger.info("Start ConnectorProcessThreadAbstract: " + name);
    }

    @Override
    public void stop() {
        super.stop();
        logger.info("Stop ConnectorProcessThreadAbstract: " + name);
    }

    @Override
    protected final void process() {
        VasRequest req = null;
        VasResponse rsp = null;
        VasGwRecord log = null;
        long startTime = 0l;
        try {

            while (true) {
                req = dispatcher.getRequest();
                if (req != null && !req.getId().equals("PING_RESP")) {
                    startTime = System.currentTimeMillis();
                    break;
                }
                Thread.sleep(200);//sleep 0.2 s
            }

            logger.debug(name + ": get req msg from receive queue");
            logger.debug(name + ": request: " + req);
            logger.debug(name + ": request: " + req.getConnectorId());
            logger.debug(name + ": params: " + req.getParams());
            logger.debug(name + ": send response");
            rsp = req.makeResponse();
            rsp.setConnectorId(req.getConnectorId());
            //xu ly req
            log = processRequest(req);
            //
            rsp.setContent(log.getResponse());
            logger.debug(name + " response: " + rsp);
            logger.debug(name + " response: " + log.getResponse());
            if (delayTimeResp > 0) {
                Thread.sleep(delayTimeResp);
                logger.trace(" ----->sleep in " + getDelayTimeResp() + "s");
            }

        } catch (Exception e) {
            logger.error(name + "- Has an error: " + e.getMessage(), e);
            rsp = req.makeResponse();
            rsp.setConnectorId(req.getConnectorId());
            rsp.setContent("ERROR");
            //log
            log = new VasGwRecord(req);
            log.setResponse("ERROR");
            log.setErrorCode(-1); //loi he thong
            log.setErrorDescription(name + "System error" + e.getMessage());
            logger.debug(name + " response: " + rsp);
        }
        //gui response
        try {
            dispatcher.sendResponse(rsp);
            logger.debug(name + " send sucess");
        } catch (Exception ex) {
            logger.error(" Has an error: " + ex.getMessage(), ex);
        } finally {
            //ghi log giao dich            
            log.setResponseTime(new Date());
            long endTime = System.currentTimeMillis();
            logger.info(name + ", process request in:" + (endTime - startTime) 
                    + " (ms),msisdn " + req.getMsisdn() + " ,transaction " + req.getTransId());
            ProcessVasgwLog.enqueue(log);
        }

    }

    protected abstract void initBeforeStart() throws Exception;

    protected abstract VasGwRecord processRequest(VasRequest req) throws Exception;

    public void setReturnKey(String returnKey) {
        this.returnKey = returnKey;
    }

    public String getReturnKey() {
        return this.returnKey;
    }

    public void setDelayTimeResp(long delayTimeResp) {
        this.delayTimeResp = delayTimeResp;
    }

    public long getDelayTimeResp() {
        return this.delayTimeResp;
    }
}
