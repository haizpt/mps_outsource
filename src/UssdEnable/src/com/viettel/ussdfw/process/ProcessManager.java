/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.process;

import com.viettel.mmserver.base.ProcessThreadMX;
import com.viettel.ussdfw.log.ProcessVasgwLog;
import com.viettel.ussdfw.log.ProcessTransLog;
import com.viettel.ussdfw.manager.AppManager;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author TuanNS3_S
 * @since 14/05/2013
 */
public class ProcessManager extends ProcessThreadMX {

    private int idThread[];
    private int idLogVasgwThread[];
    private int idLogTransThread[];
    private int idThreadSync[];

    public ProcessManager() throws Exception {
        super(ProcessManager.class.getSimpleName());
        registerAgent(AppManager.appId + ":type=" + ProcessManager.class.getSimpleName());
        //"../etc/connector.xml"
    }

    public void loadConnector(String fileConfig) throws Exception {
        DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = df.newDocumentBuilder();
        Document dc = db.parse(fileConfig);
        Element root = dc.getDocumentElement();
        HashMap connectors = new HashMap();
        int index = 0;
        //
        NodeList list = root.getElementsByTagName("vas");
        if (list.getLength() < 1) {
            throw new Exception("No vas config");
        }
        ClassLoader cl = new ClassLoader() {
        };
        //
        int countThread = 0;
        for (int i = 0; i < list.getLength(); ++i) {
            Element vasElm = (Element) list.item(i);
            countThread += Integer.parseInt(vasElm.getAttribute("instance"));
        }
        idThread = new int[countThread];
        //
        for (int i = 0; i < list.getLength(); ++i) {
            Element vasElm = (Element) list.item(i);
            String name = vasElm.getAttribute("name");

            String returnKey = vasElm.getAttribute("return_key");
            String delayTimeResp = vasElm.getAttribute("delay_resp");
            String processClass = vasElm.getAttribute("process_class");
            logger.debug("===> Load class: " + processClass);
            NodeList listConnector = vasElm.getElementsByTagName("connector");
            if (listConnector.getLength() < 1) {
                throw new Exception("No vas connector");
            }
            //doc list id connector
            HashMap<String, String> listFileConfig = new HashMap<String, String>();
            for (int j = 0; j < listConnector.getLength(); j++) {
                Element connector = (Element) listConnector.item(j);
                String id = connector.getAttribute("id");
                String file_config = connector.getAttribute("file_config");
                if (connectors.containsKey(id)) {
                    throw new Exception("same connector id: " + id);
                }
                connectors.put(Integer.parseInt(id), 1);
                listFileConfig.put(id, file_config);
            }

            int instance = Integer.parseInt(vasElm.getAttribute("instance"));
            for (int j = 0; j < instance; j++) {
                Class c = cl.loadClass(processClass);
                logger.info("===> Load class: " + c.getName());
                ConnectorProcessThreadAbstract connectorProcessThreadAbstract = (ConnectorProcessThreadAbstract) c.newInstance();
                connectorProcessThreadAbstract.config(name, name + "-" + j, listFileConfig);
                connectorProcessThreadAbstract.setDelayTimeResp(Long.parseLong(delayTimeResp));
                connectorProcessThreadAbstract.setReturnKey(returnKey);
                idThread[index] = connectorProcessThreadAbstract.getId();
                index++;
            }
        }

    }

    public void loadLogThread() {
        idLogVasgwThread = new int[AppManager.numThreadLogVasgw];
        for (int i = 0; i < AppManager.numThreadLogVasgw; i++) {
            ProcessVasgwLog log = new ProcessVasgwLog("ProcessVasgwLog" + i, AppManager.logToDatabase, AppManager.logDatabaseConf, AppManager.mainDatabaseId);
            idLogVasgwThread[i] = log.getId();
        }
        idLogTransThread = new int[AppManager.numThreadLogTrans];
        for (int i = 0; i < AppManager.numThreadLogTrans; i++) {
            ProcessTransLog log = new ProcessTransLog("ProcessTransLog" + i, AppManager.logToDatabase, AppManager.logDatabaseConf, AppManager.mainDatabaseId);
            idLogTransThread[i] = log.getId();
        }
    }

    @Override
    protected void process() {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    @Override
    public void start() {
        Dispatcher.start();
        for (int i = 0; i < idThread.length; i++) {
            com.viettel.mmserver.base.ProcessManager.getInstance().getMmProcess(idThread[i]).start();
        }

        for (int i = 0; i < idLogVasgwThread.length; i++) {
            com.viettel.mmserver.base.ProcessManager.getInstance().getMmProcess(idLogVasgwThread[i]).start();
        }
        for (int i = 0; i < idLogTransThread.length; i++) {
            com.viettel.mmserver.base.ProcessManager.getInstance().getMmProcess(idLogTransThread[i]).start();
        }
//        idThreadSync = new int[AppManager.numThreadSync];
//        for (int i = 0; i < AppManager.numThreadSync; i++) {
////            Thread_CC log = new Thread_CC("ThreadSyncDb" + i, i, AppManager.numThreadSync);
////            idThreadSync[i] = log.getId();
//            com.viettel.mmserver.base.ProcessManager.getInstance().getMmProcess(idThreadSync[i]).start();
//        }

        super.start();
        logger.info("+++ SYSTEM PROCESS STARTED  +++");
    }

    @Override
    public void stop() {
        Dispatcher.stop();
        for (int i = 0; i < idThread.length; i++) {
            com.viettel.mmserver.base.ProcessManager.getInstance().getMmProcess(idThread[i]).stop();
        }
        for (int i = 0; i < idLogVasgwThread.length; i++) {
            com.viettel.mmserver.base.ProcessManager.getInstance().getMmProcess(idLogVasgwThread[i]).stop();
        }
        for (int i = 0; i < idLogTransThread.length; i++) {
            com.viettel.mmserver.base.ProcessManager.getInstance().getMmProcess(idLogTransThread[i]).stop();
        }
//        for (int i = 0; i < idThreadSync.length; i++) {
//            com.viettel.mmserver.base.ProcessManager.getInstance().getMmProcess(idThreadSync[i]).stop();
//        }

        super.stop();
        logger.info("+++ SYSTEM PROCESS STOPPED  +++");
    }
}
