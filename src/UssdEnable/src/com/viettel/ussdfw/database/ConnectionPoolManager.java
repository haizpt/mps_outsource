/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.database;

import com.viettel.database.oracle.OracleConnection;
import com.viettel.ussdfw.object.ConnectionPoolObject;
import com.viettel.utility.PropertiesUtils;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author minhnh_s
 */
public class ConnectionPoolManager {

    private static Logger logger = Logger.getLogger(ConnectionPoolManager.class);
    private static Map<String, DataSource> connectionMap = null;
    //
    public static int QUERY_TIMEOUT = 300; // Time out query
    public static int TIME_BREAK = 0;
    public static int TIME_BREAK_DELETE_RECORD_TIMEOUT = 0;
    
    private static OracleConnection oracleConnection ;
    
    /**
     * *
     *
     * @param dbConfigXml
     * @throws Exception
     */
    public static void loadConfig(String dbConfigXml) throws Exception {

        if (connectionMap == null) {
            connectionMap = new HashMap();
            try {
//                PropertiesUtils proUtils = new PropertiesUtils();
//                proUtils.loadProperties(fis);
//                StringBuilder build = new StringBuilder();
//                String[] proStr = proUtils.getProperties();
//                //
//                for (int i = 0; i < proStr.length; i++) {
//                    build.append(proStr[i]);
//                }

                String content = readFile(dbConfigXml);
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();

                InputStream inputStream = new ByteArrayInputStream(content.getBytes());
                Document document = db.parse(inputStream);

//                InputSource is = new InputSource(new StringReader(build.toString()));
//                DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
//                DocumentBuilder db = df.newDocumentBuilder();
//                Document dc = db.parse(is);
                Element root = document.getDocumentElement();
                int index = 0;
                //load cac time out
                NodeList listTimeOut = root.getElementsByTagName("timeout-config");
                for (int i = 0; i < listTimeOut.getLength(); i++) {
                    Element ele = (Element) listTimeOut.item(i);
                    NodeList listProperty = ele.getElementsByTagName("property");
                    for (int j = 0; j < listProperty.getLength(); j++) {
                        Element elePro = (Element) listProperty.item(j);
                        String name = elePro.getAttribute("name");
                        String value = elePro.getTextContent();
                        if (name.equals("queryDbTimeout")) {
                            try {
                                ConnectionPoolManager.QUERY_TIMEOUT = Integer.parseInt(value);
                            } catch (Exception e) {
                                ConnectionPoolManager.QUERY_TIMEOUT = 60;
                            }

                        } else if (name.equals("timeBreak")) {
                            try {
                                ConnectionPoolManager.TIME_BREAK = Integer.parseInt(value);
                            } catch (Exception e) {
                                ConnectionPoolManager.TIME_BREAK = 90000;
                            }
                        } else if (name.equals("timeBreakDeleteRecordTimeOut")) {
                            try {
                                ConnectionPoolManager.TIME_BREAK_DELETE_RECORD_TIMEOUT = Integer.parseInt(value);
                            } catch (Exception e) {
                                ConnectionPoolManager.TIME_BREAK_DELETE_RECORD_TIMEOUT = 120000;
                            }
                        }
                    }
                }

                //
                NodeList list = root.getElementsByTagName("named-config");
                if (list.getLength() < 1) {
                    throw new Exception("No named-config");
                }
                //
                for (int i = 0; i < list.getLength(); ++i) {
                    Element ele = (Element) list.item(i);
                    String dbName = ele.getAttribute("name");

                    NodeList listProperty = ele.getElementsByTagName("property");
                    if (listProperty.getLength() < 1) {
                        throw new Exception("No property");
                    }
                    //doc list
                    Properties pro = new Properties();
                    for (int j = 0; j < listProperty.getLength(); j++) {
                        Element elePro = (Element) listProperty.item(j);
                        String name = elePro.getAttribute("name");
                        String value = elePro.getTextContent();
                        pro.setProperty(name, value);
                    }

                    // init pool Database
                    ConnectionPoolObject obj = new ConnectionPoolObject(pro);
                    obj.setId(dbName);
                    try {
                        connectionMap.put(dbName, obj.createPoolConnection());
                    } catch (Exception e) {
                        logger.error("==> ERROR WHEN INIT DATABASE ID=" + dbName, e);
                    }
                }
            } catch (Exception ex) {
                logger.error("==> ERROR INIT DATABASE LIST", ex);
                throw ex;
            }
        }
    }
    
    private static String readFile(String filePath) {
        StringBuilder sb = new StringBuilder("");
        BufferedReader br = null;

        try {

            String sCurrentLine;

            br = new BufferedReader(new FileReader(filePath));

            while ((sCurrentLine = br.readLine()) != null) {
                sb.append(sCurrentLine);
            }

        } catch (IOException e) {
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    java.util.logging.Logger.getLogger(ConnectionPoolManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return sb.toString();
    }

    public static void reLoadConfig(String dbConfigXml) throws Exception {
        if (connectionMap == null) {
            connectionMap = new ConcurrentHashMap<String, DataSource>();
        } else {
            connectionMap.clear();
        }
        try {
            PropertiesUtils proUtils = new PropertiesUtils();
            proUtils.loadPropertiesEpt(dbConfigXml);
            StringBuilder build = new StringBuilder();
            String[] proStr = proUtils.getProperties();
            //
            for (int i = 0; i < proStr.length; i++) {
                build.append(proStr[i]);
            }
            InputSource is = new InputSource(new StringReader(build.toString()));
            DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = df.newDocumentBuilder();
            Document dc = db.parse(is);
            Element root = dc.getDocumentElement();
            HashMap connectors = new HashMap();
            int index = 0;
            //
            NodeList list = root.getElementsByTagName("named-config");
            if (list.getLength() < 1) {
                throw new Exception("No named-config");
            }
            //
            for (int i = 0; i < list.getLength(); ++i) {
                Element ele = (Element) list.item(i);
                String dbName = ele.getAttribute("name");

                NodeList listProperty = ele.getElementsByTagName("property");
                if (listProperty.getLength() < 1) {
                    throw new Exception("No property");
                }
                //doc list
                Properties pro = new Properties();
                for (int j = 0; j < listProperty.getLength(); j++) {
                    Element elePro = (Element) listProperty.item(j);
                    String name = elePro.getAttribute("name");
                    String value = elePro.getTextContent();
                    pro.setProperty(name, value);
                }

                // init pool Database
                ConnectionPoolObject obj = new ConnectionPoolObject(pro);
                obj.setId(dbName);
                try {
                    connectionMap.put(dbName, obj.createPoolConnection());
                } catch (Exception e) {
                    logger.error("==> ERROR WHEN INIT DATABASE ID=" + dbName, e);
                }
            }
        } catch (Exception ex) {
            logger.error("==> ERROR INIT DATABASE LIST", ex);
            throw ex;
        }
    }

    /**
     * @param id
     * @return **
     *
     */
    public static synchronized Connection getConnection(String id) {
        Connection conn = null;
        try {
            if (connectionMap.containsKey(id)) {
                conn = (connectionMap.get(id)).getConnection();
            } else {
                logger.warn("PooledDataSource don't have ID: " + id);
            }
        } catch (SQLException ex) {
            logger.error("Can't connect DB, with ID: " + id, ex);
        }

        return conn;
    }

    public static Map<String, DataSource> getConnectionMap() {
        return connectionMap;
    }

    public static void setConnectionMap(HashMap<String, DataSource> connectionMap) {
        ConnectionPoolManager.connectionMap = connectionMap;
    }

    public static List<String> getListDatabaseName() {

        List<String> listTemp = new ArrayList<String>();
        List<String> listReturn = new ArrayList<String>();
        for (Map.Entry<String, DataSource> entry : connectionMap.entrySet()) {
            String databaseName = entry.getKey();
            listTemp.add(databaseName);
        }
        //dao nguoc chuoi cho dung thu tu
        for (int i = listTemp.size(); i > 0; i--) {
            listReturn.add(listTemp.get(i - 1));
        }
        return listReturn;
    }
    
    public static DataSource getDataSource(String databaseId) {
        return connectionMap.get(databaseId);
    }
    
    public static OracleConnection getOracleConnection(){
        return oracleConnection;
    }
    
    public static void setOracleConnection(OracleConnection oracleConnection){
        ConnectionPoolManager.oracleConnection = oracleConnection;
    }
    
    public static Connection getDefaultConnection(){
        try {
            return ConnectionPoolManager.oracleConnection.getDataSource().getConnection();
        } catch (SQLException ex) {
             logger.error("Can't connect DB, with ID: ", ex);
        }
        return null;
    }
    
    
    public static void closeQuite(ResultSet rs, Statement statement , Connection connection){
        try {
            if (rs != null) {
                rs.close();
            }
            
            if (statement != null) {
                statement.close();
            }
            
            if (connection != null) {
                connection.close();
            }
        } catch(Exception ex){
            logger.error("Can't close: ", ex);
        }
    }
}
