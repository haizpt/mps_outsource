/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.database;

import com.viettel.ussdfw.manager.AppManager;
import com.viettel.ussdfw.object.TransactionLog;
import com.viettel.ussdfw.object.VasGwRecord;
import com.viettel.ussdfw.object.WebserviceObject;
import com.viettel.ussdfw.webservice.WebserviceGroup;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author TuanNS3_S
 */
public class DbProcessorFW {

    protected Logger logger;
    private String loggerLabel = "DbProcessor-FW: ";
    private long timeSt;
    private long timeEx;
    private StringBuffer br = new StringBuffer();
    
    //sql
    private static String SQL_INSERT_VAS_GW_LOG = "INSERT INTO vas_gw_log "
            + "(VAS_GW_LOG_ID,MSISDN,IMSI,SID,CPID,BIZID,REQUEST,REQUEST_TIME"
            + ",RESPONSE,RESPONSE_TIME,TRANSACTION_ID,ERROR_CODE,ERROR_DESCRIPTION) "
            + " VALUES(vas_gw_log_seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static String SQL_INSERT_TRANS_LOG = "INSERT INTO transaction_log "
            + "(TRANSACTION_ID,TYPE,MSISDN,REQUEST,ERROR_CODE"
            + ",COMMAND,REQUEST_TIME,RESPONSE_TIME,RESPONSE,CHANNEL) "
            + "VALUES(transaction_log_seq.nextval,?,?,?,?,?,?,?,?,?)";
    private static String SQL_SELECT_WEBSERVICE = "SELECT * FROM webservice WHERE STATUS=1 ORDER BY ws_code, ws_id";

    public DbProcessorFW(String name) throws Exception {
        this.logger = Logger.getLogger(name);
    }

    public DbProcessorFW(Logger logger) throws Exception {
        this.logger = logger;
    }

    public DbProcessorFW(String name, String path) throws Exception {
        this.logger = Logger.getLogger(name);
    }

    public DbProcessorFW(Logger logger, String path) throws Exception {
        this.logger = logger;
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public void setTimeSt(long timeSt) {
        this.timeSt = timeSt;
    }

    public void logTime(String strLog) {
        timeEx = System.currentTimeMillis() - timeSt;
        if (timeEx >= AppManager.minTimeDb && AppManager.loggerDbMap != null) {
            br.setLength(0);
            br.append(loggerLabel).
                    append(AppManager.getTimeLevelDb(timeEx)).append(": ").
                    append(strLog).
                    append(": ").
                    append(timeEx).
                    append(" ms");
            logger.warn(br);
        } else {
            br.setLength(0);
            br.append(loggerLabel).
                    append(strLog).
                    append(": ").
                    append(timeEx).
                    append(" ms");

            logger.info(br);
        }
    }

//    public Connection getConnection(String dbId) {
//        return ConnectionPoolManager.getConnection(dbId);
//    }
    
    //hungtv45
//    public OracleConnection getConnection() {
//        return DatabaseConnectionPool.getConnection();
//    }

    protected void closeResource(ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
                rs = null;
            }
        } catch (SQLException e) {
            logger.warn(e);
            rs = null;
        }
    }

    protected void closeResource(Statement stmt) {
        try {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        } catch (SQLException e) {
            logger.warn(e);
            stmt = null;
        }
    }

    protected void closeResource(Connection conn) {
        try {
            if (conn != null) {
                conn.close();
                conn = null;
            }
        } catch (SQLException e) {
            logger.warn(e);
            conn = null;
        }
    }

    /**
     *
     * @param vasGwLog
     * @param dbId
     * @return
     */
    public int iInsertVasGwLog(VasGwRecord vasGwLog, String dbId) {
        int result = -1;
        long timeBegin = System.currentTimeMillis();
        int count = 1;
        while (result < 0) {
            try {
                result = insertVasGwLog(vasGwLog, dbId);
            } catch (SQLException ex) {
                br.setLength(0);
                br.append(loggerLabel).append("ERROR - ").append(count).
                        append("\n").append(SQL_INSERT_VAS_GW_LOG);
                logger.error(br, ex);
                count++;
            } catch (Exception ex) {
                br.setLength(0);
                br.append(loggerLabel).
                        append(new Date()).
                        append("\nERROR insert VAS_GW_LOG");
                logger.error(br, ex);
                break;
            }
            if (result < 0 && System.currentTimeMillis() - timeBegin > AppManager.breakQuery) {
                br.setLength(0);
                br.append(loggerLabel).append(new Date()).
                        append("==> BREAK query insert VAS_GW_LOG\n");
                logger.error(br);
                break;
            }
        }
        return result;
    }

    /**
     *
     * @param vasGwLog
     * @param dbId
     * @return
     * @throws SQLException
     */
    private int insertVasGwLog(VasGwRecord vasGwLog, String dbId) throws SQLException {
        //"INSERT INTO vas_gw_log "
        //"(VAS_GW_LOG_ID,MSISDN,IMSI,SID,CPID,BIZID,REQUEST,REQUEST_TIME"
        //",RESPONSE,RESPONSE_TIME,TRANSACTION_ID,ERROR_CODE,ERROR_DESCRIPTION) "
        // " VALUES(vas_gw_log_seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?)"
        int result = -1;
        PreparedStatement ps = null;        
        Connection mConnection = null;

        try {
             mConnection = ConnectionPoolManager.getConnection(dbId);
            ps = mConnection.prepareStatement(SQL_INSERT_VAS_GW_LOG);
            ps.setQueryTimeout(AppManager.queryDbTimeout);
            //Set thuoc tinh dau vao
            ps.setString(1, vasGwLog.getMsisdn());
            ps.setString(2, vasGwLog.getImsi());
            ps.setLong(3, vasGwLog.getSid());
            ps.setLong(4, vasGwLog.getCpId());
            ps.setLong(5, vasGwLog.getBizid());
            ps.setString(6, vasGwLog.getRequest());
            ps.setTimestamp(7, new Timestamp(vasGwLog.getRequestTime().getTime()));
            ps.setString(8, vasGwLog.getResponse());
            ps.setTimestamp(9, new Timestamp(vasGwLog.getResponseTime().getTime()));
            ps.setString(10, vasGwLog.getTransactionId());
            ps.setInt(11, vasGwLog.getErrorCode());
            ps.setString(12, vasGwLog.getErrorDescription());
            result = ps.executeUpdate();
            mConnection.commit();
            
        } catch (SQLException ex) {
            throw ex;
        } finally {
            closeResource(ps);
            logTime(SQL_INSERT_VAS_GW_LOG);
            if (mConnection != null) {
                mConnection.close();
            }
        }
        return result;
    }

    /**
     *
     * @param listVasGwLog
     * @param dbId
     * @return
     */
    public int[] iInsertVasGwLog(List<VasGwRecord> listVasGwLog, String dbId) {
        int result[] = null;
        long timeBegin = System.currentTimeMillis();
        int count = 1;
        while (result == null) {
            try {
                result = insertVasGwLog(listVasGwLog, dbId);
            } catch (SQLException ex) {
                br.setLength(0);
                br.append(loggerLabel).append("ERROR - ").append(count).
                        append("\n").append(SQL_INSERT_VAS_GW_LOG);
                logger.error(br, ex);
                count++;
            } catch (Exception ex) {
                br.setLength(0);
                br.append(loggerLabel).
                        append(new Date()).
                        append("\nERROR insert VAS_GW_LOG");
                logger.error(br, ex);
                break;
            }
            if (result == null && System.currentTimeMillis() - timeBegin > AppManager.breakQuery) {
                br.setLength(0);
                br.append(loggerLabel).append(new Date()).
                        append("==> BREAK query insert VAS_GW_LOG\n");
                logger.error(br);
                break;
            }
        }
        return result;
    }

    /**
     *
     * @param listVasGwLog
     * @param dbId
     * @return
     * @throws SQLException
     */
    private int[] insertVasGwLog(List<VasGwRecord> listVasGwLog, String dbId) throws SQLException {
        //"INSERT INTO vas_gw_log "
        //"(VAS_GW_LOG_ID,MSISDN,IMSI,SID,CPID,BIZID,REQUEST,REQUEST_TIME"
        //",RESPONSE,RESPONSE_TIME,TRANSACTION_ID,ERROR_CODE,ERROR_DESCRIPTION) "
        // " VALUES(vas_gw_log_seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?)"
        int result[] = null;
        PreparedStatement ps = null;
        Connection mConnection = null;

        try {
            mConnection = ConnectionPoolManager.getConnection(dbId);
            setTimeSt(System.currentTimeMillis());
            ps = mConnection.prepareStatement(SQL_INSERT_VAS_GW_LOG);
            ps.setQueryTimeout(AppManager.queryDbTimeout);
            br.setLength(0);
            br.append(" INSERT_VAS_GW_LOG: ");
            //Set thuoc tinh dau vao
            for (VasGwRecord vasGwLog : listVasGwLog) {
                br.append("[").
                        append(vasGwLog.getMsisdn()).
                        append("|").
                        append(vasGwLog.getRequest()).
                        append("]|");
                ps.setString(1, vasGwLog.getMsisdn());
                ps.setString(2, vasGwLog.getImsi());
                ps.setLong(3, vasGwLog.getSid());
                ps.setLong(4, vasGwLog.getCpId());
                ps.setLong(5, vasGwLog.getBizid());
                ps.setString(6, vasGwLog.getRequest());
                ps.setTimestamp(7, new Timestamp(vasGwLog.getRequestTime().getTime()));
                ps.setString(8, vasGwLog.getResponse());
                ps.setTimestamp(9, new Timestamp(vasGwLog.getResponseTime().getTime()));
                ps.setString(10, vasGwLog.getTransactionId());
                ps.setInt(11, vasGwLog.getErrorCode());
                ps.setString(12, vasGwLog.getErrorDescription());
                ps.addBatch();
            }
            result = ps.executeBatch();
            mConnection.commit();
        } catch (SQLException ex) {
            br.setLength(0);
            throw ex;
        } finally {
            closeResource(ps);
            logTime(br.toString());
            br.setLength(0);
            if (mConnection != null) {
                mConnection.close();
            }
        }
        return result;
    }

    /**
     *
     * @param listTransLog
     * @param dbId
     * @return
     */
    public int[] iInsertTransLog(List<TransactionLog> listTransLog) {
        int result[] = null;
        long timeBegin = System.currentTimeMillis();
        int count = 1;
        while (result == null) {
            try {
                result = insertTransLog(listTransLog);
            } catch (SQLException ex) {
                br.setLength(0);
                br.append(loggerLabel).append("ERROR - ").append(count).
                        append("\n").append(SQL_INSERT_TRANS_LOG);
                logger.error(br, ex);
                count++;
            } catch (Exception ex) {
                br.setLength(0);
                br.append(loggerLabel).
                        append(new Date()).
                        append("\nERROR insert TRANSACTION_LOG");
                logger.error(br, ex);
                break;
            }
            if (result == null && System.currentTimeMillis() - timeBegin > AppManager.breakQuery) {
                br.setLength(0);
                br.append(loggerLabel).append(new Date()).
                        append("==> BREAK query insert TRANSACTION_LOG\n");
                logger.error(br);
                break;
            }
        }
        return result;
    }

    /**
     *
     * @param listVasGwLog
     * @param dbId
     * @return
     * @throws SQLException
     */
    private int[] insertTransLog(List<TransactionLog> listTransLog) throws SQLException {
        //"INSERT INTO transaction_log "
        //+ "(TRANSACTION_ID,TYPE,MSISDN,REQUEST,ERROR_CODE"
        //+ ",COMMAND,REQUEST_TIME,RESPONSE_TIME,RESPONSE,CHANNEL) "
        //+ "VALUES(transaction_log_seq.nextval,?,?,?,?,?,?,?,?,?)";
        int result[] = null;
        PreparedStatement ps = null;
        Connection connection = null;

        try {
           
            connection = ConnectionPoolManager.getDefaultConnection();
            setTimeSt(System.currentTimeMillis());
            ps = connection.prepareStatement(SQL_INSERT_TRANS_LOG);
            ps.setQueryTimeout(AppManager.queryDbTimeout);
            br.setLength(0);
            br.append(" INSERT TRANSACTION_LOG: ");
            //Set thuoc tinh dau vao
            for (TransactionLog transLog : listTransLog) {
                br.append("[").
                        append(transLog.getMsisdn()).
                        append("|").
                        append(transLog.getCommand()).
                        append("]|");
                ps.setInt(1, transLog.getType());
                ps.setString(2, transLog.getMsisdn());
                ps.setString(3, transLog.getRequest());
                ps.setString(4, transLog.getErrorCode());
                ps.setString(5, transLog.getCommand());
                ps.setTimestamp(6, new Timestamp(transLog.getRequestTime().getTime()));
                ps.setTimestamp(7, new Timestamp(transLog.getResponseTime().getTime()));
                ps.setString(8, transLog.getResponse());
                ps.setString(9, transLog.getChannel());
                ps.addBatch();
            }
            result = ps.executeBatch();
            connection.commit();
        } catch (SQLException ex) {
            br.setLength(0);
            throw ex;
        } finally {
            closeResource(ps);
            logTime(br.toString());
            br.setLength(0);
            if (connection != null) {
                connection.close();
            }
        }
        return result;
    }

    public HashMap<String, WebserviceGroup> iGetWebservice() {
        HashMap<String, WebserviceGroup> hashmap = null;
        long timeBegin = System.currentTimeMillis();
        int count = 1;
        while (hashmap == null) {
            try {
                hashmap = getWebservice();
            } catch (SQLException ex) {
                br.setLength(0);
                br.append(loggerLabel).append("ERROR - ").append(count).
                        append("\n").append(SQL_SELECT_WEBSERVICE);
                logger.error(br, ex);
                count++;
            } catch (Exception ex) {
                br.setLength(0);
                br.append(loggerLabel).
                        append(new Date()).
                        append("\nERROR select WEBSERVICE");
                logger.error(br, ex);
                break;
            }
            if (hashmap == null && System.currentTimeMillis() - timeBegin > AppManager.breakQuery) {
                br.setLength(0);
                br.append(loggerLabel).append(new Date()).
                        append("==> BREAK query select WEBSERVICE \n");
                logger.error(br);
                break;
            }
        }
        return hashmap;
    }

    private HashMap<String, WebserviceGroup> getWebservice() throws SQLException {
        HashMap<String, WebserviceGroup> hashmap = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        Connection connection = null;

        try {
            connection = ConnectionPoolManager.getDefaultConnection();
            setTimeSt(System.currentTimeMillis());
            ps = connection.prepareStatement(SQL_SELECT_WEBSERVICE);
            ps.setQueryTimeout(AppManager.queryDbTimeout);

            rs = ps.executeQuery();
            hashmap = new HashMap<String, WebserviceGroup>();
            String wsCode = "";
            List<WebserviceObject> listWs = new ArrayList<WebserviceObject>();
            while (rs.next()) {
                String wsCodeTmp = rs.getString(WebserviceObject.WS_CODE);
                //
                if (!wsCode.equals(wsCodeTmp)) {
                    if (!wsCode.equals("")) {
                        WebserviceGroup wsGroup = new WebserviceGroup();
                        wsGroup.setListWs(listWs);
                        //
                        hashmap.put(wsCode, wsGroup);
                        listWs = new ArrayList<WebserviceObject>();
                    }
                    wsCode = wsCodeTmp;
                }
                WebserviceObject obj = new WebserviceObject();
                obj.setWsId(rs.getLong(WebserviceObject.WS_ID));
                obj.setWsName(rs.getString(WebserviceObject.WS_NAME));
                obj.setWsCode(rs.getString(WebserviceObject.WS_CODE));
                obj.setWsdl(rs.getString(WebserviceObject.WSDL));
                obj.setMsgTemplate(rs.getString(WebserviceObject.MSG_TEMPLATE));
                obj.setStatus(rs.getInt(WebserviceObject.STATUS));
                listWs.add(obj);
            }
            if (!wsCode.equals("")) {
                WebserviceGroup wsGroup = new WebserviceGroup();
                wsGroup.setListWs(listWs);
                //
                hashmap.put(wsCode, wsGroup);
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {
            closeResource(rs);
            closeResource(ps);
            logTime("Time to select WEBSERVICE");
            if (connection != null) {
                connection.close();
            }
        }
        return hashmap;
    }
}
