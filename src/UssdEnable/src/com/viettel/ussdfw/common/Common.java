/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.common;

/**
 *
 * @author TuanNS3_S
 */
public class Common {

    public static class TransType {

        public static final int EXCHANGE = 1;
        public static final int SERVICE = 2;
        public static final int WEBSERVICE = 3;
    }

    public static class ErrorCode {

        public static final String TIMEOUT = "001";
        public static final String ERROR = "000";
    }
    
    public static class Config{
        public static final String COUNTRY = "855";
    }

    
    public static String formatMobileNumber(String phone)
    {
        boolean flag = true;
        String country = Common.Config.COUNTRY;
        String digit = "\\d";
        flag = phone.matches(digit);
        if (!flag){
            return "FAIL";
        }
        if(phone.substring(0, 1).equals("0")) // 09xxx
        {
            phone = phone;
        }
        else if(phone.substring(0, country.length()).equals(country)){
            phone = phone.substring(0, country.length());
        }
        else{
            phone = phone;
        }
        return phone;
        
    }
}
