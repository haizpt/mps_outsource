/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.log;

import com.viettel.logging.LogWriter;
import com.viettel.mmserver.base.ProcessThreadMX;
import com.viettel.ussdfw.database.DbProcessorFW;
import com.viettel.ussdfw.manager.AppManager;
import com.viettel.ussdfw.object.TransactionLog;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author TuanNS3_S
 */
public class ProcessTransLog extends ProcessThreadMX {

    private static final LinkedList<TransactionLog> queue = new LinkedList<TransactionLog>();
    private static final Object queueLock = new Object();
    public final static int BATCH_SIZE = 500;
    public final static int DEFAULT_SLEEP = 5000;
    
    private boolean logToDb;
    private static LogWriter logTransWriter;
    private String dbId;
    private DbProcessorFW dbProcessor;

    public ProcessTransLog(String name, boolean logToDb, String dbPath, String dbId) {
        super(name);
        try {
            this.dbId = dbId;
            this.logToDb = logToDb;
            registerAgent(AppManager.appId + 
                    ":type=ProcessTransLog,name=" + name);
            //
            if (logToDb) {
                //khoi tao ghi log vao db                
                dbProcessor = new DbProcessorFW(logger, dbPath);
            } else {
                //khoi tao ghi log ra file
                logTransWriter = new LogWriter("../etc/log_writer.cfg", 
                        "/trans", "translog");
            }
        } catch (Exception ex) {
            logger.error("ERROR when init ProcessTransLog", ex);
        }
    }

    public static void enqueue(TransactionLog transLog) {
        synchronized(queueLock) {
            queue.add(transLog);
        }
    }
    
    private List<TransactionLog> dequeue() {
        synchronized (queueLock) {
            if (queue == null || queue.isEmpty()) {
                return null;
            }
            
            List<TransactionLog> datas = new LinkedList<TransactionLog>();
            int i = 0;
            while (!queue.isEmpty() && (i < BATCH_SIZE)) {
                TransactionLog transaction = queue.poll();
                datas.add(transaction);
                i++;
            }

            return datas;
        }
    }

    @Override
    protected void process() {
        List<TransactionLog> datas = dequeue();
        if (datas == null) {
            try {
                logger.debug("do not have transaction, sleep: 5s");
                Thread.sleep(DEFAULT_SLEEP);
            } catch (InterruptedException ex) {
                logger.error(ex);
            }
        } else {
            if (logToDb) { //log to database
                logger.debug("have datas to log database: " + datas.size());
                try {
                    dbProcessor.iInsertTransLog(datas);
                    int length = datas.size();
                    int sleep = 1000 * BATCH_SIZE / length; // ms
                    if (sleep > DEFAULT_SLEEP) {
                        sleep = DEFAULT_SLEEP;
                    }
                    Thread.sleep(sleep);
                } catch (Exception ex) {
                    logger.error(ex);
                }
                datas.clear();
                datas = null;
            } else {
                logger.debug("have datas to log file: " + datas.size());
                for (TransactionLog transactionLog : datas) {
                    logTransWriter.writeLn(transactionLog.toLog(LogWriter.seperator));
                }
            }
        }

    }
}
