/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.ussdfw.log;

import com.viettel.logging.LogWriter;
import com.viettel.mmserver.base.ProcessThreadMX;
import com.viettel.ussdfw.database.DbProcessorFW;
import com.viettel.ussdfw.manager.AppManager;
import com.viettel.ussdfw.object.VasGwRecord;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author TuanNS3_S
 */
public class ProcessVasgwLog extends ProcessThreadMX {
    public final static int BATCH_SIZE = 500;
    public final static int DEFAULT_SLEEP = 5000;
    
    private static final LinkedList<VasGwRecord> queue = new LinkedList<VasGwRecord>();
    private static final Object queueLock = new Object();
    private boolean logToDb;
    private static LogWriter logVasGwWriter;
    private String dbId;
    private DbProcessorFW dbProcessor;

    public ProcessVasgwLog(String name, boolean logToDb, String dbPath, String dbId) {
        super(name);
        try {
            this.logToDb = logToDb;
            this.dbId = dbId;
            //
            registerAgent(AppManager.appId + ":type=ProcessVasgwLog,name=" + name);
            //
            if (logToDb) {
                //khoi tao ghi log vao db                
                dbProcessor = new DbProcessorFW(logger, dbPath);
            } else {
                //khoi tao ghi log ra file
                logVasGwWriter = new LogWriter("../etc/log_writer.cfg", "/vasgw", "vasgwlog");
            }
        } catch (Exception ex) {
            logger.error("ERROR when init processLog", ex);
        }
    }

    public static void enqueue(VasGwRecord transLog) {
        synchronized(queueLock) {
            queue.add(transLog);
        }
    }
    
    private List<VasGwRecord> dequeue() {
        synchronized (queueLock) {
            if (queue == null || queue.isEmpty()) {
                return null;
            }
            
            List<VasGwRecord> datas = new LinkedList<VasGwRecord>();
            int i = 0;
            while (!queue.isEmpty() && (i < BATCH_SIZE)) {
                VasGwRecord transaction = queue.poll();
                datas.add(transaction);
                i++;
            }

            return datas;
        }
    }

    @Override
    protected void process() {
        List<VasGwRecord> datas = dequeue();
        if (datas == null) {
            try {
                logger.debug("do not have transaction, sleep: 5s");
                Thread.sleep(DEFAULT_SLEEP);
            } catch (Exception ex) {
                logger.error(ex);
            }
        } else {
            if (logToDb) { //log to database
                logger.info(getThreadName() + ",have datas to log database: " + datas.size());
                try {
                    dbProcessor.iInsertVasGwLog(datas, dbId);
                    int length = datas.size();
                    int sleep = 1000 * BATCH_SIZE / length; // ms
                    if (sleep > DEFAULT_SLEEP) {
                        sleep = DEFAULT_SLEEP;
                    }
                    Thread.sleep(sleep);
                } catch (Exception ex) {
                    logger.error(getThreadName() + ",error insert vaslog",ex);
                }
                datas.clear();
                datas = null;
            } else {
                logger.debug(getThreadName() + ",have datas to log file: " + datas.size());
                for (VasGwRecord logRecord : datas) {
                    logVasGwWriter.writeLn(logRecord.toLog(LogWriter.seperator));
                }
            }
        }
    }
   
}
