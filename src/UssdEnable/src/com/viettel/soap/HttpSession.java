/**
 *
 * handsome boy
 */

package com.viettel.soap;

import com.viettel.cache.HCacheMgt;
import java.net.Socket;
import java.io.*;
import java.net.InetSocketAddress;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.Properties;

/**
 * Handles one session, i.e. parses the HTTPS request
 * and returns the response.
 */
public class HttpSession implements Runnable {
    /**
     * GMT date formatter
     */
    private static java.text.SimpleDateFormat gmtFrmt;

    static {
        gmtFrmt = new java.text.SimpleDateFormat("E, d MMM yyyy HH:mm:ss 'GMT'", Locale.US);
        gmtFrmt.setTimeZone(TimeZone.getTimeZone("GMT"));
    }
//    private Queue rx = null;

    /**
     * create new HttpSession with connected socket
     * @param s current connected socket
     * @param manager web service manager which contain all registed web service
     */
    public HttpSession() {}
    public HttpSession(Socket s) {
        this.mySocket = s;
//        this.rx = rx;
        Thread t = new Thread(this);
        t.setDaemon(true);
        t.start();
    }

    public void process(Object content, String method, Properties header) {
        String response = "Not support";
        header.put("content-length", "" + response.getBytes().length);
        sendResponse(HttpResponse.HTTP_OK, HttpResponse.mimeType, header,
                new ByteArrayInputStream(response.getBytes()));
    }
    


    /**
     * Main process when receiver request from VAS CP.
     *
     */
    public void run() {
        InputStream is = null;
        String s = null;
        String ipClient="";
        int portClient=-1;
        try {
            is = mySocket.getInputStream();
            //thangtq modified
            ipClient=((InetSocketAddress)mySocket.getRemoteSocketAddress()).getAddress().getHostAddress();
            portClient=mySocket.getLocalPort();
            if (is == null) {
                return;
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            s = in.readLine();
            if(s==null) 
            {
                Thread.sleep(200);
                s = in.readLine();
            }
            //GET /process/services/ProcessClients?wsdl HTTP/1.1
            // Read the request line
            StringTokenizer st = new StringTokenizer(s);
            if (!st.hasMoreTokens()) {
                sendError(HttpResponse.HTTP_BADREQUEST, "BAD REQUEST: Syntax error. Usage: GET /example/file.html");
            }
            String method = st.nextToken();
            if (!st.hasMoreTokens()) {
                sendError(HttpResponse.HTTP_BADREQUEST, "BAD REQUEST: Missing URI. Usage: GET /example/file.html");
            }
            String uri = decodePercent(st.nextToken());
            // Decode parameters from the URI
            Properties parms = new Properties();
            int qmi = uri.indexOf('?');
            if (qmi >= 0) {
                decodeParms(uri.substring(qmi + 1), parms);
                uri = decodePercent(uri.substring(0, qmi));
            }
            /************************************ PROCESS TO CLEINT ******************************/
            // If there's another token, it's protocol version,
            // followed by HTTP headers. Ignore version but parse headers.
            // NOTE: this now forces header names uppercase since they are
            // case insensitive and vary by client.
            Properties header = new Properties();
            if (st.hasMoreTokens()) {
                String line = in.readLine();
                while (line.trim().length() > 0) {
                    int p = line.indexOf(':');
                    header.put(line.substring(0, p).trim().toLowerCase(), line.substring(p + 1).trim());
                    line = in.readLine();
                }
            }
            String postLine = "";
            if (method.equalsIgnoreCase("POST")) {
                long size = 0x7FFFFFFFFFFFFFFFl;
                String contentLength = header.getProperty("content-length");
                if (contentLength != null) {
                    try {
                        size = Integer.parseInt(contentLength);
                    } catch (NumberFormatException ex) {
                        //ignore this exception
                        ex.printStackTrace();
                    }
                }
                char buf[] = new char[512];
                int read = in.read(buf);
                while (read >= 0 && size > 0 && !postLine.endsWith("\r\n")) {
                    size -= read;
                    postLine += String.valueOf(buf, 0, read);
                    if (size > 0) {
                        read = in.read(buf);
                    }
                }
                postLine = postLine.trim();
//                decodeParms(postLine, parms);
                process(postLine, method, header);
            }
            
            if (method.equals("GET")) {
                if (uri.toLowerCase().indexOf(HttpSoap.RELOAD_CFG.toLowerCase()) >= 0) {
                    reloadConfig(header);
                } 
                if (uri.toLowerCase().indexOf(HttpSoap.GET_STATISTICS.toLowerCase()) >= 0) {
                    statistics(header);
                }else {
                    sendResponse(HttpResponse.HTTP_NOTFOUND, HttpResponse.MIME_HTML, null, new ByteArrayInputStream("NOT FOUND".getBytes()));
                }
                
            }
       
            in.close();
        } catch (Exception ioe) {
            try {
                sendError(HttpResponse.HTTP_SOAP_ERROR, "SERVER INTERNAL LOG_ERROR, logName: IOException: " + ioe.getMessage());
            } catch (Exception ex) {
            }
        } finally {
            try {
                mySocket.close();
                if (is != null) {
                    is.close();
                }
            } catch (IOException ex) {
            }
        }
    }

    /**
     * Decodes the percent encoding scheme. <br/>
     * For example: "an+example%20string" -> "an example string"
     * @param str <code>String</code> value will be decoded
     * @return value after decode
     * @throws InterruptedException when processing in String value, may be out of index bound,...
     */
    private String decodePercent(
            String str) throws InterruptedException {
        try {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i <
                    str.length(); i++) {
                char c = str.charAt(i);
                switch (c) {
                    case '+':
                        sb.append(' ');
                        break;
                    case '%':
                        sb.append((char) Integer.parseInt(str.substring(i + 1, i + 3), 16));
                        i +=
                                2;
                        break;

                    default:

                        sb.append(c);
                        break;
                }
            }
            return new String(sb.toString().getBytes());
        } catch (Exception e) {
            sendError(HttpResponse.HTTP_BADREQUEST, "BAD REQUEST: Bad percent-encoding.");
            return null;
        }

    }

    /**
     * Decodes parameters in percent-encoded URI-format
     * ( e.g. "name=Jack%20Daniels&pass=Single%20Malt" ) and
     * adds them to given Properties.
     * @param parms param string will be parse
     * @param p stored properties to store value
     * @throws InterruptedException when processing in String value, may be out of index bound,...
     */
    private void decodeParms(String parms, Properties p)
            throws InterruptedException {
        if (parms == null) {
            return;
        }
        StringTokenizer st = new StringTokenizer(parms, "&");
        while (st.hasMoreTokens()) {
            String e = st.nextToken();
            int sep = e.indexOf('=');
            if (sep >= 0) {
                p.put(decodePercent(e.substring(0, sep)).trim(),
                        decodePercent(e.substring(sep + 1)));
            }
        }
    }

    /**
     * Returns an error message as a HTTP response and
     * throws InterruptedException to stop furhter request processing.
     * @param status status of http processing
     * @param msg http message content
     * @throws InterruptedException exception will be throwed after sending
     */
    private void sendError(String status, String msg) throws InterruptedException {
        sendResponse(status, HttpResponse.MIME_HTML, null, new ByteArrayInputStream(msg.getBytes()));
        throw new InterruptedException();
    }

    /**
     * Sends given response to the socket.
     * @param status status of http processing
     * @param  mime content type of response
     * @param header header values
     * @param data <code>InputStream</code> stored content of response
     */
    public void sendResponse(String status, String mime, Properties header, InputStream data) {
        try {
            if (status == null) {
                throw new Error("sendResponse(): Status can't be null.");
            }
            OutputStream out = mySocket.getOutputStream();
            PrintWriter pw = new PrintWriter(out);
            pw.print("HTTP/1.0 " + status + " \r\n");
            if (mime != null) {
//                pw.print("Content-Type: " + mime + "\r\n");
            }
            if (header == null || header.getProperty("Date") == null) {
                pw.print("Date: " + gmtFrmt.format(new Date().getTime()) + "\r\n");
            }
            if (header != null) {
                Enumeration e = header.keys();                
                while (e.hasMoreElements()) {
                    String key = (String) e.nextElement();
                    String value = header.getProperty(key);
                    pw.print(key + ": " + value + "\r\n");
                }                
            }
            pw.print("\r\n");
            pw.flush();
            if (data != null) {
                byte[] buff = new byte[2048];
                StringBuffer sbuf = new StringBuffer();
                while (true) {
                    int read = data.read(buff, 0, 2048);
                    if (read <= 0) {
                        break;
                    }

                    int i = 0;
                    for (i = 0; i <
                            buff.length; i++) {
                        if (buff[i] == 0) {
                            break;
                        }
                    }
                    if (i > 0) {
                        sbuf.append(new String(buff, 0, i));
                    }
                    out.write(buff, 0, read);
                }
            }
            out.flush();
            out.close();
            if (data != null) {
                data.close();
            }
        } catch (IOException ioe) 
        {
            try {
                mySocket.close();
            } catch (Exception t) {
            }
        }
    }

    public HttpResponse serve(
            String uri, String method, Properties header, String content) throws Exception {
        Enumeration e = header.propertyNames();
        String host = header.getProperty("host");
        int length = host.indexOf(":");
        String wsdl = host.substring(length + 1);
        while (e.hasMoreElements()) {
            String name = (String) e.nextElement();
        }
        if (method.equals("GET")) {
            String  hostPort = "localhost" + ":" + wsdl;
            HttpResponse response = new HttpResponse(HttpResponse.HTTP_OK, HttpResponse.MIME_XML, hostPort);
            return response;
        }
        return null;
    }

   
    private Socket mySocket;

    private void reloadConfig(Properties header) {
        HCacheMgt.clear();
        String response = "System already reload";
         header.put("content-length", "" + response.getBytes().length);
        sendResponse(HttpResponse.HTTP_OK, HttpResponse.mimeType, header,
                new ByteArrayInputStream(response.getBytes()));
    }

    private void statistics(Properties header) {
        String response = HCacheMgt.statistic();
         header.put("content-length", "" + response.getBytes().length);
        sendResponse(HttpResponse.HTTP_OK, HttpResponse.mimeType, header,
                new ByteArrayInputStream(response.getBytes()));
    }
    
}
