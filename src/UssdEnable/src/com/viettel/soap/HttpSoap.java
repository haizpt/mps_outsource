/**
 *
 * handsome boy
 */

package com.viettel.soap;

/**
 *
 * @author haind25 <Apllication Development Department - Viettel Global>
 * @since Dec 14, 2015
 * @mail haind25@viettel.com.vn
 */

import java.util.Properties;

/**
 *
 * @author Administrator
 */
public class HttpSoap  {
    
    public static String GET_WSDL = "/molistener?wsdl";
    public static String GET_STATISTICS = "/statistics";
    public static String RELOAD_CFG = "/reloadconfig";
    
    private Properties p = null;
    private String portHttp = null;
    private HttpListener http;

    public HttpSoap(String ports)
    {
        this.portHttp = ports;
    }

    public void start() {
        startHTTP(portHttp);
    }

    public void startHTTP(String listPort) {
      
        String[] ports = listPort.split(",");
        int i = ports.length;
        for (int j = 0; j < i; j++) {
            http = new HttpListener(Integer.parseInt(ports[j]));
            http.start();
        }
    }


}