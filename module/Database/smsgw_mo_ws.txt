REM INSERTING into SMSGW_MO_WS
SET DEFINE OFF;

Insert into SMSGW_MO_WS (WS_NAME,URL,SOAP_ACTION,USERNAME,PASSWORD,RAW_XML,RETURN_TAG,CONVERT_TO_ASCII,USE_HEX,STATUS) values ('smshandler_all','http://10.226.46.24:9203/molistener?wsdl','POST','mps190','Mps190','<?xml version=''1.0'' encoding=''UTF-8''?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
<S:Body>
<ws:mtRequest>
	<username>#USERNAME</username>
    	<password>#PASSWORD</password>
	<source>#MSISDN</source>
	<dest>#SHORT_CODE</dest>
	<content>#CONTENT</content>
</ws:mtRequest>
</S:Body>
</S:Envelope>','return',1,0,'1');
